<?php
/*
 Template Name: Bellelift Thank You Page Template
*/

?>

<!DOCTYPE html>
<html>

<!-- Mirrored from bellalift.com.br/mobile/thankyou.aspx by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Mar 2019 13:08:38 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Goji Berry</title>
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/thankyou.css" />
    <style type="text/css">
        #mainWrapper{min-width:unset;}
        #wrapper{height:100%;width:100%;}
    </style>

<link type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" /><script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js"></script><script type="text/javascript">var dep = false;</script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/d3t95j2l2mupha.cloudfront.net/api/js/jquery.validationEngine-pt-BR.js"></script><script type="text/javascript" src="../../d3t95j2l2mupha.cloudfront.net/api/js/jquery.validationEngine-2.6.3.js"></script><link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/d3t95j2l2mupha.cloudfront.net/api/css/validationEngine.jquery.css" type="text/css" media="all" />
</head>



<body>
    <form name="ctl00" method="post" action="https://bellalift.com.br/mobile/thankyou.aspx" id="ctl00" enablevalidation="true">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTM5NDI4Njc1MQ9kFgICAQ9kFg4CAQ8WAh4EVGV4dAUBMGQCAw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFgAWABYADBQrAABkAgUPFgIfAAUBIGQCBw8WAh8ABQMgICBkAgkPFgIfAGVkAgsPFgIfAGVkAg0PFgIfAGVkGAEFDmd2T3JkZXJEZXRhaWxzDzwrAAwBCGZkiQ1vJbDMVValgKXwpw9BsQvZa1oHu8AyvvUW4dY5+Yo=" />
</div>



<script type="text/javascript">function ExitPopup() { }</script> 

	    <div id="mainWrapper">
		    <div id="wrapper">
			    <div id="topBar">
				    <p class="contact">Finalize seu pedido abaixo com preços exclusivos ou compre pelo telefone: 0 xx (21) 3509 1860</p>

		      </div><!-- END topBar -->

                <div style="text-align:center; padding-top:50px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:52px;"><span style="font-size:56px; color:#08AAC3;">PARABÉNS </span><br />



                <div style="font-family:Arial, Helvetica, sans-serif; font-size:17px; color:#111111; font-weight:bold; text-align:center; padding-top:20px;">
	                SEU PEDIDO #0
                    <div style="width: 100%; text-align: center;">

                    <div>

	<table cellspacing="0" cellpadding="5" rules="all" border="1" id="gvOrderDetails" style="border-color:Black;border-width:1px;border-style:Solid;width:90%;border-collapse:collapse;margin: 0 auto;">

		<tr style="background-color:#08AAC3;">

			<th scope="col" style="border-width:1px;border-style:solid;">Nome do produto</th><th scope="col" style="border-width:1px;border-style:solid;">Quantidade </th><th scope="col" style="border-width:1px;border-style:solid;">Total</th>

		</tr><tr>

			<td colspan="3">Dados não encontrados</td>

		</tr>

	</table>

</div>

                    </div>

                    SERÁ ENVIADO PARA:

	                <div class="order-details">

		                <span style="font-weight: bold;  margin-bottom: 10px;"> </span><br/>

		                <span style="font-weight: normal; margin-bottom: 10px;">

		

                               <br />

                            ,   

	                </div>

                </div>

            </div>

		    </div><!-- END wrapper -->

            

            

	<div id="footer">

	    <p class="links">&copy; 2014 BellaLift Todos os Direitos Reservados. <a href="Termos.html" target="_blank">Termos e Condicoes</a> | <a href="Pol%c3%adtica%20de%20Privacidade.html" target="_blank">Politica de Privacidade</a> | <a href="Contato.html" target="_blank">Contato</a></p>

    </div><!-- footer -->

	    </div><!-- END mainWrapper -->

    

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FA49BB8A" />

</div>



<script type="text/javascript">

//<![CDATA[

var ucrmSkipValidation = null; var ucrmAllowSubmit = true//]]>

</script>

</form> 

<script type="text/javascript">

  (function () {

    var tagjs = document.createElement("script");

    var s = document.getElementsByTagName("script")[0];

    tagjs.async = true;

    tagjs.src = "../../s.btstatic.com/tag.js#site=YdB4XyS";

    s.parentNode.insertBefore(tagjs, s);

  }());

</script>

<noscript>
  <iframe src="http://s.thebrighttag.com/iframe?c=YdB4XyS" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>

<!-- Segment Pixel - Retargeting - DO NOT MODIFY -->
<img src="https://secure.adnxs.com/seg?add=2673908&amp;t=2" width="1" height="1" />
<!-- End of Segment Pixel -->





<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&amp;.yp=413769&amp;ec=conversao"/>
<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&amp;.yp=10004323&amp;ec=conversao"/>



<script language='JavaScript'>
 var OB_ADV_ID=22877;
 var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");
 var str = '<script src="'+scheme+'widgets.outbrain.com/obtp.js" type="text/javascript"><\/script>';
 document.write(str);
 </script>

 <!-- 3-14-2016 --->
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63127604-1', 'auto');
  ga('send', 'pageview');



</script>
</body>


<!-- Mirrored from bellalift.com.br/mobile/thankyou.aspx by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Mar 2019 13:08:39 GMT -->
</html>