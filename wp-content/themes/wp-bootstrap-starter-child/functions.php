<?php

/*Example Functions*/
add_action( 'wp_enqueue_scripts', 'fkwp_child_enqueue_styles' );

if (!function_exists('fkwp_child_enqueue_styles')) {       

    function fkwp_child_enqueue_styles() {
    
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array() );
        wp_dequeue_style('child-style');
        wp_enqueue_style(
            'child-style',
            get_stylesheet_directory_uri() . '/style.css',
            array('parent-style')
        );
    }
}


add_action( 'wp_enqueue_scripts', 'vep_cep' );
      
function vep_cep() {
    if(is_page('checkout-page')) {
        wp_enqueue_script( 'vep_cep', get_template_directory_uri() . '/js/vep_cep.js', array('jquery') );
    }
}



/* Add to cart programatically AJAX  */
add_action('wp_ajax_add_to_card_bella', 'fkwp_add_to_card_bella');
add_action('wp_ajax_nopriv_add_to_card_bella', 'fkwp_add_to_card_bella');

function fkwp_add_to_card_bella()
{
    global $woocommerce;
    
    // select ID
    if(isset($_GET['product_id']))
    {
        $product_id = (int) $_GET['product_id'];
        
        $woocommerce->cart->empty_cart();
        
        //check if cart is empty
        if (WC()->cart->get_cart_contents_count() == 0)
        {
            // if no products in cart, add it
            WC()->cart->add_to_cart( $product_id );
            
            echo json_encode('success');
            wp_die();
                      
        }
        else
        {
            echo json_encode('error');
            wp_die();
        }
    }
    else
    {
        echo json_encode('error');
        wp_die();
    }
    
}

/* Customize Woocommerce Checkout Fields */

// removes Order Notes Title - Additional Information
add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'fkwp_custom_override_checkout_fields', 50 );

function fkwp_custom_override_checkout_fields( $fields ) 
{
    
    // echo "<pre>";
    // print_r($fields);
    // echo "</pre>"; die();
    
    
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_2']);
    // unset($fields['billing']['billing_country']);
    unset($fields['order']);
    
    /*unset($fields['billing']['ebanx_billing_colombia_document_type']);
    unset($fields['billing']['ebanx_billing_colombia_document']);
    unset($fields['billing']['ebanx_billing_argentina_document_type']);
    unset($fields['billing']['ebanx_billing_argentina_document']);
    unset($fields['billing']['ebanx_billing_peru_document']);
    unset($fields['billing']['ebanx_billing_chile_document']);*/
    
    
    
    /* Custom Checkout Fields */
    $fields['billing']['billing_gender'] = array(
        'label'     => __('Gender', 'woocommerce'),
        // 'placeholder'   => _x('Gender', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('form-row-wide', 'test-klasa'),
        'input_class' => array('d-none'),
        'label_class' => array('d-none'),
        'clear'     => true,
    );
     
    $fields['billing']['billing_number'] = array(
        'label'     => __('Number', 'woocommerce'),
        // 'placeholder'   => _x('Gender', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('form-row-wide', 'test-klasa'),
        'input_class' => array('samo-input'),
        'label_class' => array('cls-na-lbl'),
        'clear'     => true,
    );
    
    $fields['billing']['billing_compliment'] = array(
        'label'     => __('Compliment', 'woocommerce'),
        // 'placeholder'   => _x('Gender', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('form-row-wide', 'test-klasa'),
        'input_class' => array('samo-input'),
        'label_class' => array('cls-na-lbl'),
        'clear'     => true,
    );
    
    $fields['billing']['billing_area'] = array(
        'label'     => __('Area', 'woocommerce'),
        // 'placeholder'   => _x('Gender', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('form-row-wide', 'test-klasa'),
        'input_class' => array('samo-input'),
        'label_class' => array('cls-na-lbl'),
        'clear'     => true,
    );
    
    
    
    $fields['billing']['billing_first_name']['priority'] = 10;
    $fields['billing']['billing_last_name']['priority'] = 20;
    $fields['billing']['billing_phone']['priority'] = 40;
    $fields['billing']['billing_email']['priority'] = 50;
    $fields['billing']['ebanx_billing_brazil_document']['priority'] = 60; /* CPF Brazil */
    $fields['billing']['billing_postcode']['priority'] = 70;
    $fields['billing']['billing_address_1']['priority'] = 80;
    $fields['billing']['billing_number']['priority'] = 90;
    $fields['billing']['billing_compliment']['priority'] = 100;
    $fields['billing']['billing_area']['priority'] = 110;
    $fields['billing']['billing_city']['priority'] = 120;
    $fields['billing']['billing_state']['priority'] = 130;
    $fields['billing']['billing_gender']['priority'] = 140;
    
    //Customize address placeholder
    $fields['billing']['billing_address_1']['placeholder'] = '';
    
    
    
    /* Label Translation */
    $fields['billing']['billing_first_name']['label'] = 'Nome';
    $fields['billing']['billing_last_name']['label'] =  'Sobrenome';
    $fields['billing']['billing_gender']['label'] = 'Sexo';
    $fields['billing']['billing_gender']['required'] = false;
    $fields['billing']['billing_phone']['label'] = 'Telefone';
    $fields['billing']['billing_email']['label'] = 'Email';
    $fields['billing']['billing_postcode']['label'] = 'CEP';
    $fields['billing']['billing_address_1']['label'] = 'Endereco';
    $fields['billing']['billing_number']['label'] = 'Número';
    $fields['billing']['billing_compliment']['label'] = 'Complemento';
    $fields['billing']['billing_compliment']['required'] = false;
    $fields['billing']['billing_area']['label'] = 'Bairro';
    $fields['billing']['billing_city']['label'] = 'Cidade';
    $fields['billing']['billing_state']['label'] = 'Estado';
    
    $fields['billing']['ebanx_billing_brazil_document']['required'] = false;
    
    
    /* Reorder Field By Priority EBANX */
    
    /*$fields['billing']['ebanx_billing_colombia_document_type']['priority'] = 990;
    $fields['billing']['ebanx_billing_colombia_document']['priority'] = 1000;
    $fields['billing']['ebanx_billing_argentina_document_type']['priority'] = 1010;
    $fields['billing']['ebanx_billing_argentina_document']['priority'] = 1020;
    $fields['billing']['ebanx_billing_peru_document']['priority'] = 1030;
    $fields['billing']['ebanx_billing_chile_document']['priority'] = 1040;*/
    
    
    
    //paragraph class
    $fields['billing']['billing_first_name']['class'][] = 'test test2';
    //input class
    $fields['billing']['billing_first_name']['input_class'][] = 'form-control';
    $fields['billing']['billing_last_name']['input_class'][] = 'form-control';
    $fields['billing']['billing_gender']['input_class'][] = 'form-control';
    $fields['billing']['billing_gender']['input_class'][] = 'form-control';
    $fields['billing']['billing_phone']['input_class'][] = 'form-control';
    $fields['billing']['billing_email']['input_class'][] = 'form-control';
    $fields['billing']['billing_postcode']['input_class'][] = 'form-control';
    $fields['billing']['billing_address_1']['input_class'][] = 'form-control';
    $fields['billing']['billing_number']['input_class'][] = 'form-control';
    $fields['billing']['billing_compliment']['input_class'][] = 'form-control';
    $fields['billing']['billing_compliment']['input_class'][] = 'form-control';
    $fields['billing']['billing_area']['input_class'][] = 'form-control';
    $fields['billing']['billing_city']['input_class'][] = 'form-control';
    $fields['billing']['billing_state']['input_class'][] = 'form-control';
    $fields['billing']['ebanx_billing_brazil_document']['input_class'][] = 'form-control';
    
    //label class
    $fields['billing']['billing_first_name']['label_class'][] = 'test';
    
    $fields['billing']['billing_country']['class'][] = 'd-none';
    $fields['billing']['billing_country']['label_class'][] = 'd-none';
    $fields['billing']['billing_country']['input_class'][] = 'd-none';
    
 
    return $fields;
}


add_filter( 'woocommerce_default_address_fields' , 'fkwp_default_address_fields' );

function fkwp_default_address_fields( $address_fields ) 
{
    // echo "<pre>";
    // print_r($address_fields);
    // echo "</pre>";
    
    $address_fields['postcode']['label'] = 'CEP';
    $address_fields['address_1']['label'] = 'RUA';
    $address_fields['address_1']['placeholder'] = '';
    $address_fields['city']['label'] = 'Cidade';
    $address_fields['state']['label'] = 'Estado';
    
    return $address_fields;
}


add_filter( 'woocommerce_checkout_get_value', 'fkwp_populating_checkout_fields', 10, 2 );

function fkwp_populating_checkout_fields ( $value, $input ) 
{
    
    @session_start();
    
    if(isset($_SESSION['user']))
    {
        switch($input)
        {
            case 'billing_first_name': { $value = $_SESSION['user']['first_name']; break; }
            case 'billing_last_name': { $value = $_SESSION['user']['last_name']; break; }
            case 'billing_email': { $value = $_SESSION['user']['email']; break; }
            case 'billing_postcode': { $value = $_SESSION['user']['zip']; break; }
            case 'billing_phone': { $value = $_SESSION['user']['phone']; break; }
            case 'billing_gender': { $value = $_SESSION['user']['gender']; break; }
            case 'billing_country': { $value = 'BR'; break; }
        }
    }
    
    // unset($_SESSION['user']);
    
    return $value;
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['billing_compliment'] ) ) {
        update_post_meta( $order_id, 'billing_compliment', sanitize_text_field( $_POST['billing_compliment'] ) );
    }
    
    if ( ! empty( $_POST['billing_area'] ) ) {
        update_post_meta( $order_id, 'billing_area', sanitize_text_field( $_POST['billing_area'] ) );
    }
    
    if ( ! empty( $_POST['billing_number'] ) ) {
        update_post_meta( $order_id, 'billing_number', sanitize_text_field( $_POST['billing_number'] ) );
    }
    
    if ( ! empty( $_POST['billing_gender'] ) ) {
        update_post_meta( $order_id, 'billing_gender', sanitize_text_field( $_POST['billing_gender'] ) );
    }
}

add_filter( 'woocommerce_order_formatted_billing_address' , 'woo_custom_order_formatted_billing_address', 10, 2 );

function woo_custom_order_formatted_billing_address( $address, $WC_Order ) {
    
    $compliment = get_post_meta( $WC_Order->get_id(), 'billing_compliment', true );
    $bairro = get_post_meta( $WC_Order->get_id(), 'billing_area', true );
    $numerro = get_post_meta( $WC_Order->get_id(), 'billing_number', true );
    
    $address = array(
        'first_name'    => $WC_Order->billing_first_name,
        'last_name'     => $WC_Order->billing_last_name,
        'address_1'     => $WC_Order->billing_address_1 . " " . $numerro . ", " . $compliment,
        'city'          => $bairro . ", " . $WC_Order->billing_city,
        'state'         => $WC_Order->billing_state,
        'postcode'      => $WC_Order->billing_postcode,
        'country'       => $WC_Order->billing_country
        );

    return $address;

}

add_filter( 'woocommerce_order_formatted_shipping_address' , 'woo_custom_order_formatted_shipping_address', 10, 2 );

function woo_custom_order_formatted_shipping_address( $address, $WC_Order ) {
    
    $compliment = get_post_meta( $WC_Order->get_id(), 'billing_compliment', true );
    $bairro = get_post_meta( $WC_Order->get_id(), 'billing_area', true );
    $numerro = get_post_meta( $WC_Order->get_id(), 'billing_number', true );
    
    $address = array(
        'first_name'    => $WC_Order->billing_first_name,
        'last_name'     => $WC_Order->billing_last_name,
        'address_1'     => $WC_Order->billing_address_1 . " " . $numerro . ", " . $compliment,
        'city'          => $bairro . ", " . $WC_Order->billing_city,
        'state'         => $WC_Order->billing_state,
        'postcode'      => $WC_Order->billing_postcode,
        'country'       => $WC_Order->billing_country
        );

    return $address;

}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order) {
    
    echo '<p><strong>'.__('Sexo').':</strong> <br/>' . get_post_meta( $order->get_id(), 'billing_gender', true ) . '</p>';
    
    // echo '<p><strong>'.__('Complemento').':</strong> <br/>' . get_post_meta( $order->get_id(), 'billing_compliment', true ) . '</p>';
}

add_action( 'woocommerce_admin_order_data_after_order_details', 'fkwp_after_order_details_admin' );

function fkwp_after_order_details_admin($order)
{
    /*echo "<p id='bairro_order'><strong>Bairro:</strong> " . get_post_meta( $order->get_id(), 'billing_area', true ) . "</p>";
    echo "<p id='numero_order'><strong>Numero:</strong> " . get_post_meta( $order->get_id(), 'billing_number', true ) . "</p>";
    
    echo "
        <script>
            jQuery(function(){
                
                var bairro_order = jQuery('#bairro_order').html();
                var numero_order = jQuery('#numero_order').html();
                
                jQuery('.address p:first').append(bairro_order);
                jQuery('.address p:first').append(numero_order);
                
                jQuery('#bairro_order').remove();
                jQuery('#numero_order').remove();
                 
            });
        
        </script>
    
    ";*/
}


//Change the Billing Address checkout label
function wc_billing_field_strings( $translated_text, $text, $domain ) 
{
    switch ( $translated_text ) 
    {
        /*case 'Billing & Shipping': {
            $translated_text = 'Sua Oferta Escolhida!';
            break;
        }*/
        // case 'Your order': {
        //     $translated_text = 'Your';
        //     break;
        // }
        case 'Place order': {
            $translated_text = 'FAZEU MEU PEDIDO AGORA';
            break;
        }
        
    }
    
    return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );