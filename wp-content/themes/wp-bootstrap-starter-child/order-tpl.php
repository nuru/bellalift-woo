<?php
/*
 Template Name: Bellelift Order Page Template
*/

?>


<!DOCTYPE html>

<html>
<head>

<title></title>

	<!-- Meta Tags -->
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<meta name="robots" content="index, follow" />
	
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

	<!-- CSS -->
	<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700,800,300italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" media="screen,projection" type="text/css" />

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/Installments.js" type="text/javascript"></script>
	<style type="text/css">
        #exit_pop{
	        display:none;position:fixed;top:0;left:0;width:100%;height:100%;margin:0px;padding:0px;
	        z-index:9999;clear:none;background-color:rgba(0,0,0,0.5);
        }
        #exit_pop_content{
	        cursor:pointer;
	        position:fixed;
	        height: 408px;
            left: 50%;
            margin: -204px 0 0 -344px;
            position: absolute;
            top: 50%;
            width: 688px;
        }
    	</style>
    	
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<!--Start of Zopim Live Chat Script-->
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		$.src="//v2.zopim.com/?3OXZQgNnY1KpCvCU0raFE6IhzEWGb3XJ";z.t=+new Date;$.
		type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		<!--End of Zopim Live Chat Script-->
</head>

<body>
	<div id="exit_pop" style="display:none;">
        <div id="exit_pop_content">
            <img style="width:688px;height:408" src="<?php echo get_stylesheet_directory_uri(); ?>/images/popup.jpg"/>
        </div>
    	</div>
    	<div id="discountHeader" class="product-top" style="text-align:center;" runat="server" visible="false">
		<p style="width:100%">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/exclamacao.png" align="left" style="width:75px;"/>
		<span style="display:inline-block; font-size:24px;line-height:30px;">Parabéns, seu desconto de R$20 foi adicionado.<br />Finalize seu pedido nos próximos: <span name="countdown" id="countdown"></span> minutos para garantir seu Desconto.</span>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/exclamacao.png" align="right" style="width:75px;"/>
		</p>
            <script type="text/javascript">
                var totalseconds = 0; //Timer.GetTimerTotalSeconds
                var timer;

                $(document).ready(function () {
                    timer = setInterval(function () { if (totalseconds <= 0) { clearInterval(timer); } CountDown(totalseconds--); }, 1000);
                });

                function CountDown(to) {
                    minutes = (Math.floor(to / 60));
                    seconds = to % 60;

                    if (to == 0) {
                        $("#countdown").text("compre agora");
                        return;
                    }

                    minutes = '' + minutes
                    if (seconds < 10)
                        seconds = '0' + seconds
                    $("#countdown").text(minutes + ":" + seconds);
                };
            </script>
	</div>
	<form id="Form1" method="post" enablevalidation="true" runat="server">	
	<div class="header3">
	
		<div class="logo"><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/checkout-logo.png"/></a></div>
	
	</div><!--header3-->
	
	

	<div id="content" class="fixat">
	
		<div class="order">
		
			<div class="heading">
			
				<p><span class="first">Producto</span><span class="second">Quantidate</span><span class="third">Subtotal</span></p>
			
				<div class="clear"></div>
			</div><!--heading-->
			
			<div class="your-order">
			
				<div class="product">
					<img src="#"/> <!--  SelectedPackage.OrderProductImages-->
				</div><!--product-->
				
				<div class="amount">
				
					<div class="num"><p>SelectedPackage.NotFreeQty</p></div>
					<h3>SelectedPackage.NotFreeQty frasco</h3>
				
				</div><!--amount-->
				
				<div class="total">
				
					<p>Pack x R$ SelectedPack.Price
						<image src="<?php echo get_stylesheet_directory_uri(); ?>/images/semjuros.png" style="width: 150px;height:115px;position: relative;margin-top: -120px;right: -90%;"/>
					</p>
				</div><!--total-->
				
				<div class="clear"></div>
			</div><!--your-order-->
			
			<div class="bottom-part">
			
				<div class="guarantee">
					<div class="seal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/seal-guarantee.png"/></div>
					<p>Nós garantimos a eficácia de nossos produtos, e se por alguma razão você  estiver insatisfeito com qualquer de suas compras, devolveremos o seu  dinheiro. Sem perguntas. </p>
				</div><!--guarantee-->
				
				<div class="rightside">
				
						<p>
                            <span>Frete:</span><span id="spShippingFree" runat="server" visible="false">GRÁTIS</span>
                            <asp:dropdownlist id="ddlShipping" runat="server">
                                <asp:listitem value="1" text="PAC - 7a 14 dias úteis R$28,95"></asp:listitem>
                                <asp:listitem value="2" text="SEDEX - 4 a 7 dias úteis R$43,95"></asp:listitem>
                            </asp:dropdownlist>
                        </p>
					
					<div class="clear"></div>
					
					<p class="final">em até <span id="totalPrice"> Pack x R SelectedPack.Pack</span><br />Frete incluso</p>
				
					<div class="clear"></div>
				</div><!--rightside-->
			
				<div class="clear"></div>
			</div><!--bottom-part-->
		
			<div class="clear"></div>
		
		</div><!--order-->
		
		<div class="ordering-info">
		
			<h2>Para onde enviaremos seu pedido?</h2>
			
			<div class="mid">
			
				<div class="left">
				
					<p><span>Nome* :</span><asp:textbox id="txtFirstName" cssclass="validate[required]" runat="server"/></p>
					<p><span>Sobrenome* :</span><asp:textbox id="txtLastName" cssclass="validate[required]" runat="server"/></p>
					<p><span>Telefone* :</span><asp:textbox id="txtPhone" cssclass="validate[required]" runat="server" maxlength="11"/></p>
					<p><span>Email* :</span><asp:textbox id="txtEmail" cssclass="validate[required,custom[email],funcCall[email]]" runat="server"/></p>
					<p><span>CPF* :</span><asp:textbox id="txtCpf" cssclass="validate[required,funcCall[cpf]]" runat="server" /></p>
					<p class="segura">Nos respeitamos sua privacidade</p>
				
				</div><!--left-->
				
				<div class="left">
				
					<p><span>CEP* :</span><asp:textbox id="txtZip" cssclass="validate[required,funcCall[cepv2]]" runat="server"/></p>
					<p><span>Endereco* :</span><asp:textbox id="txtAddress" cssclass="validate[required]" runat="server"/></p>
					<p><span>Numero* :</span><asp:textbox id="txtNumero" cssclass="validate[required]" runat="server"/></p>
					<p><span>Complemento :</span><asp:textbox id="txtComplemento" runat="server"/></p>
					<p><span>Bairro* :</span><asp:textbox id="txtArea" cssclass="validate[required]" runat="server"/></p>
					<p><span>Cidade* :</span><asp:textbox id="txtCity" cssclass="validate[required]" runat="server"/></p>
					<p><span>Estado* :</span><asp:dropdownlist id="States" runat="server"/></p>
				    <asp:HiddenField runat="server" ID="ibge" />
                    <asp:HiddenField runat="server" ID="addressType" />
				</div><!--left-->
				
			
				<div class="clear"></div>
			</div><!--mid-->
		
			<div class="clear"></div>
		</div><!--ordering-info-->
		
		<div class="card-info">
		
			<h2>Informações de Pagamento</h2>
			
			<div class="card-type">
			
				<p>
					<span class="first"><label><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/plata1.png" class="hidedesktop"><input type="radio" name="group1" value="visa" checked="true"></label></span>
					<span class="second"><label><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/plata2.png" class="hidedesktop"><input type="radio" name="group1" value="master"></label></span>
					<span class="third"><label><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/plata3.png" class="hidedesktop"><input type="radio" name="group1" value="dinners"></label></span>
					<span class="fourth"><label><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/plata4.png" class="hidedesktop"><input type="radio" name="group1" value="american"></label></span>
					<span class="fifth"><label><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/plata5.png" class="hidedesktop"><input type="radio" name="group1" value="elo"></label></span>
					<span class="sixth"><label><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/plata6.png" class="hidedesktop"><input id="rbPayPal" type="radio" name="group1" value="paypal" runat="server"/></label></span>
					<span class="seventh"><label><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/plata7.png" class="hidedesktop"><input id="rbPagseguro" type="radio" name="group1" value="pagseguro" runat="server"/></label></span>
				</p>
			
			</div><!--card-type-->
			
			<div class="card-valid">
			
				<p class="sum">Nos respeitamos sua privacidade</p>
				
				<div class="last">
				
					<div class="secure credit"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bottom-part-secure.jpg" class="hidemobile"/><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bottom-part-secure-mob.png" class="hidedesktop"/></div>
					
					<div class="data">
					
						<p class="credit"><span>Número do Cartão* :</span><asp:textbox id="txtCardNumber" MaxLength="16" cssclass="validate[required,custom[creditCardNumber]]" runat="server"/></p>
						<p class="valid credit"><span>Data de Validade* :</span><asp:dropdownlist id="ExpirationMonth" runat="server"></asp:dropdownlist><asp:dropdownlist id="ExpirationYear" CssClass="year validate[required,funcCall[year]]" runat="server"></asp:dropdownlist></p>
						<p class="credit"><span>Código de Segurança* :</span><asp:TextBox runat="server" ID="securitycode" MaxLength="4" name="securitycode" CssClass="textbox validate[required]" /></p>
						<p class="credit"><span>Titular* :</span><asp:TextBox runat="server" ID="ccholder" name="ccholder" CssClass="validate[required]" MaxLength="30"/></p>
						<p class="credit"><span>Parcelas* (sem juros) :</span><asp:DropDownList ID="Installments" runat="server"></asp:DropDownList></p>
					
					</div><!--data-->
					
					<div class="clear"></div>
					<div style='text-align:center; font-size:12px' class="credit"></div>
                    <asp:Button ID="btnSubmit" onclientclick="PreventExitPop = 1;" CssClass="submit" runat="server" />
				
					<div class="clear"></div>
				</div><!--last-->
			
				<div class="clear"></div>
			</div><!--card-valid-->
		
			<div class="clear"></div>
		</div><!--card-info-->
	
		<div class="clear"></div>
	</div><!-- content -->
	
	<div id="bottom-parts"></div>
	
	<uc:footer id="Footer" isDefaultpage="true"  runat="server" />
	</form>
        <script type="text/javascript">
        /*function switchPaymentMethod(sender) {
            if ($("#rbPagseguro").is(":checked") || $("#rbPayPal").is(":checked")) {
                $('form1').validationEngine('hideAll');
                $(".credit").hide();
            } else {
                $(".credit").show();
            }
        }

        $(document).ready(function () {
                $("#ddlShipping").change(
                function () { ShippingOptionChange($(this)); }
            );
                $("#ddlShipping").change();

                $('#txtCpf').mask("999.999.999-99");
                $('#txtZip').mask("99999-999").change(function () { $('#Form1').validationEngine('validateField', '#txtZip'); });

                $("input[type=radio]").live("click", switchPaymentMethod);
                switchPaymentMethod();
        });

        function ShippingOptionChange(option) {
        if (option.val() === "1") {
            $('#totalPrice').html('<%=Package.InstallmentSiteVersion%>x R$<%=SelectedPackage.PacWithInstallments%>');
            InitInstallments(<%=SelectedPackage.StrTotalPac.Replace(",",".")%>);
        } else {
            $('#totalPrice').html('<%=Package.InstallmentSiteVersion%>x R$<%=SelectedPackage.SedexWithInstallments%>');
            InitInstallments(<%=SelectedPackage.StrTotalSedex.Replace(",",".")%>);
        }
    }*/
    </script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.numeric.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#dialog-modal p").html("Um momento... Processando seu pedido...");
			$('#txtPhone').numeric();
        });
    </script>
    <script type="text/javascript">
        $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());

        var ffversion = 0;

        if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))

            ffversion = new Number(RegExp.$1);

        var PreventExitPop = 0; //<%=PreventExitPop%>

        window.submitted = false;


        function bunload() {

            var message = "";

            if (PreventExitPop != 1 && !window.submitted) {

                PreventExitPop = 1;

                document.getElementById('exit_pop').style.display = 'block';


                if ($.browser.chrome || $.browser.safari) {

                    scroll(0, 0);

                    window.onbeforeunload = null;

                    return message;
                }

                else {

                    return message;
                }

            } else {
                PreventExitPop = 0;
            }
        }

        if (PreventExitPop != 1) {

            window.onbeforeunload = bunload;

        } else {
            PreventExitPop = 0;
        }
        imgpopup = new Image();
        imgpopup.src = "<?php echo get_stylesheet_directory_uri(); ?>/images/popup.jpg";
    </script>
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.btstatic.com/tag.js#site=YdB4XyS";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//s.thebrighttag.com/iframe?c=YdB4XyS" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>

<!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type="text/javascript">
        function VersionSite(version) {
            switch (version) {
                case 1:
                    $("#btnSubmit").css("background-image","url(https://bellalift.com.br/v1/images/bella_lift_button.png)")
                    break;
            }
        }

		$(document).ready(function(){
		
		
		_vwo_code=(function(){
var account_id=26173,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
		
		
		});
		
       </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->

<script type="text/javascript">
    adroll_adv_id = "PJ7RXCX2CZBGZE65RUAOCU";
    adroll_pix_id = "7CAT4Y247ZCIDOLHH35SEP";
    (function () {
        var oldonload = window.onload;
        window.onload = function () {
            __adroll_loaded = true;
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            if (oldonload) { oldonload() } 
        };
    } ());
</script>

<!-- Segment Pixel - Retargeting - DO NOT MODIFY -->
<img src="https://secure.adnxs.com/seg?add=2673908&t=2" width="1" height="1" />
<!-- End of Segment Pixel -->

<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&.yp=413769&ec=carrinho"/>

<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&.yp=10004323&ec=carrinho"/>

<img src="https://amplifypixel.outbrain.com/pixel?mid=00ee9d92ecb44fb0f090c10d43ab47aa99"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63127604-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>