function meu_callback(conteudo) {
    console.log('blur');
    if (!("erro" in conteudo)) {
        
        setTimeout(function(){
            
            jQuery("#billing_address_1").val(conteudo.logradouro);
            jQuery("#billing_area").val(conteudo.bairro);
            jQuery("#billing_city").val(conteudo.localidade);
            jQuery("#billing_state").val(conteudo.uf);
            
            jQuery('#billing_state').trigger('change');
                
        }, 300);
        
    } else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        //alert("CEP não encontrado.");
    }
}

function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
        document.getElementById('billing_address_1').value=("");
        document.getElementById('billing_area').value=("");
        document.getElementById('billing_city').value=("");
        document.getElementById('billing_state').value=("");
        // document.getElementById('ibge').value=("");
}

jQuery(function() 
{
    function cep_generate() {
        
        var valor = jQuery(this).val();
        
        //Nova variável "cep" somente com dígitos.
        var billing_postcode = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (billing_postcode != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(billing_postcode)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                // document.getElementById('billing_address_1').value="...";
                // document.getElementById('billing_area').value="...";
                // document.getElementById('billing_city').value="...";
                // document.getElementById('billing_state').value="...";
                //document.getElementById('ibge').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ billing_postcode + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                //alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
        
    };
    
    jQuery("#customer_details").on('keyup', '#billing_postcode', cep_generate);
    jQuery("#customer_details").on('blur', '#billing_postcode', cep_generate);
    
});
    



