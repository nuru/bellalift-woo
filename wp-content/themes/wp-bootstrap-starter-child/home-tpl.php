<?php
/*
 Template Name: Bellelift Main Page Template
*/

?>

<!DOCTYPE html>
<html>

<!-- Mirrored from bellalift.com.br/mobile/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Mar 2019 13:07:56 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<?php wp_head(); ?>
<head>

<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Meta Tags -->
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<meta name="robots" content="index, follow" />
	
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

	<!-- CSS -->
	<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700,800,300italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" media="screen,projection" type="text/css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" media="screen,projection" type="text/css" />
<link type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" /><script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js"></script><script type="text/javascript">var dep = false;</script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/d3t95j2l2mupha.cloudfront.net/api/js/jquery.validationEngine-pt-BR.js"></script><script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/d3t95j2l2mupha.cloudfront.net/api/js/jquery.validationEngine-2.6.3.js"></script><link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/d3t95j2l2mupha.cloudfront.net/api/css/validationEngine.jquery.css" type="text/css" media="all" />
</head>



<body>
<!--pixelcode start--><img src='https://secure1.m57media.com/clients/p2c/u/pc/?cid1=0&amp;cid2=20783&amp;cid3=0&amp;cid4=0&amp;caid=31164&amp;caCode=Google-DP&amp;stID=449&amp;eType=2&amp;misc1=&amp;misc2=&amp;misc3=' style='height:1px;width:1px;border-width:0px;position:absolute'/><!--pixelcode end-->

			
	<div id="header">
	
		<div class="header-con">
		
			<div class="logo"><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="logodesktop"/><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo2.png" class="logomobil"/></a></div>
			
			<div class="example hidemobile">
				<p><span>ANTES</span><span>DEPOIS</span></p>
			</div><!--example-->
			<div class="example hidedesktop">
				<h1>VOCÊ lINDA E Anos Mais Jovem</h1>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bella-1.png" class=""/>
			</div><!--example-->
			
			<div class="cloud">
				<h4>Finalmente</h4>
				<p> um serum anti-idade<br />que realmente funciona</p>
			</div><!--cloud-->
			<div class="banner hidedesktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/new-bg.png" class=""/></div>
			<div class="text hidemobile">
			
				<h1>VOCÊ lINDA E Anos Mais Jovem</h1>
				
				<p>notaram uma significativa redução das rugas</p>
				<p>notaram uma significativa redução dos olherias</p>
				<p>notaram um significativo aumento da firmeza</p>
			
			</div><!--text-->
			
			<div class="button">
			
				<a href="#" onclick="document.location = '#dvInfo';return false;">compre já! <span>só<br />
				R$ 53*</span></a>
			
			</div><!--button-->
		<div class="sigle hidedesktop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/natural-1.png" class=""/></div>
			<div class="clear"></div>
		</div><!--header-con-->
	
		<div class="clear"></div>
	</div><!-- header -->

	<div id="content">
	
		<div class="about">
		
			<div class="leftside">
			
				<h3>COMO O <span>BELLALIFT</span> FUNCIONA?</h3>
				<p>Você está prestes a experimentar um dos maiores avanços nos cuidados com a pele e na redução de rugas: a combinação de <strong>Ácido Hialurônico e Vitamina C</strong></p>
				<p>O <strong>Ácido Hialurônico</strong> é um excelente preenchedor facial, suavizando rugas e linhas de expressão, além de restaurar e regenerar a pele  desidratada e sem vida. A <strong>Vitamina C </strong>é um poderoso antioxidante, promovendo a renovação celular e estimulando a produção de colágeno, aumetando a firmeza, elasticidade e hidratação da pele, além de combater as rugas.</p>
				<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic1.jpg"/></p>
				<h4>Outros ingredientes poderosos:</h4>
				<ul>
				  <li><strong>Vitamina A</strong> - Aumenta a elasticidade da pele, estimula a produção de colágeno e ajuda a reduzir as rugas e danos acumulados causados pelo sol</li>
				  <li><strong>Vitamina D3</strong> - condiciona e hidrata a pele, além de estimular a renovação celular. Deixa a pele com a aparência jovem e impecável</li>
				  <li><strong>Vitamina E</strong> - outro potente antioxidante natural, que mantém sua pele hidratada, renovada e com a aparência muito mais jovem</li>
			  </ul>
				<p>&nbsp;</p>
				<h3>TUDO CIÊNCIA. <strong>SEM FICÇÃO.</strong></h3>
				<div class="img-left"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic2.jpg"/></div>
				<h4> Natural sem Nenhum efeito colateral </h4> 
                Nosso serum BellaLift hidrata profundamente a pele e a deixa mais macia e firme, além de suavizar as rugas e linhas de expressão. Esse serum potente contém grande quantidade de nutrientes que penetram profundamente na sua pele.<br />
100% vegetal, sem parabenos, sem sulfato, sem óleo mineral, sem lanolina, sem glúten e sem OGM.</p>
				<h5>9 em cada 10 Dermatologistas</h5>
				<p>recomendam vitamina c para seus pacientes</p>
			
			</div><!--leftside-->
			
			<div class="rightside">
			
				<h3>TRÊS PASSOS PARA   UMA PELE <span>LINDA E JOVEM</span></h3>
				<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic3.jpg"/></p>
				<p class="first">PASSO1 <span>PASSO2</span> <span>PASSO3</span></p>
				
				<p class="add"><span>limpe e seque seu rosto.</span><span>Aplique o SERUM Bellalift  EM TODO ROSTO E PESCOÇO.</span><span>Deixe que o produto seja absorvido. Use diariamente para ter o resultado mÁximo.</span></p>
				
				<p class="one"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic4.jpg"/></p>
			
			</div><!--rightside-->
		
			<div class="clear"></div>
		</div><!--about-->
		
		<div class="divider">
		
			<div class="button"><a href="#" onclick="document.location = '#dvInfo';return false;">PEÇA AGORA!</a></div>
		
			<h3>PareÇa atÉ 10 anos mais jovem. Garanta o seu agora!   >></h3>
		
			<div class="clear"></div>
		</div><!--divider-->
		
		<div class="new">
		
			<h3><span>MILHARES DE MULHERES USAM BELLALIFT</span></h3>
			<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/faces.png"/></p>
			<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic5.jpg"/></p>
		
			<div class="clear"></div>
		</div><!--new-->
		
		<div class="facts">
		
			<div class="left"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic6.jpg"/></div>
			
			<div class="right">
			
				<h3>Resultados reais de  estudoS clínicoS</h3>
				<p><strong>O cientificamente avançado BellaLift comprovadamente proporciona resultados reais</strong>. Nosso produto foi testado pelo laboratório do prestigiado <strong>Grupo Investiga</strong>, que fica em São Paulo. </p>
                <p>Durante o estudo clínico com 35 participantes ( com idades entre 30 e 65) usaram a BellaLift uma vez por dia por 30 dias. O teste foi monitorado pelo Dermatologista Dr. André Luiz Verganini. Nenhuma das participantes sentiram efeitos colaterais ou alergias.</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic7.jpg"/>
			
			</div><!--right-->
		
			<div class="clear"></div>
		</div><!--facts-->
		
		<div class="testimonials">
		
			<h2>Pessoas Reais.  <span>Resultados Reais.</span></h2>
			
			<div class="test-box">
			
				<div class="top">
				
					<div class="person"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/peson1.jpg"/></div>
					
					<h4>O BellaLift® literalmente<br /> salvou minha pele!</h4>
				
					<div class="clear"></div>
				</div><!--top-->
				
				<p>Devido a exposiçao solar frequente minha pele começou a sofrer os efeitos do envelhecimento precoce. Aí uma amiga me falou sobre a BellaLift® e eu simplesmente amei esse produto, pois minha pele esta muito mais jovem, hidratada e firme. Super recomendado. </p>
<p class="name">-Angela, Rio de Janeiro</p>
			
			</div><!--test-box-->
			
			<div class="test-box">
			
				<div class="top">
				
					<div class="person"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/person2.jpg"/></div>
					
					<h4>Eu fiquei de boca aberta com que vi no espelho!</h4>
				
					<div class="clear"></div>
				</div><!--top-->
				
				<p>O BellaLift® foi um grande achado. É tão fácil de usar e quando eu me dei conta meu rosto pareceia muito mais jovem. Os pés de galhinha e rugas do meu rosto começaram a desaparecer. Não vivo mais sem meu Bellalift.</p>
				<p class="name"> - Joana, Salvador</p>
			
			</div><!--test-box-->
			
			<div class="test-box">
			
				<div class="top">
				
					<div class="person"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/person3.jpg"/></div>
					
					<h4>Fiquei com aparência anos mais jovem em poucos dias!</h4>
				
					<div class="clear"></div>
				</div><!--top-->
				
				<p>Os pés de galinha e as rugas sumiram depois de poucos dias - incrível, eu pareço ANOS mais nova. O melhor é que o efeito não passa. É só eu usar que fico bem! Realmente deixou minha vida muito mais feliz. Obrigada Bellalift.</p>
				<p class="name"> Andrea, São Paulo</p>
			
			</div><!--test-box-->
		
			<div class="clear"></div>
		</div><!--testimonials-->
		
		<div class="divider">
		
			<div class="button"><a href="#" onclick="document.location = '#dvInfo';return false;">compre já!</a></div>
		
			<h3>PareÇa atÉ 10 anos mais jovem <strong> Garanta o seu agora!</strong> >></h3>
		
			<div class="clear"></div>
		</div><!--divider-->
		
		<div class="pictures">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic8.jpg"/>
		</div><!--pictures-->
		
		<div class="pictures">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pic9.jpg"/>
		</div><!--pictures-->
		
		<div id="dvInfo" class="info">
		
			<h2>PARA ONDE DEVEMOS <span>ENVIAR SEU PEDIDO?</span></h2>
			<h3>Preencha os campos abaixo e lhe enviaremos um <strong>frasco extra totalmente GRATIS.</strong></h3>
			
			<div class="bottom">
			
				<div class="your-info">
				
					<h4>ENDEREÇO DE ENTREGA</h4>
					
					<form name="Form1" method="post" action="<?php echo get_site_url() . "/products-page/"; ?>" id="Form1" enablevalidation="true">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJLTg0MzI5MTIyD2QWAgIBD2QWAgINDw9kFgIeB09uQ2xpY2sFIWphdmFzY3JpcHQ6ZGVwPXRydWU7cmV0dXJuIGFiMSgpO2RkyQrQhRWnfUIo16VUdgRhh8rExQeZZz/cZdyPXKpN2wk=" />
</div>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/d3t95j2l2mupha.cloudfront.net/api/js/jquery.validationEngine-pt-BR-custom-messages.js"></script>
<script type="text/javascript">
	$('#Form1').validationEngine();
	function ab1() 
	{
		ShowExitPopup=false;
		if( (ucrmSkipValidation) || ((ucrmSkipValidation==null) && $('#Form1').validationEngine('validate')) )
		{ 
			if(ucrmAllowSubmit)
			{
		        
		        ucrmAllowSubmit=false; 
				return true;
				
			}
		}
		return false;
	}
	</script>
	<script type="text/javascript">function ExitPopup() { }

</script>
						<div class="left">
							<p><input name="txtFirstName" type="text" id="txtFirstName" class="validate[required]" placeholder="Nome" /></p>
							<p><input name="txtLastName" type="text" id="txtLastName" class="validate[required]" placeholder="Sobrenome" /></p>
							<p><input name="txtEmail" type="text" id="txtEmail" class="validate[required,custom[email],funcCall[email]]" placeholder="Email" /></p>
							
						</div><!--left-->
						<div class="right">
							
							<p><input name="txtPhone" type="text" maxlength="11" id="txtPhone" class="validate[required]" placeholder="Telefone" /></p>
							<p><input name="txtZip" type="text" id="txtZip" class="validate[required]" placeholder="CEP" /></p>
							<p><select name="ddlGender" id="ddlGender">
									<option selected="selected" value="">Sexo</option>
									<option value="Male">Masculino</option>
									<option value="Female">Feminino</option>
								</select>
							</p>
						</div><!--right-->
						<div class="clear"></div>
                        <input type="submit" name="btnSubmit" value="" onclick="javascript:dep=true;return ab1();" id="btnSubmit" class="submit" />
					
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D8EEF26F" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAryuQUvazP9uPNGhGbK1tgETNr8+Dx/H+2fNfPhxgyex+tZBdiRon5d1qE03oo5TvNDB2YfWbjwUYntrwPx36ESwxzms0Q07G4CNarrnTsXefnuSvN2ujDLViqJoMSOWQeIV/T68rcq/BMoaOJyCvoEkthMW/osrKq8c4rzoKr1O+R2cybPq3NZ96koemPVisc85pbWlDO2hADfoPXD/5tdtkjocSfFqOkglaKq/G/BUKqgP7XEoy/O7v0TrSkUQWw=" />
</div>

<script type="text/javascript">
//<![CDATA[
var ucrmSkipValidation = null; var ucrmAllowSubmit = true//]]>
</script>
</form>
				    <p class="def-segura">Nos respeitamos sua privacidade</p>
					<div class="clear"></div>
				</div><!--your-info-->
				
				<div class="arrow">
				
					<h3>
PEÇA JÁ E RECEBE ATÉ <br />
  <strong>3 FRASCOS GRÁTIS</strong> </h3>
</div>
			</div><!--bottom-->
		
			<div class="clear"></div>
		</div><!--info-->
	
		<div class="clear"></div>
	</div><!-- content -->
	
    
	<div id="footer">
	
		<div class="footer-con">
		
			<div class="flogo"><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flogo.png"/></a></div>
			
			<div class="fnav">
			
				<ul>
					<li class="first"><a href="<?php echo get_site_url(); ?>/termos-e-condicoes" target="_blank">BellaLift Termos e Condicoes</a></li>
					<li><a href="<?php echo get_site_url(); ?>/politica-de-privacidade" target="_blank">Politica de Privacidade </a></li>
					<li><a href="<?php echo get_site_url(); ?>/contato" target="_blank">Contato</a></li>
				</ul>
			
			</div><!--fnav-->
		
		</div><!--foooer-con-->
	
		<div class="clear"></div>
    
    </div><!-- footer -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.placeholder.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.numeric.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             $('#txtZip').mask("99999-999");
			 $('#txtPhone').numeric();
         });
    </script>


<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "<?php echo get_stylesheet_directory_uri(); ?>/s.btstatic.com/tag.js#site=YdB4XyS";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="http://s.thebrighttag.com/iframe?c=YdB4XyS" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<!-- Segment Pixel - Retargeting - DO NOT MODIFY -->
<img src="https://secure.adnxs.com/seg?add=2673908&amp;t=2" width="1" height="1" />
<!-- End of Segment Pixel -->

<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&amp;.yp=413769&amp;ec=home"/>

<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&amp;.yp=10004323&amp;ec=home"/>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','<?php echo get_stylesheet_directory_uri(); ?>/www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63127604-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

<!-- Mirrored from bellalift.com.br/mobile/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Mar 2019 13:08:30 GMT -->
</html>