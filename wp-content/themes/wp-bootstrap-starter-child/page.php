<?php

?>


<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <?php wp_head(); ?>
        
        
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">      

        
        
        
    </head>
    
    <body>

<?php

if(have_posts())
{
    while(have_posts())
    {
        the_post();
        the_content();
    }
}

?>

<?php wp_footer(); ?>
</body>
</html>





