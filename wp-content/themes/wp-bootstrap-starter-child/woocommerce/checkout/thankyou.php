<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


?>

<!--<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php _e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php _e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php _e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php _e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php _e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>-->

<form name="ctl00" method="post" action="https://bellalift.com.br/mobile/thankyou.aspx" id="ctl00" enablevalidation="true">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTM5NDI4Njc1MQ9kFgICAQ9kFg4CAQ8WAh4EVGV4dAUBMGQCAw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFgAWABYADBQrAABkAgUPFgIfAAUBIGQCBw8WAh8ABQMgICBkAgkPFgIfAGVkAgsPFgIfAGVkAg0PFgIfAGVkGAEFDmd2T3JkZXJEZXRhaWxzDzwrAAwBCGZkiQ1vJbDMVValgKXwpw9BsQvZa1oHu8AyvvUW4dY5+Yo=" />
</div>



<script type="text/javascript">function ExitPopup() { }</script> 

	    <div id="mainWrapper" class="bground_color">
		    <div id="wrapper">
			    <div id="topBar" class="top_bar_ty">
			    	<div class="top_bar_d">
				    	<p class="contact top_bar_txt">Finalize seu pedido abaixo com preços exclusivos ou compre pelo telefone: 0 xx (21) 3509 1860</p>
					</div>
		      </div><!-- END topBar -->

                <div style="text-align:center; padding-top:50px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:52px;"><span style="font-size:56px; color:#08AAC3;">PARABÉNS </span><br />



                <div style="font-family:Arial, Helvetica, sans-serif; font-size:17px; color:#111111; font-weight:bold; text-align:center; padding-top:20px;">
	                SEU PEDIDO #0
                    <div style="width: 100%; text-align: center;">

                    <div>

	<table cellspacing="0" cellpadding="5" rules="all" border="1" id="gvOrderDetails" style="border-color:Black;border-width:1px;border-style:Solid;width:90%;border-collapse:collapse;margin: 0 auto;">

		<tr style="background-color:#08AAC3;">

			<th  scope="col" style="border-width:1px;border-style:solid;">Nome do produto</th><th scope="col" style="border-width:1px;border-style:solid;">Quantidade </th><th scope="col" style="border-width:1px;border-style:solid;">Total</th>
	
		</tr>
		<?php 
			
				if($order)
				{
					// Iterate though each order item
					foreach ($order->get_items() as $item_id => $item) 
					{
						echo "<tr>";
						// Get the WC_Order_Item_Product object properties in an array
					    $item_data = $item->get_data();
					
					    if ($item['quantity'] > 0) 
					    {
					        // get the WC_Product object
					        $product = wc_get_product($item['product_id']);
					        
					        $product_name = $product->get_name();
					        
					        $product_img = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'single-post-thumbnail' )[0];
					        $quantity = '';
					        
					        if($product->get_id() == 38) {
								$quantity = "<span class='quantity_circle center'>6</span>";
							} else if($product->get_id() == 37) {
								$quantity = "<span class='quantity_circle center'>3</span>";
							} else if($product->get_id() == 36) {
								$quantity = "<span class='quantity_circle center'>1</span>";
							}
							
					        echo "<td><img src='{$product_img}' class='product_img'><p>{$product_name}</p></td>";
					        echo "<td>{$quantity}</td>";
					        echo "<td>{$item->get_total()}</td>";
							
					        
					    }
					    
					    echo "</tr>";
					}
				}
			
			?>

	</table>

</div>

                    </div>
                    
                    <?php
                    
						$order_data = $order->get_data();
						
						$order_shipping_first_name = $order_data['shipping']['first_name'];
						$order_shipping_last_name = $order_data['shipping']['last_name'];
						$order_shipping_address_1 = $order_data['shipping']['address_1'];
						$order_shipping_city = $order_data['shipping']['city'];
						$order_shipping_state = $order_data['shipping']['state'];
						$order_shipping_postcode = $order_data['shipping']['postcode'];
						$order_shipping_country = $order_data['shipping']['country'];
						
					?>

                    SERÁ ENVIADO PARA:
                    
                    <div class='full_name_address'>
                    	<p>
                    		<?php
                    			
                    			echo "<h3>" . $order_shipping_first_name . " " . $order_shipping_last_name . "</h3>";
                    		?>
                    	</p>
                    	<p>
                    		<?php 
                    			echo "<h3>" . $order_shipping_address_1 . ", " . $order_shipping_city . ", " . $order_shipping_state . ", " . $order_shipping_postcode . ", " .  $order_shipping_country . "</h3>";
                    		?>
                    	</p>
                    </div>


	      <!--          <div class="order-details" style="max-width: 90%; margin: 0 auto;">-->
	                	
	                	
	                	
							<!--<div class="table-responsive">-->
							<!--  <table class="table table-bordered">-->
							<!--    <thead>-->
							<!--      <tr style="background: #08aac3; color: #000000;">-->
							<!--      	<th scope="col">#</th>-->
							<!--        <th scope="col">Nome</th>-->
							<!--        <th scope="col">Sobrenome</th>-->
							<!--        <th scope="col">Endereco</th>-->
							<!--        <th scope="col">Citate</th>-->
							<!--        <th scope="col">Estado</th>-->
							<!--        <th scope="col">CEP</th>-->
							<!--        <th scope="col">País</th>-->
							<!--      </tr>-->
							<!--    </thead>-->
							<!--    <tbody style='background-color: white;'>-->
							<!--      <tr>-->
							<!--        <th scope="row">1</th>-->
							<!--        <td><?php echo   $order_shipping_first_name   ?></td>-->
							<!--        <td><?php echo   $order_shipping_last_name   ?></td>-->
							<!--        <td><?php echo   $order_shipping_address_1   ?></td>-->
							<!--        <td><?php echo   $order_shipping_city   ?></td>-->
							<!--        <td><?php echo   $order_shipping_state   ?></td>-->
							<!--        <td><?php echo   $order_shipping_postcode   ?></td>-->
							<!--        <td><?php echo   $order_shipping_country   ?></td>-->
							<!--      </tr>-->
							      
							      
							<!--    </tbody>-->
							<!--  </table>-->
							<!--</div>-->
                            
                            

	      <!--          </div>-->

                </div>

            </div>









		    </div><!-- END wrapper -->

        <div class="home_ty">
	      	<a href='<?php echo get_site_url(); ?>'>HOME</a>
        </div>

      
	    </div><!-- END mainWrapper -->

    

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FA49BB8A" />

</div>



<script type="text/javascript">

//<![CDATA[

var ucrmSkipValidation = null; var ucrmAllowSubmit = true//]]>

</script>

</form>



<!--<script type="text/javascript">

//   (function () {

//     var tagjs = document.createElement("script");

//     var s = document.getElementsByTagName("script")[0];

//     tagjs.async = true;

//     tagjs.src = "../../s.btstatic.com/tag.js#site=YdB4XyS";

//     s.parentNode.insertBefore(tagjs, s);

//   }());

 </script>-->

<noscript>
  <iframe src="http://s.thebrighttag.com/iframe?c=YdB4XyS" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>

<!-- Segment Pixel - Retargeting - DO NOT MODIFY -->
<img src="https://secure.adnxs.com/seg?add=2673908&amp;t=2" width="1" height="1" />
<!-- End of Segment Pixel -->





<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&amp;.yp=413769&amp;ec=conversao"/>
<img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&amp;.yp=10004323&amp;ec=conversao"/>



<!--<script language='JavaScript'>-->
<!-- var OB_ADV_ID=22877;-->
<!-- var scheme = (("https:" == document.location.protocol) ? "https://" : "http://");-->
<!-- var str = '<script src="'+scheme+'widgets.outbrain.com/obtp.js" type="text/javascript"><\/script>';-->
<!-- document.write(str);-->
<!-- </script>-->

 <!-- 3-14-2016 --->
<!--<script>-->

<!--  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){-->
<!--  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),-->
<!--  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)-->
<!--  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');-->

<!--  ga('create', 'UA-63127604-1', 'auto');-->
<!--  ga('send', 'pageview');-->



<!--</script>-->


<div style="background-color:#b7f5fe;">
		
	<div id="footer">

	    <p class="links foot_ty">&copy; 2014 BellaLift Todos os Direitos Reservados. <a href="<?php echo get_site_url(); ?>/termos-e-condicoes" target="_blank">Termos e Condicoes</a> | <a href="<?php echo get_site_url(); ?>/politica-de-privacidade" target="_blank">Politica de Privacidade</a> | <a href="<?php echo get_site_url(); ?>/contato" target="_blank">Contato</a></p>

    </div><!-- footer -->

</div>