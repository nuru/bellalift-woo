<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$base_url = get_site_url();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>
<div class="container mb-3"> <img src="<?php echo $base_url; ?>/wp-content/themes/wp-bootstrap-starter-child/images/checkout-logo.png"></div>
<div class="container">
	
<form name="checkout" method="post" class="checkout woocommerce-checkout checkout_form" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
	
	<h3 class='primary_col'>Sua Oferta Escolhida!</h3>
	<div id="order_review" class="woocommerce-checkout-review-order review_top">
		<?php 
			remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
			do_action( 'woocommerce_checkout_order_review' );
		?>
	</div>
	
	<?php if ( $checkout->get_checkout_fields() ) : ?>
		
		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="" id="customer_details">
			<div class="">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<!--<div class="col-2 test">-->
			<!--	<?php //do_action( 'woocommerce_checkout_shipping' ); ?>-->
			<!--</div>-->
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<h3 id="order_review_heading"><?php //esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
	
	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php 
			remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
			add_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
			do_action( 'woocommerce_checkout_order_review' );
		?>
	</div>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>
</div>
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>


<div class="form_foot mt-3">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<img src="<?php echo $base_url; ?>/wp-content/themes/wp-bootstrap-starter-child/images/flogo.png"></img>
			</div>
			<div class="col-md-9">
				<p class="white_col text-right">
					CNPJ 24.323.789/000180 <a href="https://bellalift.com.br/v1sc/Termos.htm" class="white_col">BellaLit Termos e Condicoes</a> | <a href="https://bellalift.com.br/v1sc/Pol%C3%ADtica%20de%20Privacidade.htm" class="white_col">Politica de Privacidade</a> | <a href="https://bellalift.com.br/v1sc/Contato.htm" class="white_col"> Contato</a>
				</p>
			</div>
		</div>
	</div>
</div>