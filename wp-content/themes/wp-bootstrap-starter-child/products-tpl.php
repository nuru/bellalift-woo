<?php
/*
 Template Name: Bellelift Products Page Template
*/

$product_three = wc_get_product( 38 );
$product_two = wc_get_product( 37 );
$product_one = wc_get_product( 36 );

$base_url = get_site_url();

if(isset($_POST['btnSubmit']))
{
	@session_start();
	
	$_SESSION['user']['first_name'] = trim($_POST['txtFirstName']);
	$_SESSION['user']['last_name'] = trim($_POST['txtLastName']);
	$_SESSION['user']['email'] = trim($_POST['txtEmail']);
	$_SESSION['user']['zip'] = trim($_POST['txtZip']);
	$_SESSION['user']['phone'] = trim($_POST['txtPhone']);
	$_SESSION['user']['gender'] = trim($_POST['ddlGender']);
}

?>

<!DOCTYPE html>

<html>
<head>

<title>Products</title>

	<!-- Meta Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<meta name="robots" content="index, follow" />
	
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

	<!-- CSS -->
	<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700,800,300italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" media="screen,projection" type="text/css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" media="screen,projection" type="text/css" />
	
    <style type="text/css">
        #exit_pop{
	        display:none;position:fixed;top:0;left:0;width:100%;height:100%;margin:0px;padding:0px;
	        z-index:9999;clear:none;background-color:rgba(0,0,0,0.5);
        }
        #exit_pop_content{
	        cursor:pointer;
	        position:fixed;
	        height: 408px;
            left: 50%;
            margin: -204px 0 0 -344px;
            position: absolute;
            top: 50%;
            width: 688px;
        }
    </style>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3OXZQgNnY1KpCvCU0raFE6IhzEWGb3XJ";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
	<!--End of Zopim Live Chat Script-->
</head>


<style>
	
	@media only screen and (min-width: 320px) and (max-width: 479px){ 
	
    .top_mr{
        display: none;
    }
    .product-top{
        display: none;
    }

 }

@media only screen and (min-width: 480px) and (max-width: 767px){ 
		
    .top_mr{
        display: none;
    }
    .product-top{
        display: none;
    }

 }

@media only screen and (min-width: 768px) and (max-width: 991px){ 
	
    .top_mr{
        display: none;
    }
    .product-top{
        display: none;
    }
 }

@media only screen and (min-width: 992px) and (max-width: 1999px){ 
    	
   
}

	
</style>

<body>
	
    <div id="exit_pop" style="display:none;">
        <div id="exit_pop_content">
            <img style="width:688px;height:408" src="<?php echo get_stylesheet_directory_uri(); ?>/images/popup.jpg"/>
        </div>
    </div>
	<div id="discountHeader" class="product-top product-top_mr" style="text-align:center;" runat="server" visible="false">
		<p style="width:100%">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/exclamacao.png" align="left" style="width:75px;"/>
		<span style="display:inline-block; font-size:24px;line-height:30px;">Parabéns, seu desconto de R$20 foi adicionado.<br />Finalize seu pedido nos próximos: <span name="countdown" id="countdown">09:59</span> minutos para garantir seu Desconto.</span>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/exclamacao.png" align="right" style="width:75px;"/>
		</p>
            <script type="text/javascript">
            
            	jQuery(window).unbind();
            	window.alert = function () {}
            	
                jQuery(function()
	            {
	                var minutes = 9;
	                var seconds = 59;
	                
	                var endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
	                var showTime = (+new Date) + 1000 * (60*10 + 0) + 500;
	                
	                function my_time()
	                {
	                    let timeLeft = endTime - (+new Date);
	                    let showTimeLeft = showTime - (+new Date);
	                    
	                    // if(showTimeLeft <= 0) {
	                    //   jQuery(".first_cls_cont").addClass('d-none');
	                    //   jQuery(".second_cls_cont").removeClass('d-none');
	                    // }
	                    
	                    if(timeLeft <= 0) {
	                        jQuery("#countdown").html("compre agora");
	                    } else {
	                        
	                        time = new Date(timeLeft);
	                        mins = time.getMinutes();
	                        scs = time.getSeconds();
	                        
	                        if(mins < 10) {
	                          mins = "0" + mins;
	                        }
	                        
	                        if(scs < 10) {
	                          scs = "0" + scs;
	                        }
	                        
	                        jQuery("#countdown").html(mins + ":" + scs);
	                        
	                    }
	                }
	                setInterval(my_time, 1000);
	            });
            </script>
	</div>
	<!--<form id="Form1" method="post" enablevalidation="true" runat="server">	-->
	<div id="header2">
	
		<div class="top top_mr">
			<h1>proveite os precos exclusivos do nosso site, aquiapresentamos</h1>
			<h2>3 ofertas com descontos especials para voce.</h2>
		</div>
		
		<div class="bottom">
		
			<h3 class="hidemobile">Votado ANTI<br />AGE <span>BELLA LIFT no1</span></h3>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/banut2.png" class="hidedesktop">
		</div><!--bottom-->
	
		<div class="clear"></div>
	</div><!--header2-->

	<div id="content">
	
		<div class="post">
		
			<h3><?php echo $product_three->post->post_title; ?><span style="color:#0094FF;text-decoration:underline;"></span></h3>
			<div class="pack">
			
				<div class="product-pic">
				
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product1.jpg"/>
					<p>Disponivel em estoque. Risco atual de estogar: ALTO</p>
				
				</div><!--product-pic-->
				
				<div class="cost">
				
					<p class="price">6 x R$ 56,59</p>
					<p class="por">por apenas:</p>
					<p class="description">Nos garantimos a eficacia de nossos produtos, e se por alguma razao voce estiver insatisfeito com qualquer de suas compras, devolveremos o seu dinheiro!</p>
					<p class="por">Preco varejo <span class="line">RS <?php echo $product_three->get_regular_price(); ?></span>  <span>Preco final: RS <?php echo $product_three->get_sale_price(); ?></span></p>
					
					<p id='pr_three' class='hand_hover'><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/buy-button.png" alt="Buy Now"/></p>
				
				</div><!--cost-->
				
				<div class="discount">
				
					<p>DESCONTO DE<br /><span>65%</span></p>
				
				</div><!--discount-->
			
				<div class="clear"></div>
			</div><!--pack-->
		
			<div class="clear"></div>
		</div><!--post-->
		
		<div class="post">
		
			<h3><?php echo $product_two->post->post_title; ?></h3>
			
			<div class="pack">
			
				<div class="product-pic">
				
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product2.jpg"/>
					<p>Disponivel em estoque. Risco atual de estogar: ALTO</p>
				
				</div><!--product-pic-->
				
				<div class="cost">
				
					<p class="price">6 x R$41,50</p>
					<p class="por">por apenas:</p>
					<p class="description">Nos garantimos a eficacia de nossos produtos, e se por alguma razao voce estiver insatisfeito com qualquer de suas compras, devolveremos o seu dinheiro!</p>
					<p class="por">Preco varejo <span class="line">RS <?php echo $product_two->get_regular_price(); ?></span>  <span>Preco final: RS <?php echo $product_two->get_sale_price(); ?></span></p>
					
					<p id='pr_two' class='hand_hover'><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/buy-button.png" alt="Buy Now"/></p>
				
				</div><!--cost-->
				
				<div class="discount">
				
					<p>DESCONTO DE<br /><span>46%</span></p>
				
				</div><!--discount-->
			
				<div class="clear"></div>
			</div><!--pack-->
		
			<div class="clear"></div>
		</div><!--post-->
		
		<div class="post">
		
			<h3><?php echo $product_one->post->post_title; ?></h3>
			
			<div class="pack">
			
				<div class="product-pic">
				
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product3.jpg"/>
					<p>Disponivel em estoque. Risco atual de estogar: ALTO</p>
				
				</div><!--product-pic-->
				
				<div class="cost">
				
					<p class="price">6 x R$ 23,76</p>
					<p class="por">por apenas:</p>
					<p class="description">Nos garantimos a eficacia de nossos produtos, e se por alguma razao voce estiver insatisfeito com qualquer de suas compras, devolveremos o seu dinheiro!</p>
					<p class="por">Preco varejo <span class="line">RS <?php echo $product_one->get_regular_price(); ?></span>  <span>Preco final: RS <?php echo $product_one->get_sale_price(); ?></span></p>
					
					<p id='pr_one' class='hand_hover'><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/buy-button.png" alt="Buy Now"/></p>
				
				</div><!--cost-->
				
				<div class="discount">
				
					<p>DESCONTO DE<br /><span>10%</span></p>
				
				</div><!--discount-->
			
				<div class="clear"></div>
			</div><!--pack-->
		
			<div class="clear"></div>
		</div><!--post-->
	
		<div class="clear"></div>
	</div><!-- content -->
	
	
	<!--</form>-->
<script type="text/javascript">
        /*jQuery.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());

        var ffversion = 0;

        if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))

            ffversion = new Number(RegExp.$1);

        var PreventExitPop = 0;//PreventExitPop;

        window.submitted = false;


        function bunload() {

            var message = "";

            if (PreventExitPop != 1 && !window.submitted) {

                PreventExitPop = 1;

                // document.getElementById('exit_pop').style.display = 'block';


                if ($.browser.chrome || $.browser.safari) {

                    scroll(0, 0);

                    window.onbeforeunload = null;

                    return message;
                }

                else {

                    return message;
                }

            } else {
                PreventExitPop = 0;
            }
        }

        if (PreventExitPop != 1) {

            window.onbeforeunload = bunload;

        } else {
            PreventExitPop = 0;
        }
        imgpopup = new Image(); 
        imgpopup.src = "<?php //echo get_stylesheet_directory_uri(); ?>/images/popup.jpg"; */
    </script>
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
        var _vwo_code = (function () {
            var account_id = 26173,
    settings_tolerance = 2000,
    library_tolerance = 2500,
    use_existing_jquery = false,
            // DO NOT EDIT BELOW THIS LINE
    f = false, d = document; return { use_existing_jquery: function () { return use_existing_jquery; }, library_tolerance: function () { return library_tolerance; }, finish: function () { if (!f) { f = true; var a = d.getElementById('_vis_opt_path_hides'); if (a) a.parentNode.removeChild(a); } }, finished: function () { return f; }, load: function (a) { var b = d.createElement('script'); b.src = a; b.type = 'text/javascript'; b.innerText; b.onerror = function () { _vwo_code.finish(); }; d.getElementsByTagName('head')[0].appendChild(b); }, init: function () { settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance); this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random()); var a = d.createElement('style'), b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}', h = d.getElementsByTagName('head')[0]; a.setAttribute('id', '_vis_opt_path_hides'); a.setAttribute('type', 'text/css'); if (a.styleSheet) a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b)); h.appendChild(a); return settings_timer; } };
        } ()); _vwo_settings_timer = _vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.btstatic.com/tag.js#site=YdB4XyS";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//s.thebrighttag.com/iframe?c=YdB4XyS" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>

<script type="text/javascript">
    adroll_adv_id = "PJ7RXCX2CZBGZE65RUAOCU";
    adroll_pix_id = "7CAT4Y247ZCIDOLHH35SEP";
    (function () {
        var oldonload = window.onload;
        window.onload = function () {
            __adroll_loaded = true;
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            if (oldonload) { oldonload() } 
        };
    } ());
</script>

<!-- Segment Pixel - Retargeting - DO NOT MODIFY -->
<img src="https://secure.adnxs.com/seg?add=2673908&t=2" width="1" height="1" />
<!-- End of Segment Pixel -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63127604-1', 'auto');
  ga('send', 'pageview');

</script>


<script>
	
	var base_url = '<?php echo $base_url; ?>';
	var admin_url = '<?php echo admin_url('admin-ajax.php'); ?>';
	
	jQuery(function() {
		
		var product_id = null;
		
		jQuery(".hand_hover").on("click", function(e) {
			
			if(jQuery(this).attr('id') == 'pr_one') {
				product_id = 36;
			} else if(jQuery(this).attr('id') == 'pr_two') {
				product_id = 37;
			} else if(jQuery(this).attr('id') == 'pr_three') {
				product_id = 38;
			}
			
			
			if(product_id) {
				
				jQuery.ajax({
		            url: admin_url + "/?action=add_to_card_bella&product_id=" + product_id,
		            type: 'get',
		            dataType: 'json',
		            success: function(response) {
	            		
	            		if(response == 'success') {
	            			window.location.href = base_url + "/checkout-page";
	            		}
	            		
		            }
		        });
			}
			
			
				
		});
		
		
		
			
	});
	
</script>


</body>
</html>


