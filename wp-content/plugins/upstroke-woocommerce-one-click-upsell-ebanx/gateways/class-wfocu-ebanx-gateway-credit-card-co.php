<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use Ebanx\Benjamin\Models\Payment;
use Ebanx\Benjamin\Models\Address;
use Ebanx\Benjamin\Models\Country;
use Ebanx\Benjamin\Models\Item;
use Ebanx\Benjamin\Models\Person;
use Ebanx\Benjamin\Models\Card;

/**
 * WFOCU_Ebanx_Gateway_Credit_Card_CO class.
 *
 * @extends WFOCU_Gateway
 */
class WFOCU_Ebanx_Gateway_Credit_Card_CO extends WFOCU_Gateway {

	public $key = 'ebanx-credit-card-co';
	public $token = false;
	public $names = array();
	public $configs = array();
	protected static $ins = null;

	/**
	 * WFOCU_Ebanx_Gateway_Credit_Card_CO constructor.
	 */
	public function __construct() {

		parent::__construct();

		$ebanx_gateway = new WC_EBANX_Gateway();
		$this->names   = $ebanx_gateway->get_billing_field_names();
		$this->configs = new WC_EBANX_Global_Gateway();
		remove_action( 'wp_enqueue_scripts', array( $ebanx_gateway, 'checkout_assets' ), 100 );

		add_action( 'woocommerce_pre_payment_complete', array( $this, 'wfocu_ebanx_compatibility_update_order_meta' ), 25, 1 );

	}

	/**
	 * @param $order_id
	 *
	 * @throws Exception
	 */
	public function wfocu_ebanx_compatibility_update_order_meta( $order_id ) {

		$order = wc_get_order( $order_id );

		if ( 'ebanx-credit-card-co' === $order->get_payment_method() ) {
			$card                = new \stdClass();
			$card->brand         = WC_EBANX_Request::read( 'ebanx_brand' );
			$card->token         = WC_EBANX_Request::read( 'ebanx_token' );
			$card->masked_number = WC_EBANX_Request::read( 'ebanx_masked_card_number' );
			$card->cvv           = WC_EBANX_Request::read( 'ebanx_billing_cvv' );
			$card->device_id     = WC_EBANX_Request::read( 'ebanx_device_fingerprint' );
			$card->document      = static::get_colombian_document( $order, $this->names, $this->key );

			update_post_meta( $order_id, '_ebanx_credit_card_token', $card );
		}
	}

	/**
	 * Try and get the payment token saved by the gateway
	 *
	 * @param WC_Order $order
	 *
	 * @return true on success false otherwise
	 */
	public function has_token( $order ) {
		$get_id = WFOCU_WC_Compatibility::get_order_id( $order );

		$this->token = get_post_meta( $get_id, '_ebanx_credit_card_token', true );

		if ( ! empty( $this->token ) ) {
			return true;
		}

		return false;
	}

	/**
	 * @return null|WFOCU_Ebanx_Gateway_Credit_Card_CO
	 */
	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self;
		}

		return self::$ins;
	}

	/**
	 * @param WC_Order $order
	 *
	 * @return true
	 */
	public function process_charge( $order ) {
		$is_successful = false;
		try {

			$payment_data = $this->generate_payment_request( $order );
			$ebanx        = ( new WC_EBANX_Api( $this->configs ) )->ebanx();
			$response     = $ebanx->creditCard()->create( $payment_data );

			if ( 'SUCCESS' !== ( $response['status'] ) ) {
				//@todo handle the error part here/failure of order

			} else {
				if ( empty( $response['payment'] ) ) {
					$is_successful = false;
				} else {
					WFOCU_Core()->data->set( '_transaction_id', $response['payment']['hash'] );

					$is_successful = true;
					//@todo handle the success part here/failure of order
				}
			}
		} catch ( Exception $e ) {

			//@todo transaction failure to handle here
			$order_note = sprintf( __( 'Ebanx Credit Card - Colombia Transaction Failed (%s)', WFOCU_Ebanx_Compatibility::$slug ), $e->getMessage() );
		}

		return $this->handle_result( $is_successful );
	}

	/**
	 * Generate the request for the payment.
	 *
	 * @param  WC_Order $order
	 *
	 * @return array()
	 */
	protected function generate_payment_request( $order ) {

		$payment = self::transform_token( $order, $this->configs, $this->names, $this->key );

		$payment->instalments = '1';

		$payment->device_id = $this->token->device_id;

		$payment->card = new Card( [
			'autoCapture' => ( 'yes' === $this->configs->settings['capture_enabled'] ),
			'token'       => $this->token->token,
			'cvv'         => $this->token->cvv,
			'type'        => $this->token->brand,
		] );

		$payment->manualReview = 'yes' === $this->configs->settings['manual_review_enabled']; // phpcs:ignore WordPress.NamingConventions.ValidVariableName

		return $payment;
	}

	/**
	 * @param $order
	 * @param $configs
	 * @param $names
	 * @param $gateway_id
	 *
	 * @return Payment
	 * @throws Exception
	 */
	public static function transform_token( $order, $configs, $names, $gateway_id ) {
		$get_package  = WFOCU_Core()->data->get( '_upsell_package' );
		$order_id     = WFOCU_WC_Compatibility::get_order_id( $order );
		$get_offer_id = WFOCU_Core()->data->get( 'current_offer' );

		return new Payment( [
			'amountTotal'         => $get_package['total'], //$order->get_total(),
			'orderNumber'         => $order_id . '_' . $get_offer_id,
			'dueDate'             => static::transform_due_date( $configs ),
			'address'             => static::transform_token_address( $order ),
			'person'              => static::transform_token_person( $order, $configs, $names, $gateway_id ),
			'responsible'         => static::transform_token_person( $order, $configs, $names, $gateway_id ),
			'items'               => static::transform_token_items( $get_package ),
			'merchantPaymentCode' => substr( $order->id . '-' . md5( rand( 123123, 9999999 ) ), 0, 40 ),
			'riskProfileId'       => 'Wx' . str_replace( '.', 'x', WC_EBANX::get_plugin_version() ),
		] );
	}

	/**
	 *
	 * @param WC_Order $order
	 *
	 * @return Address
	 * @throws Exception Throws parameter missing exception.
	 */
	private static function transform_token_address( $order ) {
		$addresses = $order->billing_address_1;
		$addresses .= ' - ' . $order->billing_address_2;

		$addresses     = WC_EBANX_Helper::split_street( $addresses );
		$street_number = empty( $addresses['houseNumber'] ) ? 'S/N' : trim( $addresses['houseNumber'] . ' ' . $addresses['additionToAddress'] );

		return new Address( [
			'address'      => $addresses['streetName'],
			'streetNumber' => $street_number,
			'city'         => $order->billing_city,
			'country'      => Country::fromIso( $order->billing_country ),
			'state'        => $order->billing_state,
			'zipcode'      => $order->billing_postcode,
		] );
	}

	/**
	 * @param $order
	 * @param $configs
	 * @param $names
	 * @param $gateway_id
	 *
	 * @return Person
	 * @throws Exception
	 */
	private static function transform_token_person( $order, $configs, $names, $gateway_id ) {
		$order_id = WFOCU_WC_Compatibility::get_order_id( $order );

		return new Person( [
			'type'        => self::get_person_type( $configs, $names ),
			'document'    => get_post_meta( $order_id, '_ebanx_credit_card_token', true )->document,
			'email'       => $order->billing_email,
			'ip'          => WC_Geolocation::get_ip_address(),
			'name'        => $order->billing_first_name . ' ' . $order->billing_last_name,
			'phoneNumber' => '' === $order->billing_phone ? '' : $order->billing_phone,
		] );
	}

	/**
	 *
	 * @param WC_EBANX_Global_Gateway $configs
	 *
	 * @return DateTime|string
	 */
	private static function transform_due_date( $configs ) {
		$due_date = '';
		if ( ! empty( $configs->settings['due_date_days'] ) ) {
			$due_date = new DateTime();
			$due_date->modify( "+{$configs->settings['due_date_days']} day" );
		}

		return $due_date;
	}

	/**
	 *
	 * @param WC_EBANX_Global_Gateway $configs
	 * @param array $names
	 *
	 * @return string
	 * @throws Exception Throws parameter missing exception.
	 */
	private static function get_person_type( $configs, $names ) {
		$fields_options = array();
		$person_type    = Person::TYPE_PERSONAL;

		if ( isset( $configs->settings['brazil_taxes_options'] ) && is_array( $configs->settings['brazil_taxes_options'] ) ) {
			$fields_options = $configs->settings['brazil_taxes_options'];
		}

		if ( count( $fields_options ) === 1 && 'cnpj' === $fields_options[0] ) {
			$person_type = Person::TYPE_BUSINESS;
		}

		if ( in_array( 'cpf', $fields_options ) && in_array( 'cnpj', $fields_options ) ) {
			$person_type = 'cnpj' === WC_EBANX_Request::read( $names['ebanx_billing_brazil_person_type'], 'cpf' ) ? Person::TYPE_BUSINESS : Person::TYPE_PERSONAL;
		}

		return $person_type;
	}

	/**
	 *
	 * @param $order WC_Order $order
	 *
	 * @return array
	 */
	private static function transform_token_items( $get_package ) {

		return array_map( function ( $product ) {
			return new Item( [
				'name'      => $product['data']->get_title(),
				'unitPrice' => $product['price'],
				'quantity'  => ( ! empty( $product['qty'] ) ) ? absint( $product['qty'] ) : 1,
			] );
		}, $get_package['products'] );
	}

	/**
	 *
	 * @param WC_Order $order
	 * @param array $names
	 * @param string $gateway_id
	 *
	 * @return string
	 * @throws Exception Throws parameter missing exception.
	 */
	private static function get_colombian_document( $order, $names, $gateway_id ) {
		$document = WC_EBANX_Request::read( $names['ebanx_billing_colombia_document'], null ) ?: WC_EBANX_Request::read( $gateway_id, null )['ebanx_billing_colombia_document'];

		if ( null === $document && 'ebanx-credit-card-co' === $order->get_payment_method() ) {
			throw new Exception( 'BP-DR-22' );
		}

		return $document;
	}
}

WFOCU_Ebanx_Gateway_Credit_Card_CO::get_instance();
