=== UpStroke: WooCommerce One Click Upsell for EBANX ===
Contributors: WooFunnels
Tested up to: 4.9.8
Stable tag: 1.0.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html


== Change log ==

= 1.0.0 (02/10/2018) =
* Public Release