<?php
/**
 * Plugin Name: UpStroke: WooCommerce One Click Upsell for EBANX
 * Plugin URI: https://buildwoofunnels.com
 * Description: An UpStroke add-on which adds compatibility with 'EBANX Payment Gateway for Woocommerce' to work for upsells.
 * Version: 1.0.0
 * Author: WooFunnels
 * Author URI: https://buildwoofunnels.com
 * License: GPLv3 or later
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: upstroke-woocommerce-one-click-upsell-ebanx
 *
 * Requires at least: 4.2.1
 * Tested up to: 4.9.8
 * WC requires at least: 3.0.0
 * WC tested up to: 3.4.5
 *
 * WooFunnels: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! function_exists( 'wfocu_ebanx_compatibility_dependency' ) ) {

	/**
	 * Function to check if ebanx payment gateway for woocommerce and upstroke are enabled and activated or not?
	 * @return bool True|False
	 */
	function wfocu_ebanx_compatibility_dependency() {

		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		if ( false === file_exists( plugin_dir_path( __DIR__ ) . 'ebanx-payment-gateway-for-woocommerce/woocommerce-gateway-ebanx.php' ) || false === file_exists( plugin_dir_path( __DIR__ ) . 'woofunnels-upstroke-one-click-upsell/woofunnels-upstroke-one-click-upsell.php' ) ) {
			return false;
		}

		return ( ( in_array( 'woofunnels-upstroke-one-click-upsell/woofunnels-upstroke-one-click-upsell.php', $active_plugins ) || array_key_exists( 'woofunnels-upstroke-one-click-upsell/woofunnels-upstroke-one-click-upsell.php', $active_plugins ) ) && ( in_array( 'ebanx-payment-gateway-for-woocommerce/woocommerce-gateway-ebanx.php', $active_plugins ) || array_key_exists( 'ebanx-payment-gateway-for-woocommerce/woocommerce-gateway-ebanx.php', $active_plugins ) ) );
	}
}


class WFOCU_Ebanx_Compatibility {

	/**
	 * @var instance
	 */
	public static $instance;

	public static $slug;

	public $gateway_dir_path = '/gateways/';
	public $class_prefix = 'WFOCU_Ebanx_Gateway_';

	/**
	 * WFOCU_Ebanx_Compatibility constructor.
	 * @throws Exception
	 */
	public function __construct() {
		$this->init_constants();

		//Adding license check
		add_action( 'plugins_loaded', array( $this, 'wfocu_add_upstroke_ebanx_compatibility_licence_support_file' ) );

		//Incluing gateways integration files
		spl_autoload_register( array( $this, 'ebanx_integration_autoload' ) );
		$this->init_hooks();
	}

	/**
	 * Intializing contants
	 */
	public function init_constants() {
		define( 'WFOCU_EBANX_COMPATIBILITY_VERSION', '1.0.0' );
		define( 'WFOCU_EBANX_COMPATIBILITY_BASENAME', plugin_basename( __FILE__ ) );
		define( 'WFOCU_EBANX_COMPATIBILITY_PLUGIN_DIR', __DIR__ );
		self::$slug = 'upstroke-woocommerce-one-click-upsell-ebanx';
	}

	/**
	 * Adding license support file
	 */
	public function wfocu_add_upstroke_ebanx_compatibility_licence_support_file() {
		include_once plugin_dir_path( __FILE__ ) . 'class-upstroke-woocommerce-one-click-upsell-ebanx-support.php';
	}

	/**
	 * Auto-loading the payment classes as they called.
	 *
	 * @param $class_name
	 */
	public function ebanx_integration_autoload( $class_name ) {

		if ( false !== strpos( $class_name, $this->class_prefix ) ) {
			require_once WFOCU_EBANX_COMPATIBILITY_PLUGIN_DIR . $this->gateway_dir_path . 'class-' . WFOCU_Common::slugify_classname( $class_name ) . '.php';
		}
	}

	/**
	 * @return instance|WFOCU_Ebanx_Compatibility
	 * @throws Exception
	 */
	public static function instance() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Adding functions on hooks
	 */
	public function init_hooks() {

		// Initialize Localization
		add_action( 'init', array( $this, 'wfocu_ebanx_compatibility_localization' ) );

		//Adding link to global settings page
		add_filter( "plugin_action_links_" . WFOCU_EBANX_COMPATIBILITY_BASENAME, function ( $actions ) {
			return array_merge( array(
				'<a href="' . esc_url( admin_url( 'admin.php?page=upstroke&tab=settings' ) ) . '">' . __( 'Settings', self::$slug ) . '</a>',
			), $actions );
		} );

		//Adding ebanx gateways on global settings on upstroke admin page
		add_filter( 'wfocu_wc_get_supported_gateways', array( $this, 'wfocu_ebanx_gateways_integration' ), 10, 1 );
	}

	/**
	 * Adding text-domain
	 */
	public static function wfocu_ebanx_compatibility_localization() {
		load_plugin_textdomain( self::$slug, false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
	}

	/**
	 * Adding gateways name for choosing on UpStroke global settings page
	 */
	public function wfocu_ebanx_gateways_integration( $gateways ) {
		$gateways['ebanx-credit-card-br'] = 'WFOCU_Ebanx_Gateway_Credit_Card_BR';
		$gateways['ebanx-credit-card-co'] = 'WFOCU_Ebanx_Gateway_Credit_Card_CO';
		$gateways['ebanx-credit-card-ar'] = 'WFOCU_Ebanx_Gateway_Credit_Card_AR';
		$gateways['ebanx-credit-card-mx'] = 'WFOCU_Ebanx_Gateway_Credit_Card_MX';

		return $gateways;
	}

}

if ( wfocu_ebanx_compatibility_dependency() ) {
	new WFOCU_Ebanx_Compatibility();
} else {
	add_action( 'admin_notices', 'wfoucu_ebanx_not_installed_notice' );
}

/**
 * Adding notice for inactive state of Ebanx Payment Gateway for Woocommerce and UpStroke
 */
function wfoucu_ebanx_not_installed_notice() { ?>
    <div class="notice notice-error">
        <p>
			<?php
			echo __( '<strong> Attention: </strong>"Ebanx Payment Gateway for Woocommerce" and/or "UpStroke: WooCommerce One Click Upsells" plugins is/are not installed or activated. UpStroke: WooCommerce One Click Upsell for Ebanx wouldn\'t work.', 'upstroke-woocommerce-one-click-upsell-ebanx' );
			?>
        </p>
    </div>
	<?php
}
