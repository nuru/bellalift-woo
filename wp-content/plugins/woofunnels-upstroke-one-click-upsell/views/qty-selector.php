<?php
/**
 * Author PhpStorm.
 */

$product_key = $data['key'];
$product     = $data['product'];


if ( true === apply_filters( 'wfocu_is_show_quantity_selector', $this->offer_data->settings->qty_selector ) ) {

	$get_max_qty_decimal = $this->offer_data->settings->qty_max;


	/**
	 * Check if product Stock max qty is set,
	 * IF set
	 */
	if ( isset( $product->max_qty ) && $product->max_qty && $product->max_qty > 0 && ( $product->max_qty <= ( $get_max_qty_decimal * $product->quantity ) ) ) {

		$get_max_qty_decimal = absint( $product->max_qty / absint( $product->quantity ) );
	}
	?>
    <div class="wfocu-prod-qty-wrapper">
        <label><?php _e( 'Quantity', 'woocommerce' ); ?> </label>

        <span class="wfocu-select-wrapper">
					<select class="wfocu-select-qty-input" data-key="<?php echo $product_key; ?>">

	<?php for ( $i = 1; $i <= $get_max_qty_decimal; $i ++ ) { ?>
        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
     </select>
				</span>
    </div>
	<?php
} else {
	?>
    <input type="hidden" class="wfocu-select-qty-input" data-key="<?php echo $product_key; ?>" value="1"/>
	<?php
}
