<?php

class WFOCU_Compatibility_With_XLWCTY {

	public function __construct() {
		if ( true === function_exists( 'XLWCTY_Core' ) ) {

			/**
			 * Remove compatibility provided by snippet
			 */
			remove_action( 'xlwcty_woocommerce_order_details_after_order_table', 'wfocu_maybe_show_additional_order', 10, 1 );
			remove_action( 'woocommerce_order_details_after_order_table', 'wfocu_maybe_show_additional_order', 10, 1 );
			/**
			 * Show Additional order details in NextMove's Order page.
			 */
			add_action( 'xlwcty_woocommerce_order_details_after_order_table', array( $this, 'wfocu_maybe_show_additional_order' ), 10, 1 );
			add_action( 'woocommerce_order_details_after_order_table', array( $this, 'wfocu_maybe_show_additional_order' ), 10, 1 );

		}
	}


	public function is_enable() {
		if ( true === function_exists( 'XLWCTY_Core' ) ) {
			return false;
		}

		return true;
	}

	/**
	 * @param WC_Order $order_object Current order opening in thank you page.
	 */
	function wfocu_maybe_show_additional_order( $order_object ) {

		if ( ! class_exists( 'XLWCTY_Core' ) ) {
			return;
		}

		if ( false === XLWCTY_Core()->public->is_xlwcty_page() ) {
			return;
		}

		if ( ! function_exists( 'WFOCU_Core' ) ) {
			return;
		}
		remove_action( 'xlwcty_woocommerce_order_details_after_order_table', array( $this, 'wfocu_maybe_show_additional_order' ), 10, 1 );
		remove_action( 'woocommerce_order_details_after_order_table', array( $this, 'wfocu_maybe_show_additional_order' ), 10, 1 );

		$sustain_id = $order_object->get_id();

		/**
		 * Try to get if any upstroke order is created for this order as parent
		 */
		$results = WFOCU_Core()->track->query_results( array(
			'data'         => array(),
			'where'        => array(
				array(
					'key'      => 'session.order_id',
					'value'    => WFOCU_WC_Compatibility::get_order_id( $order_object ),
					'operator' => '=',
				),
				array(
					'key'      => 'events.action_type_id',
					'value'    => 4,
					'operator' => '=',
				),
			),
			'where_meta'   => array(
				array(
					'type'       => 'meta',
					'meta_key'   => '_new_order',
					'meta_value' => '',
					'operator'   => '!=',
				),
			),
			'session_join' => true,
			'order_by'     => 'events.id DESC',
			'query_type'   => 'get_results',
		) );

		if ( is_wp_error( $results ) || ( is_array( $results ) && empty( $results ) ) ) {

			/**
			 * Fallback when we are unable to fetch it through our session table, case of cancellation of primary order
			 */
			$get_meta = $order_object->get_meta( '_wfocu_sibling_order', false );
			if ( ( is_array( $get_meta ) && ! empty( $get_meta ) ) ) {
				$results = [];
				foreach ( $get_meta as $meta ) {
					$single             = new stdClass();
					$single->meta_value = $meta->get_data()['value']->get_id();
					$results[]          = $single;
				}
			}
		}
		if ( empty( $results ) ) {
			return;
		}
		foreach ( $results as $rows ) {

			XLWCTY_Core()->data->load_order( $rows->meta_value );
			XLWCTY_Components::get_components( '_xlwcty_order_details' )->render_view( '_xlwcty_order_details' );

		}

		XLWCTY_Core()->data->load_order( $sustain_id );

	}


}

WFOCU_Plugin_Compatibilities::register( new WFOCU_Compatibility_With_XLWCTY(), 'xlwcty' );



