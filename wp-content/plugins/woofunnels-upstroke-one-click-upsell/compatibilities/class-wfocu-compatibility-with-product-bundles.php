<?php

class WFOCU_Compatibility_With_Product_Bundles {
	public $accepted_ids = [];

	public function __construct() {
		if ( true === class_exists( 'WC_Bundles' ) ) {


			add_action( 'woocommerce_bundled_add_to_order', array( $this, 'record_bundled_ids_during_upsell' ) );
			add_filter( 'wfocu_add_products_to_the_order', array( $this, 'maybe_add_bundle_to_the_cart' ), 10, 2 );
			add_action( 'wfocu_after_reduce_stock_on_batching', array( $this, 'maybe_reduce_stock_for_bundled_upsells' ), 10, 2 );

			add_filter( 'wfocu_offer_product_types', array( $this, 'allow_bundle_products_in_offer' ) );

		}
	}


	/**
	 * @hooked over `wfocu_add_products_to_the_order`
	 *
	 * @param array $products products in the package
	 * @param WC_Order $order current order
	 *
	 * @return mixed
	 */
	public function maybe_add_bundle_to_the_cart( $products, $order ) {
		$ins = WC_PB_Order::instance();

		foreach ( $products['products'] as $key => $product ) {
			$get_product = wc_get_product( $product['id'] );
			if ( $get_product && $get_product->is_type( 'bundle' ) ) {

				$ins->add_bundle_to_order( $get_product, $order, $product['qty'], $product['args'] );
				unset( $products['products'][ $key ] );
			}
		}

		return $products;

	}

	public function is_enable() {
		if ( false === class_exists( 'WC_Bundles' ) ) {
			return false;
		}

		return true;
	}

	/**
	 * @hooked over `woocommerce_bundled_add_to_order`
	 * lets collect ids that are added during the upsell so that we can further reduce stock on that basis
	 *
	 * @param $bundled_order_item_id
	 */
	public function record_bundled_ids_during_upsell( $bundled_order_item_id ) {
		array_push( $this->accepted_ids, $bundled_order_item_id );
	}


	/**
	 * @hooked over `wfocu_after_reduce_stock_on_batching`
	 * Lets reduce the bundled item stock in case of batching, order item ids are collected by WFOCU_Compatibility_With_Product_Bundles::record_bundled_ids_during_upsell()
	 *
	 * @param $products
	 * @param WC_Order $order
	 */
	public function maybe_reduce_stock_for_bundled_upsells( $products, $order ) {

		if ( empty( $this->accepted_ids ) ) {
			return;
		}
		$get_all_items = $order->get_items();
		foreach ( $this->accepted_ids as $id ) {

			$product_item = ( isset( $get_all_items[ $id ] ) ) ? $get_all_items[ $id ] : null;
			if ( null === $product_item ) {
				continue;
			}

			$product = $product_item->get_product();

			if ( $product->managing_stock() ) {
				$qty       = apply_filters( 'woocommerce_order_item_quantity', $product_item->get_quantity(), $order, $id );
				$item_name = $product->get_formatted_name();
				$new_stock = wc_update_product_stock( $product, $qty, 'decrease' );

				if ( ! is_wp_error( $new_stock ) ) {
					/* translators: 1: item name 2: old stock quantity 3: new stock quantity */
					$order->add_order_note( sprintf( __( '%1$s stock reduced from %2$s to %3$s.', 'woocommerce' ), $item_name, $new_stock + $qty, $new_stock ) );

					// Get the latest product data.
					$product = wc_get_product( $product->get_id() );

					if ( '' !== get_option( 'woocommerce_notify_no_stock_amount' ) && $new_stock <= get_option( 'woocommerce_notify_no_stock_amount' ) ) {
						do_action( 'woocommerce_no_stock', $product );
					} elseif ( '' !== get_option( 'woocommerce_notify_low_stock_amount' ) && $new_stock <= get_option( 'woocommerce_notify_low_stock_amount' ) ) {
						do_action( 'woocommerce_low_stock', $product );
					}

					if ( $new_stock < 0 ) {
						do_action( 'woocommerce_product_on_backorder', array(
							'product'  => $product,
							'order_id' => WFOCU_WC_Compatibility::get_order_id( $order ),
							'quantity' => $qty,
						) );
					}
				}
			}

		}
	}

	/**
	 * @hooked into wfocu_offer_product_types
	 * Allow subscription product in the offers
	 *
	 * @param array $product_types
	 *
	 * @return mixed
	 */
	public function allow_bundle_products_in_offer( $product_types ) {

		array_push( $product_types, 'bundle' );

		return $product_types;
	}


}

WFOCU_Plugin_Compatibilities::register( new WFOCU_Compatibility_With_Product_Bundles(), 'product_bundles' );



