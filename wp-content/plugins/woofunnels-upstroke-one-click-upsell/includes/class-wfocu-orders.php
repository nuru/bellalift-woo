<?php

/**
 * Create,show,delete,edit and manages the process related to offers in the plugin.
 * Class WFOCU_Offers
 */
class WFOCU_Orders {

	private static $ins = null;
	public $initial_order_status = 'pending';
	public $gatways_do_not_support_payment_complete = array( 'bacs', 'cheque', 'cod' );
	public $order_table_rendered = false;
	public $is_shortcode_output = false;

	public function __construct() {

		/**
		 * Register custom order status
		 */
		add_filter( 'woocommerce_register_shop_order_post_statuses', array( $this, 'register_order_status' ), 99, 1 );

		/**
		 * Adding custom order status to WooCommerce native ones
		 */
		add_filter( 'wc_order_statuses', array( $this, 'order_statuses' ), 99, 1 );

		/**
		 * Cron Handler for `wfocu_schedule_normalize_order_statuses`
		 * @see WFOCU_Public::register_cron_event()
		 */
		add_action( 'wfocu_schedule_normalize_order_statuses', array( $this, 'maybe_handle_cron_normalize_stasuses' ), 99 );

		/**
		 * Normalizing the order statuses on funnel end
		 */
		add_action( 'wfocu_funnel_ended_event', array( $this, 'maybe_normalize_order_statuses' ), 10, 3 );

		/**
		 * Handles order status change during primary order
		 */
		add_action( 'wfocu_front_init_funnel_hooks', array( $this, 'register_order_status_to_primary_order' ), 10 );

		/**
		 * Show related orders in thankyou page
		 */
		add_action( 'woocommerce_before_template_part', array( $this, 'maybe_show_related_orders' ), 10, 4 );
		add_action( 'woocommerce_after_template_part', array( $this, 'maybe_show_related_orders' ), 10, 4 );

		/**
		 * prevent order notes that might confuse admin about status transitions
		 */
		//add_action( 'wfocu_before_normalize_order_status', array( $this, 'prevent_order_notes' ), 10 );

		/**
		 * Allow order status notes after the normalisation done
		 */
		//add_action( 'wfocu_after_normalize_order_status', array( $this, 'allow_order_notes' ), 10 );

		/**
		 * Add custom order note after transition of order status
		 */
		//add_action( 'wfocu_after_normalize_order_status', array( $this, 'add_custom_order_note_after_normalising' ), 12, 2 );

		/**
		 * Process cleanup of comments
		 */
		add_action( 'wfocu_scheduled_comments_clean', array( $this, 'clean_comments' ) );

		/**
		 * Record payment complete action attempt by payment gateway.
		 */
		add_action( 'woocommerce_payment_complete_order_status_wfocu-pri-order', array( $this, 'maybe_record_payment_complete_during_funnel_run' ), 999 );

		/**
		 * Run necessary hooks in order to make payment complete for the order
		 */
		add_action( 'wfocu_after_normalize_order_status', array( $this, 'maybe_run_payment_complete_actions' ), 10 );

		add_shortcode( 'wfocu_order_details_section', array( $this, 'maybe_show_order_details' ) );

		add_action( 'wfocu_before_normalize_order_status', array( $this, 'maybe_detach_increase_stock' ) );

		add_filter( 'woocommerce_order_data_store_cpt_get_orders_query', array( $this, 'handle_custom_query_var' ), 10, 2 );

		add_action( 'woocommerce_thankyou', array( $this, 'mark_order_as_thankyou_visited' ) );

		add_action( 'wfocu_schedule_thankyou_action', array( $this, 'maybe_execute_thankyou_hook' ) );

		add_filter( 'woocommerce_valid_order_statuses_for_cancel', array( $this, 'mark_primary_order_status_as_applicable_for_cancel' ) );
	}

	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self;
		}

		return self::$ins;
	}

	/**
	 * Handle a custom 'customvar' query var to get orders with the 'customvar' meta.
	 *
	 * @param array $query - Args for WP_Query.
	 * @param array $query_vars - Query vars from WC_Order_Query.
	 *
	 * @return array modified $query
	 */
	public function handle_custom_query_var( $query, $query_vars ) {

		if ( isset( $query_vars['_wfocu_schedule_status'] ) && true === $query_vars['_wfocu_schedule_status'] ) {
			$query['meta_query'][] = array(
				'key'     => '_wfocu_schedule_status',
				'compare' => 'EXISTS',

			);
		}

		if ( isset( $query_vars['_wfocu_status_schedule_for_cb'] ) && true === $query_vars['_wfocu_status_schedule_for_cb'] ) {
			$query['meta_query'][] = array(
				'key'     => '_wfocu_status_schedule_for_cb',
				'compare' => 'EXISTS',

			);
		}

		if ( isset( $query_vars['_wfocu_pending_mails'] ) && true === $query_vars['_wfocu_pending_mails'] ) {
			$query['meta_query'][] = array(
				'key'     => '_wfocu_pending_mails',
				'compare' => 'EXISTS',

			);
		}

		if ( isset( $query_vars['_wfocu_pending_thankyou'] ) && true === $query_vars['_wfocu_pending_thankyou'] ) {
			$query['meta_query'][] = array(
				'key'     => '_wfocu_thankyou_visited',
				'compare' => 'NOT EXISTS',

			);
		}

		if ( isset( $query_vars['_wfocu_version'] ) ) {
			$query['meta_query'][] = array(
				'key'     => '_wfocu_version',
				'value'   => $query_vars['_wfocu_version'],
				'compare' => '>=',

			);
		}

		return $query;
	}

	/**
	 * @hooked into `woocommerce_register_shop_order_post_statuses`
	 *
	 * @param $status
	 *
	 * @return mixed
	 */
	public function register_order_status( $status ) {
		$get_all_global_options = get_option( 'wfocu_global_settings' );
		if ( is_array( $get_all_global_options ) && isset( $get_all_global_options['primary_order_status_title'] ) && ! empty( $get_all_global_options['primary_order_status_title'] ) ) {
			$get_title = $get_all_global_options['primary_order_status_title'];
		} else {
			$get_title = _x( 'Primary Order Accepted', 'Order status', 'woofunnels-upstroke-one-click-upsell' );
		}
		$status['wc-wfocu-pri-order'] = array(
			'label'                     => $get_title,
			'public'                    => false,
			'exclude_from_search'       => true,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( $get_title . ' <span class="count">(%s)</span>', $get_title . ' <span class="count">(%s)</span>' ),
		);

		return $status;
	}


	/**
	 * @hooked into `wc_order_statuses`
	 *
	 * @param $status
	 *
	 * @return mixed
	 */
	public function order_statuses( $status ) {

		$status['wc-wfocu-pri-order'] = WFOCU_Core()->data->get_option( 'primary_order_status_title' );

		return $status;
	}

	/**
	 * @hooked into `wfocu_front_init_funnel_hooks`
	 * Register filter to modify payment_complete_order_status to our custom status
	 * WC_Order @param $order
	 */
	public function register_order_status_to_primary_order( $order ) {

		if ( false === is_a( $order, 'WC_Order' ) ) {
			WFOCU_Core()->log->log( 'Order #' . WFOCU_WC_Compatibility::get_order_id( $order ) . ': No valid order' . __FUNCTION__ );

			return;
		}
		$order_behavior = WFOCU_Core()->funnels->get_funnel_option( 'order_behavior' );
		$is_batching_on = ( 'batching' === $order_behavior ) ? true : false;

		if ( false === $is_batching_on ) {
			WFOCU_Core()->log->log( 'Order #' . WFOCU_WC_Compatibility::get_order_id( $order ) . ': Avoid changing the order' . __FUNCTION__ );

			return;
		}

		add_filter( 'woocommerce_payment_complete_order_status', array( $this, 'maybe_set_completed_order_status' ), 999, 3 );

	}

	/**
	 * @hooked into `woocommerce_payment_complete_order_status`
	 *
	 * @param string $status
	 * @param string $id
	 * @param WC_Order $order
	 *
	 * @return string
	 */
	public function maybe_set_completed_order_status( $status, $id, $order ) {
		if ( false === is_a( $order, 'WC_Order' ) ) {
			WFOCU_Core()->log->log( 'Order #' . $id . ': No valid order' . __FUNCTION__ );

			return $status;
		}

		/**
		 * Removing our filter, as Sometimes there may be chances that as we modify order status, 3rd party plugin try to manage order statsues on the change of the subscription status
		 * @see WC_Subscriptions_Renewal_Order::maybe_record_subscription_payment()
		 * This function tries to run `woocommerce_payment_complete_order_status` again and this could be the reason for the wrong order status save in our cookies.
		 */
		remove_filter( 'woocommerce_payment_complete_order_status', array( $this, 'maybe_set_completed_order_status' ), 999 );

		do_action( 'wfocu_front_primary_order_status_change', 'wc-wfocu-pri-order', $status, $order );

		return 'wfocu-pri-order';

	}

	/**
	 * @hooked into `wfocu_funnel_ended_event`
	 * @see WFOCU_Orders::normalize_order_statuses()
	 *
	 * @param $funnel_id
	 * @param $funnel_key
	 * @param int $order_id
	 */
	public function maybe_normalize_order_statuses( $funnel_id, $order_id, $email ) {

		$old_status     = WFOCU_Core()->data->get( 'porder_status' );
		$initial_status = WFOCU_Core()->data->get( 'sorder_status' );

		$this->normalize_order_statuses( $order_id, $old_status, $initial_status );
	}

	/**
	 * @hooked into cron action `wfocu_schedule_normalize_order_statuses`
	 * Checks for the order status and if the order status is `wc-wfocu-pri-order`
	 * Switch the status to the valid one
	 * @see WFOCU_Orders::maybe_normalize_order_statuses()
	 *
	 * @param int $order_id order id in the process
	 * @param string $order_status order status to move order to
	 * @param string $initial_status order status as bridge status to move order to initial_status and then successful one
	 */
	public function normalize_order_statuses( $order_id, $order_status, $initial_status = 'pending' ) {

		$order = wc_get_order( $order_id );

		if ( false === is_a( $order, 'WC_Order' ) ) {
			WFOCU_Core()->log->log( 'Order #' . $order_id . ': No valid order' . __FUNCTION__ );

			return;
		}

		$get_status = WFOCU_WC_Compatibility::get_order_status( $order );

		if ( 'wc-wfocu-pri-order' !== $get_status ) {
			return;
		}

		$order_status = apply_filters( 'wfocu_front_order_status_after_funnel', $order_status, $order );
		WFOCU_Core()->log->log( 'Order # ' . $order_id . ': Normalizing Order stasuses with hook:' . current_action() );
		/**
		 * Allowing hooks to register before we apply the initial order status i.e. pending by default
		 */
		do_action( 'wfocu_before_normalize_order_status', $order_status );

		/**
		 * This is the status we will apply to the order as we need other plugin (and woocommerce) to work smoothly
		 * 3rd party plugins as well as woocommerce uses status transition hooks (eg: order_status_changed_from_pending_to_processing) to accomplish some of the tasks.
		 * By first moving to pending from our custom status we ensure the above mentioned functionality
		 */
		$midway_status = apply_filters( 'wfocu_mail_initial_status', $initial_status );

		$order->update_status( $midway_status );

		/**
		 * Moving forward and allowing plugin to unhook/hook according to the situation
		 * In this process we move to the actual successful status we already contained
		 */
		do_action( 'wfocu_before_normalize_order_status_to_successful', $midway_status, $order_status );

		$order->update_status( $order_status );

		do_action( 'wfocu_after_normalize_order_status', $order, $order_status );

		$order->delete_meta_data( '_wfocu_schedule_status' );
		$order->save_meta_data();
		WFOCU_Core()->log->log( 'Order # ' . $order_id . ': ' . $get_status . ' -> ' . $midway_status . '->' . $order_status );
	}

	public function maybe_handle_cron_normalize_stasuses() {

		WFOCU_Common::$start_time = time();

		$get_orders = wc_get_orders( array(
			'_wfocu_schedule_status' => true,
			'status'                 => 'wfocu-pri-order',
			'limit'                  => 100,
		) );
		$i          = 0;
		$get_ttl    = WFOCU_Core()->data->get_option( 'ttl_funnel' );
		if ( ! empty( $get_orders ) ) {

			do {
				if ( ( WFOCU_Common::time_exceeded() || WFOCU_Common::memory_exceeded() ) ) {
					// Batch limits reached.
					break;
				}
				$order             = $get_orders[ $i ];
				$get_schedule_meta = $order->get_meta( '_wfocu_schedule_status', true );

				list( $status, $source_status, $time ) = array_values( $get_schedule_meta );

				/**
				 * check if the funnel end time reached or not
				 */
				if ( $time + ( MINUTE_IN_SECONDS * $get_ttl ) <= time() ) {
					$this->normalize_order_statuses( $order->get_id(), $status, $source_status );
				}

				unset( $get_orders[ $i ] );
				$i ++;
			} while ( ! ( WFOCU_Common::time_exceeded() || WFOCU_Common::memory_exceeded() ) && ! empty( $get_orders ) );
		}
	}

	public function maybe_execute_thankyou_hook() {

		WFOCU_Common::$start_time = time();

		$get_orders = wc_get_orders( array(
			'_wfocu_pending_thankyou' => true,
			'_wfocu_version'          => '1.13.0',
			'limit'                   => 100,
		) );
		$i          = 0;
		$get_ttl    = WFOCU_Core()->data->get_option( 'ttl_funnel' );
		if ( ! empty( $get_orders ) ) {

			do {
				if ( ( WFOCU_Common::time_exceeded() || WFOCU_Common::memory_exceeded() ) ) {
					// Batch limits reached.
					break;
				}
				$order = $get_orders[ $i ];
				$time  = $order->get_date_created()->getTimestamp();
				/**
				 * check if the funnel end time reached or not
				 */
				if ( $time + ( MINUTE_IN_SECONDS * $get_ttl ) <= time() ) {

					remove_action( 'woocommerce_thankyou', array( WFOCU_Core()->public, 'maybe_log_thankyou_visited' ), 999 );
					do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() );
					do_action( 'woocommerce_thankyou', $order->get_id() );
				}

				unset( $get_orders[ $i ] );
				$i ++;
			} while ( ! ( WFOCU_Common::time_exceeded() || WFOCU_Common::memory_exceeded() ) && ! empty( $get_orders ) );
		}
	}

	/**
	 * @hooked over `wfocu_before_normalize_order_status`
	 * Attaching filter to escape order note/comments insertion.
	 */
	public function prevent_order_notes() {
		add_filter( 'woocommerce_new_order_note_data', array( $this, 'maybe_empty_comment_data' ), 10 );
	}

	/**
	 * @hooked over `woocommerce_new_order_note_data`
	 * @see WFOCU_Orders::prevent_order_notes()
	 * returns array blank to make sure no related order note inserted during the transition
	 */
	public function maybe_empty_comment_data() {

		return array();
	}


	/**
	 * @hooked over `wfocu_after_normalize_order_status`
	 * Removing filter to prevent order notes, after the normalisation completed
	 */
	public function allow_order_notes() {
		remove_filter( 'woocommerce_new_order_note_data', array( $this, 'maybe_empty_comment_data' ), 10 );
	}


	/**
	 * @hooked over `wfocu_after_normalize_order_status`
	 * As we have skipped adding meta during the transition, we need to add an order note custom to let the admin know the transition from custom order status to the completed one.
	 *
	 * @param $order
	 * @param $order_status
	 */
	public function add_custom_order_note_after_normalising( $order, $order_status ) {
		$transition_note = sprintf( __( 'Order status changed from %1$s to %2$s.', 'woocommerce' ), wc_get_order_status_name( 'wc-wfocu-pri-order' ), wc_get_order_status_name( $order_status ) );

		$order->add_order_note( trim( $transition_note ), 0, false );

	}


	/**
	 * @hooked over `wfocu_scheduled_comments_clean`
	 * @see WFOCU_Orders::maybe_schedule_cleaning_comments()
	 * Scheduled action to remove comments that get added during the transition and then we clean them to ensure comments table will not get populated unnecesserly
	 */
	public function clean_comments() {
		global $wpdb;

		$wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->comments WHERE `comment_post_ID` = %d", 0 ) );

	}

	/**
	 * Adding product to the order
	 *
	 * @param $package
	 * @param WC_Order $order
	 */
	public function add_products_to_order( $package, $order ) {
		$ids     = array();
		$package = apply_filters( 'wfocu_add_products_to_the_order', $package, $order );
		foreach ( $package['products'] as $product ) {

			$ids[] = $item_id = $order->add_product( wc_get_product( $product['id'] ), $product['qty'], $product['args'] );
			wc_add_order_item_meta( $item_id, '_upstroke_purchase', 'yes' );
		}

		$order->calculate_totals();

		return apply_filters( 'wfocu_added_products_to_the_order', $ids, $order );

	}

	/**
	 *
	 * @param $get_package
	 * @param WC_Order $order
	 */
	public function set_total( $get_package, $order ) {

		$order->set_total( $get_package['total'] );
	}

	/**
	 *
	 * @param $get_package
	 * @param WC_Order $order
	 */
	public function maybe_handle_shipping( $get_package, $order ) {
		if ( is_array( $get_package['shipping'] ) ) {

			$get_shipping_items = $order->get_items( 'shipping' );

			if ( $get_shipping_items && is_array( $get_shipping_items ) ) {

				/**
				 * var WC_Order_Item_Shipping $item_shipping
				 */
				$item_shipping     = current( $get_shipping_items );
				$item_shipping_key = key( $get_shipping_items );
				if ( false === strpos( $get_package['shipping']['value'], 'free_shipping' ) || 'fixed' == $get_package['shipping']['value'] ) {

					if ( isset( $get_package['shipping']['override'] ) && 'true' === $get_package['shipping']['override'] ) {
						$item = new WC_Order_Item_Shipping();
						$item->set_props( array(
							'method_title' => $get_package['shipping']['label'],
							'method_id'    => $get_package['shipping']['value'],
							'total'        => $get_package['shipping']['diff']['cost'],
						) );
						$item->save();
						$order->add_item( $item );

						$offer_shipping_items = array();
						if ( isset( $get_package['products'] ) && is_array( $get_package['products'] ) ) {
							foreach ( $get_package['products'] as $prodct ) {
								$offer_shipping_items[] = get_the_title( $prodct['id'] );
							}
						}

						$item_id     = $item->get_id();
						$offer_itmes = implode( ',', $offer_shipping_items );

						wc_add_order_item_meta( $item_id, 'Items', $offer_itmes );

					} else {
						$item_shipping->set_total( $item_shipping->get_total() + $get_package['shipping']['diff']['cost'] );
					}
				} else {

					/**
					 * If we are in this case that means user opted for the free shipping option provided by us.
					 * We have to apply free shipping method to the current order and remove the previous one.
					 */
					$order->remove_item( $item_shipping_key );
					$item = new WC_Order_Item_Shipping();
					$item->set_props( array(
						'method_title' => $get_package['shipping']['label'],
						'method_id'    => $get_package['shipping']['value'],
						'total'        => 0,
					) );
					$item->save();
					$order->add_item( $item );

				}

				/**
				 * @todo handle for local-pickup case for out of shop base address users.
				 * In case of local pickup shipping, taxes were calculated based on shop base address but not users shipping on front end.
				 * But as soon as we run WC_Order::calculate_totals(), WC does not consider local pickup and apply taxes based on customer.
				 * This ends up messing prices in the order.
				 */
				$order->calculate_totals();

				$order->save();
			} else {

				/**
				 * When there is no shipping exists for the parent order we have to add a new method
				 */
				/**
				 * @todo handle this case as we have to allow customer to chosen between the offered shipping methods
				 */

			}
		}

	}

	/**
	 *
	 * @param array $package
	 * @param WC_Order $parent
	 * @param WC_Order $new
	 */
	public function maybe_handle_shipping_new_order( $package, $parent, $new ) {
		if ( is_array( $package['shipping'] ) ) {

			$get_shipping_items = $parent->get_items( 'shipping' );

			if ( $get_shipping_items && is_array( $get_shipping_items ) ) {

				if ( 'free_shipping' !== $package['shipping']['value'] || 'fixed' == $package['shipping']['value'] ) {

					$item = new WC_Order_Item_Shipping();
					$item->set_props( array(
						'method_title' => $package['shipping']['label'],
						'method_id'    => $package['shipping']['value'],
						'total'        => $package['shipping']['diff']['cost'],
					) );
					$item->save();
					$new->add_item( $item );
				} else {
					/**
					 * If we are in this case that means user opted for the free shipping option provided by us.
					 * We have to apply free shipping method to the current order and remove the previous one.
					 */

					$item = new WC_Order_Item_Shipping();
					$item->set_props( array(
						'method_title' => $package['shipping']['label'],
						'method_id'    => $package['shipping']['value'],
						'total'        => 0,
					) );
					$item->save();
					$new->add_item( $item );

				}

				/**
				 * @todo handle for local-pickup case for  out of shop base address users.
				 * In case of local pickup shipping, taxes were calculated based on shop base address but not users shipping on front end.
				 * But as soon as we run WC_Order::calculate_totals(), WC does not consider local pickup and apply taxes based on customer.
				 * This ends up messing prices in the order.
				 */
				$new->calculate_totals();

				$new->save();
			} else {

				/**
				 * When there is no shipping exists for the parent order we have to add a new method
				 */
				/**
				 * @todo handle this case as we have to allow customer to chosen between the offered shipping methods
				 */

			}
		}
	}

	/**
	 * @param array $args
	 * @param WC_Order $order_to_inherit
	 *
	 * @return WC_Order
	 */
	public function create_new_order( $args = array(), $order_to_inherit ) {
		$args['basic'] = array();
		//$args['basic']['status'] = WFOCU_Core()->data->get_option( 'create_new_order_status_success' );
		$args['basic']['status'] = 'wc-pending';

		$args = wp_parse_args( $args, $this->get_default_order_args() );

		return $this->create_order( $args, $order_to_inherit );

	}

	public function get_default_order_args() {
		return array(
			'basic'    => array(
				'status' => 'pending',
			),
			'products' => array(),

		);
	}

	/**
	 * Create a new order in woocommerce
	 *
	 * @param $args
	 * @param WC_Order $order_to_inherit
	 *
	 * @return bool|WC_Order|WP_Error
	 */
	private function create_order( $args, $order_to_inherit ) {
		/**
		 * @todo make the functions compatible to  WC < 3.0
		 */
		if ( ! empty( $order_to_inherit ) ) {
			$parent_order_billing = $order_to_inherit->get_address( 'billing' );

			if ( ! empty( $parent_order_billing['email'] ) ) {
				$customer_id = $order_to_inherit->get_customer_id();

				$order = wc_create_order( array(
					'customer_id' => $customer_id,
					'status'      => $args['basic']['status'],
				) );
				$args  = apply_filters( 'wfocu_add_products_to_the_order', $args, $order );
				foreach ( $args['products'] as $product ) {
					$item_id = $order->add_product( wc_get_product( $product['id'] ), $product['qty'], $product['args'] );
					wc_add_order_item_meta( $item_id, '_upstroke_purchase', 'yes' );
				}

				$order->set_address( $order_to_inherit->get_address( 'billing' ), 'billing' );
				$order->set_address( $order_to_inherit->get_address( 'shipping' ), 'shipping' );

				// set shipping

				$order->set_payment_method( $order_to_inherit->get_payment_method() );
				$order->set_payment_method_title( $order_to_inherit->get_payment_method_title() );

				// reports won't track orders if these values are not set
				if ( ! wc_tax_enabled() ) {
					$order->set_shipping_tax( 0 );
					$order->set_cart_tax( 0 );
				}

				/**
				 * Copying the meta provided by the user from primary order to the new one
				 */
				$meta_keys_to_copy = WFOCU_Core()->data->get_option( 'order_copy_meta_keys' );

				$explode_meta_keys = apply_filters( 'wfocu_order_copy_meta_keys', explode( '|', $meta_keys_to_copy ) );
				if ( is_array( $explode_meta_keys ) ) {
					foreach ( $explode_meta_keys as $key ) {
						$get_meta = get_post_meta( WFOCU_WC_Compatibility::get_order_id( $order_to_inherit ), $key, true );
						update_post_meta( WFOCU_WC_Compatibility::get_order_id( $order ), $key, $get_meta );
					}
				}

				$order->calculate_totals();
			}

			return $order;
		}

		return false;
	}

	/**
	 * @param array $args
	 * @param $order_to_inherit
	 *
	 * @return bool|WC_Order|WP_Error
	 */
	public function create_failed_order( $args = array(), $order_to_inherit ) {

		$args['basic']           = array();
		$args['basic']['status'] = WFOCU_Core()->data->get_option( 'create_new_order_status_fail' );
		$args                    = wp_parse_args( $args, $this->get_default_order_args() );

		return $this->create_order( $args, $order_to_inherit );

	}

	/**
	 * Controller of WC_Order::payment_complete() & reduction of stock for non completed gateways
	 * We need to restrict payment_complete function to run for the not supported gateways
	 *
	 * @param $transaction_id
	 * @param WC_Order $order
	 */
	public function payment_complete( $transaction_id, $order ) {

		if ( false == in_array( $order->get_payment_method(), $this->gatways_do_not_support_payment_complete ) ) {
			$order->payment_complete( $transaction_id );
		} elseif ( 'cod' === $order->get_payment_method() ) {
			$order->set_status( 'processing' );
			wc_reduce_stock_levels( $order );

		} elseif ( 'bacs' === $order->get_payment_method() || 'cheque' === $order->get_payment_method() ) {
			$order->set_status( 'on-hold' );
			wc_reduce_stock_levels( $order );

		}
	}


	public function process_cancellation() {
		$get_parent_order   = WFOCU_Core()->data->get( 'porder', false, '_orders' );
		$gateway_controller = WC_Payment_Gateways::instance();
		$all_gateways       = $gateway_controller->payment_gateways();
		$payment_method     = $get_parent_order->get_payment_method();
		$gateway            = isset( $all_gateways[ $payment_method ] ) ? $all_gateways[ $payment_method ] : false;

		/**
		 * some gateways which do not supports refunds, we just need to mark then cancelled.
		 */
		if ( ! $gateway->supports( 'refunds' ) ) {
			$get_parent_order->update_status( 'wc-cancelled' );
		} else {
			wc_create_refund( array(
				'order_id'       => WFOCU_WC_Compatibility::get_order_id( $get_parent_order ),
				'amount'         => $get_parent_order->get_total(),
				'reason'         => __( 'Refund Processed', 'woofunnels-upstroke-one-click-upsell' ),
				'refund_payment' => true,
				'restock_items'  => true,
			) );
		}

		do_action( 'wfocu_front_primary_order_cancelled', $get_parent_order );

		return;
	}


	/**
	 * Controller of stock reduction after an order
	 *
	 * @param WC_Order $order
	 */
	public function reduce_stock( $order, $items = array() ) {

		$package       = WFOCU_Core()->data->get( '_upsell_package' );
		$stock_reduced = $order->get_data_store()->get_stock_reduced( $order->get_id() );
		if ( true === $stock_reduced && isset( $package['products'] ) && is_array( $package['products'] ) && count( $package['products'] ) > 0 && 'yes' === get_option( 'woocommerce_manage_stock' ) ) {

			$index = 0;

			foreach ( $package['products'] as $product_data ) {

				$product = $product_data['data'];

				$get_item_from_id = ( isset( $items[ $index ] ) ? $order->get_item( $items[ $index ] ) : 0 );

				if ( $product->managing_stock() ) {
					$qty       = apply_filters( 'woocommerce_order_item_quantity', $product_data['qty'], $order, $get_item_from_id );
					$item_name = $product->get_formatted_name();
					$new_stock = wc_update_product_stock( $product, $qty, 'decrease' );

					if ( ! is_wp_error( $new_stock ) ) {
						/* translators: 1: item name 2: old stock quantity 3: new stock quantity */
						$order->add_order_note( sprintf( __( '%1$s stock reduced from %2$s to %3$s.', 'woocommerce' ), $item_name, $new_stock + $qty, $new_stock ) );

						// Get the latest product data.
						$product = wc_get_product( $product->get_id() );

						if ( '' !== get_option( 'woocommerce_notify_no_stock_amount' ) && $new_stock <= get_option( 'woocommerce_notify_no_stock_amount' ) ) {
							do_action( 'woocommerce_no_stock', $product );
						} elseif ( '' !== get_option( 'woocommerce_notify_low_stock_amount' ) && $new_stock <= get_option( 'woocommerce_notify_low_stock_amount' ) ) {
							do_action( 'woocommerce_low_stock', $product );
						}

						if ( $new_stock < 0 ) {
							do_action( 'woocommerce_product_on_backorder', array(
								'product'  => $product,
								'order_id' => WFOCU_WC_Compatibility::get_order_id( $order ),
								'quantity' => $qty,
							) );
						}
					}
				}
				$index ++;
			}
			do_action( 'wfocu_after_reduce_stock_on_batching', $package['products'], $order, $items );
		}

	}


	public function maybe_show_related_orders( $template_name, $template_path, $located, $args ) {

		/**
		 * There are two areas that we need to hook in our code
		 * It could be above the customer details OR After the order details
		 * So when it entered with one of the condition we need to remove the other one so that it would not get printed twice.
		 */
		if ( ( ( 'woocommerce_before_template_part' === current_action() && 'order/order-details-customer.php' == $template_name ) || ( 'woocommerce_after_template_part' === current_action() && 'order/order-details.php' == $template_name ) ) ) {

			if ( isset( $args['order_id'] ) ) {
				$order = wc_get_order( $args['order_id'] );
			} else {
				$order = $args['order'];
			}

			if ( ! $order instanceof WC_Order ) {
				return;
			}

			$results = WFOCU_Core()->track->query_results( array(
				'data'         => array(),
				'where'        => array(
					array(
						'key'      => 'session.order_id',
						'value'    => WFOCU_WC_Compatibility::get_order_id( $order ),
						'operator' => '=',
					),
					array(
						'key'      => 'events.action_type_id',
						'value'    => 4,
						'operator' => '=',
					),
				),
				'where_meta'   => array(
					array(
						'type'       => 'meta',
						'meta_key'   => '_new_order',
						'meta_value' => '',
						'operator'   => '!=',
					),
				),
				'session_join' => true,
				'order_by'     => 'events.id DESC',
				'query_type'   => 'get_results',
			) );

			if ( is_wp_error( $results ) || ( is_array( $results ) && empty( $results ) ) ) {

				/**
				 * Fallback when we are unable to fetch it through our session table, case of cancellation of primary order
				 */
				$get_meta = $order->get_meta( '_wfocu_sibling_order', false );
				if ( ( is_array( $get_meta ) && ! empty( $get_meta ) ) ) {
					$results = [];
					foreach ( $get_meta as $meta ) {
						$single             = new stdClass();
						$single->meta_value = $meta->get_data()['value']->get_id();
						$results[]          = $single;
					}
				}
				if ( is_array( $results ) && empty( $results ) ) {
					return;
				}
			}

			?>

            <style>
                .wfocu-additional-order-wrapper .woocommerce-order-details__title {
                    display: none !important;
                }

                .wfocu-additional-order-wrapper .woocommerce-customer-details {
                    display: none !important;
                }
            </style>

			<?php

			foreach ( $results as $rows ) {
				$order = wc_get_order( $rows->meta_value );
				echo '<div class="wfocu-additional-order-wrapper">';
				wc_get_template( 'order/order-details.php', array(
					'order_id' => $order->get_id(),
				) );
				echo '</div>';

				?>

				<?php
			}
			?>
            </tbody>
            </table>
			<?php
			if ( 'woocommerce_before_template_part' == current_action() ) {
				remove_action( 'woocommerce_after_template_part', array( $this, 'maybe_show_related_orders' ), 10, 4 );

			} else {
				remove_action( 'woocommerce_before_template_part', array( $this, 'maybe_show_related_orders' ), 10, 4 );

			}
		}
	}

	/**
	 * @hooked over `woocommerce_payment_complete_order_status_wfocu-pri-order`
	 * Record attempt for payment complete in the meta
	 *
	 * @param $order_id
	 */
	public function maybe_record_payment_complete_during_funnel_run( $order_id ) {
		$get_order = wc_get_order( $order_id );

		WFOCU_Core()->log->log( 'Order # ' . $order_id . ': Recorded Payment Complete' );

		if ( $get_order instanceof WC_Order ) {

			$get_order->update_meta_data( '_wfocu_payment_complete_on_hold', 'yes' );
			$get_order->save_meta_data();
		}
	}

	/**
	 * @hooked over `wfocu_after_normalize_order_status`
	 * Perform action to let other plugin know that payment is completed for the given order
	 *
	 * @param $order
	 */
	public function maybe_run_payment_complete_actions( $order ) {
		$get_order = $order;

		if ( $get_order instanceof WC_Order ) {

			$have_pending_payment_complete_action = $get_order->get_meta( '_wfocu_payment_complete_on_hold', true );
			if ( 'yes' === $have_pending_payment_complete_action ) {
				do_action( 'woocommerce_payment_complete', WFOCU_WC_Compatibility::get_order_id( $get_order ) );
			}
		}

	}

	public function maybe_show_order_details( $atts ) {
		$result = false;

		$atts = shortcode_atts( array(
			'order_id' => '',
		), $atts );

		if ( ! empty( $_GET['key'] ) ) {
			$order_key = '';

			if ( ! empty( $_GET['key'] ) ) {
				$order_key = $_GET['key'];
			}

			$order = wc_get_order_id_by_order_key( $order_key );

			$order_id = WFOCU_WC_Compatibility::get_order_id( $order );

			if ( empty( $atts['order_id'] ) && ! empty( $order_id ) ) {
				$atts['order_id'] = $order_id;
			}
		}

		if ( ! empty( $atts['order_id'] ) ) {
			$order = wc_get_order( intval( $atts['order_id'] ) );

			if ( ! empty( $order ) ) {
				$this->is_shortcode_output = true;
				ob_start();

				echo '<div class="woocommerce">';

				echo '<link rel="stylesheet" href="' . str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/css/woocommerce.css' . '" type="text/css" media="all" />';

				wc_get_template( 'checkout/thankyou.php', array( 'order' => $order ) );

				echo '</div>';

				$result                    = ob_get_clean();
				$this->is_shortcode_output = false;

			}
		}

		return $result;
	}


	/**
	 * While we are transitioning order stasuses in case batching then there would be no case we want to increase stock
	 * Unhooking woocommerce functions wc_maybe_increase_stock_levels() so escape any chances to increase stock in case of pending status.
	 */
	public function maybe_detach_increase_stock() {

		remove_action( 'woocommerce_order_status_pending', 'wc_maybe_increase_stock_levels' );
	}

	public function mark_order_as_thankyou_visited( $order_id ) {

		$get_order = wc_get_order( $order_id );

		$get_order->update_meta_data( '_wfocu_thankyou_visited', 'yes' );
		$get_order->save_meta_data();

	}

	public function mark_primary_order_status_as_applicable_for_cancel( $order_stasuses ) {
		array_push( $order_stasuses, 'wfocu-pri-order' );

		return $order_stasuses;
	}

	public function maybe_set_funnel_running_status( $order ) {
		/**
		 * Move to our custom status
		 */
		$old_status = $order->get_status();
		WFOCU_Core()->log->log( 'Moving Order status to "Primary-order"' );

		/**
		 * Tell the plugin that order status modified so that we can initiate schedule hooks
		 */
		do_action( 'wfocu_front_primary_order_status_change', 'wc-wfocu-pri-order', $old_status, $order );
		remove_filter( 'woocommerce_payment_complete_order_status', array( WFOCU_Core()->orders, 'maybe_set_completed_order_status' ), 999, 3 );
		$order->set_status( 'wfocu-pri-order' );
		$order->save();
	}
}

if ( class_exists( 'WFOCU_Core' ) ) {
	WFOCU_Core::register( 'orders', 'WFOCU_Orders' );
}
