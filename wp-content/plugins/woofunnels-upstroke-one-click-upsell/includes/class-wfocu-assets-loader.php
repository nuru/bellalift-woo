<?php
/**
 * Author PhpStorm.
 */

class WFOCU_Assets_Loader {

	private static $ins = null;
	public $environment = 'customizer-preview';
	private $scripts = array();

	private $styles = array();

	public function __construct() {

	}

	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self();
		}

		return self::$ins;
	}

	public function localize_script( $handle, $object, $data ) {
		if ( ! isset( $this->scripts[ $handle ] ) ) {
			return;
		}

		if ( ! isset( $this->scripts[ $handle ]['data'] ) ) {
			$this->scripts[ $handle ]['data'] = array();
		}

		$this->scripts[ $handle ]['data'][ $object ] = $data;
	}

	public function setup_assets( $environment ) {

		$get_scripts       = $this->get_scripts();
		$this->environment = $environment;
		foreach ( $get_scripts as $handle => $scripts ) {

			if ( in_array( $environment, $scripts['supports'] ) ) {

				$this->add_scripts( $handle, $scripts['path'], $scripts['version'], $scripts['in_footer'] );
			}
		}

		$get_styles = $this->get_styles();

		foreach ( $get_styles as $handle => $styles ) {

			if ( in_array( $environment, $styles['supports'] ) ) {
				$this->add_styles( $handle, $styles['path'], $styles['version'], $styles['in_footer'] );
			}
		}

	}

	public function get_scripts() {

		$live_or_dev = 'live';

		if ( defined( 'WFOCU_IS_DEV' ) && true === WFOCU_IS_DEV ) {
			$live_or_dev = 'dev';
			$suffix      = '';
		} else {
			$suffix = '.min';
		}

		return apply_filters( 'wfocu_assets_scripts', array(
			'jquery'     => array(
				'path'      => includes_url() . 'js/jquery/jquery.js',
				'version'   => null,
				'in_footer' => false,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
					'offer-page',
				),
			),
			'underscore' => array(
				'path'      => includes_url() . 'js/underscore.min.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
					'offer-page',
				),
			),
			'wp-util'    => array(
				'path'      => includes_url() . 'js/wp-util.min.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
					'offer-page',
				),
			),

			'accounting' => array(
				'path'      => WC()->plugin_url() . '/assets/js/accounting/accounting.min.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'offer',
					'offer-page',
					'customizer-preview',
				),
			),

			'flickity' => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/flickity/flickity.pkgd.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
				),
			),

			'wfocu-product'               => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/' . $live_or_dev . '/js/wfocu-product' . $suffix . '.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
				),
			),
			'wfocu-jquery-countdown'      => array(
				'path' => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/' . $live_or_dev . '/js/jquery.countdown.min.js',

				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
				),
			),
			'wfocu-polyfill'              => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'admin/assets/js/wfocu-polyfill.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'offer',
					'offer-page',
				),
			),
			'wfocu-swal'                  => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'admin/assets/js/wfocu-sweetalert.min.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'offer',
					'offer-page',
				),
			),
			'global'                      => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/' . $live_or_dev . '/js/wfocu-public' . $suffix . '.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'offer',
					'offer-page',
					'customizer-preview',
				),
			),
			'customize-base'              => array(
				'path'      => includes_url() . 'js/customize-base.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
				),
			),
			'customize-preview'           => array(
				'path'      => includes_url() . 'js/customize-preview.min.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
				),
			),
			'wfocu_customizer_live'       => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/' . $live_or_dev . '/js/customizer' . $suffix . '.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
				),
			),
			'customize-selective-refresh' => array(
				'path'      => includes_url() . 'js/customize-selective-refresh.min.js',
				'version'   => null,
				'in_footer' => true,
				'supports'  => array(
					'customizer',
					'customizer-preview',
				),
			),
		) );
	}

	public function add_scripts( $handle, $src, $version = null, $is_footer = false ) {
		if ( isset( $this->scripts[ $handle ] ) ) {
			return;
		}
		$this->scripts[ $handle ] = array(
			'src'     => $src,
			'version' => ( is_null( $version ) ) ? WFOCU_VERSION_DEV : $version,
			'foot'    => $is_footer,
		);
	}

	public function get_styles() {

		return apply_filters( 'wfocu_assets_styles', array(
			'grid-css'               => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/css/grid.min.css',
				'version'   => null,
				'in_footer' => false,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
				),
			),
			'global-css'             => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/css/style.min.css',
				'version'   => null,
				'in_footer' => false,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
				),
			),
			'offer-confirmation-css' => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/css/style-offer-confirmation.min.css',
				'version'   => null,
				'in_footer' => false,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
					'offer-page',
				),
			),
			'flickity'               => array(
				'path'      => plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'assets/flickity/flickity.css',
				'version'   => null,
				'in_footer' => false,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
				),
			),
			'customize-preview'      => array(
				'path'      => includes_url() . 'css/customize-preview.min.css',
				'version'   => null,
				'in_footer' => false,
				'supports'  => array(
					'customizer',
					'customizer-preview',
				),
			),
			'dashicons'              => array(
				'path'      => includes_url() . 'css/dashicons.min.css',
				'version'   => null,
				'in_footer' => false,
				'supports'  => array(
					'customizer',
					'customizer-preview',
					'offer',
					'offer-page',
				),
			),
		) );
	}

	public function add_styles( $handle, $src, $version = null, $is_footer = false ) {
		if ( isset( $this->styles[ $handle ] ) ) {
			return;
		}

		$this->styles[ $handle ] = array(
			'src'     => $src,
			'version' => ( is_null( $version ) ) ? WFOCU_VERSION_DEV : $version,
			'foot'    => $is_footer,
		);

	}

	public function print_scripts( $is_head = false ) {
		$script = '';

		if ( true === $is_head ) {
			foreach ( $this->scripts as $handle => $data ) {
				if ( false === $data['foot'] ) {
					if ( isset( $data['data'] ) ) {
						foreach ( $data['data'] as $var => $data_loc ) {
							$script .= 'var ' . $var . ' = ' . wp_json_encode( $data_loc ) . ';';
						}
						printf( ' <script type="text/javascript" >%s</script>', $script );
					}

					/**
					 * Ensuring if script has been enqueued already
					 */
					//if ( 'customizer-preview' == $this->environment  ) {
					printf( ' <script type="text/javascript" id="%s" src="%s"></script>', 'script_' . $handle, $data['src'] . '?v=' . $data['version'] );

					//}
					unset( $this->scripts[ $handle ] );
				}
			}
		} else {

			foreach ( $this->scripts as $handle => $data ) {
				if ( isset( $data['data'] ) ) {
					foreach ( $data['data'] as $var => $data_loc ) {
						$script .= 'var ' . $var . ' = ' . wp_json_encode( $data_loc ) . ';';
					}
					printf( ' <script type="text/javascript" >%s</script>', $script );
				}

				//if ( 'customizer-preview' == $this->environment  ) {

				printf( ' <script type="text/javascript" id="%s" src="%s"></script>', 'script_' . $handle, $data['src'] . '?v=' . $data['version'] );

				//}
				unset( $this->scripts[ $handle ] );
			}
		}
	}

	public function print_styles( $is_head = false ) {
		if ( true === $is_head ) {
			foreach ( $this->styles as $handle => $data ) {
				if ( false === $data['foot'] ) {
					printf( ' <link rel="stylesheet" type="text/css" media="all" id="%s" href="%s"/>', 'style_' . $handle, $data['src'] . '?v=' . $data['version'] );
					unset( $this->styles[ $handle ] );
				}
			}
		} else {
			foreach ( $this->styles as $handle => $data ) {
				printf( ' <link rel="stylesheet" type="text/css" media="all" id="%s" href="%s"/>', 'style_' . $handle, $data['src'] . '?v=' . $data['version'] );
				unset( $this->styles[ $handle ] );
			}
		}
	}


}


if ( class_exists( 'WFOCU_Core' ) ) {
	WFOCU_Core::register( 'assets', 'WFOCU_Assets_Loader' );
}
