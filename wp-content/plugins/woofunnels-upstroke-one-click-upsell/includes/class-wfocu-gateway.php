<?php


/**
 * Abstract Class for all the Gateway Support Class
 * Class WFOCU_Gateway
 */
abstract class WFOCU_Gateway extends WFOCU_SV_API_Base {


	public $amount = 0;
	public $token = null;
	public $refund_supported = false;
	protected $key = '';

	public function __construct() {

	}


	/**
	 * @return WC_Payment_Gateway
	 */
	public function get_wc_gateway() {
		global $woocommerce;
		$gateways = $woocommerce->payment_gateways->payment_gateways();

		return $gateways[ $this->key ];
	}

	public function get_amount() {
		return $this->amount;
	}

	public function set_amount( $amount ) {
		$this->amount = $amount;
	}

	public function get_key() {
		return $this->key;
	}

	/**
	 * This function checks for the need to do the tokenization.
	 * We have to fetch the funnel to decide whether to tokenize the user or not.
	 * @return int|false funnel ID on success false otherwise
	 *
	 */
	public function should_tokenize() {

		return WFOCU_Core()->data->is_funnel_exists();
	}


	/**
	 * Try and get the payment token saved by the gateway
	 *
	 * @param WC_Order $order
	 *
	 * @return true on success false otherwise
	 */
	public function has_token( $order ) {
		return false;

	}


	/**
	 * Try and get the payment token saved by the gateway
	 *
	 * @param WC_Order $order
	 *
	 * @return true on success false otherwise
	 */
	public function get_token( $order ) {
		return false;

	}

	/**
	 * Charge the upsell and capture payments
	 *
	 * @param WC_Order $order
	 *
	 * @return true on success false otherwise
	 */
	public function process_charge( $order ) {
		return false;

	}

	public function handle_result( $result, $message = '' ) {
		if ( $result ) {
			WFOCU_Core()->data->set( '_transaction_status', 'successful' );

			WFOCU_Core()->data->set( '_transaction_message', __( 'Your order is updated.', 'woofunnels-upstroke-one-click-upsell' ) );

		} else {
			WFOCU_Core()->data->set( '_transaction_status', 'failed' );

			WFOCU_Core()->data->set( '_transaction_message', ( ! empty( $message ) ) ? $message : __( 'Unable to process at the moment.', 'woofunnels-upstroke-one-click-upsell' ) );

		}

		return $result;
	}

	/**
	 * @param WC_Order $order
	 *
	 * @return bool
	 */
	public function is_enabled( $order = false ) {
		$get_chosen_gateways = WFOCU_Core()->data->get_option( 'gateways' );
		if ( is_array( $get_chosen_gateways ) && in_array( $this->key, $get_chosen_gateways ) ) {

			return apply_filters( 'wfocu_front_payment_gateway_integration_enabled', true, $order );
		}

		return false;
	}


	public function get_order_number( $order ) {

		$get_offer_id = WFOCU_Core()->data->get( 'current_offer' );

		if ( ! empty( $get_offer_id ) ) {
			return WFOCU_WC_Compatibility::get_order_id( $order ) . '_' . $get_offer_id;
		} else {
			return WFOCU_WC_Compatibility::get_order_id( $order );
		}

	}

	/**
	 * Tell the system to run without a token or not
	 * @return bool
	 */
	public function is_run_without_token() {
		return false;
	}

	/**
	 * @param WC_Order $order
	 *
	 * @return bool
	 */
	public function is_refund_supported( $order = false ) {

		if ( $this->refund_supported ) {

			return apply_filters( 'wfocu_payment_gateway_refund_supported', true, $order );
		}

		return false;
	}

	public function process_refund_offer( $order ) {
		return false;
	}


}
