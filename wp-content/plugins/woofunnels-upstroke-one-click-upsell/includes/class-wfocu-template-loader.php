<?php

/**
 * Class contains the basic functions responsible for front end views.
 * Class WFOCU_View
 */

class WFOCU_Template_loader {

	private static $ins = null;
	public $current_template = null;
	public $customizer_key_prefix = '';
	public $offer_id = null;
	public $product_data = null;
	public $offer_data = null;
	public $invalidation_reason = null;
	public $multiple_p = false;
	protected $customize_manager_ins = null;
	protected $templates = array(
		'single'   => array(),
		'multiple' => array(),
	);

	public function __construct() {

		add_action( 'template_redirect', function () {
			add_filter( 'template_include', array( $this, 'maybe_load' ), 99 );
		}, 999 );

		add_action( 'wfocu_header_print_in_head', array( $this, 'typography_custom_css' ) );

		/** Template common */
		add_action( 'wfocu_before_template_load', array( $this, 'add_common_scripts' ) );
		add_action( 'wfocu_header_print_in_head', array( $this, 'add_fonts' ) );
		add_filter( 'wfocu_offer_product_data', array( $this, 'maybe_add_variations' ), 1, 5 );
		add_action( 'init', array( $this, 'maybe_setup_offer' ), 15 );
		add_action( 'init', array( $this, 'setup_offer_for_wfocukirki' ), 20 );
		add_action( 'init', array( $this, 'customizer_product_check' ), 25 );

		/** Late priority in case themes also using wfocukirki */
		add_filter( 'wfocukirki/config', array( $this, 'wfocu_wfocukirki_configuration' ), 9999 );
		add_action( 'init', array( $this, 'wfocu_wfocukirki_fields' ), 30 );
		add_action( 'wfocu_front_template_after_validation_success', array( $this, 'set_data_object' ) );
		if ( $this->is_customizer_preview() ) {

			/** Set host url in allowed redirect url for customizer in case wp defined site url is different then home url */
			add_filter( 'allowed_redirect_hosts', function ( $wpp, $lp_host ) {
				$wpp[] = $lp_host;

				return array_unique( $wpp );
			}, 10, 2 );

			$this->set_offer_id( $_REQUEST['offer_id'] );
			/** Kirki */
			require WFOCU_PLUGIN_DIR . '/admin/includes/wfocukirki/wfocukirki.php';

			/** wfocukirki custom controls */
			require WFOCU_PLUGIN_DIR . '/includes/class-wfocu-wfocukirki.php';
		}

		//      add_filter( 'wfocukirki/postmessage/script', array( $this, 'wfocu_wfocukirki_scripts' ) );
		$this->add_default_template();
		add_action( 'wfocu_footer_after_print_scripts', array( $this, 'maybe_print_notices_in_hidden' ) );
		add_action( 'wfocu_footer_after_print_scripts', array( $this, 'maybe_log_rendering_complete' ), 999 );
		add_action( 'wfocu_front_template_after_validation_success', array( $this, 'empty_shortcodes' ) );

		/**
		 * Modify template if necessary
		 */
		add_action( 'wfocu_offer_updated', array( $this, 'maybe_autoswitch_templates' ), 10, 3 );

	}

	public function is_customizer_preview() {
		if ( isset( $_REQUEST['wfocu_customize'] ) && 'loaded' == $_REQUEST['wfocu_customize'] ) {
			return true;
		}

		return false;
	}

	public function add_default_template() {
		$template = array(
			'sp-classic' => array(
				'path'      => WFOCU_TEMPLATE_DIR . '/sp-classic/template.php',
				'name'      => __( 'Product Upsell', 'woofunnels-upstroke-one-click-upsell' ),
				'thumbnail' => WFOCU_PLUGIN_URL . '/templates/sp-classic/assets/img/thumbnail.jpg',
				'large_img' => '//storage.googleapis.com/upstroke/sp-classic-full.jpg',
				'type'      => 'view',
			),
			'sp-vsl'     => array(
				'path'      => WFOCU_TEMPLATE_DIR . '/sp-vsl/template.php',
				'name'      => __( 'VSL Upsell', 'woofunnels-upstroke-one-click-upsell' ),
				'thumbnail' => WFOCU_PLUGIN_URL . '/templates/sp-vsl/assets/img/thumbnail.jpg',
				'large_img' => '//storage.googleapis.com/upstroke/sp-vsl-full.jpg',
				'type'      => 'view',
			),
		);
		foreach ( $template as $temp_key => $temp_val ) {
			$this->register_template( $temp_key, $temp_val );
		}

	}

	public function register_template( $slug, $data, $type = 'single' ) {
		if ( '' !== $slug && ! empty( $data ) ) {
			if ( file_exists( $data['path'] ) ) {

				$this->templates[ $type ][ $slug ] = $data;
			}
		}
	}

	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self;
		}

		return self::$ins;
	}

	/**
	 * @hooked over `template_include`
	 * This method checks for the current running funnels and controller to setup data & offer validation
	 * It also loads and echo/prints current template if the offer demands to.
	 *
	 * @param $template current template in WordPress ecosystem
	 *
	 * @return mixed
	 */
	public function maybe_load( $template ) {
		if ( WFOCU_Core()->public->if_is_offer() ) {

			/**
			 * Run validation
			 */

			$validation_result = WFOCU_Core()->offers->validate_product_offers( $this->product_data );

			$validation_result = apply_filters( 'wfocu_offer_validation_result', $validation_result, $this->product_data );

			if ( false === $validation_result ) {
				$get_current_offer = WFOCU_Core()->data->get_current_offer();
				$get_order         = WFOCU_Core()->data->get_current_order();

				$get_type_of_offer       = WFOCU_Core()->data->get( '_current_offer_type' );
				$get_type_index_of_offer = WFOCU_Core()->data->get( '_current_offer_type_index' );

				do_action( 'wfocu_offer_skipped_event', $get_current_offer, WFOCU_WC_Compatibility::get_order_id( $get_order ), WFOCU_Core()->data->get_funnel_id(), $get_type_of_offer, $get_type_index_of_offer, WFOCU_Core()->data->get( 'useremail' ), $this->invalidation_reason );

				$get_offer = WFOCU_Core()->offers->get_the_next_offer();
				$redirect  = WFOCU_Core()->public->get_the_upsell_url( $get_offer );

				WFOCU_Core()->log->log( 'Offer Validation failed, Moving to next offer/order-received, moving to ' . $get_offer );

				WFOCU_Core()->data->set( 'current_offer', $get_offer );
				WFOCU_Core()->data->save();
				wp_redirect( $redirect );
				die();
			}

			/**
			 * If there is a custom page assign to the current loading offer, then discard here and fire a hook
			 */
			if ( WFOCU_Core()->offers->is_custom_page ) {

				do_action( 'wfocu_front_before_custom_offer_page' );
				add_action( 'wp_footer', array( $this, 'maybe_print_notices_in_hidden' ) );

				return $template;
			}

			/**
			 * Loads a assigned template
			 */
			do_action( 'wfocu_front_template_after_validation_success' );

			$this->current_template->set_data( $this->product_data );

			$this->current_template->get_view();

			die();
		}

		return $template;

	}

	public function get_default_template( $offer_data ) {

		if ( ! is_object( $offer_data ) ) {
			return $this->get_default_single_template();
		}
		if ( count( get_object_vars( $offer_data->products ) ) > 1 ) {
			return $this->get_default_multiple_template();
		} else {
			return $this->get_default_single_template();
		}
	}

	public function get_default_single_template() {
		return 'sp-classic';
	}

	public function get_default_multiple_template() {
		return 'mp-grid';
	}

	public function load_footer() {
		$this->get_template_part( 'footer-end' );
	}

	public function get_template_part( $slug, $args = '' ) {
		if ( ! empty( $args ) && ( is_array( $args ) || is_object( $args ) ) ) {
			extract( array( 'data' => $args ) ); // @codingStandardsIgnoreLine
		}

		if ( '/' !== substr( $slug, 0, 1 ) ) {
			$slug = '/' . $slug;
		}
		if ( file_exists( plugin_dir_path( WFOCU_PLUGIN_FILE ) . 'views' . $slug . '.php' ) ) {
			$located = plugin_dir_path( WFOCU_PLUGIN_FILE ) . 'views' . $slug . '.php';
		} else {
			$located = apply_filters( 'wfocu_get_template_part_path', plugin_dir_path( WFOCU_PLUGIN_FILE ) . 'views' . $slug . '.php', $slug, $args );

		}
		if ( ! file_exists( $located ) ) {
			/* translators: %s template */
			wc_doing_it_wrong( __FUNCTION__, sprintf( __( '%s does not exist.', 'woo-funnels-one-click-upsell' ), '<code>' . $located . '</code>' ), '2.1' );

			return;
		}
		include $located;
	}

	public function load_header() {
		$this->get_template_part( 'header' );
	}

	public function locate_template( $slug, $type = 1 ) {
		$template_type = 'single';
		if ( 2 === $type ) {
			$template_type = 'multiple';
		}

		if ( array_key_exists( $slug, $this->templates[ $template_type ] ) ) {
			return $this->templates[ $template_type ][ $slug ]['path'];
		}

		return false;
	}

	/**
	 * @param string $is_single
	 *
	 * @return array
	 */
	public function get_templates( $is_single = 'single' ) {
		return $this->templates;
	}

	public function get_buy_button_url( $product_hash, $product_data ) {
		ob_start();
		?>
        <a class="wfocu_upsell" href="javascript:void(0);" data-key="<?php echo $product_hash; ?>"> Add to my order</a>
		<?php
		return ob_get_clean();
	}

	public function get_skip_button_url( $product_hash, $product_data ) {
		ob_start();

		?>
        <a class="wfocu_skip_offer" href="javascript:void(0);" data-key="<?php echo $product_hash; ?>"> SKip the offer</a>
		<?php
		return ob_get_clean();
	}

	public function body_classes() {
		$body_classes = apply_filters( 'wfocu_view_body_classes', array() );

		return implode( ' ', $body_classes );
	}


	public function typography_custom_css() {
		$style_custom_css = WFOCU_Common::get_option( 'wfocu_custom_css_css_code' );

		printf( '<style>%s</style>', $style_custom_css );
	}

	public function add_common_scripts() {
		if ( false === is_customize_preview() ) {
			WFOCU_Core()->assets->setup_assets( 'offer' );
		} else {
			WFOCU_Core()->assets->setup_assets( 'customizer-preview' );
		}
	}

	public function add_fonts() {
		?>
        <link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
		<?php
	}

	public function maybe_add_variations( $product, $output, $offer_data, $is_front, $hash ) {

		if ( ( 'variable' === $product->data->get_type() || 'variable-subscription' === $product->data->get_type() ) && true == $is_front ) {

			$attributes = $product->data->get_variation_attributes();

			$attribute_keys             = array_keys( $attributes );
			$prices                     = array();
			$available_variation_stock  = array();
			$prepare_dimension_hash     = array();
			$weight_html                = array();
			$dimensions_html            = array();
			$available_variations       = array();
			$images                     = array();
			$all_common_attribute_slugs = array();
			$variation_objects          = array();
			if ( isset( $offer_data->variations ) && isset( $offer_data->variations->{$hash} ) && is_array( $offer_data->variations->{$hash} ) ) {
				add_filter( 'woocommerce_available_variation', array( $this, 'add_variation_object_in_custom_variation_key' ), 10, 3 );

				foreach ( $offer_data->variations->{$hash} as $variation => $variation_data ) {
					$variation = $product->data->get_available_variation( $variation );

					if ( false == $variation ) {
						continue;
					}

					// Enable or disable the add to cart button
					if ( ! $variation['is_purchasable'] || ! $variation['is_in_stock'] || ! $variation['variation_is_visible'] ) {

						continue;
					}

					$current_stock = null;
					if ( isset( $variation['is_in_stock'] ) && ( isset( $variation['max_qty'] ) && '' !== $variation['max_qty'] ) ) {
						$current_stock = $variation['max_qty'];
						$offer_qty     = (int) $product->quantity;

						if ( $current_stock < $offer_qty ) {

							continue;
						}
					}

					$attributes_json = array_combine( array_map( function ( $k ) {

						return '@' . WFOCU_Common::clean_ascii_characters( $k );
					}, array_keys( $variation['attributes'] ) ), array_map( function ( $k ) {

						return WFOCU_Common::handle_single_quote_variation( $k );
					}, $variation['attributes'] ) );

					$keys = array_keys( $variation['attributes'] );
					array_walk( $keys, function ( $k ) use ( &$all_common_attribute_slugs ) {

						$all_common_attribute_slugs[ WFOCU_Common::clean_ascii_characters( $k ) ] = $k;

					} );

					$attributes_json['id'] = $variation['variation_id'];

					$prepare_dimension_hash[ $variation['variation_id'] ]    = md5( json_encode( array(
						$variation['dimensions_html'],
						$variation['weight_html'],
					) ) );
					$available_variations[ $variation['variation_id'] ]      = $attributes_json;
					$available_variation_stock[ $variation['variation_id'] ] = $current_stock;
					$weight_html[ $variation['variation_id'] ]               = $variation['weight_html'];
					$images[ $variation['variation_id'] ]                    = $variation['image_id'];
					$dimensions_html[ $variation['variation_id'] ]           = $variation['dimensions_html'];
					$variation_settings                                      = new stdClass();
					$variation_settings->quantity                            = $product->quantity;
					$variation_settings->discount_type                       = $product->discount_type;
					$variation_settings->discount_amount                     = $variation_data->discount_amount;

					$prices[ $variation['variation_id'] ]            = apply_filters( 'wfocu_variation_prices', array(
						'price_incl_tax'             => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, true, $offer_data ),
						'price_incl_tax_raw'         => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, true, $offer_data ),
						'price_excl_tax'             => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, false, $offer_data ),
						'price_excl_tax_raw'         => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, false, $offer_data ),
						'regular_price_incl_tax'     => wc_get_price_including_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,
						'regular_price_incl_tax_raw' => wc_get_price_including_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,
						'regular_price_excl_tax'     => wc_get_price_excluding_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,
						'regular_price_excl_tax_raw' => wc_get_price_excluding_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,

					), $variation['_wfocu_variation_object'], $product );
					$variation_objects[ $variation['variation_id'] ] = $variation['_wfocu_variation_object'];

				}
			} else {
				add_filter( 'woocommerce_available_variation', array( $this, 'add_variation_object_in_custom_variation_key' ), 10, 3 );

				$all_variations = $product->data->get_available_variations();

				foreach ( $all_variations as $variation ) {

					if ( false == $variation ) {
						continue;
					}

					// Enable or disable the add to cart button
					if ( ! $variation['is_purchasable'] || ! $variation['is_in_stock'] || ! $variation['variation_is_visible'] ) {

						continue;
					}

					$current_stock = null;
					if ( isset( $variation['is_in_stock'] ) && ( isset( $variation['max_qty'] ) && '' !== $variation['max_qty'] ) ) {
						$current_stock = $variation['max_qty'];
						$offer_qty     = (int) $product->quantity;

						if ( $current_stock < $offer_qty ) {

							continue;
						}
					}

					$attributes_json = array_combine( array_map( function ( $k ) {

						return '@' . WFOCU_Common::clean_ascii_characters( $k );
					}, array_keys( $variation['attributes'] ) ), array_map( function ( $k ) {

						return WFOCU_Common::handle_single_quote_variation( $k );
					}, $variation['attributes'] ) );

					$keys = array_keys( $variation['attributes'] );
					array_walk( $keys, function ( $k ) use ( &$all_common_attribute_slugs ) {

						$all_common_attribute_slugs[ WFOCU_Common::clean_ascii_characters( $k ) ] = $k;

					} );

					$attributes_json['id'] = $variation['variation_id'];

					$prepare_dimension_hash[ $variation['variation_id'] ]    = md5( json_encode( array(
						$variation['dimensions_html'],
						$variation['weight_html'],
					) ) );
					$available_variations[ $variation['variation_id'] ]      = $attributes_json;
					$available_variation_stock[ $variation['variation_id'] ] = $current_stock;
					$weight_html[ $variation['variation_id'] ]               = $variation['weight_html'];
					$images[ $variation['variation_id'] ]                    = $variation['image_id'];
					$dimensions_html[ $variation['variation_id'] ]           = $variation['dimensions_html'];
					$variation_settings                                      = new stdClass();
					$variation_settings->quantity                            = $product->quantity;
					$variation_settings->discount_type                       = $product->discount_type;
					$variation_settings->discount_amount                     = $product->discount_amount;

					$prices[ $variation['variation_id'] ]            = apply_filters( 'wfocu_variation_prices', array(
						'price_incl_tax'             => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, true ),
						'price_incl_tax_raw'         => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, true ),
						'price_excl_tax'             => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, false ),
						'price_excl_tax_raw'         => WFOCU_Core()->offers->get_product_price( $variation['_wfocu_variation_object'], $variation_settings, false ),
						'regular_price_incl_tax'     => wc_get_price_including_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,
						'regular_price_incl_tax_raw' => wc_get_price_including_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,
						'regular_price_excl_tax'     => wc_get_price_excluding_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,
						'regular_price_excl_tax_raw' => wc_get_price_excluding_tax( $variation['_wfocu_variation_object'], array( 'price' => $variation['_wfocu_variation_object']->get_regular_price() ) ) * $variation_settings->quantity,

					), $variation['_wfocu_variation_object'] );
					$variation_objects[ $variation['variation_id'] ] = $variation['_wfocu_variation_object'];

				}
			}
			WFOCU_Core()->data->set( 'attribute_variation_stock_' . $hash, $all_common_attribute_slugs, 'variations' );
			WFOCU_Core()->data->save( 'variations' );
			$product->variations_data = array(
				'available_variations'      => $available_variations,
				'attributes'                => $attributes,
				'attribute_keys'            => $attribute_keys,
				'available_variation_stock' => $available_variation_stock,
				'prices'                    => $prices,
				'default'                   => ( isset( $offer_data->fields->{$hash}->default_variation ) && ! empty( $offer_data->fields->{$hash}->default_variation ) && true == array_key_exists( $offer_data->fields->{$hash}->default_variation, $available_variations ) ) ? $offer_data->fields->{$hash}->default_variation : key( $available_variations ),
				'shipping_hash'             => $prepare_dimension_hash,
				'weight_htmls'              => $weight_html,
				'dimension_htmls'           => $dimensions_html,
				'images'                    => $images,
				'variation_objects'         => $variation_objects,
			);

		}

		return $product;

	}

	/**
	 * @param WP_Customize_Manager $customize_manager
	 */
	public function maybe_add_customize_preview_init( $customize_manager ) {
		$this->customize_manager_ins = $customize_manager;
	}

	public function load_customizer_styles() {
		if ( ( 'loaded' == filter_input( INPUT_GET, 'wfocu_customize' ) ) && $this->customize_manager_ins instanceof WP_Customize_Manager ) {
			$this->customize_manager_ins->customize_preview_loading_style();
			$this->customize_manager_ins->remove_frameless_preview_messenger_channel();
		}
	}

	public function load_customizer_footer_before_scripts() {
		if ( ( 'loaded' == filter_input( INPUT_GET, 'wfocu_customize' ) ) && $this->customize_manager_ins instanceof WP_Customize_Manager ) {
			$this->customize_manager_ins->customize_preview_settings();
			$this->customize_manager_ins->selective_refresh->export_preview_data();
		}
	}

	public function wfocu_wfocukirki_configuration( $path ) {
		if ( $this->is_valid_state_for_data_setup() ) {
			return array(
				'url_path' => WFOCU_PLUGIN_URL . '/admin/includes/wfocukirki/',
			);
		}

		return $path;
	}

	/**
	 * Finds out if its safe to initiate data setup for the current request.
	 * Checks for the environmental conditions and provide results.
	 * @see WFOCU_Template_loader::maybe_setup_offer()
	 * @return bool true on success| false otherwise
	 */
	public function is_valid_state_for_data_setup() {

		if ( WFOCU_AJAX_Controller::is_wfocu_front_ajax() ) {
			return true;
		}

		if ( $this->is_customizer_preview() && ( null !== $this->offer_id && 0 !== $this->offer_id && false !== $this->offer_id ) ) {
			return true;
		}

		if ( true === WFOCU_Core()->public->is_front() && ( null !== $this->offer_id && 0 !== $this->offer_id && false !== $this->offer_id ) ) {
			return true;
		}

		return apply_filters( 'wfocu_valid_state_for_data_setup', false );

	}

	public function wfocu_wfocukirki_fields() {
		$temp_ins = $this->get_template_ins();

		/** if ! customizer */
		if ( ! $this->is_customizer_preview() ) {
			return;
		}

		if ( $temp_ins instanceof WFOCU_Template_Common && is_array( $temp_ins->customizer_data ) && count( $temp_ins->customizer_data ) > 0 ) {

			foreach ( $temp_ins->customizer_data as $panel_single ) {
				/** Panel */
				foreach ( $panel_single as $panel_key => $panel_arr ) {
					/** Section */
					if ( is_array( $panel_arr['sections'] ) && count( $panel_arr['sections'] ) > 0 ) {
						foreach ( $panel_arr['sections'] as $section_key => $section_arr ) {
							$section_key_final = $panel_key . '_' . $section_key;
							/** Fields */
							if ( is_array( $section_arr['fields'] ) && count( $section_arr['fields'] ) > 0 ) {
								foreach ( $section_arr['fields'] as $field_key => $field_data ) {
									$field_key_final = $section_key_final . '_' . $field_key;

									$field_data = array_merge( $field_data, array(
										'settings' => $field_key_final,
										'section'  => $section_key_final,
									) );

									/** unset wfocu_partial key if present as not required for wfocukirki */
									if ( isset( $field_data['wfocu_partial'] ) ) {
										unset( $field_data['wfocu_partial'] );
									}

									WFOCUKirki::add_field( WFOCU_SLUG, $field_data );

									/** Setting fields: type and element class for live preview */
									if ( isset( $field_data['wfocu_transport'] ) && is_array( $field_data['wfocu_transport'] ) ) {
										$field_key_final = $this->customizer_key_prefix . '[' . $field_key_final . ']';

										$temp_ins->fields[ $field_key_final ] = $field_data['wfocu_transport'];
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public function get_template_ins() {
		return $this->current_template;
	}

	public function setup_offer_for_wfocukirki() {

		if ( true === $this->is_customizer_preview() ) {
			add_action( 'customize_preview_init', array( $this, 'maybe_add_customize_preview_init' ) );

			/** wp customizer scripts and styles */
			add_action( 'wfocu_header_print_in_head', array( $this, 'load_customizer_styles' ) );
			add_action( 'wfocu_footer_before_print_scripts', array( $this, 'load_customizer_footer_before_scripts' ) );

			//$this->customizer_key_prefix = WFOCU_SLUG . '_c_' . $_REQUEST['offer_id'];
		}
		add_action( 'wfocu_header_print_in_head', array( $this, 'print_internal_css' ) );

		$this->customizer_key_prefix = WFOCU_SLUG . '_c_' . $this->offer_id;

		/** Set customizer key prefix in common */
		WFOCU_Common::$customizer_key_prefix = $this->customizer_key_prefix;

		/** wfocukirki */
		if ( class_exists( 'WFOCUKirki' ) ) {
			WFOCUKirki::add_config( WFOCU_SLUG, array(
				'option_type' => 'option',
				'option_name' => $this->customizer_key_prefix,
			) );
		}
	}

	public function customizer_product_check() {

		if ( $this->is_customizer_preview() && $this->is_valid_state_for_data_setup() ) {


			$variation_field = true;
			$offer_data      = $this->product_data;

			$temp_product = get_object_vars( $offer_data->products );
			if ( is_array( $temp_product ) && count( $temp_product ) > 0 ) {
				/** Checking for variation single product */
				if ( is_array( $temp_product ) && count( $temp_product ) > 1 ) {
					$variation_field = false;
				} else {
					/** Only 1 product */
					foreach ( $offer_data->products as $hash_key => $product ) {
						if ( isset( $product->id ) && $product->id > 0 ) {
							$product_obj = wc_get_product( $product->id );

							$this->current_template->products_data[ $hash_key ] = array(
								'id'  => $product->id,
								'obj' => $product_obj,
							);

							/** Checking if product variation and single product */
							$product_type = $product_obj->get_type();
							if ( ! in_array( $product_type, WFOCU_Common::get_variable_league_product_types() ) ) {
								$variation_field = false;
							}
						}
					}
				}
			} else {
				wp_die( __( 'Your offer must have at least one product to show preview.', 'woofunnels-upstroke-one-click-upsell' ) );
			}

			$this->current_template->variation_field = $variation_field;
		}
	}

	/**
	 * @hooked over `init`:15
	 * This method try and sets up the data for all the existing pages.
	 * customizer-admin | customizer-preview | front-end-funnel | front-end-ajax-request-during-funnel
	 * For the given environments we have our offer ID setup at this point. So its safe and necessary to set the data.
	 * This method does:
	 * 1. Fetches and sets up the offer data based on the offer id provided
	 * 2. Finds & loads the appropriate template.
	 * 3. loads offer data to the template instance
	 * 4. Build offer data for the current offer
	 */
	public function maybe_setup_offer() {

		if ( $this->is_valid_state_for_data_setup() ) {
			$id = $this->get_offer_id();

			$get_customizer_instance = WFOCU_Core()->customizer;
			$this->offer_data        = WFOCU_Core()->offers->get_offer( $id );

			$get_customizer_instance->load_template( $this->offer_data );

			$this->product_data = WFOCU_Core()->offers->build_offer_product( $this->offer_data, $id, true );

			$this->current_template = $get_customizer_instance->get_template_instance();
			if ( false === is_null( $this->current_template ) ) {
				$get_customizer_instance->get_template_instance()->set_offer_data( $this->offer_data );

				$this->current_template->set_data( $this->product_data );
			}

			$this->set_data_object();
		}

	}

	public function get_offer_id() {
		return $this->offer_id;
	}

	public function set_offer_id( $id ) {
		$this->offer_id = $id;
	}

	/**
	 * Sets up the current offer data related data objects
	 * These objects will be later gets accessed using data class
	 *
	 */
	public function set_data_object() {

		WFOCU_Core()->data->set( '_current_offer_data', $this->product_data );
		WFOCU_Core()->data->set( '_current_offer', $this->offer_data );

		if ( ! $this->is_customizer_preview() ) {
			WFOCU_Core()->data->set( '_current_offer_type', WFOCU_Core()->offers->get_offer_attributes( WFOCU_Core()->data->get( 'current_offer' ), 'type' ) );
			WFOCU_Core()->data->set( '_current_offer_type_index', WFOCU_Core()->offers->get_offer_attributes( WFOCU_Core()->data->get( 'current_offer' ), 'index' ) );
		}
	}

	public function print_internal_css() {
		if ( ! empty( $this->current_template->internal_css ) ) {
			include WFOCU_PLUGIN_DIR . '/views/internal-css.php';
		}
	}


	/**
	 * @hooked over `woocommerce_available_variation`
	 * Adds a variation object in the variation array on the upsell pages
	 *
	 * @param $variation_array
	 * @param $variation_object
	 * @param $variation
	 *
	 * @return mixed
	 */
	public function add_variation_object_in_custom_variation_key( $variation_array, $variation_object, $variation ) {

		$variation_array['_wfocu_variation_object'] = $variation;

		return $variation_array;

	}

	/**
	 * @hooked over `footer_after_print_scripts`
	 * @hooked over `wp_footer` conditionaly
	 * We need to take care of the notices in our template so that it wont be visible to the user as well as it will not get forward by the WC for another page
	 * We are printing notices in the html in non display mode
	 */
	public function maybe_print_notices_in_hidden() {
		?>
        <div class="wfocu-wc-notice-wrap" style="display: none; !important;">
			<?php wc_print_notices(); ?>
        </div>
		<?php
	}

	public function empty_shortcodes() {
		global $shortcode_tags;
		$new_shortcode_tags = $shortcode_tags;
		foreach ( $new_shortcode_tags as $tag => $callback ) {

			if ( false === strpos( $tag, 'wfocu_' ) ) {
				unset( $shortcode_tags[ $tag ] );
			}
		}

	}

	public function add_attributes_to_buy_button() {
		global $buy_button_count;
		if ( null === $buy_button_count ) {
			$buy_button_count = 1;
		} else {
			$buy_button_count ++;
		}
		$attributes     = apply_filters( 'wfocu_front_buy_button_attributes', array(), $buy_button_count );
		$attributes_str = '';
		foreach ( $attributes as $attr => $val ) {
			$attributes_str .= sprintf( ' %1$s=%3$s%2$s%3$s', $attr, $val, '"' );
		}

		echo $attributes_str;
	}

	public function add_attributes_to_confirmation_button() {
		$attributes     = apply_filters( 'wfocu_front_confirmation_button_attributes', array() );
		$attributes_str = '';
		foreach ( $attributes as $attr => $val ) {
			$attributes_str .= sprintf( ' %1$s=%3$s%2$s%3$s', $attr, $val, '"' );
		}

		echo $attributes_str;
	}

	public function maybe_autoswitch_templates( $offer_data, $offer_id, $funnel_id ) {

		/**
		 * Bail out if no products in there
		 */
		if ( count( get_object_vars( $offer_data->products ) ) == 0 ) {
			return;
		}

		$count = count( get_object_vars( $offer_data->products ) );

		/**
		 * return if we do not need any switching
		 */
		if ( false === $this->template_needs_switching( $count, $offer_data->template ) ) {
			return;
		}

		/**
		 * Get default templates
		 */
		if ( count( get_object_vars( $offer_data->products ) ) === 1 ) {
			$offer_data->template = $this->get_default_single_template();

		} else {
			$offer_data->template = $this->get_default_multiple_template();
		}

		/**
		 * Save the modified template
		 */
		WFOCU_Common::update_offer( $offer_id, $offer_data, $funnel_id );

	}

	/**
	 * Checks if current template set matches the product count
	 *
	 * @param $count
	 * @param $current_template
	 *
	 * @return bool
	 */
	public function template_needs_switching( $count, $current_template ) {

		/**
		 * if current template is mp and count is 1
		 */
		if ( 0 === strpos( $current_template, 'mp' ) && 1 === $count ) {
			return true;
		}

		/**
		 * if current template is sp and count is greater than one
		 */
		if ( 0 === strpos( $current_template, 'sp' ) && 1 < $count ) {
			return true;
		}

		return false;

	}

	/**
	 * Offer templates rendered successfully, let the system log it in the file
	 */
	public function maybe_log_rendering_complete() {
		$is_preview = WFOCU_Core()->public->if_is_preview();
		$offer_id   = WFOCU_Core()->template_loader->get_offer_id();
		if ( false === $is_preview ) {
			WFOCU_Core()->log->log( 'Offer: #' . $offer_id . ' Page Rendered successfully' );
		}
	}
}

if ( class_exists( 'WFOCU_Core' ) ) {
	WFOCU_Core::register( 'template_loader', 'WFOCU_Template_loader' );
}
