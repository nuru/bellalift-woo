<?php


class WFOCU_Customizer {

	private static $ins = null;
	public $orders;
	public $offer_id = 0;
	protected $offer_data = 0;
	private $template_path = '';
	private $template_url = '';
	private $template = null;
	private $template_view = null;
	/**
	 * @var WFOCU_Template_Common
	 */
	private $template_ins = null;

	public function __construct() {
		if ( isset( $_REQUEST['offer_id'] ) ) {
			$this->offer_id = $_REQUEST['offer_id'];
		}

		$this->template_path   = WFOCU_PLUGIN_DIR . '/templates';
		$this->template_url    = WFOCU_PLUGIN_URL . '/templates';
		$this->template_assets = WFOCU_PLUGIN_URL . '/assets';
		add_filter( 'wfocu_customizer_fieldset', array( $this, 'add_offer_confirmation_setting' ) );
		add_filter( 'wp_insert_post', array( $this, 'mark_changsets_as_dismissed' ), 10, 2 );
		$this->maybe_load_customizer();
		add_action( 'admin_enqueue_scripts', array( $this, 'dequeue_unnecessary_customizer_scripts' ), 999 );
	}

	public function maybe_load_customizer() {
		if ( isset( $_REQUEST['wfocu_customize'] ) && $_REQUEST['wfocu_customize'] == 'loaded' && isset( $_REQUEST['offer_id'] ) && $_REQUEST['offer_id'] > 0 ) {
			$this->customize_hooks();

		}
	}

	public function customize_hooks() {
		global $wp_version;
		add_filter( 'customize_register', array( $this, 'remove_sections' ), 110 );
		add_action( 'customize_save_after', array( $this, 'maybe_update_customize_save' ) );
		if ( version_compare( $wp_version, '4.4', '>=' ) ) {
			/** Hiding Widgets, Menus & Theme Panel */
			//          add_filter( 'customize_loaded_components', array( $this, 'remove_extra_panels' ), 100 );
		} else {
			//          add_filter( 'customize_register', array( $this, 'remove_panels' ), 100 );
		}
		add_filter( 'customize_changeset_branching', '__return_true' );
		add_action( 'customize_controls_print_styles', function () {
			echo '<style>#customize-theme-controls li#accordion-panel-nav_menus,
#customize-theme-controls li#accordion-panel-widgets,
#customize-theme-controls li#accordion-section-astra-pro,
#customize-controls .customize-info .customize-help-toggle,
.ast-control-tooltip {display: none !important;}</style>';
		} );
		add_filter( 'customize_control_active', array( $this, 'control_filter' ), 10, 2 );

		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue_scripts' ), 9999 );
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'maybe_remove_script_customizer' ), 10000 );

		add_filter( 'customize_register', array( $this, 'add_sections' ), 101 );
		add_action( 'wfocu_before_template_load', array( $this, 'customizer_js' ) );

		add_action( 'customize_update_' . self::get_type(), array( $this, 'save' ), 10, 2 );
		add_filter( 'template_redirect', array( WFOCU_Core()->template_loader, 'empty_shortcodes' ), 90 );
		add_filter( 'template_redirect', array( $this, 'setup_preview' ), 99 );

		add_action( 'customize_save_validation_before', array( $this, 'add_sections1' ), 101 );
		add_action( 'wfocu_header_print_in_head', array( $this, 'offer_confirmation_html' ), 1 );

	}

	public static function get_type() {
		return 'wfocu';
	}

	public static function get_instance() {
		if ( self::$ins == null ) {
			self::$ins = new self();
		}

		return self::$ins;
	}

	/**
	 * Locate Template using offer meta data also setup data
	 *
	 * @param $offer_data
	 *
	 * @return mixed|null
	 */

	public function load_template( $offer_data ) {

		if ( ! empty( $offer_data ) ) {

			if ( count( get_object_vars( $offer_data ) ) > 0 ) {
				$this->offer_data = $offer_data;
				$this->template   = $offer_data->template;

				if ( $this->template != '' ) {
					$template_file = WFOCU_TEMPLATE_DIR . '/sp-classic/template.php';

					$template_type = 1;
					if ( isset( $offer_data->have_multiple_product ) && $offer_data->have_multiple_product > 1 ) {
						$template_type = 2;
					}
					$locate_template = WFOCU_Core()->template_loader->locate_template( $this->template, $template_type );

					if ( false !== $locate_template ) {
						$template_file = $locate_template;
					}

					if ( file_exists( $template_file ) ) {
						$this->template_ins = include $template_file;
						if ( method_exists( $this->template_ins, 'get_slug' ) ) {
							$this->template_ins->set_offer_id( $this->offer_id );
							$this->template_ins->set_offer_data( $this->offer_data );
							$this->template_ins->load_hooks();
							if ( isset( $_REQUEST['customized'] ) ) {
								$change_set = json_decode( $_REQUEST['customized'], true );
								if ( ! is_null( $change_set ) ) {
									$this->template_ins->set_changeset( $change_set );
								}
							}

							return $this->template_ins;
						}
					}
				}
			}
		}

		return null;

	}

	/**
	 * Remove any unwanted default controls.
	 *
	 * @param WP_Customize_Manager $wp_customize
	 *
	 * @return bool
	 */
	public function remove_sections( $wp_customize ) {
		global $wp_customize;

		$wp_customize->remove_panel( 'themes' );
		$wp_customize->remove_control( 'active_theme' );

		/** Mesmerize theme */
		$wp_customize->remove_section( 'mesmerize-pro' );

		return true;
	}

	/**
	 * Depreciated - Storefront calling settings direct
	 * Removes the core 'Widgets' or 'Menus' panel from the Customizer.
	 *
	 * @param array $components Core Customizer components list.
	 *
	 * @return array (Maybe) modified components list.
	 */
	public function remove_extra_panels( $components ) {
		/** widgets */
		$i = array_search( 'widgets', $components );
		if ( false !== $i ) {
			unset( $components[ $i ] );
		}

		/** menus */
		$i = array_search( 'nav_menus', $components );
		if ( false !== $i ) {
			unset( $components[ $i ] );
		}

		return $components;
	}

	/**
	 * Depreciated - Storefront calling settings direct
	 * Remove any unwanted default panels.
	 *
	 * @param object $wp_customize
	 *
	 * @return bool
	 */
	public function remove_panels( $wp_customize ) {
		global $wp_customize;
		$wp_customize->get_panel( 'nav_menus' )->active_callback = '__return_false';
		$wp_customize->remove_panel( 'widgets' );

		return true;
	}


	public function control_filter( $active, $control ) {
		return $this->template_ins->control_filter( $control );
	}

	public function maybe_remove_script_customizer() {
		global $wp_scripts, $wp_styles;

		$accepted_scripts = array(
			0  => 'heartbeat',
			1  => 'customize-controls',
			2  => 'wfocukirki_field_dependencies',
			3  => 'customize-widgets',
			4  => 'storefront-plugin-install',
			5  => 'wfocu-modal',
			6  => 'customize-nav-menus',
			7  => 'jquery-ui-button',
			8  => 'customize-views',
			9  => 'media-editor',
			10 => 'media-audiovideo',
			11 => 'mce-view',
			12 => 'image-edit',
			13 => 'code-editor',
			14 => 'csslint',
			15 => 'wp-color-picker',
			16 => 'wp-color-picker-alpha',
			17 => 'selectWoo',
			18 => 'wfocukirki-script',
			19 => 'wfocu-control-responsive-js',
			20 => 'updates',
			21 => 'wfocukirki_panel_and_section_icons',
			22 => 'wfocukirki-custom-sections',
			23 => 'wfocu_customizer_common',
			24 => 'acf-input',
		);

		$accepted_styles = array(
			0  => 'customize-controls',
			1  => 'customize-widgets',
			2  => 'storefront-plugin-install',
			3  => 'woocommerce_admin_menu_styles',
			4  => 'woofunnels-admin-font',
			5  => 'wfocu-modal',
			6  => 'customize-nav-menus',
			7  => 'media-views',
			8  => 'imgareaselect',
			9  => 'code-editor',
			10 => 'wp-color-picker',
			11 => 'selectWoo',
			12 => 'wfocukirki-selectWoo',
			13 => 'wfocukirki-styles',
			14 => 'wfocu-control-responsive-css',
			15 => 'wfocukirki-custom-sections',
		);

		$wp_scripts->queue = $accepted_scripts;
		$wp_styles->queue  = $accepted_styles;

	}

	public function enqueue_scripts() {
		$live_or_dev = 'live';

		if ( defined( 'WFOCU_IS_DEV' ) && true === WFOCU_IS_DEV ) {
			$live_or_dev = 'dev';
			$suffix      = '';
		} else {
			$suffix = '.min';
		}
		wp_enqueue_script( 'wfocu_customizer_common', $this->template_assets . '/' . $live_or_dev . '/js/customizer-common' . $suffix . '.js', array( 'customize-controls' ), WFOCU_VERSION_DEV, true );
		$template_fields = $this->template_ins->get_fields();

		$offer_data = $this->template_ins->data;

		$pd = array();

		if ( isset( $offer_data->products ) && count( get_object_vars( $offer_data->products ) ) > 0 ) {
			foreach ( $offer_data->products as $hash_key => $product ) {
				if ( isset( $product->id ) && $product->id > 0 ) {
					$pd[ 'regular_price_' . $hash_key ]  = '{{product_regular_price key="' . $hash_key . '"}}';
					$pd[ '_regular_price_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_regular_price key="' . $hash_key . '"}}' );

					$pd[ 'offer_price_' . $hash_key ]  = '{{product_offer_price key="' . $hash_key . '"}}';
					$pd[ '_offer_price_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_offer_price key="' . $hash_key . '"}}' );

					$pd[ 'product_price_full_' . $hash_key ]  = '{{product_price_full key="' . $hash_key . '"}}';
					$pd[ '_product_price_full_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_price_full key="' . $hash_key . '"}}' );

					$pd[ 'product_regular_price_raw_' . $hash_key ]  = '{{product_regular_price_raw key="' . $hash_key . '"}}';
					$pd[ '_product_regular_price_raw_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_regular_price_raw key="' . $hash_key . '"}}' );

					$pd[ 'product_offer_price_raw_' . $hash_key ]  = '{{product_offer_price_raw key="' . $hash_key . '"}}';
					$pd[ '_product_offer_price_raw_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_offer_price_raw key="' . $hash_key . '"}}' );

					$pd[ 'product_save_value_' . $hash_key ]  = '{{product_save_value key="' . $hash_key . '"}}';
					$pd[ '_product_save_value_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_save_value key="' . $hash_key . '"}}' );

					$pd[ 'product_save_percentage_' . $hash_key ]  = '{{product_save_percentage key="' . $hash_key . '"}}';
					$pd[ '_product_save_percentage_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_save_percentage key="' . $hash_key . '"}}' );

					$pd[ 'product_savings_' . $hash_key ]  = '{{product_savings key="' . $hash_key . '"}}';
					$pd[ '_product_savings_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_savings key="' . $hash_key . '"}}' );

					$pd[ 'product_single_unit_price_' . $hash_key ]  = '{{product_single_unit_price key="' . $hash_key . '"}}';
					$pd[ '_product_single_unit_price_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_single_unit_price key="' . $hash_key . '"}}' );

				}
			}
		}

		ob_start();
		include WFOCU_PLUGIN_DIR . '/admin/view/offer-save-template.php';
		$save_preset_html = ob_get_clean();

		wp_localize_script( 'wfocu_customizer_common', 'wfocu_customizer', array(
			'is_loaded'        => 'yes',
			'save_preset_html' => $save_preset_html,
			'offer_id'         => $this->offer_id,
			'fields'           => $template_fields,
			'ajax_url'         => admin_url( 'admin-ajax.php' ),
			'preset_message'   => __( 'Make sure your customizer setting is published', 'woofunnels-upstroke-one-click-upsell' ),
			'pd'               => $pd,
		) );
	}


	public function customizer_js() {
		//      WFOCU_Core()->template_loader->add_scripts( 'wp-util', includes_url() . 'js/wp-util.min.js', null, true );
		//      WFOCU_Core()->template_loader->add_scripts( 'customize-base', includes_url() . 'js/customize-base.js', null, true );
		//      WFOCU_Core()->template_loader->add_scripts( 'customize-preview', includes_url() . 'js/customize-preview.min.js', null, true );
		//      WFOCU_Core()->template_loader->add_styles( 'customize-preview', includes_url() . 'css/customize-preview.css', null );
		//
		//      WFOCU_Core()->template_loader->add_scripts( 'wfocu_product', $this->template_assets . '/js/wfocu-product.js', '1.0', true );
		//      WFOCU_Core()->template_loader->add_scripts( 'wfocu_customizer_live', $this->template_assets . '/js/customizer.js', '1.0', true );
		//      WFOCU_Core()->template_loader->add_scripts( 'customize-selective-refresh', includes_url() . '/js/customize-selective-refresh.min.js', null, true );
		//

		$template_fields = $this->template_ins->get_fields();

		$offer_data = $this->template_ins->data;

		$pd = array();
		if ( count( get_object_vars( $offer_data->products ) ) > 0 ) {
			foreach ( $offer_data->products as $hash_key => $product ) {
				if ( isset( $product->id ) && $product->id > 0 ) {
					$pd[ 'regular_price_' . $hash_key ]  = '{{product_regular_price key="' . $hash_key . '"}}';
					$pd[ '_regular_price_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_regular_price key="' . $hash_key . '"}}' );

					$pd[ 'offer_price_' . $hash_key ]  = '{{product_offer_price key="' . $hash_key . '"}}';
					$pd[ '_offer_price_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_offer_price key="' . $hash_key . '"}}' );

					$pd[ 'product_price_full_' . $hash_key ]  = '{{product_price_full key="' . $hash_key . '"}}';
					$pd[ '_product_price_full_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_price_full key="' . $hash_key . '"}}' );

					$pd[ 'product_regular_price_raw_' . $hash_key ]  = '{{product_regular_price_raw key="' . $hash_key . '"}}';
					$pd[ '_product_regular_price_raw_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_regular_price_raw key="' . $hash_key . '"}}' );

					$pd[ 'product_offer_price_raw_' . $hash_key ]  = '{{product_offer_price_raw key="' . $hash_key . '"}}';
					$pd[ '_product_offer_price_raw_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_offer_price_raw key="' . $hash_key . '"}}' );

					$pd[ 'product_save_value_' . $hash_key ]  = '{{product_save_value key="' . $hash_key . '"}}';
					$pd[ '_product_save_value_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_save_value key="' . $hash_key . '"}}' );

					$pd[ 'product_save_percentage_' . $hash_key ]  = '{{product_save_percentage key="' . $hash_key . '"}}';
					$pd[ '_product_save_percentage_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_save_percentage key="' . $hash_key . '"}}' );

					$pd[ 'product_savings_' . $hash_key ]  = '{{product_savings key="' . $hash_key . '"}}';
					$pd[ '_product_savings_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_savings key="' . $hash_key . '"}}' );

					$pd[ 'product_single_unit_price_' . $hash_key ]  = '{{product_single_unit_price key="' . $hash_key . '"}}';
					$pd[ '_product_single_unit_price_' . $hash_key ] = WFOCU_Common::maybe_parse_merge_tags( '{{product_single_unit_price key="' . $hash_key . '"}}' );

				}
			}
		}

		WFOCU_Core()->assets->localize_script( 'wfocu_customizer_live', 'wfocu_customizer', array(
			'is_loaded' => 'yes',
			'offer_id'  => $this->offer_id,
			'fields'    => $template_fields,
			'pd'        => $pd,
		) );
	}


	public function add_sections( $wp_customize ) {
		$this->template_ins->get_section( $wp_customize );

	}

	public function add_sections1( $wp_customize ) {
		$this->template_ins->get_section( $wp_customize );

	}

	public function save( $value, $WP_Customize_Setting ) {
		$this->template_ins->save( $WP_Customize_Setting->id, $value );
	}

	public function setup_preview() {

		$this->template_ins->get_view();
	}

	public function offer_confirmation_html() {
		include_once plugin_dir_path( WFOCU_PLUGIN_FILE ) . 'admin/view/offer-confirmation-static.php';
	}

	public function add_offer_confirmation_setting( $customizer_data ) {

		$offer_data = $this->get_template_instance()->data;
		if ( false === $offer_data->settings->ask_confirmation ) {
			return $customizer_data;
		}
		$offer_confirmation_panel = array();

		$get_defaults = WFOCU_Core()->data->get_option();

		/** PANEL: LAYOUT */
		$offer_confirmation_panel['wfocu_offer_confirmation'] = array(
			'panel'    => 'no',
			'data'     => array(
				'priority'    => 110,
				'title'       => __( 'Offer Confirmation', 'woofunnels-upstroke-one-click-upsell' ),
				'description' => '',
			),
			'sections' => array(
				'offer_confirmation' => array(
					'data'   => array(
						'title'    => __( 'Offer Confirmation', 'woofunnels-upstroke-one-click-upsell' ),
						'priority' => 110,
					),
					'fields' => array(
						'header_text'  => array(
							'type'      => 'text',
							'label'     => __( 'Header Text', 'woofunnels-upstroke-one-click-upsell' ),
							'default'   => $get_defaults['offer_header_text'],
							'transport' => 'postMessage',

							'wfocu_transport' => array(
								array(
									'type' => 'html',
									'elem' => '.wfocu-mc-heading .wfocu-mc-head-text',
								),
							),
							'priority'        => 10,
						),
						'cta_yes_text' => array(
							'type'      => 'text',
							'label'     => __( 'Accept Button Text', 'woofunnels-upstroke-one-click-upsell' ),
							'default'   => $get_defaults['offer_yes_btn_text'],
							'priority'  => 20,
							'transport' => 'postMessage',

							'wfocu_transport' => array(
								array(
									'type' => 'html',
									'elem' => '.wfocu-mc-footer-btn .wfocu-mc-button',
								),
							),
						),
						'cta_no_text'  => array(
							'type'            => 'text',
							'label'           => __( 'Decline Offer Link Text', 'woofunnels-upstroke-one-click-upsell' ),
							'default'         => $get_defaults['offer_skip_link_text'],
							'priority'        => 30,
							'transport'       => 'postMessage',
							'wfocu_transport' => array(
								array(
									'type' => 'html',
									'elem' => '.wfocu-mc-footer-btm-text .wfocu_skip_offer_mc',
								),
							),
						),

						/**** YES BUTTON COLOR SETTINGS START *******/
						'ct_colors'    => array(
							'type'     => 'custom',
							'default'  => '<div class="options-title-divider">' . esc_html__( 'Accept Button Colors', 'woofunnels-upstroke-one-click-upsell' ) . '</div>',
							'priority' => 40,
						),

						'ct_accept_btn_state1' => array(
							'type'      => 'radio-buttonset',
							'label'     => '',
							'default'   => 'normal',
							'choices'   => array(
								'normal' => __( 'Normal', 'woofunnels-upstroke-one-click-upsell' ),
								'hover'  => __( 'Hover', 'woofunnels-upstroke-one-click-upsell' ),
							),
							'transport' => 'postMessage',
							'priority'  => 50,
						),
						'yes_btn_bg_color'     => array(
							'label'           => __( 'Background Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_yes_btn_bg_cl'],
							'priority'        => 60,
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state1',
									'value'    => 'normal',
									'operator' => '==',
								),
							),
							'transport'       => 'postMessage',
							'wfocu_transport' => array(
								array(
									'type'     => 'css',
									'internal' => true,
									'prop'     => array( 'background-color' ),
									'elem'     => '.wfocu-sidebar-cart .wfocu-mc-button',
								),

							),
						),

						'yes_btn_text_color'   => array(
							'label'           => __( 'Text Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_yes_btn_txt_cl'],
							'priority'        => 70,
							'transport'       => 'postMessage',
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state1',
									'value'    => 'normal',
									'operator' => '==',
								),
							),
							'wfocu_transport' => array(
								array(
									'type'     => 'css',
									'prop'     => array( 'color' ),
									'elem'     => '.wfocu-sidebar-cart .wfocu-mc-button',
									'internal' => true,
								),

							),
						),
						'yes_btn_shadow_color' => array(
							'label'           => __( 'Shadow Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_yes_btn_sh_cl'],
							'priority'        => 80,
							'transport'       => 'postMessage',
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state1',
									'value'    => 'normal',
									'operator' => '==',
								),
							),
							'wfocu_transport' => array(
								array(
									'type'     => 'css',
									'prop'     => array( 'box-shadow', '-moz-box-shadow', '-webkit-box-shadow', '-ms-box-shadow', '-o-box-shadow' ),
									'prefix'   => '0px 4px 0px ',
									'elem'     => '.wfocu-sidebar-cart .wfocu-mc-button',
									'internal' => true,
								),

							),
						),
						'yes_btn_hover_color'  => array(
							'label'           => __( 'Background Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_yes_btn_bg_cl_h'],
							'priority'        => 90,
							'transport'       => 'postMessage',
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state1',
									'value'    => 'hover',
									'operator' => '==',
								),
							),
							'wfocu_transport' => array(
								array(
									'type'  => 'css',
									'hover' => true,
									'prop'  => array( 'background-color' ),
									'elem'  => '.wfocu-sidebar-cart .wfocu-mc-button',
								),

							),

						),

						'yes_btn_hover_color_text' => array(
							'label'           => __( 'Text Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_yes_btn_txt_cl_h'],
							'priority'        => 100,
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state1',
									'value'    => 'hover',
									'operator' => '==',
								),
							),
							'transport'       => 'postMessage',
							'wfocu_transport' => array(
								array(
									'type'  => 'css',
									'hover' => true,
									'prop'  => array( 'color' ),
									'elem'  => '.wfocu-sidebar-cart .wfocu-mc-button',
								),

							),
						),

						'yes_btn_hover_shadow_color' => array(
							'label'           => __( 'Shadow Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_yes_btn_sh_cl_h'],
							'priority'        => 110,
							'transport'       => 'postMessage',
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state1',
									'value'    => 'hover',
									'operator' => '==',
								),
							),
							'wfocu_transport' => array(

								array(
									'hover'  => true,
									'type'   => 'css',
									'prop'   => array( 'box-shadow', '-moz-box-shadow', '-webkit-box-shadow', '-ms-box-shadow', '-o-box-shadow' ),
									'prefix' => '0px 4px 0px ',
									'elem'   => '.wfocu-sidebar-cart .wfocu-mc-button',
								),

							),
						),

						/**** YES BUTTON COLOR SETTINGS ENDS *******/

						/**** NO BUTTON COLOR SETTINGS STARTS *******/
						'ct_colors3'                 => array(
							'type'     => 'custom',
							'default'  => '<div class="options-title-divider">' . esc_html__( 'Decline Offer Colors', 'woofunnels-upstroke-one-click-upsell' ) . '</div>',
							'priority' => 120,
						),

						'ct_accept_btn_state2' => array(
							'type'      => 'radio-buttonset',
							'label'     => '',
							'default'   => 'normal',
							'choices'   => array(
								'normal' => __( 'Normal', 'woofunnels-upstroke-one-click-upsell' ),
								'hover'  => __( 'Hover', 'woofunnels-upstroke-one-click-upsell' ),
							),
							'transport' => 'postMessage',
							'priority'  => 130,
						),
						'no_btn_color'         => array(
							'label'           => __( 'Text Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_no_btn_txt_cl'],
							'priority'        => 140,
							'transport'       => 'postMessage',
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state2',
									'value'    => 'normal',
									'operator' => '==',
								),
							),
							'wfocu_transport' => array(
								array(
									'type'     => 'css',
									'prop'     => array( 'color' ),
									'elem'     => '.wfocu-sidebar-cart .wfocu-mc-footer-btm-text a',
									'internal' => true,
								),

							),
						),

						'no_btn_color_hover' => array(
							'label'           => __( 'Text Color', 'woofunnels-upstroke-one-click-upsell' ),
							'type'            => 'color',
							'default'         => $get_defaults['offer_no_btn_txt_cl_h'],
							'priority'        => 150,
							'transport'       => 'postMessage',
							'active_callback' => array(
								array(
									'setting'  => 'wfocu_offer_confirmation_offer_confirmation_ct_accept_btn_state2',
									'value'    => 'hover',
									'operator' => '==',
								),
							),
							'wfocu_transport' => array(
								array(
									'hover' => true,
									'type'  => 'css',
									'prop'  => array( 'color' ),
									'elem'  => '.wfocu-sidebar-cart .wfocu-mc-footer-btm-text a',
								),

							),
						),

						/**** NO BUTTON COLOR SETTINGS ENDS *******/

						'ct_cart_opener'      => array(
							'type'    => 'custom',
							'default' => '<div class="options-title-divider">' . esc_html__( 'Offer Confirmation Opener', 'woofunnels-upstroke-one-click-upsell' ) . '</div>',

							'priority' => 160,
						),
						'ct_cart_opener_desc' => array(
							'type'        => 'custom',
							'default'     => '',
							'description' => esc_attr__( 'This element will only display after user closes Offer Confirmation.', 'woofunnels-upstroke-one-click-upsell' ),
							'priority'    => 170,
						),
						'cart_opener_text'    => array(
							'type'      => 'text',
							'label'     => __( 'Text', 'woofunnels-upstroke-one-click-upsell' ),
							'default'   => $get_defaults['cart_opener_text'],
							'transport' => 'postMessage',

							'wfocu_transport' => array(
								array(
									'type' => 'html',
									'elem' => '.wfocu-confirm-order-btn .wfocu-opener-btn-bg',
								),
							),
							'priority'        => 180,
						),

						'cart_opener_color' => array(
							'type'      => 'color',
							'label'     => __( 'Background Color', 'woofunnels-upstroke-one-click-upsell' ),
							'default'   => $get_defaults['cart_opener_background_color'],
							'transport' => 'postMessage',

							'wfocu_transport' => array(
								array(
									'type' => 'css',
									'prop' => array( 'background-color' ),
									'elem' => '.wfocu-confirm-order-btn .wfocu-opener-btn-bg',
								),
								array(
									'type' => 'css',
									'prop' => array( 'border-right-color' ),
									'elem' => '.wfocu-confirm-order-btn .wfocu-left-arrow',

								),
							),
							'priority'        => 190,
						),

						'cart_opener_text_color' => array(
							'type'      => 'color',
							'label'     => __( 'Text Color', 'woofunnels-upstroke-one-click-upsell' ),
							'default'   => $get_defaults['cart_opener_text_color'],
							'transport' => 'postMessage',

							'wfocu_transport' => array(
								array(
									'type' => 'css',
									'prop' => array( 'color' ),
									'elem' => '.wfocu-confirm-order-btn',
								),

							),
							'priority'        => 200,
						),

					),
				),
			),
		);

		$customizer_data[] = $offer_confirmation_panel;

		return $customizer_data;
	}

	/**
	 * @return null
	 */
	public function get_template_instance() {
		return $this->template_ins;
	}

	public function maybe_update_customize_save() {
		if ( 0 != $this->offer_id ) {
			update_post_meta( $this->offer_id, '_wfocu_edit_last', time() );
		}
	}

	public function mark_changsets_as_dismissed( $post_id, $post ) {
		if ( ! $post instanceof WP_Post ) {
			return;
		}

		if ( 'customize_changeset' === $post->post_type ) {
			update_post_meta( $post_id, '_customize_restore_dismissed', true );
		}
	}

	public function dequeue_unnecessary_customizer_scripts() {

		if ( isset( $_REQUEST['wfocu_customize'] ) && $_REQUEST['wfocu_customize'] == 'loaded' && isset( $_REQUEST['offer_id'] ) && $_REQUEST['offer_id'] > 0 ) {

			/**
			 * wp-titan framework add these color pickers, that breaks our customizer page
			 */
			wp_deregister_script( 'wp-color-picker-alpha' );
			wp_dequeue_script( 'wp-color-picker-alpha' );

		}

	}


}

if ( class_exists( 'WFOCU_Core' ) ) {
	WFOCU_Core::register( 'customizer', 'WFOCU_Customizer' );
}
