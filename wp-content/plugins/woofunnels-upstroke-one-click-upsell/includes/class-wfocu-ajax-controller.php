<?php

/**
 * Class WFOCU_AJAX_Controller
 * Handles All the request came from front end or the backend
 */
class WFOCU_AJAX_Controller {

	const CHARGE_ACTION = 'wfocu_front_charge';
	const SHIPPING_CALCULATION_ACTION = 'wfocu_front_calculate_shipping';

	public static function init() {

		add_action( 'init', array( __CLASS__, 'maybe_set_error_reporting_false' ) );
		/**
		 * Front End AJAX actions
		 */
		add_action( 'wp_ajax_wfocu_front_charge', array( __CLASS__, 'handle_charge' ) );
		add_action( 'wp_ajax_nopriv_wfocu_front_charge', array( __CLASS__, 'handle_charge' ) );

		add_action( 'wp_ajax_wfocu_front_offer_skipped', array( __CLASS__, 'handle_offer_skipped' ) );
		add_action( 'wp_ajax_nopriv_wfocu_front_offer_skipped', array( __CLASS__, 'handle_offer_skipped' ) );

		add_action( 'wp_ajax_wfocu_front_calculate_shipping', array( __CLASS__, 'calculate_shipping' ) );
		add_action( 'wp_ajax_nopriv_wfocu_front_calculate_shipping', array( __CLASS__, 'calculate_shipping' ) );

		add_action( 'wp_ajax_wfocu_front_calculate_shipping_variation', array( __CLASS__, 'calculate_shipping_variation' ) );
		add_action( 'wp_ajax_nopriv_wfocu_front_calculate_shipping_variation', array( __CLASS__, 'calculate_shipping_variation' ) );

		add_action( 'wp_ajax_wfocu_front_register_views', array( __CLASS__, 'register_views' ) );
		add_action( 'wp_ajax_nopriv_wfocu_front_register_views', array( __CLASS__, 'register_views' ) );

		add_action( 'wp_ajax_wfocu_front_offer_expired', array( __CLASS__, 'offer_expired' ) );
		add_action( 'wp_ajax_nopriv_wfocu_front_offer_expired', array( __CLASS__, 'offer_expired' ) );

		add_action( 'wp_ajax_wfocu_front_catch_error', array( __CLASS__, 'catch_error' ) );
		add_action( 'wp_ajax_nopriv_wfocu_front_catch_error', array( __CLASS__, 'catch_error' ) );

		/**
		 * Backend AJAX actions
		 */
		if ( is_admin() ) {
			self::handle_admin_ajax();
		}

	}

	public static function handle_admin_ajax() {

		add_action( 'wp_ajax_wfocu_add_new_funnel', array( __CLASS__, 'add_funnel' ) );

		add_action( 'wp_ajax_wfocu_add_offer', array( __CLASS__, 'add_offer' ) );

		add_action( 'wp_ajax_wfocu_add_product', array( __CLASS__, 'add_product' ) );

		add_action( 'wp_ajax_wfocu_remove_product', array( __CLASS__, 'remove_product' ) );

		add_action( 'wp_ajax_wfocu_save_funnel_steps', array( __CLASS__, 'save_funnel_steps' ) );

		add_action( 'wp_ajax_wfocu_save_funnel_offer_products', array( __CLASS__, 'save_funnel_offer_products' ) );

		add_action( 'wp_ajax_wfocu_save_funnel_offer_settings', array( __CLASS__, 'save_funnel_offer_settings' ) );

		add_action( 'wp_ajax_wfocu_product_search', array( __CLASS__, 'product_search' ) );

		add_action( 'wp_ajax_wfocu_page_search', array( __CLASS__, 'page_search' ) );

		add_action( 'wp_ajax_wfocu_update_offer', array( __CLASS__, 'update_offer' ) );

		add_action( 'wp_ajax_wfocu_update_funnel', array( __CLASS__, 'update_funnel' ) );

		add_action( 'wp_ajax_wfocu_remove_offer_from_funnel', array( __CLASS__, 'removed_offer_from_funnel' ) );

		add_action( 'wp_ajax_wfocu_get_custom_page', array( __CLASS__, 'get_custom_page' ) );

		add_action( 'wp_ajax_wfocu_save_rules_settings', array( __CLASS__, 'update_rules' ) );

		add_action( 'wp_ajax_wfocu_update_template', array( __CLASS__, 'update_template' ) );

		add_action( 'wp_ajax_wfocu_save_funnel_settings', array( __CLASS__, 'save_funnel_settings' ) );

		add_action( 'wp_ajax_wfocu_save_global_settings', array( __CLASS__, 'save_global_settings' ) );

		add_action( 'wp_ajax_wfocu_preview_details', array( __CLASS__, 'preview_details' ) );

		add_action( 'wp_ajax_wfocu_duplicate_funnel', array( __CLASS__, 'duplicate_funnel' ) );

		add_action( 'wp_ajax_wfocu_toggle_funnel_state', array( __CLASS__, 'toggle_funnel_state' ) );
		add_action( 'wp_ajax_wfocu_save_template', array( __CLASS__, 'save_template' ) );
		add_action( 'wp_ajax_wfocu_apply_template', array( __CLASS__, 'apply_template' ) );
		add_action( 'wp_ajax_wfocu_delete_template', array( __CLASS__, 'delete_template' ) );

		add_action( 'wp_ajax_wfocu_admin_refund_offer', array( __CLASS__, 'refund_offer' ) );
	}

	public static function handle_offer_skipped() {
		$data                 = array();
		$data_posted          = $_POST['data'];   //@codingStandardsIgnoreLine
		$get_type_of_offer    = $data_posted['offer_type'];
		$get_type_index_offer = $data_posted['offer_type_index'];
		$get_current_offer    = WFOCU_Core()->data->get_current_offer();
		$get_order            = WFOCU_Core()->data->get_current_order();
		$args                 = array(
			'order_id'         => WFOCU_WC_Compatibility::get_order_id( $get_order ),
			'funnel_id'        => WFOCU_Core()->data->get_funnel_id(),
			'offer_id'         => $get_current_offer,
			'funnel_unique_id' => WFOCU_Core()->data->get_funnel_key(),
			'offer_type'       => $get_type_of_offer,
			'offer_index'      => $get_type_index_offer,
			'email'            => WFOCU_Core()->data->get( 'useremail' ),
		);
		WFOCU_Core()->data->set( '_offer_result', false );
		do_action( 'wfocu_offer_rejected_event', $args );
		WFOCU_Core()->log->log( 'Order #' . WFOCU_WC_Compatibility::get_order_id( $get_order ) . ': UpSell Reject For Offer: ' . $get_current_offer );

		$get_offer = WFOCU_Core()->offers->get_the_next_offer( 'no' );

		$data['redirect_url'] = WFOCU_Core()->public->get_the_upsell_url( $get_offer );

		WFOCU_Core()->data->set( 'current_offer', $get_offer );
		WFOCU_Core()->data->save();

		$response_ajax = array( 'success' => 'true' );

		if ( isset( $data ) ) {
			$response_ajax['data'] = $data;
		}
		wp_send_json( $response_ajax );
	}


	public static function offer_expired() {

		check_ajax_referer( 'wfocu_front_offer_expired', 'nonce' );
		$data = array();

		$get_current_offer = WFOCU_Core()->data->get_current_offer();

		$get_type_of_offer    = WFOCU_Core()->data->get( '_current_offer_type' );
		$get_type_index_offer = WFOCU_Core()->data->get( '_current_offer_type_index' );

		$get_order = WFOCU_Core()->data->get_current_order();
		$args      = array(
			'order_id'         => WFOCU_WC_Compatibility::get_order_id( $get_order ),
			'funnel_id'        => WFOCU_Core()->data->get_funnel_id(),
			'offer_id'         => $get_current_offer,
			'funnel_unique_id' => WFOCU_Core()->data->get_funnel_key(),
			'offer_type'       => $get_type_of_offer,
			'offer_index'      => $get_type_index_offer,
			'email'            => WFOCU_Core()->data->get( 'useremail' ),
		);
		do_action( 'wfocu_offer_expired_event', $args );

		if ( 'redirect_to_next' === filter_input( INPUT_POST, 'next_action' ) ) {
			$get_offer = WFOCU_Core()->offers->get_the_next_offer( 'yes' );

			$data['redirect_url'] = WFOCU_Core()->public->get_the_upsell_url( $get_offer );

			WFOCU_Core()->data->set( 'current_offer', $get_offer );
			WFOCU_Core()->data->save();
		}
		$response_ajax = array( 'success' => 'true' );

		$response_ajax = array_merge( $response_ajax, $data );
		wp_send_json( $response_ajax );
	}


	public static function handle_charge() {

		check_ajax_referer( 'wfocu_front_charge', 'nonce' );
		/**
		 * @todo code here to validate the request params
		 * @todo code here to instantiate the core class that needs to handle further call probably WFOCU_Core()->public
		 */
		$get_current_offer      = WFOCU_Core()->data->get( 'current_offer' );
		$get_current_offer_meta = WFOCU_Core()->offers->get_offer_meta( $get_current_offer );
		WFOCU_Core()->data->set( '_offer_result', true );
		$posted_data = WFOCU_Core()->process_offer->parse_posted_data();

		$response  = false;
		$get_order = WFOCU_Core()->data->get_current_order();

		WFOCU_Core()->log->log( 'Order #' . WFOCU_WC_Compatibility::get_order_id( $get_order ) . ': UpSell Accept Call Received for Offer: ' . $get_current_offer );

		try {
			if ( true === self::validate_charge_request( $posted_data ) ) {

				WFOCU_Core()->process_offer->execute( $get_current_offer_meta );

				/**
				 * Perform Charge by Gateway
				 */
				$response = WFOCU_Core()->public->charge_upsell();

				/**
				 * Handle order creation or batching
				 */
				$data = WFOCU_Core()->process_offer->_handle_upsell_charge( $response );

				$message = $data['message'];
			} else {
				$message = __( 'Unable to process, Validation Failed!', 'woofunnels-upstroke-one-click-upsell' );
			}
		} catch ( Exception $e ) {
			$response = false;
			$message  = __( 'Unable to process, Validation Failed!', 'woofunnels-upstroke-one-click-upsell' );
			WFOCU_Core()->log->log( "Exception Captured: Detils" . print_r( $e->getMessage(), true ) );
			$data = WFOCU_Core()->process_offer->_handle_upsell_charge( $response );

		} catch ( Error $e ) {
			$response = false;
			$message  = __( 'Unable to process, Validation Failed!', 'woofunnels-upstroke-one-click-upsell' );
			WFOCU_Core()->log->log( "Error Captured: Detils" . print_r( $e->getMessage(), true ) );
			$data = WFOCU_Core()->process_offer->_handle_upsell_charge( $response );

		}


		$response_ajax            = array( 'success' => $response );
		$response_ajax['message'] = $message;

		if ( isset( $data ) ) {
			$response_ajax['data'] = $data;
		}

		wp_send_json( $response_ajax );

	}

	/**
	 * perform checks to validate if all the hashes that came in the charge call belongs to the current running offer and funnel
	 *
	 * @param $posted_data
	 *
	 * @return bool
	 */
	public static function validate_charge_request( $posted_data ) {

		if ( count( $posted_data ) > 0 ) {
			$get_current_offer      = WFOCU_Core()->data->get( 'current_offer' );
			$get_current_offer_meta = WFOCU_Core()->offers->get_offer_meta( $get_current_offer );

			if ( is_object( $get_current_offer_meta ) && isset( $get_current_offer_meta->products ) ) {

				foreach ( $posted_data as $hash => $data ) {

					if ( ! isset( $get_current_offer_meta->products->{$data['hash']} ) ) {
						return false;
					}
				}

				return true;
			}
		}

		return false;
	}

	public static function add_funnel() {
		$resp = array(
			'msg'    => 'funnel not found',
			'status' => false,
		);
		if ( isset( $_POST['funnel_name'] ) && '' !== $_POST['funnel_name'] ) { // @codingStandardsIgnoreLine
			$post                 = array();
			$post['post_title']   = $_POST['funnel_name']; // @codingStandardsIgnoreLine
			$post['post_type']    = WFOCU_Common::get_funnel_post_type_slug();
			$post['post_status']  = WFOCU_SLUG . '-disabled';
			$post['post_content'] = isset( $_POST['funnel_desc'] ) ? $_POST['funnel_desc'] : ''; // @codingStandardsIgnoreLine
			$post['menu_order']   = WFOCU_Common::get_next_funnel_priority();

			if ( ! empty( $post ) ) {
				$funnel_id = wp_insert_post( $post );
				if ( 0 !== $funnel_id && ! is_wp_error( $funnel_id ) ) {
					$resp['status']       = true;
					$resp['redirect_url'] = add_query_arg( array(
						'page'    => 'upstroke',
						'section' => 'rules',
						'edit'    => $funnel_id,
					), admin_url( 'admin.php' ) );
					$resp['msg']          = 'Funnel Successfully Created';
					WFOCU_Core()->funnels->save_funnel_priority( $funnel_id, $post['menu_order'] );

				} else {
					$resp['redirect_url'] = '#';
					$resp['msg']          = 'Funnel Successfully Updated';
				}
			}
		}
		wp_send_json( $resp );
	}

	public static function add_offer() {
		$resp = array(
			'msg'    => 'funnel not found',
			'status' => false,
		);
		if ( isset( $_POST['funnel_id'] ) && isset( $_POST['step_name'] ) ) { // @codingStandardsIgnoreLine

			$offer_type = 'upsell';
			if ( isset( $_POST['step_type'] ) && '' !== $_POST['step_type'] ) { // @codingStandardsIgnoreLine
				$offer_type = $_POST['step_type'];
			}

			$funnel_id = $_POST['funnel_id'];  // @codingStandardsIgnoreLine
			$post_type = WFOCU_Common::get_offer_post_type_slug();
			$post      = array(
				'post_title'  => $_POST['step_name'], // @codingStandardsIgnoreLine
				'post_type'   => $post_type,
				'post_status' => 'publish',
			);

			$id = wp_insert_post( $post );
			if ( ! is_wp_error( $id ) ) {
				$default_settings = array(
					'funnel_id' => $funnel_id,
					'type'      => $offer_type,
					'products'  => array(),
					'fields'    => array(),
					'settings'  => array(),
				);
				update_post_meta( $id, '_funnel_id', $funnel_id );
				update_post_meta( $id, '_offer_type', $offer_type );

				update_post_meta( $funnel_id, '_wfocu_is_rules_saved', 'yes' );
				WFOCU_Common::update_funnel_time( $funnel_id );

				$resp['msg']       = 'Offer Add Successfully';
				$resp['type']      = $offer_type;
				$resp['id']        = $id;
				$resp['url']       = get_the_permalink( $id );
				$resp['slug']      = get_post( $id )->post_name;
				$resp['title']     = $_POST['step_name'];
				$resp['funnel_id'] = $funnel_id;
				$resp['status']    = true;
				$resp['data']      = $default_settings;
			}
		}

		wp_send_json( $resp );

	}

	public static function save_funnel_offer_settings() {

		$resp = array(
			'msg'    => '',
			'status' => false,
		);

		if ( isset( $_POST['funnel_id'] ) ) {
			$post = get_post( $_POST['funnel_id'] );
			if ( ! is_null( $post ) ) {
				$funnel_id = $post->ID;
				$offer_id  = isset( $_POST['offer_id'] ) ? $_POST['offer_id'] : 0;
				$settings  = ( isset( $_POST['settings'] ) && is_array( $_POST['settings'] ) && count( $_POST['settings'] ) ) > 0 ? (object) $_POST['settings'] : new stdClass();

				$old_data = WFOCU_Common::get_offer( $offer_id );

				if ( $old_data != '' ) {

					$old_data->settings = WFOCU_Common::maybe_filter_boolean_strings( $settings );
					WFOCU_Common::update_offer( $offer_id, $old_data, $funnel_id );
					WFOCU_Common::update_funnel_time( $funnel_id );

					do_action( 'wfocu_offer_updated', $old_data, $offer_id, $funnel_id );
					$resp['msg']    = 'Setting Updated';
					$resp['status'] = true;
				}
			}
		}
		wp_send_json( $resp );
	}

	public static function save_funnel_steps() {

		$resp = array(
			'msg'    => '',
			'status' => false,
		);

		if ( isset( $_POST['funnel_id'] ) ) {

			$post         = get_post( $_POST['funnel_id'] );
			$update_steps = array();
			if ( ! is_null( $post ) ) {
				$funnel_id = $post->ID;
				$steps     = ( isset( $_POST['steps'] ) && is_array( $_POST['steps'] ) && count( $_POST['steps'] ) ) > 0 ? $_POST['steps'] : new stdClass();
				foreach ( $steps as $key => $step ) {
					if ( ! empty( $step ) ) {
						$step = WFOCU_Core()->offers->filter_step_object_for_db( $step );

						$update_steps[ $key ] = $step;
					}
				}
				if ( count( $update_steps ) > 0 ) {

					$update_steps = array_values( $update_steps );
				}
				$upsell_downsell = WFOCU_Core()->funnels->prepare_upsell_downsells( $update_steps );
				WFOCU_Common::update_funnel_steps( $funnel_id, $update_steps );
				WFOCU_Common::update_funnel_upsell_downsell( $funnel_id, $upsell_downsell );

				WFOCU_Common::update_funnel_time( $funnel_id );

				$resp = array(
					'msg'    => __( 'All Offers Saved', 'woofunnels-upstroke-one-click-upsell' ),
					'status' => true,
				);
			}
		}
		wp_send_json( $resp );
	}

	public static function save_funnel_offer_products() {
		$resp          = array(
			'msg'    => '',
			'status' => false,
		);
		$products_list = array();
		if ( isset( $_POST['funnel_id'] ) ) {

			$post = get_post( $_POST['funnel_id'] );
			if ( ! is_null( $post ) ) {

				$funnel_id   = $post->ID;
				$offer_id    = isset( $_POST['offer_id'] ) ? $_POST['offer_id'] : 0;
				$offers      = ( isset( $_POST['offers'] ) && is_array( $_POST['offers'] ) && count( $_POST['offers'] ) ) > 0 ? $_POST['offers'] : array();
				$offer_state = ( isset( $_POST['offer_state'] ) && $_POST['offer_state'] == 'on' ) ? '1' : '0';

				$offers_setting = new stdClass();
				if ( ! empty( $offers ) && count( $offers ) > 0 && isset( $offers[ $offer_id ] ) ) {
					$offer = $offers[ $offer_id ];
					if ( ! empty( $offer['products'] ) && count( $offer['products'] ) > 0 ) {
						$offers_setting->products   = new stdClass();
						$offers_setting->variations = new stdClass();
						$offers_setting->fields     = new stdClass();

						foreach ( $offer['products'] as $hash_key => $pro ) {
							$offers_setting->products->{$hash_key}                   = $pro['id'];
							$offers_setting->fields->{$hash_key}                     = new stdClass();
							$offers_setting->fields->{$hash_key}->discount_amount    = $pro['discount_amount'];
							$offers_setting->fields->{$hash_key}->discount_type      = $pro['discount_type'];
							$offers_setting->fields->{$hash_key}->quantity           = $pro['quantity'];
							$offers_setting->fields->{$hash_key}->shipping_cost_flat = floatval( $pro['shipping_cost_flat'] );
							array_push( $products_list, $pro['id'] );
							if ( isset( $pro['variations'] ) && count( $pro['variations'] ) > 0 ) {
								$offers_setting->variations->{$hash_key} = array();
								foreach ( $pro['variations'] as $variation_id => $settings ) {
									if ( isset( $settings['is_enable'] ) && 'on' == $settings['is_enable'] ) {
										$offers_setting->variations->{$hash_key}[ $variation_id ]                  = new stdClass();
										$offers_setting->variations->{$hash_key}[ $variation_id ]->vid             = $variation_id;
										$offers_setting->variations->{$hash_key}[ $variation_id ]->discount_amount = $settings['discount_amount'];
										$offers_setting->variations->{$hash_key}[ $variation_id ]                  = apply_filters( 'wfocu_variations_offers_setting_data', $offers_setting->variations->{$hash_key}[ $variation_id ] );

									}
								}

								$offers_setting->fields->{$hash_key}->default_variation = isset( $pro['default_variation'] ) ? $pro['default_variation'] : '';
							}
						}

						$offers_setting->have_multiple_product = is_array( $offer['products'] ) && count( $offer['products'] ) > 1 ? 2 : 1;
					}
				}
				$steps = WFOCU_Core()->funnels->get_funnel_steps( $funnel_id );
				if ( $steps && is_array( $steps ) && count( $steps ) > 0 ) {
					foreach ( $steps as $key => $step ) {
						if ( ! empty( $step ) ) {

							if ( $step['id'] == $offer_id ) {

								$step['state'] = $offer_state;
								$step          = WFOCU_Core()->offers->filter_step_object_for_db( $step );

							}
							$update_steps[ $key ] = $step;

						}
					}
				}

				$upsell_downsell = WFOCU_Core()->funnels->prepare_upsell_downsells( $update_steps );

				WFOCU_Common::update_funnel_steps( $funnel_id, $update_steps );
				WFOCU_Common::update_funnel_upsell_downsell( $funnel_id, $upsell_downsell );

				$getsettings              = WFOCU_Common::get_offer( $offer_id );
				$offers_setting->template = ( isset( $getsettings->template ) ? $getsettings->template : WFOCU_Core()->template_loader->get_default_template( $offers_setting ) );

				if ( ! empty( $offers_setting ) ) {
					WFOCU_Common::update_offer( $offer_id, $offers_setting, $funnel_id );
					if ( '' !== $funnel_id ) {
						WFOCU_Common::update_funnel_time( $funnel_id );
					}

					do_action( 'wfocu_offer_updated', $offers_setting, $offer_id, $funnel_id );
					$resp['msg']    = 'data is saved';
					$resp['status'] = true;
					$resp['offers'] = $offers_setting;
				}
			}
			$woofunnels_transient_obj = WooFunnels_Transient::get_instance();
			$woofunnels_transient_obj->delete_all_transients( 'upstroke' );
		}
		wp_send_json( $resp );
	}

	public static function product_search( $term = false, $return = false ) {
		$term = wc_clean( empty( $term ) ? stripslashes( $_POST['term'] ) : $term );

		if ( empty( $term ) ) {
			wp_die();
		}

		$variations = true;
		if ( 'true' !== $_POST['variations'] ) {
			$variations = false;
		}
		$ids = WFOCU_Common::search_products( $term, $variations );

		/**
		 * Products types that are allowed in the offers
		 */
		$allowed_types   = apply_filters( 'wfocu_offer_product_types', array(
			'simple',
			'variable',
			'variation',
		) );
		$product_objects = array_filter( array_map( 'wc_get_product', $ids ), 'wc_products_array_filter_editable' );
		$product_objects = array_filter( $product_objects, function ( $arr ) use ( $allowed_types ) {

			return $arr && is_a( $arr, 'WC_Product' ) && in_array( $arr->get_type(), $allowed_types );

		} );
		$products        = array();
		foreach ( $product_objects as $product_object ) {
			if ( 'publish' === $product_object->get_status() ) {
				$products[] = array(
					'id'      => $product_object->get_id(),
					'product' => rawurldecode( WFOCU_Common::get_formatted_product_name( $product_object ) ),
				);
			}
		}
		wp_send_json( apply_filters( 'wfocu_woocommerce_json_search_found_products', $products ) );
	}

	public static function add_product() {

		$resp = array(
			'status' => false,
			'msg'    => '',
		);

		if ( isset( $_POST['funnel_id'] ) && $_POST['funnel_id'] > 0 && isset( $_POST['offer_id'] ) && $_POST['offer_id'] > 0 ) {

			$offer_id = $_POST['offer_id'];

			$funnel_id     = $_POST['funnel_id'];
			$products      = array();
			$fields        = array();
			$variations    = array();
			$products_list = ( isset( $_POST['products'] ) && is_array( $_POST['products'] ) && count( $_POST['products'] ) > 0 ) ? $_POST['products'] : array();

			if ( ! is_array( $products_list ) ) {
				$products_list = array();
				wp_send_json( $resp );

			}
			$variation_save  = array();
			$is_add_on_exist = WFOCU_Common::is_add_on_exist( 'MultiProduct' );
			if ( ! $is_add_on_exist ) {
				$first_prod = $products_list[0];
				unset( $product_details );
				$products_list = array( $first_prod );
			}

			foreach ( $products_list as $pid ) {
				$pro = wc_get_product( $pid );
				if ( $pro instanceof WC_Product ) {
					$image_url = wp_get_attachment_url( $pro->get_image_id() );
					if ( $image_url == '' ) {
						$image_url = wc_placeholder_img_src();
					}
					$hash_key               = WFOCU_Common::get_product_id_hash( $funnel_id, $offer_id, $pid );
					$product_details        = new stdClass();
					$product_details->id    = $pid;
					$product_details->name  = WFOCU_Common::get_formatted_product_name( $pro );
					$product_details->image = $image_url;
					$product_details->type  = $pro->get_type();
					if ( ! $pro->is_type( 'variable' ) ) {
						$product_details->regular_price     = wc_price( $pro->get_regular_price() );
						$product_details->regular_price_raw = $pro->get_regular_price();
						$product_details->price             = wc_price( $pro->get_price() );
						if ( $product_details->regular_price === $product_details->price ) {
							unset( $product_details->price );
						}
					}

					$products[ $hash_key ]              = $product_details;
					$variation_save[ $hash_key ]        = array();
					$variations[ $hash_key ]            = array();
					$product_fields                     = new stdClass();
					$product_fields->discount_amount    = 0;
					$product_fields->discount_type      = 'percentage';
					$product_fields->quantity           = 1;
					$product_fields->shipping_cost_flat = 0;

					if ( $pro->is_type( 'variable' ) ) {
						$pro_variations    = $pro->get_available_variations();
						$get_variation_ids = wp_list_pluck( $pro_variations, 'variation_id' );
						asort( $get_variation_ids );

						if ( count( $pro_variations ) > 0 ) {
							$first_variation = null;
							foreach ( $get_variation_ids as $variation_id ) {

								$vpro = new WC_Product_Variation( $variation_id );

								if ( $vpro ) {
									$variation_options                    = new stdClass();
									$variation_options->name              = WFOCU_Common::get_formatted_product_name( $vpro );
									$variation_options->vid               = $variation_id;
									$variation_options->attributes        = WFOCU_Common::get_variation_attribute( $vpro );
									$variation_options->regular_price     = wc_price( $vpro->get_regular_price() );
									$variation_options->regular_price_raw = wc_get_price_including_tax( $vpro, array( 'price' => $vpro->get_regular_price() ) );
									$variation_options->sale_price        = wc_price( $vpro->get_price() );
									if ( $variation_options->regular_price === $variation_options->sale_price ) {
										unset( $variation_options->sale_price );
									}
									$variation_options->display_price   = wc_price( $vpro->get_price() );
									$variation_options->discount_amount = 0;
									$variation_options->discount_type   = 'percentage';
									$variation_options->is_enable       = true;
									if ( is_null( $first_variation ) ) {
										$first_variation = true;

										$product_fields->default_variation = $variation_options->vid;
										$product_fields->variations_enable = true;

										$variation_save[ $hash_key ][ $variation_id ]                 = new stdClass();
										$variation_save[ $hash_key ][ $variation_id ]->disount_amount = 0;
										$variation_save[ $hash_key ][ $variation_id ]->vid            = $variation_id;
									}

									$variations[ $hash_key ][ $variation_id ] = $variation_options;
									unset( $variation_options );

								}
							}
						}
					}

					if ( ! empty( $product_fields ) ) {
						foreach ( $product_fields as $fkey => $fval ) {
							$products[ $hash_key ]->{$fkey} = $fval;
						}
					}

					$fields[ $hash_key ] = $product_fields;
					unset( $product_fields );
				}
			}

			$output                 = new stdClass();
			$offer_meta_data        = WFOCU_Core()->offers->get_offer( $offer_id );
			$offer_data             = new stdClass();
			$offer_data->products   = ( isset( $offer_meta_data ) && isset( $offer_meta_data->products ) ) ? $offer_meta_data->products : new stdClass();
			$offer_data->fields     = ( isset( $offer_meta_data ) && isset( $offer_meta_data->fields ) ) ? $offer_meta_data->fields : new stdClass();
			$offer_data->variations = ( isset( $offer_meta_data ) && isset( $offer_meta_data->variations ) ) ? $offer_meta_data->variations : new stdClass();
			$offer_data->settings   = ( isset( $offer_meta_data ) && isset( $offer_meta_data->settings ) ) ? $offer_meta_data->settings : WFOCU_Core()->offers->get_default_offer_setting();
			$offer_data->state      = ( isset( $offer_meta_data ) && isset( $offer_meta_data->state ) ) ? $offer_meta_data->state : '0';
			$offer_data->template   = ( isset( $offer_meta_data ) && isset( $offer_meta_data->template ) ) ? $offer_meta_data->template : WFOCU_Core()->template_loader->get_default_template( $offer_meta_data );

			if ( count( $products ) > 0 ) {
				unset( $hash );
				$output->products = new stdClass();
				foreach ( $products as $hash => $pr ) {
					$output->products->{$hash}     = $pr;
					$offer_data->products->{$hash} = $pr->id;
				}
			}

			if ( count( $fields ) > 0 ) {
				unset( $hash );
				$output->fields = new stdClass();
				foreach ( $fields as $hash => $field ) {
					$output->fields->{$hash}     = $field;
					$offer_data->fields->{$hash} = $field;
				}
			}
			if ( count( $variations ) > 0 ) {
				$variation = null;
				unset( $hash );
				$output->variations = new stdClass();
				if ( count( $variations ) > 0 ) {
					foreach ( $variations as $hash => $variation ) {
						if ( ! empty( $variation ) ) {
							$output->variations->{$hash}     = $variation;
							$offer_data->variations->{$hash} = $variation_save[ $hash ];
						}
					}
				} else {
					$offer_data->variations = new stdClass();
				}
			}

			/**
			 * @todo take care of multiple product template later
			 */
			if ( false === isset( $offer_data->template ) ) {
				$offer_data->template = WFOCU_Core()->template_loader->get_default_template( $offer_data );
			}

			$offer_data->settings = $options = WFOCU_Common::maybe_filter_boolean_strings( $offer_data->settings );
			WFOCU_Common::update_offer( $offer_id, $offer_data, $funnel_id );
			WFOCU_Common::update_funnel_time( $funnel_id );

			do_action( 'wfocu_offer_updated', $offer_data, $offer_id, $funnel_id );

			apply_filters( 'wfocu_offer_product_added', $output, $offer_data, $offer_id, $funnel_id );

			$resp['status'] = true;
			$resp['msg']    = __( 'Product saved to funnel', 'woofunnels-upstroke-one-click-upsell' );
			$resp['data']   = $output;
		}

		wp_send_json( $resp );
	}

	public static function remove_product() {
		$resp = array(
			'status' => false,
			'msg'    => '',
		);
		if ( isset( $_POST['funnel_id'] ) && $_POST['funnel_id'] > 0 && isset( $_POST['offer_id'] ) && $_POST['offer_id'] > 0 && isset( $_POST['product_key'] ) && $_POST['product_key'] != '' ) {

			$funnel_id   = $_POST['funnel_id'];
			$offer_id    = $_POST['offer_id'];
			$product_key = $_POST['product_key'];

			$updatable       = 0;
			$offer_meta_data = WFOCU_Core()->offers->get_offer( $offer_id );
			if ( isset( $offer_meta_data->products ) && isset( $offer_meta_data->products->{$product_key} ) ) {
				$updatable ++;
				unset( $offer_meta_data->products->{$product_key} );
			}
			if ( isset( $offer_meta_data->fields ) && isset( $offer_meta_data->fields->{$product_key} ) ) {
				$updatable ++;
				unset( $offer_meta_data->fields->{$product_key} );
			}
			if ( isset( $offer_meta_data->variations ) && isset( $offer_meta_data->variations->{$product_key} ) ) {
				$updatable ++;
				unset( $offer_meta_data->variations->{$product_key} );
			}

			if ( $updatable > 0 ) {

				WFOCU_Common::update_offer( $offer_id, $offer_meta_data, $funnel_id );
				WFOCU_Common::update_funnel_time( $funnel_id );
				do_action( 'wfocu_offer_updated', $offer_meta_data, $offer_id, $funnel_id );

				$resp = array(
					'status' => true,
					'msg'    => 'product removed from database',
				);
			}
		}
		wp_send_json( $resp );
	}

	public static function update_offer() {
		$resp = array(
			'msg'    => '',
			'status' => false,
		);
		if ( isset( $_POST['funnel_id'] ) && isset( $_POST['offer_id'] ) && isset( $_POST['step_name'] ) && $_POST['step_name'] != '' ) {
			$offer_id  = $_POST['offer_id'];
			$funnel_id = $_POST['funnel_id'];
			$title     = $_POST['step_name'];
			$post      = get_post( $offer_id );
			if ( ! is_wp_error( $post ) ) {
				$args        = array(
					'ID'         => $offer_id,
					'post_title' => $title,
					'post_name'  => $_POST['funnel_step_slug'],
				);
				$update_post = wp_update_post( $args );

				if ( ! is_wp_error( $update_post ) ) {
					WFOCU_Common::update_funnel_time( $funnel_id );
					$resp['status'] = true;
					$resp['msg']    = __( 'Offer Updated successfully', 'woofunnels-upstroke-one-click-upsell' );
					$resp['url']    = get_the_permalink( $offer_id );
					$resp['name']   = $title;
					$resp['slug']   = $_POST['funnel_step_slug'];
					$resp['type']   = $_POST['step_type'];
				}
			}
		}
		wp_send_json( $resp );
	}

	public static function update_funnel() {
		$resp = array(
			'msg'    => '',
			'status' => false,
		);
		if ( isset( $_POST['funnel_id'] ) ) {
			$funnel_id = $_POST['funnel_id'];
			$args      = array( 'ID' => $funnel_id );
			if ( $_POST['funnel_name'] != '' ) {
				$args['post_title'] = $_POST['funnel_name'];
			}
			if ( $_POST['funnel_desc'] != '' ) {
				$args['post_content'] = $_POST['funnel_desc'];
			}

			if ( count( $args ) > 1 ) {
				$post = wp_update_post( $args );
				if ( ! is_wp_error( $post ) ) {
					$resp['status'] = true;
					$resp['msg']    = __( 'funnel updated successfully', 'woofunnels-upstroke-one-click-upsell' );
					$resp['data']   = array(
						'name' => $args['post_title'],
					);
				}
			}
		}

		wp_send_json( $resp );
	}

	public static function removed_offer_from_funnel() {
		$resp = array(
			'msg'    => '',
			'status' => true,
		);
		if ( isset( $_POST['offer_id'] ) && $_POST['offer_id'] > 0 ) {
			$offer_id  = $_POST['offer_id'];
			$funnel_id = $_POST['funnel_id'];
			$status    = wp_delete_post( $offer_id, true );
			WFOCU_Common::update_funnel_time( $funnel_id );

			if ( null !== $status && false !== $status ) {
				$resp['status'] = true;
				$resp['msg']    = __( 'Offer Removed from funnel', 'woofunnels-upstroke-one-click-upsell' );
			}
			/**
			 * Updating overall funnel products
			 */
			$funnel_products = WFOCU_Core()->funnels->get_funnel_products( $funnel_id );
			if ( isset( $funnel_products[ $offer_id ] ) ) {
				unset( $funnel_products[ $offer_id ] );
			}
		}

		wp_send_json( $resp );
	}

	public static function update_rules() {
		$resp = array(
			'msg'    => '',
			'status' => false,
		);
		$data = array();
		wp_parse_str( $_POST['data'], $data );

		if ( isset( $data['funnel_id'] ) && $data['funnel_id'] > 0 && isset( $data['wfocu_rule'] ) && ! empty( $data['wfocu_rule'] ) > 0 ) {
			$funnel_id = $data['funnel_id'];
			$rules     = $data['wfocu_rule'];
			$post      = get_post( $funnel_id );

			if ( ! is_wp_error( $post ) ) {
				WFOCU_Common::update_funnel_rules( $funnel_id, $rules );
				WFOCU_Common::update_funnel_time( $funnel_id );
				$resp = array(
					'msg'    => __( 'Rules Updated successfully', 'woofunnels-upstroke-one-click-upsell' ),
					'status' => true,
				);
			}
		}
		wp_send_json( $resp );
	}

	/**
	 * @hooked over `wp_ajax_wfocu_calculate_shipping`
	 * When user accepts upsell and clicks on "add to my order" button, we check & calculate shipping for the current bucket
	 */
	public static function calculate_shipping() {

		check_ajax_referer( 'wfocu_front_calculate_shipping', 'nonce' );

		//prepare shipping call package
		$order_behavior = WFOCU_Core()->funnels->get_funnel_option( 'order_behavior' );
		$is_batching_on = ( 'batching' === $order_behavior ) ? true : false;

		$products                          = array();
		$get_order                         = WFOCU_Core()->data->get_current_order();
		$posted_data                       = WFOCU_Core()->offers->parse_posted_data();
		$existing_methods                  = array();
		$methods                           = $get_order->get_shipping_methods();
		$shipping                          = array();
		$old_shipping_cost                 = ( $is_batching_on ) ? array(
			'cost' => $get_order->get_shipping_total(),
			'tax'  => $get_order->get_shipping_tax(),
		) : array(
			'cost' => 0,
			'tax'  => 0,
		);
		$get_shipping_methods_from_session = WFOCU_Core()->data->get( 'chosen_shipping_methods', array() );

		//If parent order have a shipping applied
		if ( $methods && is_array( $methods ) && count( $methods ) ) {
			foreach ( $methods as $method ) {
				$method_id = WFOCU_WC_Compatibility::get_method_id( $method ) . ':' . WFOCU_WC_Compatibility::get_instance_id( $method );

				/**
				 * Detect if user opted free shipping in the previous order
				 * return from there straight away so we do not need to check for shipping, just apply it free
				 */
				if ( $is_batching_on && ( WFOCU_Core()->shipping->is_free_shipping( WFOCU_WC_Compatibility::get_method_id( $method ) ) ) ) {

					$get_free_shipping = array(
						$method_id => array(
							'method'       => WFOCU_WC_Compatibility::get_method_id( $method ),
							'label'        => $method->get_name(),
							'cost'         => 0,
							'shipping_tax' => 0,
						),
					);

					$response_ajax = array( 'success' => 'true' );

					$response_ajax['data'] = array(
						'free_shipping' => $get_free_shipping,
						'shipping'      => $shipping,
						'shipping_prev' => $old_shipping_cost,

					);

					wp_send_json( $response_ajax );

				}

				array_push( $existing_methods, $method_id );

				/**
				 * Since WooCommerce 2.1, WooCommerce allows to add multiple shipping methods in one single order
				 * The idea was to split a cart in some logical grouping, more info here https://www.xadapter.com/woocommerce-split-cart-items-order-ship-via-multiple-shipping-methods/
				 * For now we just need to break it after one iteration, so that we always know which shipping method we need to process & replace.
				 */

				//break;
			}
		} else {
			$response_ajax = array( 'success' => 'true' );

			$response_ajax['data'] = array(
				'free_shipping' => array(),
				'shipping'      => array(),
				'shipping_prev' => array(
					'cost' => 0,
					'tax'  => 0,
				),
			);

			wp_send_json( $response_ajax );

		}
		//if previous order have some shipping methods pass it as existing methods to calculate new shipping methods
		if ( ! empty( $get_shipping_methods_from_session ) ) {
			$existing_methods = $get_shipping_methods_from_session;
		}
		//If previous order opted free shipping then passing remove all shipping from existing methods
		if ( count( $existing_methods ) > 0 && WFOCU_Core()->shipping->is_free_shipping( $existing_methods[0] ) ) {
			$existing_methods = array();
		}

		$get_current_offer      = WFOCU_Core()->data->get( 'current_offer' );
		$get_current_offer_meta = WFOCU_Core()->offers->get_offer_meta( $get_current_offer );

		$build_offer_data = WFOCU_Core()->offers->build_offer_product( $get_current_offer_meta );

		/**
		 * In case of fixed shipping, cost is returned as shipping gateway array
		 */
		if ( 'flat' === $build_offer_data->shipping_preferece ) {
			$response_ajax = array( 'success' => 'true' );

			$response_ajax['data'] = array(
				'free_shipping' => array(),
				'shipping'      => array(
					'fixed' => array(
						'cost'         => $build_offer_data->shipping_fixed,
						'shipping_tax' => WFOCU_Core()->shipping->get_flat_shipping_rates( $build_offer_data->shipping_fixed ),
						'label'        => WFOCU_Core()->data->get_option( 'flat_shipping_label' ),
					),
				),
				'shipping_prev' => array(
					'cost' => 0,
					'tax'  => 0,
				),

			);

			wp_send_json( $response_ajax );

		} else {
			/**
			 * Add previous order items in the queue to checking shipping over
			 */
			if ( $is_batching_on ) {
				foreach ( $get_order->get_items() as $item_id => $item ) {

					$price = wc_get_order_item_meta( $item_id, '_line_total', true );
					if ( true === WFOCU_WC_Compatibility::display_prices_including_tax() ) {
						$price = $price + wc_get_order_item_meta( $item_id, '_line_tax', true );
					}

					array_push( $products, array(
						'product_id' => $item->get_variation_id() ? $item->get_variation_id() : $item->get_product_id(),
						'qty'        => $item->get_quantity(),
						'price'      => $price / $item->get_quantity(),
					) );
				}
			}

			$upsell_package = WFOCU_Core()->offers->prepare_shipping_package( $get_current_offer_meta, $posted_data );

			foreach ( $upsell_package as $item ) {
				$price = $item['price'];
				if ( true === WFOCU_WC_Compatibility::display_prices_including_tax() ) {
					$price = $item['price_with_tax'];
				}
				array_push( $products, array(
					'product_id'    => $item['product'],
					'qty'           => (int) $item['qty'],
					'price'         => $price / (int) $item['qty'],
					'offer_product' => true,
				) );
			}

			/**
			 * Setting the location
			 */
			$location = array( $get_order->get_shipping_country(), $get_order->get_shipping_state(), $get_order->get_shipping_city(), $get_order->get_shipping_postcode() );

			if ( class_exists( 'WooFunnels_UpStroke_Dynamic_Shipping' ) ) {
				$get_dynamic_shipping_module = WooFunnels_UpStroke_Dynamic_Shipping::instance();

				/**
				 * Calculate shipping
				 */
				$get_shipping = $get_dynamic_shipping_module->calculate_dynamic_shipping( $products, $location, $existing_methods, $get_order );

				$response_ajax = array( 'success' => 'true' );

				$response_ajax['data'] = wp_parse_args( $get_shipping, array(
					'free_shipping' => array(),
					'shipping'      => array(),
					'shipping_prev' => $old_shipping_cost,

				) );
			} else {
				$response_ajax['data'] = array(
					'free_shipping' => array(),
					'shipping'      => array(),
					'shipping_prev' => $old_shipping_cost,

				);
			}
		}

		wp_send_json( $response_ajax );
	}


	/**
	 * @hooked over `wp_ajax_wfocu_calculate_shipping_variation`
	 * On selection of variation, we try to get the shipping cost to show the user
	 */
	public static function calculate_shipping_variation() {
		check_ajax_referer( 'wfocu_front_calculate_shipping_variation', 'nonce' );
		$get_shipping = WFOCU_Core()->shipping->calculate_shipping_variation( $_POST['variationID'], $_POST['qty'] );;
		if ( $get_shipping['shipping'] && is_array( $get_shipping['shipping'] ) && count( $get_shipping['shipping'] ) ) {
			$current_shipping     = current( $get_shipping['shipping'] );
			$current_shipping_key = key( $get_shipping['shipping'] );

			wp_send_json( array(
				'diff'   => array(
					'cost' => ( $current_shipping['cost'] - $get_shipping['shipping_prev']['cost'] ),
					'tax'  => ( $current_shipping['shipping_tax'] - $get_shipping['shipping_prev']['tax'] ),
				),
				'method' => $current_shipping_key,
				'label'  => $current_shipping['label'],
			) );
		} elseif ( $get_shipping['free_shipping'] && is_array( $get_shipping['free_shipping'] ) && count( $get_shipping['free_shipping'] ) > 0 ) {
			$current_shipping_key = key( $get_shipping['free_shipping'] );
			$new_shipping         = 0;

			/**
			 * Continue from there
			 */
			wp_send_json( array(
				'diff'   => 0,
				'method' => $current_shipping_key,
				'label'  => '',
			) );
		}
		wp_send_json( array( 'diff' => 0 ) );

	}


	/**
	 * Register product and offer views
	 */
	public static function register_views() {
		check_ajax_referer( 'wfocu_front_register_views', 'nonce' );

		$data                 = $_POST['data'];
		$get_current_offer    = WFOCU_Core()->data->get_current_offer();
		$get_order            = WFOCU_Core()->data->get_current_order();
		$get_type_of_offer    = $data['offer_type'];
		$get_type_index_offer = $data['offer_type_index'];

		do_action( 'wfocu_offer_viewed_event', $get_current_offer, WFOCU_WC_Compatibility::get_order_id( $get_order ), WFOCU_Core()->data->get_funnel_id(), $get_type_of_offer, $get_type_index_offer, WFOCU_Core()->data->get( 'useremail' ) );

	}

	public static function page_search( $return = false ) {
		$args      = array(
			'post_type'        => 'page',
			'post_status'      => 'publish',
			'suppress_filters' => false,
			's'                => filter_input( INPUT_POST, 'term' ),
		);
		$page_list = new WP_Query( $args );
		$pages     = array();
		foreach ( $page_list->posts as $page_object ) {

			$get_assignment    = get_post_meta( $page_object->ID, '_wfocu_offer', true );
			$assignment_string = '';
			if ( '' !== $get_assignment ) {
				$get_assignment_offer = get_post( $get_assignment );
				if ( is_object( $get_assignment_offer ) ) {
					$assignment_string = sprintf( '(associated with : %s)', rawurldecode( $get_assignment_offer->post_title ) );
				}
			}

			$pages[] = array(
				'id'        => $page_object->ID,
				'page_name' => '#' . $page_object->ID . ': ' . rawurldecode( $page_object->post_title ) . ' ' . $assignment_string,
				'url'       => get_permalink( $page_object->ID ),
			);
		}
		wp_send_json( $pages );
	}

	public static function get_custom_page() {
		$offer_id = $_POST['offer_id'];
		$page_id  = $_POST['page_id'];

		update_post_meta( $offer_id, '_wfocu_custom_page', $page_id );
		update_post_meta( $page_id, '_wfocu_offer', $offer_id );
		$resp['status'] = true;
		$resp['msg']    = __( 'Product saved to funnel', 'woofunnels-upstroke-one-click-upsell' );
		$resp['data']   = array(
			'title' => get_the_title( $page_id ),
			'link'  => get_edit_post_link( $page_id ),
		);

		wp_send_json( $resp );
	}

	public static function update_template() {
		$offer     = $_POST['offer_id'];
		$funnel_id = $_POST['id'];

		$meta = get_post_meta( $offer, '_wfocu_setting', true );

		if ( is_object( $meta ) ) {
			$meta->template = sanitize_text_field( $_POST['template'] );
			update_post_meta( $offer, '_wfocu_setting', $meta );
		}
		if ( '' !== $funnel_id ) {
			WFOCU_Common::update_funnel_time( $funnel_id );
		}

		$resp['status'] = true;
		$resp['msg']    = __( 'Product saved to funnel', 'woofunnels-upstroke-one-click-upsell' );
		$resp['data']   = '';
		wp_send_json( $resp );
	}

	public static function save_funnel_settings() {
		$funnel_id = $_POST['funnel_id'];

		$options = $_POST['data'];
		if ( is_array( $options ) ) {

			$options['offer_success_message_pop'] = sanitize_textarea_field( $options['offer_success_message_pop'] );
			$options['offer_failure_message_pop'] = sanitize_textarea_field( $options['offer_failure_message_pop'] );
			$options['offer_wait_message_pop']    = sanitize_textarea_field( $options['offer_wait_message_pop'] );
			$options['funnel_priority']           = sanitize_text_field( $options['funnel_priority'] );
		}

		$options = WFOCU_Common::maybe_filter_boolean_strings( $options );

		WFOCU_Core()->funnels->save_funnel_options( $funnel_id, $options );
		WFOCU_Core()->funnels->save_funnel_priority( $funnel_id, $options['funnel_priority'] );
		WFOCU_Common::update_funnel_time( $funnel_id );

		$resp['status'] = true;
		$resp['msg']    = __( 'Settings are updated', 'woofunnels-upstroke-one-click-upsell' );
		$resp['data']   = '';
		wp_send_json( $resp );
	}

	public static function save_global_settings() {

		$options = $_POST['data'];

		if ( is_array( $options ) ) {

			$options['primary_order_status_title'] = sanitize_text_field( $options['primary_order_status_title'] );
			$options['ga_key']                     = sanitize_text_field( $options['ga_key'] );
			$options['fb_pixel_key']               = sanitize_text_field( $options['fb_pixel_key'] );
			$options['ttl_funnel']                 = sanitize_text_field( $options['ttl_funnel'] );
		}

		$options = WFOCU_Common::maybe_filter_boolean_strings( $options );

		/**
		 * This code ensures that intersection of supported and enabled gateways will used for checking to prevent any value/gateway to save when its not enabled.
		 *
		 */
		$supported_gateways = WFOCU_Core()->gateways->get_gateways_list();
		if ( isset( $options['gateways'] ) && $supported_gateways && is_array( $supported_gateways ) && count( $supported_gateways ) > 0 ) {
			$supported_gateways_keys = wp_list_pluck( $supported_gateways, 'value' );

			$parsed = array_filter( $options['gateways'], function ( $val ) use ( $supported_gateways_keys ) {
				return in_array( $val, $supported_gateways_keys );
			} );
			if ( isset( $options['gateways'] ) ) {
				$options['gateways'] = $parsed;
			}
		}

		if ( ! isset( $options['gateways'] ) ) {
			$options['gateways'] = [];
		}

		WFOCU_Core()->data->update_options( $options );
		update_option( 'wfocu_needs_rewrite', 'yes', true );
		$resp['status'] = true;
		$resp['msg']    = __( 'Settings Updated', 'woofunnels-upstroke-one-click-upsell' );
		$resp['data']   = '';
		wp_send_json( $resp );
	}

	public static function preview_details() {
		$resp = array(
			'msg'    => '',
			'status' => true,
		);
		if ( isset( $_POST['funnel_id'] ) && $_POST['funnel_id'] > 0 ) {
			$funnel_id    = $_POST['funnel_id'];
			$funnel_post  = get_post( $funnel_id );
			$data_funnels = WFOCU_Core()->funnels->get_funnel_offers_admin( $funnel_id );

			$resp['status']      = ( 'publish' == $funnel_post->post_status ) ? __( 'Active', 'woofunnels-upstroke-one-click-upsell' ) : __( 'Deactivated', 'woofunnels-upstroke-one-click-upsell' );
			$resp['funnel_id']   = $funnel_id;
			$resp['funnel_name'] = get_the_title( $funnel_post );
			$resp['launch_url']  = admin_url( 'admin.php' ) . '?page=upstroke&section=rules&edit=' . $funnel_id;
			$resp['offers']      = array();
			if ( ! empty( $data_funnels['steps'] ) ) {
				foreach ( $data_funnels['steps'] as $key => $steps ) {
					$resp['offers'][ $key ]                = array();
					$resp['offers'][ $key ]['offer_name']  = $steps['name'];
					$resp['offers'][ $key ]['offer_state'] = $steps['state'];
					$resp['offers'][ $key ]['offer_type']  = ucfirst( $steps['type'] );

					$get_offer      = $data_funnels['offers'][ $steps['id'] ];
					$product_offers = array();
					if ( $get_offer->products && 0 < count( get_object_vars( $get_offer->products ) ) ) {
						foreach ( $get_offer->products as $key_offer_product => $product ) {

							$qty = $product->quantity;
							if ( 'percentage' === $product->discount_type ) {
								$discount = $product->discount_amount . '%';
							} else {
								$discount = '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol() . '</span>' . $product->discount_amount;
							}
							array_push( $product_offers, $product->name . ' x' . $qty . ' @' . $discount );
						}
					}
					$resp['offers'][ $key ]['offer_products'] = implode( ', ', $product_offers );
				}
				$resp['launch_url'] = admin_url( 'admin.php' ) . '?page=upstroke&section=offers&edit=' . $funnel_id;
			}
			$resp['msg'] = __( 'Funnel Data from funnel', 'woofunnels-upstroke-one-click-upsell' );
		}
		wp_send_json( $resp );
	}

	public static function duplicate_funnel() {
		$resp = array(
			'msg'    => '',
			'status' => true,
		);
		if ( isset( $_POST['funnel_id'] ) && $_POST['funnel_id'] > 0 ) {
			$funnel_id    = $_POST['funnel_id'];
			$funnel_post  = get_post( $funnel_id );
			$data_funnels = WFOCU_Core()->funnels->get_funnel_offers_admin( $funnel_id );

			$funnel_name_new = get_the_title( $funnel_post ) . ' Copy';
			$funnel_desc     = $funnel_post->post_content;

			$funnel_post_type = WFOCU_Common::get_funnel_post_type_slug();
			$funnel_post_new  = array(
				'post_title'   => $funnel_name_new, // @codingStandardsIgnoreLine
				'post_type'    => $funnel_post_type,
				'post_status'  => 'wfocu-disabled',
				'post_content' => $funnel_desc,
			);

			$new_funnel_id = wp_insert_post( $funnel_post_new );

			if ( ! is_wp_error( $new_funnel_id ) && $new_funnel_id ) {
				$funnel_rules       = get_post_meta( $funnel_id, '_wfocu_rules', true );
				$funnel_rules_saved = get_post_meta( $funnel_id, '_wfocu_is_rules_saved', true );
				$funnel_settings    = get_post_meta( $funnel_id, '_wfocu_settings', true );

				update_post_meta( $new_funnel_id, '_wfocu_rules', $funnel_rules );
				update_post_meta( $new_funnel_id, '_wfocu_is_rules_saved', $funnel_rules_saved );
				update_post_meta( $new_funnel_id, '_wfocu_settings', $funnel_settings );

				WFOCU_Common::update_funnel_time( $new_funnel_id );

			} else {
				$resp['msg']    = is_wp_error( $new_funnel_id ) ? $new_funnel_id->get_error_message() : 'Error in duplicating funnel, Please try again later!!!';
				$resp['status'] = false;
				wp_send_json( $resp );
			}
			$new_steps = array();

			if ( ! empty( $data_funnels['steps'] ) ) {
				foreach ( $data_funnels['steps'] as $key => $steps ) {

					$offer_name_new  = $steps['name'] . ' Copy';
					$offer_state_new = $steps['state'];

					$offer_post_type = WFOCU_Common::get_offer_post_type_slug();
					$offer_post_new  = array(
						'post_title'  => $offer_name_new, // @codingStandardsIgnoreLine
						'post_type'   => $offer_post_type,
						'post_status' => 'publish',
					);

					$offer_id_new = wp_insert_post( $offer_post_new );
					if ( ! is_wp_error( $offer_id_new ) && $offer_id_new ) {
						$get_offer      = $steps['id'];
						$offer_type_new = $steps['type'];
						$offer_settings = get_post_meta( $get_offer, '_wfocu_setting', true );
						$custom_page_id = get_post_meta( $get_offer, '_wfocu_custom_page', true );

						$offer_custom = get_option( 'wfocu_c_' . $get_offer, '' );

						update_post_meta( $offer_id_new, '_funnel_id', $new_funnel_id );
						update_post_meta( $offer_id_new, '_offer_type', $offer_type_new );
						update_post_meta( $offer_id_new, '_wfocu_setting', $offer_settings );
						update_post_meta( $offer_id_new, '_wfocu_funnel_id', $new_funnel_id );
						update_post_meta( $offer_id_new, '_wfocu_edit_last', time() );
						if ( ! empty( $custom_page_id ) ) {
							update_post_meta( $offer_id_new, '_wfocu_custom_page', $custom_page_id );

						}
						if ( ! empty( $offer_custom ) ) {
							update_option( 'wfocu_c_' . $offer_id_new, $offer_custom );
						}

						$new_offer_slug = get_post( $offer_id_new )->post_name;

						$new_step['id']    = $offer_id_new;
						$new_step['name']  = $offer_name_new;
						$new_step['type']  = $offer_type_new;
						$new_step['state'] = $offer_state_new;
						$new_step['slug']  = $new_offer_slug;
						$new_step['url']   = get_site_url() . '?wfocu_offer=' . $new_offer_slug;
						array_push( $new_steps, $new_step );
					} else {
						$resp['msg']    = is_wp_error( $new_funnel_id ) ? $new_funnel_id->get_error_message() : 'Error in duplicating offer, Please try again later!!!';
						$resp['status'] = false;
						wp_send_json( $resp );
						break;
					}
				}
			}

			update_post_meta( $new_funnel_id, '_funnel_steps', $new_steps );
			$new_funnel_upsell_downsells = WFOCU_Core()->funnels->prepare_upsell_downsells( $new_steps );
			update_post_meta( $new_funnel_id, '_funnel_upsell_downsell', $new_funnel_upsell_downsells );

			$resp['msg']    = __( 'Funnel duplicated successfully.', 'woofunnels-upstroke-one-click-upsell' );
			$resp['status'] = true;
		}
		wp_send_json( $resp );
	}

	//Duplicating a funnel ajax function

	public static function toggle_funnel_state() {
		$resp = array(
			'msg'    => '',
			'status' => true,
		);
		if ( isset( $_POST['id'] ) && $_POST['id'] > 0 ) {
			$funnel_id = $_POST['id'];

			if ( 'true' === $_POST['state'] ) {
				$status = 'publish';
			} else {
				$status = WFOCU_SLUG . '-disabled';
			}
			wp_update_post( array(
				'ID'          => $funnel_id,
				'post_status' => $status,
			) );

		}
		wp_send_json( $resp );
	}

	/**
	 * Maybe hide php errors from coming to the page in woocommerce ajax on live environments
	 */
	public static function maybe_set_error_reporting_false() {

		if ( self::is_wfocu_front_ajax() ) {

			if ( ! WP_DEBUG || ( WP_DEBUG && ! WP_DEBUG_DISPLAY ) ) {
				@ini_set( 'display_errors', 0 ); // Turn off display_errors during AJAX events to prevent malformed JSON.
			}
			$GLOBALS['wpdb']->hide_errors();
		}

	}

	public static function is_wfocu_front_ajax() {

		if ( defined( 'DOING_AJAX' ) && true === DOING_AJAX && null !== filter_input( INPUT_POST, 'action' ) && false !== strpos( filter_input( INPUT_POST, 'action' ), 'wfocu_front' ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Maybe hide php errors from coming to the page in woocommerce ajax on live environments
	 */
	public static function catch_error() {

		check_ajax_referer( 'wfocu_front_catch_error', 'nonce' );
		$get_order = WFOCU_Core()->data->get_order();
		WFOCU_Core()->log->log( 'Order: #' . WFOCU_WC_Compatibility::get_order_id( $get_order ) . ' JS Error logged: ' . filter_input( INPUT_POST, 'message' ) );

	}

	public static function save_template() {
		$resp = array(
			'msg'    => '',
			'status' => false,
		);
		if ( isset( $_POST['template_name'] ) && '' !== $_POST['template_name'] && isset( $_POST['offer_id'] ) ) {

			$offer_id = absint( $_POST['offer_id'] );
			if ( $offer_id > 0 ) {
				$template_name = trim( $_POST['template_name'] );
				$customize_key = WFOCU_SLUG . '_c_' . $offer_id;
				$template_data = get_option( $customize_key, [] );

				if ( is_array( $template_data ) && count( $template_data ) > 0 ) {

					foreach ( $template_data as $tkey => $tval ) {
						if ( false !== strpos( $tkey, 'wfocu_product' ) ) {
							unset( $template_data[ $tkey ] );
						}
					}

					$template_names                   = get_option( 'wfocu_template_names', [] );
					$template_slug                    = sanitize_title( $template_name );
					$template_slug                    .= '_' . time();
					$template_names[ $template_slug ] = [
						'name' => $template_name,
						'time' => time(),
					];
					update_option( 'wfocu_template_names', $template_names );
					update_option( $template_slug, $template_data, 'no' );

					ob_start();
					?>
                    <span class="customize-inside-control-row wfocu_template_holder">
					<input type="radio" value="<?php echo $template_slug; ?>" name="wfocu_save_templates" id="wfocu_save_templates_<?php echo $template_slug; ?>" class="wfocu_template">
					<label for="wfocu_save_templates_<?php echo $template_slug; ?>"><?php echo $template_name; ?></label>
						<a href="javascript:void(0);" class="wfocu_delete_template" data-slug="<?php echo $template_slug; ?>"><?php _e( 'Delete', 'woofunnels-upstroke-one-click-upsell' ); ?></a>
						<span class="wfocu-ajax-delete-loader hide"><img src="<?php echo admin_url( 'images/spinner.gif' ); ?>"></span>
					</span>
					<?php
					$message        = ob_get_clean();
					$resp['msg']    = $message;
					$resp['status'] = true;
				}
			}
		}
		wp_send_json( $resp );
	}

	public static function apply_template() {
		$resp = array(
			'msg'    => '',
			'status' => true,
		);
		if ( isset( $_POST['template_slug'] ) && '' !== $_POST['template_slug'] && isset( $_POST['offer_id'] ) ) {

			$offer_id = absint( $_POST['offer_id'] );
			if ( $offer_id > 0 ) {
				$template_name = trim( $_POST['template_slug'] );
				$current_data  = get_option( WFOCU_SLUG . '_c_' . $offer_id, [] );
				$data          = get_option( $template_name, [] );
				if ( is_array( $data ) && count( $data ) > 0 ) {
					foreach ( $data as $key => $val ) {
						$current_data[ $key ] = $val;
					}
					update_option( WFOCU_SLUG . '_c_' . $offer_id, $current_data );
					$resp['status'] = true;
				}
			}
		}
		wp_send_json( $resp );
	}

	public static function delete_template() {
		$resp = array(
			'msg'    => '',
			'status' => true,
		);
		if ( isset( $_POST['template_slug'] ) && '' !== $_POST['template_slug'] && isset( $_POST['offer_id'] ) ) {

			$offer_id = absint( $_POST['offer_id'] );
			if ( $offer_id > 0 ) {
				$template_name  = trim( $_POST['template_slug'] );
				$template_names = get_option( 'wfocu_template_names', [] );
				if ( isset( $template_names[ $template_name ] ) ) {
					unset( $template_names[ $template_name ] );
					update_option( 'wfocu_template_names', $template_names );
					delete_option( $template_name );
					$resp['status'] = true;
				}
			}
		}
		wp_send_json( $resp );
	}

	/**
	 * Handling refund offer request
	 *
	 * @throws Exception
	 */
	public static function refund_offer() {

		check_ajax_referer( 'wfocu_admin_refund_offer', 'nonce' );

		$refund_data = $_POST;
		$order_id    = isset( $refund_data['order_id'] ) ? $refund_data['order_id'] : 0;
		$amount      = isset( $refund_data['amt'] ) ? $refund_data['amt'] : '';
		$offer_id    = isset( $refund_data['offer_id'] ) ? $refund_data['offer_id'] : '';
		$txn_id      = isset( $refund_data['txn_id'] ) ? $refund_data['txn_id'] : '';
		$funnel_id   = isset( $refund_data['funnel_id'] ) ? $refund_data['funnel_id'] : '';

		$result['success'] = false;
		$result['msg']     = __( 'Refund Failed', 'woofunnels-upstroke-one-click-upsell' );
		$refund_txn_id     = false;

		WFOCU_Core()->log->log( "Info: Beginning refund for order {$order_id} for the amount of {$amount}" );

		if ( $order_id ) {

			$order = wc_get_order( $order_id );

			$payment_method = $order->get_payment_method();

			$gateway = WFOCU_Core()->gateways->get_integration( $payment_method );

			if ( $gateway->is_refund_supported() ) {
				$refund_txn_id = $gateway->process_refund_offer( $order );
			}
			$result = array();

			if ( false !== $refund_txn_id ) {

				$refunded_offers = $order->get_meta( '_wfocu_refunded_offers', true );
				if ( empty( $refunded_offers ) ) {
					$refunded_offers = get_post_meta( $order_id, '_wfocu_refunded_offers', true );
				}
				$refunded_offers = empty( $refunded_offers ) ? array() : $refunded_offers;

				array_push( $refunded_offers, $offer_id );
				update_post_meta( $order_id, '_wfocu_refunded_offers', $refunded_offers );

				$refund_reason = sprintf( __( ' - Reason: refunded offer ID: %1$d and Transaction ID: %2$s', 'woofunnels-upstroke-one-click-upsell' ), $order_id, $refund_txn_id );

				$refund = wc_create_refund( array(
					'amount'         => $amount,
					'reason'         => $refund_reason,
					'order_id'       => $order_id,
					'refund_payment' => false,
					'restock_items'  => true,
				) );

				do_action( 'wfocu_offer_refunded_event', $order_id, $funnel_id, $offer_id, $refund_txn_id, $txn_id, $amount );

				WFOCU_Core()->log->log( 'WFOCU Offer refund object: ' . print_r( $refund, true ) );

				$result['success'] = true;
				$result['msg']     = __( 'Refund Successful', 'woofunnels-upstroke-one-click-upsell' );
			}
		}
		wp_send_json( $result );
	}
}

WFOCU_AJAX_Controller::init();
