<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * WC_Gateway_Stripe_Addons class.
 *
 * @extends WC_Gateway_Stripe
 */
class WFOCU_Gateway_Integration_Stripe extends WFOCU_Gateway {


	public $key = 'stripe';
	public $token = false;
	protected static $ins = null;

	/**
	 * Constructor
	 */
	public function __construct() {

		parent::__construct();
		add_filter( 'wc_stripe_force_save_source', array( $this, 'should_tokenize_stripe' ), 999 );
		add_filter( 'wc_stripe_display_save_payment_method_checkbox', array( $this, 'maybe_hide_save_payment' ), 999 );
		add_filter( 'wc_stripe_3ds_source', array( $this, 'maybe_modify_3ds_prams' ), 10, 2 );

		add_action( 'wc_gateway_stripe_process_response', array( $this, 'maybe_handle_redirection_stripe' ), 10, 2 );

		add_action( 'wc_gateway_stripe_process_redirect_payment', array( $this, 'maybe_log_process_redirect' ), 1 );

		add_action( 'wfocu_offer_new_order_created_stripe', array( $this, 'add_stripe_payouts_to_new_order' ), 10, 2 );

	}

	public function maybe_hide_save_payment( $is_show ) {
		if ( false !== $this->should_tokenize() ) {
			return false;
		}

		return $is_show;
	}


	public function should_tokenize_stripe( $save_token ) {

		if ( false !== $this->should_tokenize() ) {

			return true;
		}

		return $save_token;
	}

	/**
	 * Try and get the payment token saved by the gateway
	 *
	 * @param WC_Order $order
	 *
	 * @return true on success false otherwise
	 */
	public function has_token( $order ) {
		$get_id = WFOCU_WC_Compatibility::get_order_id( $order );

		$this->token = get_post_meta( $get_id, '_wfocu_stripe_source_id', true );
		if ( $this->token === '' ) {
			$this->token = get_post_meta( $get_id, '_stripe_source_id', true );

		}

		if ( ! empty( $this->token ) ) {
			return true;
		}

		return false;

	}


	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self;
		}

		return self::$ins;
	}

	public function process_charge( $order ) {

		$is_successful = false;
		try {
			$gateway = $this->get_wc_gateway();

			$source   = $gateway->prepare_order_source( $order );
			$response = WC_Stripe_API::request( $this->generate_payment_request( $order, $source ) );

			WFOCU_Core()->log->log( print_r( $response, true ) );

			if ( is_wp_error( $response ) ) {
				WFOCU_Core()->log->log( 'Order #' . WFOCU_WC_Compatibility::get_order_id( $order ) . ': Payment Failed For Stripe' );
			} else {
				if ( ! empty( $response->error ) ) {
					$is_successful = false;
					WFOCU_Core()->log->log( 'Order #' . WFOCU_WC_Compatibility::get_order_id( $order ) . ': Payment Failed For Stripe' );

				} else {
					WFOCU_Core()->data->set( '_transaction_id', $response->id );

					$is_successful = true;
				}
			}
		} catch ( Exception $e ) {
			WFOCU_Core()->log->log( 'Order #' . WFOCU_WC_Compatibility::get_order_id( $order ) . ': Payment Failed For Stripe' );
			WFOCU_Core()->log->log( print_r( $e->getMessage() ) );
		}

		if ( true === $is_successful ) {

			$fee = ! empty( $response->balance_transaction->fee ) ? WC_Stripe_Helper::format_balance_fee( $response->balance_transaction, 'fee' ) : 0;
			$net = ! empty( $response->balance_transaction->net ) ? WC_Stripe_Helper::format_balance_fee( $response->balance_transaction, 'net' ) : 0;

			/**
			 * Handling for Stripe Fees
			 */
			$order_behavior = WFOCU_Core()->funnels->get_funnel_option( 'order_behavior' );
			$is_batching_on = ( 'batching' === $order_behavior ) ? true : false;
			if ( true === $is_batching_on ) {
				$fee = $fee + WC_Stripe_Helper::get_stripe_fee( $order );
				$net = $net + WC_Stripe_Helper::get_stripe_net( $order );
				WC_Stripe_Helper::update_stripe_fee( $order, $fee );
				WC_Stripe_Helper::update_stripe_net( $order, $net );
			} else {
				$currency = ! empty( $response->balance_transaction->currency ) ? strtoupper( $response->balance_transaction->currency ) : null;

				WFOCU_Core()->data->set( 'wfocu_stripe_fee', $fee );
				WFOCU_Core()->data->set( 'wfocu_stripe_net', $net );
				WFOCU_Core()->data->set( 'wfocu_stripe_currency', $currency );

			}
		}

		return $this->handle_result( $is_successful );
	}

	/**
	 * Generate the request for the payment.
	 *
	 * @param  WC_Order $order
	 * @param  object $source
	 *
	 * @return array()
	 */
	protected function generate_payment_request( $order, $source ) {
		$get_package = WFOCU_Core()->data->get( '_upsell_package' );

		$gateway                  = $this->get_wc_gateway();
		$post_data                = array();
		$post_data['currency']    = strtolower( WFOCU_WC_Compatibility::get_order_currency( $order ) );
		$post_data['amount']      = WC_Stripe_Helper::get_stripe_amount( $get_package['total'], $post_data['currency'] );
		$post_data['description'] = sprintf( __( '%1$s - Order %2$s - 1 click upsell: %3$s', 'woocommerce-gateway-stripe' ), wp_specialchars_decode( get_bloginfo( 'name' ), ENT_QUOTES ), $order->get_order_number(), 1211 );
		$post_data['capture']     = $gateway->capture ? 'true' : 'false';
		$billing_first_name       = WFOCU_WC_Compatibility::get_billing_first_name( $order );
		$billing_last_name        = WFOCU_WC_Compatibility::get_billing_last_name( $order );
		$billing_email            = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_email' );

		if ( ! empty( $billing_email ) && apply_filters( 'wc_stripe_send_stripe_receipt', false ) ) {
			$post_data['receipt_email'] = $billing_email;
		}
		$metadata              = array(
			__( 'customer_name', 'woocommerce-gateway-stripe' )  => sanitize_text_field( $billing_first_name ) . ' ' . sanitize_text_field( $billing_last_name ),
			__( 'customer_email', 'woocommerce-gateway-stripe' ) => sanitize_email( $billing_email ),
			'order_id'                                           => $this->get_order_number( $order ),
		);
		$post_data['expand[]'] = 'balance_transaction';
		$post_data['metadata'] = apply_filters( 'wc_stripe_payment_metadata', $metadata, $order, $source );

		if ( $source->customer ) {
			$post_data['customer'] = $source->customer;
		}

		if ( $source->source ) {

			$get_secondary_source = $order->get_meta( '_wfocu_stripe_source_id', true );
			$post_data['source']  = ( '' !== $get_secondary_source ) ? $get_secondary_source : $source->source;
		}

		return apply_filters( 'wc_stripe_generate_payment_request', $post_data, $order, $source );
	}

	public function maybe_modify_3ds_prams( $threds_data, $order ) {

		$order->update_meta_data( '_wfocu_stripe_source_id', $threds_data['three_d_secure']['card'] );
		$order->save();

		return $threds_data;
	}

	/**
	 * Get the wc-api URL to redirect to
	 *
	 * @param string $action checkout action, either `set_express_checkout or `get_express_checkout_details`
	 *
	 * @return string URL
	 * @since 2.0
	 */
	public function get_callback_url( $action ) {
		return add_query_arg( 'action', $action, WC()->api_request_url( 'wfocu_stripe' ) );
	}

	/**
	 * Maybe Handle PayPal Redirection for 3DS Checkout
	 * Let our hooks modify the order received url and redirect user to offer page.
	 *
	 * @param $response
	 * @param WC_Order $order
	 */
	public function maybe_handle_redirection_stripe( $response, $order ) {

		if ( false === $this->is_enabled() ) {
			WFOCU_Core()->log->log( 'Do not initiate redirection for stripe: Stripe is disabled' );

		}

		/**
		 * Validate if its a redirect checkout call for the stripe
		 * Validate if funnel initiation happened.
		 */
		if ( 1 === did_action( 'wfocu_front_init_funnel_hooks' ) && 1 === did_action( 'wc_gateway_stripe_process_redirect_payment' ) ) {
			$get_url = $order->get_checkout_order_received_url();
			wp_redirect( $get_url );
			exit();
		}

	}

	public function maybe_log_process_redirect() {
		WFOCU_Core()->log->log( 'Entering: ' . __CLASS__ . '::' . __FUNCTION__ );
	}

	/**
	 * @param WC_Order $order
	 * @param Integer $transaction
	 */
	public function add_stripe_payouts_to_new_order( $order, $transaction ) {
		$fee = WFOCU_Core()->data->get( 'wfocu_stripe_fee' );
		$net = WFOCU_Core()->data->get( 'wfocu_stripe_net' );

		$currency = WFOCU_Core()->data->get( 'wfocu_stripe_currency' );
		WC_Stripe_Helper::update_stripe_currency( $order, $currency );
		WC_Stripe_Helper::update_stripe_fee( $order, $fee );
		WC_Stripe_Helper::update_stripe_net( $order, $net );
		$order->save_meta_data();
	}


}


WFOCU_Gateway_Integration_Stripe::get_instance();
