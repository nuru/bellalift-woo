<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * WFOCU_Gateway_Integration_WFOCU_Test class.
 *
 * @extends WFOCU_Gateway
 */
class WFOCU_Gateway_Integration_WFOCU_Test extends WFOCU_Gateway {


	public $key = 'wfocu_test';
	public $token = false;
	protected static $ins = null;


	/**
	 * Constructor
	 */
	public function __construct() {

		parent::__construct();

	}

	/**
	 * Try and get the payment token saved by the gateway
	 *
	 * @param WC_Order $order
	 *
	 * @return true on success false otherwise
	 */
	public function has_token( $order ) {

		return true;

	}


	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self;
		}

		return self::$ins;
	}


	/**
	 * If this gateway is used in the payment for the primary order that means we can run our funnels and we do not need to check for further enable.
	 * @return true
	 */
	public function is_enabled( $order = false ) {
		return true;
	}

	public function process_charge( $order ) {

		$is_successful = true;

		return $this->handle_result( $is_successful, '' );
	}


}


WFOCU_Gateway_Integration_COD::get_instance();
