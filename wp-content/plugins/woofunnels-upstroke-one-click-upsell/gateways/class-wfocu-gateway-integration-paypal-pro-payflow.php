<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * WC_Gateway_Stripe_Addons class.
 *
 * @extends WC_Gateway_Stripe
 */
class WFOCU_Gateway_Integration_PayPal_Pro_Payflow extends WFOCU_Gateway {


	public $key = 'paypal_pro_payflow';
	public $token = false;
	protected static $ins = null;

	/** @var array the request parameters */
	private $parameters = array();
	private $response_params = array();


	/**
	 * Constructor
	 */
	public function __construct() {

		parent::__construct();

	}

	/**
	 * Try and get the payment token saved by the gateway
	 *
	 * @param WC_Order $order
	 *
	 * @return true on success false otherwise
	 */
	public function has_token( $order ) {
		$get_id = WFOCU_WC_Compatibility::get_order_id( $order );

		$this->token = get_post_meta( $get_id, '_transaction_id', true );

		if ( ! empty( $this->token ) ) {
			return true;
		}

		return false;

	}

	public function get_token( $order ) {

		$get_id = WFOCU_WC_Compatibility::get_order_id( $order );

		if ( false == $this->token ) {
			return $this->token;
		}
		$this->token = $order->get_meta( '_transaction_id' );
		if ( '' == $this->token ) {
			$this->token = get_post_meta( $get_id, '_transaction_id', true );
		}
		if ( ! empty( $this->token ) ) {
			return $this->token;
		}

		return false;
	}


	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self;
		}

		return self::$ins;
	}


	public function charge_upsell_payment( $order ) {

		$this->set_api_credentials( $this->get_wc_gateway()->get_option( 'paypal_user' ), $this->get_wc_gateway()->get_option( 'paypal_partner' ), $this->get_wc_gateway()->get_option( 'paypal_vendor' ), $this->get_wc_gateway()->get_option( 'paypal_password' ), $this->get_wc_gateway()->get_option( 'paymentaction' ) );

		$this->set_post_data( $order );

		$request         = new stdClass();
		$request->path   = '';
		$request->method = 'POST';
		$request->body   = $this->to_string();

		if ( $this->get_wc_gateway()->debug ) {
			$log         = $this->get_parameters();
			$log['ACCT'] = '****';
			$log['CVV2'] = '****';
			$this->get_wc_gateway()->log( 'Do payment request ' . print_r( $log, true ) );
		}

		//      $response = wp_remote_post( $url, array(
		//          'method'      => 'POST',
		//          'body'        => urldecode( http_build_query( apply_filters( 'woocommerce-gateway-paypal-pro_payflow_request', $post_data, $order ), null, '&' ) ),
		//          'timeout'     => 70,
		//          'user-agent'  => 'WooCommerce',
		//          'httpversion' => '1.1',
		//      ) );

		return $this->perform_request( $request );
	}

	/**
	 * Return the parsed response object for the request
	 *
	 * @since 2.2.0
	 *
	 * @param string $raw_response_body
	 *
	 * @return object response class instance which implements SV_WC_API_Request
	 */
	protected function get_parsed_response( $raw_response_body ) {

		wp_parse_str( urldecode( $raw_response_body ), $this->response_params );

		return $this->response_params;
	}

	/**
	 * Get the request URI
	 *
	 * @since 2.2.0
	 * @return string
	 */
	protected function get_request_uri() {

		// API base request URI + any request-specific path
		$uri = $this->get_wc_gateway()->get_option( 'testmode' ) ? $this->get_wc_gateway()->testurl : $this->get_wc_gateway()->liveurl;

		/**
		 * Request URI Filter.
		 *
		 * Allow actors to filter the request URI. Note that child classes can override
		 * this method, which means this filter may be invoked prior to the overridden
		 * method.
		 *
		 * @since 4.1.0
		 *
		 * @param string $uri current request URI
		 * @param \WFOCU_SV_API_Base class instance
		 */
		return apply_filters( 'wc_' . $this->get_api_id() . '_api_request_uri', $uri, $this );
	}

	public function set_api_credentials( $user, $partner, $vendor, $password, $payment_action ) {

		// PayPal requires HTTP 1.1
		$this->request_http_version = '1.1';

		$this->paypal_user     = $user;
		$this->paypal_vendor   = $vendor;
		$this->paypal_partner  = $partner;
		$this->paypal_password = $password;
		$this->paymentaction   = $payment_action;
	}

	/**
	 * Get a list of parameters to send to paypal.
	 *
	 * @param object $order Order object.
	 *
	 * @return array
	 */
	protected function set_post_data( $order ) {

		$package = WFOCU_Core()->data->get( '_upsell_package' );

		$post_data            = array();
		$post_data['USER']    = $this->paypal_user;
		$post_data['VENDOR']  = $this->paypal_vendor;
		$post_data['PARTNER'] = $this->paypal_partner;
		$post_data['PWD']     = $this->paypal_password;
		$post_data['TENDER']  = 'C'; // Credit card
		$post_data['TRXTYPE'] = 'S'; // Sale / Authorize

		// Transaction Amount = Total Tax Amount + Total Freight Amount + Total Handling Amount + Total Line Item Amount.
		$post_data['AMT']          = $package['total'];
		$post_data['CURRENCY']     = ( version_compare( WC_VERSION, '3.0', '<' ) ? $order->get_order_currency() : $order->get_currency() ); // Currency code
		$post_data['CUSTIP']       = $this->get_user_ip(); // User IP Address
		$post_data['EMAIL']        = version_compare( WC_VERSION, '3.0', '<' ) ? $order->billing_email : $order->get_billing_email();
		$post_data['INVNUM']       = $this->get_order_number( $order );
		$post_data['BUTTONSOURCE'] = 'WooThemes_Cart';
		$post_data['ORIGID']       = $this->get_token( $order );

		if ( isset( $this->get_wc_gateway()->soft_descrriptor ) ) {
			$post_data['MERCHDESCR'] = $this->get_wc_gateway()->soft_descrriptor;
		}

		/* Send Item details */
		$item_loop = 0;

		if ( isset( $package['products'] ) && is_array( $package['products'] ) && count( $package['products'] ) > 0 ) {
			$ITEMAMT = 0;

			foreach ( $package['products'] as $product_data ) {

				if ( $product_data['qty'] ) {
					$post_data[ 'L_NAME' . $item_loop ] = $product_data['data']->get_name();
					$post_data[ 'L_COST' . $item_loop ] = wc_format_decimal( $product_data['price'], 2 );
					$post_data[ 'L_QTY' . $item_loop ]  = $product_data['qty'];

					if ( $product_data['data']->get_sku() ) {
						$post_data[ 'L_SKU' . $item_loop ] = $product_data['data']->get_sku();
					}

					$ITEMAMT += $product_data['args']['total'];

					$item_loop ++;
				}
			}

			//          // Shipping.
			//          if ( $order->get_total_shipping() > 0 ) {
			//              $post_data['FREIGHTAMT'] = wc_format_decimal( $order->get_total_shipping(), 2 );
			//          }

			// Discount.
			if ( $order->get_total_discount( true ) > 0 ) {
				$post_data['DISCOUNT'] = wc_format_decimal( $order->get_total_discount( true ), 2 );
			}

			// Tax.
			if ( $order->get_total_tax() > 0 ) {
				$post_data['TAXAMT'] = wc_format_decimal( $order->get_total_tax(), 2 );
			}

			$post_data['ITEMAMT'] = wc_format_decimal( $ITEMAMT, 2 );
		}

		$pre_wc_30 = version_compare( WC_VERSION, '3.0', '<' );

		$post_data['ORDERDESC'] = 'Order ' . $this->get_order_number( $order ) . ' on ' . wp_specialchars_decode( get_bloginfo( 'name' ), ENT_QUOTES );
		$post_data['FIRSTNAME'] = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_first_name' );
		$post_data['LASTNAME']  = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_last_name' );
		$post_data['STREET']    = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_address_1' );
		$post_data['COUNTRY']   = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_country' );
		$post_data['CITY']      = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_city' );
		$post_data['STATE']     = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_state' );
		$post_data['ZIP']       = WFOCU_WC_Compatibility::get_order_data( $order, 'billing_postcode' );

		if ( $pre_wc_30 ? $order->shipping_address_1 : $order->get_shipping_address_1() ) {
			$post_data['SHIPTOFIRSTNAME'] = WFOCU_WC_Compatibility::get_order_data( $order, 'shipping_first_name' );
			$post_data['SHIPTOLASTNAME']  = WFOCU_WC_Compatibility::get_order_data( $order, 'shipping_last_name' );
			$post_data['SHIPTOSTREET']    = WFOCU_WC_Compatibility::get_order_data( $order, 'shipping_address_1' );
			$post_data['SHIPTOCITY']      = WFOCU_WC_Compatibility::get_order_data( $order, 'shipping_city' );
			$post_data['SHIPTOSTATE']     = WFOCU_WC_Compatibility::get_order_data( $order, 'shipping_state' );
			$post_data['SHIPTOCOUNTRY']   = WFOCU_WC_Compatibility::get_order_data( $order, 'shipping_country' );
			$post_data['SHIPTOZIP']       = WFOCU_WC_Compatibility::get_order_data( $order, 'shipping_postcode' );
		}

		return $this->parameters = apply_filters( 'woocommerce_gateway_paypal_pro_payflow_post_data', $post_data );
	}

	public function process_charge( $order ) {

		$is_successful = false;
		try {

			$response = $this->charge_upsell_payment( $order );

			if ( is_wp_error( $response ) ) {
				//@todo handle the error part here/failure of order

			} else {
				if ( false === $this->has_api_error( $response ) ) {

					WFOCU_Core()->log->log( $response );
					//@todo handle the success part here/failure of order

					WFOCU_Core()->data->set( '_transaction_id', $response['PNREF'] );

					$is_successful = true;
				} else {
					//@todo handle the error part here/failure of order
				}
			}
		} catch ( Exception $e ) {

			//@todo transaction failure to handle here
			$order_note = sprintf( __( 'Payflow Transaction Failed (%s)', 'woocommerce-gateway-stripe' ), $e->getMessage() );

		}

		return $this->handle_result( $is_successful );
	}

	public function to_string() {

		return urldecode( http_build_query( $this->get_parameters(), '', '&' ) );
	}

	public function get_parameters() {
		return $this->parameters;
	}

	/**
	 * Get user's IP address.
	 */
	public function get_user_ip() {
		return ! empty( $_SERVER['HTTP_X_FORWARD_FOR'] ) ? $_SERVER['HTTP_X_FORWARD_FOR'] : $_SERVER['REMOTE_ADDR'];
	}


	public function get_value_from_response( $response, $key ) {

		if ( $response && isset( $response[ $key ] ) ) {

			return $response[ $key ];
		}
	}


	public function has_api_error( $response ) {
		// assume something went wrong if ACK is missing
		if ( ! isset( $response['RESULT'] ) ) {
			return true;
		}

		// any non-success ACK is considered an error, see
		// https://developer.paypal.com/docs/classic/api/NVPAPIOverview/#id09C2F0K30L7
		return ( '0' != $this->get_value_from_response( $response, 'RESULT' ) && 'SuccessWithWarning' !== $this->get_value_from_response( $response, 'ACK' ) );

	}

	public function is_enabled( $order = false ) {
		$is_enabled = parent::is_enabled( $order );

		$is_reference_transaction_on = WFOCU_Core()->data->get_option( 'paypal_ref_trans' );
		if ( $is_enabled && 'yes' === $is_reference_transaction_on ) {

			return true;
		}

		return false;
	}

}

WFOCU_Gateway_Integration_PayPal_Pro_Payflow::get_instance();
