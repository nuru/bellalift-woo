(function ($, doc, win) {
    'use strict';
    let show_logs = true;
    let wfocu_logs = [];
    let my_logging_system = console.log;
    window.console.log1 = function () {
        let arg = arguments;
        if (arg.length > 0) {
            for (let i = 0; i < arg.length; i++) {
                wfocu_logs.push(arg[i]);
                if (show_logs) {
                    my_logging_system(arg[i]);
                }
            }
        }
    };

    window.wp_admin_ajax = function (cls, is_form, cb) {
        const self = this;
        let element = null;
        let handler = {};
        let prefix = "wfocu_";
        this.action = null;

        this.data = function (form_data, formEl = null) {

            return form_data;
        };
        this.before_send = function (formEl) {
        };
        this.async = function (bool) {
            return bool
        };
        this.method = function (method) {
            return method;
        };
        this.success = function (rsp, fieldset, loader, jqxhr, status) {
        };
        this.complete = function (rsp, fieldset, loader, jqxhr, status) {
        };
        this.error = function (rsp, fieldset, loader, jqxhr, status) {
        };
        this.action = function (action) {
            return action
        };

        function reset_form(action, fieldset, loader, rsp, jqxhr, status) {
            fieldset.length > 0 ? fieldset.prop('disabled', false) : null;
            loader.remove();
            let loader2;
            loader2 = $("#offer_settings_btn_bottom .wfocu_save_funnel_offer_products_ajax_loader ");
            loader2.removeClass('ajax_loader_show');
            $(".wfocu_steps_sortable").removeClass('wfocu_disable');
            if (self.hasOwnProperty(action) === true && typeof self[action] === 'function') {
                self[action](rsp, fieldset, loader, jqxhr, status);
            }
        }

        function form_post(action) {
            let formEl = element;
            let ajax_loader = null;
            let ajax_loader2 = null;
            let form_data = new FormData(formEl);

            form_data.append('action', action);

            let form_method = $(formEl).attr('method');

            let method = (form_method !== "undefined" && form_method !== "") ? form_method : 'POST';
            if ($(formEl).find("." + action + "_ajax_loader").length === 0) {
                $(formEl).find(".wfocu_form_submit").prepend("<span class='" + action + "_ajax_loader spinner" + "'></span>");
                $(".wfocu_steps_sortable").addClass('wfocu_disable');
                ajax_loader = $(formEl).find("." + action + "_ajax_loader");
            } else {
                ajax_loader = $(formEl).find("." + action + "_ajax_loader");
            }

            ajax_loader2 = $("#offer_settings_btn_bottom .wfocu_save_funnel_offer_products_ajax_loader ");
            ajax_loader.addClass('ajax_loader_show');
            ajax_loader2.addClass('ajax_loader_show');

            let fieldset = $(formEl).find("fieldset");
            fieldset.length > 0 ? fieldset.prop('disabled', true) : null;

            self.before_send(formEl, action);

            let data = self.data(form_data, formEl);

            let request = {
                url: ajaxurl,
                async: self.async(true),
                method: self.method('POST'),
                data: data,
                processData: false,
                contentType: false,
                //       contentType: self.content_type(false),
                success: function (rsp, jqxhr, status) {
                    reset_form(action + "_ajax_success", fieldset, ajax_loader, rsp, jqxhr, status);
                    self.success(rsp, jqxhr, status, fieldset, ajax_loader);
                },
                complete: function (rsp, jqxhr, status) {
                    if ('wfocu_save_funnel_offer_products' === action) {
                        $("#modal-section_product_success").iziModal('open');
                    }

                    reset_form(action + "_ajax_complete", fieldset, ajax_loader, rsp, jqxhr, status);
                    self.complete(rsp, jqxhr, status, fieldset, ajax_loader);
                },
                error: function (rsp, jqxhr, status) {
                    reset_form(action + "_ajax_error", fieldset, ajax_loader, rsp, jqxhr, status);
                    self.error(rsp, jqxhr, status, fieldset, ajax_loader);
                }
            };
            handler.hasOwnProperty(action) ? clearTimeout(handler[action]) : handler[action] = null;
            handler[action] = setTimeout(
                function (request) {
                    $.ajax(request);
                }, 200, request
            );
        }

        function send_json(action) {
            let formEl = element;
            let data = self.data({}, formEl);
            typeof data === 'object' ? (data.action = action) : (data = {'action': action});

            self.before_send(formEl, action);

            let request = {
                url: ajaxurl,
                async: self.async(true),
                method: self.method('POST'),
                data: data,
                success: function (rsp, jqxhr, status) {
                    self.success(rsp, jqxhr, status, element);
                },
                complete: function (rsp, jqxhr, status) {
                    self.complete(rsp, jqxhr, status, element);
                },
                error: function (rsp, jqxhr, status) {
                    self.error(rsp, jqxhr, status, element);
                }
            };
            handler.hasOwnProperty(action) ? clearTimeout(handler[action]) : handler[action] = null;
            handler[action] = setTimeout(
                function (request) {
                    $.ajax(request);
                }, 200, request
            );
        }

        this.ajax = function (action, data) {
            typeof data === 'object' ? (data.action = action) : (data = {'action': action});

            data.action = prefix + action;
            self.before_send(document.body, action);

            let request = {
                url: ajaxurl,
                async: self.async(true),
                method: self.method('POST'),
                data: data,
                success: function (rsp, jqxhr, status) {
                    self.success(rsp, jqxhr, status, action);
                },
                complete: function (rsp, jqxhr, status) {
                    self.complete(rsp, jqxhr, status, action);
                },
                error: function (rsp, jqxhr, status) {
                    self.error(rsp, jqxhr, status, action);
                }
            };
            handler.hasOwnProperty(action) ? clearTimeout(handler[action]) : handler[action] = null;
            handler[action] = setTimeout(
                function (request) {
                    $.ajax(request);
                }, 200, request
            );
        };

        function form_init(cls) {
            if ($(cls).length > 0) {

                $(cls).on(
                    "submit", function (e) {
                        e.preventDefault();
                        let action = $(this).data('wfoaction');

                        if ('update_funnel' === action || 'add_offer' === action || 'update_offer' === action) {
                            let formelem = $('form[data-wfoaction="' + action + '"]');
                            if (formelem.find('div.errors').length > 0) {
                                return false;
                            }
                        }
                        if ('save_funnel_offer_products' === action && false === window.wfocuBuilder.offer_product_settings.offer_state) {
                            wfocuSweetalert2(
                                $.extend(
                                    {
                                        title: "",
                                        text: "",
                                        type: 'warning',

                                        confirmButtonText: '',
                                    }, wfocuParams.alerts.offer_edit
                                )
                            ).then(
                                (result) => {
                                    if (result.value) {


                                        if ('save_funnel_offer_products' === action && (false === window.wfocuBuilder.offer_product_settings.isEmpty(window.wfocuBuilder.offer_product_settings.variations) && true === window.wfocuBuilder.offer_product_settings.isEmpty(window.wfocuBuilder.offer_product_settings.selected_variations))) {
                                            wfocuSweetalert2(
                                                $.extend(
                                                    {
                                                        title: "",
                                                        text: "",
                                                        type: 'warning',

                                                        confirmButtonText: '',
                                                    }, wfocuParams.alerts.no_variations_chosen
                                                )
                                            ).then(
                                                (result) => {
                                                    if (result.value) {
                                                        if (action !== 'undefined') {
                                                            action = prefix + action;
                                                            action = action.trim();
                                                            element = this;
                                                            self.action = action;
                                                            form_post(action);
                                                        }
                                                    }
                                                }
                                            ).catch(
                                                (e) => {
                                                    console.log("Remove offer from list error", e);
                                                }
                                            );
                                        } else {
                                            if (action !== 'undefined') {
                                                action = prefix + action;
                                                action = action.trim();
                                                element = this;
                                                self.action = action;
                                                form_post(action);
                                            }
                                        }
                                    }
                                }
                            ).catch(
                                (e) => {
                                    console.log("Remove offer from list error", e);
                                }
                            );
                        }
                        else if ('save_funnel_offer_products' === action && (false === window.wfocuBuilder.offer_product_settings.isEmpty(window.wfocuBuilder.offer_product_settings.variations) && true === window.wfocuBuilder.offer_product_settings.isEmpty(window.wfocuBuilder.offer_product_settings.selected_variations))) {
                            wfocuSweetalert2(
                                $.extend(
                                    {
                                        title: "",
                                        text: "",
                                        type: 'warning',

                                        confirmButtonText: '',
                                    }, wfocuParams.alerts.no_variations_chosen
                                )
                            ).then(
                                (result) => {

                                }
                            ).catch(
                                (e) => {
                                    console.log("Remove offer from list error", e);
                                }
                            );
                        }
                        else {
                            if (action !== 'undefined') {
                                action = prefix + action;
                                action = action.trim();
                                element = this;
                                self.action = action;
                                form_post(action);
                            }
                        }


                    }
                );

                if (typeof cb === 'function') {

                    cb(self);
                }
            }
        }

        function click_init(cls) {
            if ($(cls).length > 0) {
                $(cls).on(
                    "click", function (e) {
                        console.log(e);
                        e.preventDefault();
                        let action = $(this).data('wfoaction');
                        if (action !== 'undefined') {
                            action = prefix + action;
                            action = action.trim();
                            element = this;
                            self.action = action;
                            send_json(action);
                        }
                    }
                );

                if (typeof cb === 'function') {
                    cb(self);
                }
            }
        }

        if (is_form === true) {
            form_init(cls, cb);
            return this;
        }

        if (is_form === false) {
            click_init(cls, cb);
            return this;
        }

        return this;
    };
    let wfocuBuilderCommons = {

        hooks: {action: {}, filter: {}},
        addAction: function (action, callable, priority, tag) {
            this.addHook('action', action, callable, priority, tag);
        },
        addFilter: function (action, callable, priority, tag) {
            this.addHook('filter', action, callable, priority, tag);
        },
        doAction: function (action) {
            this.doHook('action', action, arguments);
        },
        applyFilters: function (action) {
            console.log("applying filter");
            return this.doHook('filter', action, arguments);
        },
        removeAction: function (action, tag) {
            this.removeHook('action', action, tag);
        },
        removeFilter: function (action, priority, tag) {
            this.removeHook('filter', action, priority, tag);
        },
        addHook: function (hookType, action, callable, priority, tag) {
            if (undefined == this.hooks[hookType][action]) {
                this.hooks[hookType][action] = [];
            }
            var hooks = this.hooks[hookType][action];
            if (undefined == tag) {
                tag = action + '_' + hooks.length;
            }
            if (priority == undefined) {
                priority = 10;
            }

            this.hooks[hookType][action].push({tag: tag, callable: callable, priority: priority});
        },
        doHook: function (hookType, action, args) {

            // splice args from object into array and remove first index which is the hook name
            args = Array.prototype.slice.call(args, 1);
            if (undefined != this.hooks[hookType][action]) {
                var hooks = this.hooks[hookType][action], hook;
                //sort by priority
                hooks.sort(
                    function (a, b) {
                        return a["priority"] - b["priority"]
                    }
                );
                for (var i = 0; i < hooks.length; i++) {
                    hook = hooks[i].callable;
                    if (typeof hook != 'function') {
                        hook = window[hook];
                    }
                    if ('action' == hookType) {
                        hook.apply(null, args);
                    } else {
                        args[0] = hook.apply(null, args);
                    }
                }
            }
            if ('filter' == hookType) {
                return args[0];
            }
        },
        removeHook: function (hookType, action, priority, tag) {
            if (undefined != this.hooks[hookType][action]) {
                var hooks = this.hooks[hookType][action];
                for (var i = hooks.length - 1; i >= 0; i--) {
                    if ((undefined == tag || tag == hooks[i].tag) && (undefined == priority || priority == hooks[i].priority)) {
                        hooks.splice(i, 1);
                    }
                }
            }
        }
    };

    let wfo_builder = function () {
        if (typeof wfocu === 'undefined') {
            return false;
        }

        const self = this;

        const default_offer_state = '0';
        let update_funnel_model = false;
        let rules_container = '.wfocu_rules_container';
        let step_container = '.wfocu_steps_sortable';
        let design_container = '#wfocu_step_design';
        let settings_container = '#wfocu_funnel_setting';
        let global_settings_container = '#wfocu_global_setting';
        let global_settings_offer_confirmation = '#wfocu_global_settings_offer_confirmation';
        let step_list = '.wfocu_steps_sortable .wfocu_step';
        let remove_step_list = '.wfocu_step_delete';
        let ajax_action = 'save_funnel';
        let product_search_is_open = false;
        let page_search_is_open = false;
        let add_new_step_is_open = false;
        let product_search_timeout = null;

        let current_offer_id = 0;
        let funnel_id = 0;
        let offer_steps = [];
        let offer_forms = {};
        let global_settings_funnels = false;
        let current_index = 0;
        let selected_product = '';
        let step_change_timeout = null;
        let update_step_settings = [
            {
                type: "input",
                inputType: "text",
                label: "",
                model: "funnel_step_name",
                featured: true,
                inputName: 'step_name',
                required: true,
                placeholder: "",
                validator: VueFormGenerator.validators.string,
            }, {
                type: "radios",
                label: "Type",
                model: "step_type",
                inputName: 'step_type',
                help: "",
                styleClasses: ["wfocu_form_button"],
                values: [
                    {name: "", value: "upsell"},
                    {name: "", value: "downsell"}
                ],
            },
            {
                type: "input",
                inputType: "text",
                label: "",
                model: "funnel_step_slug",
                featured: true,
                styleClasses: ["wfocu_step_slug"],
                inputName: 'funnel_step_slug',
                required: true,
                placeholder: "",
                validator: VueFormGenerator.validators.string,
            }
        ];
        let update_step_is_open = false;
        let add_new_offer_setting_fields = [
            {
                type: "input",
                inputType: "text",
                label: "",
                model: "funnel_step_name",
                featured: true,
                inputName: 'step_name',
                required: true,
                placeholder: "",
                validator: VueFormGenerator.validators.string,
            }, {
                type: "radios",
                label: "Type",
                model: "step_type",
                inputName: 'step_type',
                help: "",
                styleClasses: ["wfocu_form_button"],
                values: [
                    {name: "", value: "upsell"},
                    {name: "", value: "downsell"}
                ],
                visible: function (model) {
                    if (model.show_select_btn) {
                        return true;
                    }
                    return false;
                }
            }];
        let funnel_settings_fields = [

            {
                type: "label",
                styleClasses: "wfocu_setting_label_head",
                label: "",
                model: "funnel_order_label"
            },
            {
                type: "radios",
                label: "",
                model: "order_behavior",
                inputName: 'order_behavior',
                values: [{name: '', value: 'batching'}, {name: '', value: 'create_order'}],

            },
            {
                type: "radios",
                label: "",
                model: "is_cancel_order",
                values: [{name: '', value: 'yes'}, {name: '', value: 'no'}],
                inputName: 'is_cancel_order',
                visible: function (model) {
                    //visible if business is selected
                    return model && model.order_behavior == 'create_order';
                }
            },

            {
                type: "label",
                styleClasses: "wfocu_setting_label_head",
                label: "",
                model: "funnel_priority_label"
            }, {
                type: "input",
                label: "",
                model: "funnel_priority"
            },{
                type: "label",
                styleClasses: "wfocu_setting_label_head",
                label: "",
                model: "funnel_display_label"
            }, {
                type: "radios",
                label: "",
                model: "is_tax_included",
                values: [{name: '', value: 'yes'}, {name: '', value: 'no'}],
                inputName: 'is_tax_included',

            },
            {
                type: "label",
                styleClasses: "wfocu_setting_label_head",
                label: "",
                model: "offer_messages_label"
            },
            {
                type: "label",
                label: "",
                styleClasses: ["wfocu_gsettings_sec_note", "wfocu_to_html"],
                model: "offer_messages_label_help",
                inputName: 'offer_messages_label_help',
            },
            {
                type: "textArea",
                label: "",
                model: "offer_success_message_pop",
                inputName: 'offer_success_message_pop',

            }, {
                type: "textArea",
                label: "",
                model: "offer_failure_message_pop",
                inputName: 'offer_failure_message_pop',

            }, {
                type: "textArea",
                label: "",
                model: "offer_wait_message_pop",
                inputName: 'offer_wait_message_pop',

            },

            {
                type: "label",
                styleClasses: "wfocu_setting_label_head",
                label: "",
                model: "offer_scripts_label"
            },
            {
                type: "textArea",
                label: "",
                model: "funnel_success_script",
                inputName: 'funnel_success_script',
                rows: 10,

            },

        ];
        let funnel_setting_fields = [
            {
                type: "input",
                inputType: "text",
                label: "Name",
                model: "funnel_name",
                inputName: 'funnel_name',
                featured: true,
                required: true,
                placeholder: "Enter Name",
                validator: VueFormGenerator.validators.string
            }, {
                type: "textArea",
                label: "Description",
                model: "funnel_desc",
                inputName: 'funnel_desc',
                featured: true,
                rows: 3,
                placeholder: "Enter Description (Optional)"
            }];
        let global_settings_order_statuses_fields = [

            /** Order section started **/
            {
                type: "label",

                label: "",
                styleClasses: "wfocu_gsettings_sec_head",
                model: "label_section_head_orders",
                inputName: 'label_section_head_orders',
            },
            {
                type: "input",
                inputType: "text",
                label: "",
                model: "primary_order_status_title",
                inputName: 'primary_order_status_title',

            },
            {
                type: "label",

                label: "",
                styleClasses: "wfocu_gsettings_sec_head",
                model: "label_section_head_orders_upsell_fails",
                inputName: 'label_section_head_orders',
            },
            {
                type: "select",
                label: "",
                model: "create_new_order_status_fail",
                inputName: 'create_new_order_status_fail',
                selectOptions: {hideNoneSelectedText: true},

            },

        ];
        let global_settings_gateway_fields = [


            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "gateways",
                inputName: 'gateways',
                visible: function (model) {

                    return !wfocuParams.isNOGateway;
                }
            },
            {
                type: "label",
                label: "",
                styleClasses: "wfocu_gsettings_sec_no_gateways",
                model: "no_gateways",
                inputName: 'no_gateways',
                visible: function (model) {

                    return wfocuParams.isNOGateway;
                }
            },

            {
                type: "radios",
                label: "",
                styleClasses: "wfocu_gsettings_paypal_ref_trans",
                model: "paypal_ref_trans",
                inputName: 'paypal_ref_trans',
                visible: function (model) {
                    return (wfocuBuilderCommons.applyFilters('wfocu_global_settings_gateway_fields', model.gateways.indexOf('paypal_pro_payflow') !== -1 || model.gateways.indexOf('paypal') !== -1 || model.gateways.indexOf('ppec_paypal') !== -1, model.gateways));
                }

            },
            {
                type: "label",
                label: "",
                styleClasses: "wfocu_gsettings_sec_note",
                model: "label_section_head_paypal_ref",
                inputName: 'label_section_head_paypal_ref',
                visible: function (model) {
                    return (wfocuBuilderCommons.applyFilters('wfocu_global_settings_gateway_fields', model.gateways.indexOf('paypal_pro_payflow') !== -1 || model.gateways.indexOf('paypal') !== -1 || model.gateways.indexOf('ppec_paypal') !== -1, model.gateways));
                }
            },


            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "gateway_test",
                inputName: 'gateway_test',

            }

        ];
        let global_settings_tan_fields = [
            /** Order section ended **/

            {
                type: "label",

                label: "",
                styleClasses: "wfocu_gsettings_sec_head",
                model: "label_section_head_tan",
                inputName: 'label_section_head_tan',
            },
            {
                type: "input",
                inputType: "text",
                label: "",
                model: "fb_pixel_key",
                inputName: 'fb_pixel_key',
            },

            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "is_fb_view_event",
                inputName: 'is_fb_view_event',

            },

            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "is_fb_purchase_event",
                inputName: 'is_fb_purchase_event',


            },
            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "custom_aud_opt_conf",
                inputName: 'custom_aud_opt_conf',
                visible: function (modal) {
                    return (modal.is_fb_purchase_event.length > 0);
                },

            },

            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "exclude_from_total",
                inputName: 'exclude_from_total',
                visible: function (modal) {
                    return (modal.is_fb_purchase_event.length > 0);
                },

            },
            // {
            //     type: "checklist",
            //     listBox: true,
            //     label: "",
            //     styleClasses: "wfocu_gsettings_sec_chlist",
            //     model: "is_disable_taxes",
            //     inputName: 'is_disable_taxes',
            //     visible: function (modal) {
            //         return (modal.is_fb_purchase_event.length > 0);
            //     },
            //
            // },


            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "is_fb_synced_event",
                inputName: 'is_fb_synced_event',


            },
            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "content_id_variable",
                inputName: 'content_id_variable',
                visible: function (modal) {
                    return (modal.is_fb_synced_event.length > 0);
                },

            },

            {
                type: "select",
                label: "",
                model: "content_id_value",
                inputName: 'content_id_value',
                selectOptions: {hideNoneSelectedText: true},
                visible: function (modal) {

                    return (modal.is_fb_synced_event.length > 0);
                },

            },

            {
                type: "input",
                inputType: "text",
                model: "content_id_prefix",
                inputName: 'content_id_prefix',
                visible: function (modal) {
                    return (modal.is_fb_synced_event.length > 0);
                },


            },
            {
                type: "input",
                inputType: "text",
                model: "content_id_suffix",
                inputName: 'content_id_suffix',
                visible: function (modal) {
                    return (modal.is_fb_synced_event.length > 0);
                },


            },
            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "enable_general_event",
                inputName: 'enable_general_event',

            },


            {
                type: "input",
                inputType: "text",
                model: "general_event_name",
                inputName: 'general_event_name',
                visible: function (modal) {
                    return modal.enable_general_event.length;
                }

            },

            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "is_fb_advanced_event",
                inputName: 'is_fb_advanced_event',

            },


            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "track_traffic_source",
                inputName: 'track_traffic_source',

            },


            {
                type: "label",

                label: "",
                styleClasses: "wfocu_gsettings_sec_head",
                model: "label_section_head_tan_ga",
                inputName: 'label_section_head_tan_ga',
            },


            {
                type: "input",
                inputType: "text",
                label: "",
                model: "ga_key",
                inputName: 'ga_key',

            },

            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "is_ga_view_event",
                inputName: 'is_ga_view_event',

            },

            {
                type: "checklist",
                listBox: true,
                label: "",
                styleClasses: "wfocu_gsettings_sec_chlist",
                model: "is_ga_purchase_event",
                inputName: 'is_ga_purchase_event',

            },


        ];
        let global_settings_scripts_fields = [
            {
                type: "textArea",
                label: "",
                model: "scripts",
                inputName: 'scripts',

            },
            {
                type: "textArea",
                label: "",
                model: "scripts_head",
                inputName: 'scripts_head',

            },
        ];
        let global_settings_emails_fields = [
            /** Head Emails starts **/
            {
                type: "label",

                label: "",
                styleClasses: "wfocu_gsettings_sec_head",
                model: "label_section_head_emails",
                inputName: 'label_section_head_orders',
            },
            {
                type: "radios",
                label: "",
                model: "send_processing_mail_on",
                inputName: 'send_processing_mail_on',

            },
            {
                type: "label",

                label: "",
                styleClasses: "wfocu_gsettings_sec_head",
                model: "label_section_head_emails_no_batch",
                inputName: 'label_section_head_orders',
            },

            {
                type: "radios",
                label: "",
                model: "send_processing_mail_on_no_batch",
                inputName: 'send_processing_mail_on_no_batch',

            },
            {
                type: "label",

                label: "",
                styleClasses: "wfocu_gsettings_sec_head",
                model: "label_section_head_emails_no_batch_cancel",
                inputName: 'label_section_head_orders_no_batch_cancel',
            },
            {
                type: "radios",
                label: "",
                model: "send_processing_mail_on_no_batch_cancel",
                inputName: 'send_processing_mail_on_no_batch_cancel',

            },
            {
                type: "label",
                label: "",
                styleClasses: "wfocu_gsettings_sec_note",
                model: "send_emails_label",
                inputName: 'send_emails_label',

            },


            /** Head Emails Ends **/];
        let global_settings_misc_fields = [
            /** Head Misc starts **/

            {
                type: "input",
                inputType: "text",
                label: "",
                model: "flat_shipping_label",
                inputName: 'flat_shipping_label',

            },
            {
                type: "input",
                inputType: "text",
                label: "",
                model: "ttl_funnel",
                inputName: 'ttl_funnel',

            },

            {
                type: "input",
                inputType: "text",
                label: "",
                model: "offer_post_type_slug",
                inputName: 'offer_post_type_slug',

            }, {
                type: "checkbox",
                label: "",
                model: "enable_log",
                inputName: 'enable_log',

            },

            {
                type: "textArea",
                label: "",
                model: "order_copy_meta_keys",
                inputName: 'order_copy_meta_keys',

            },

            /** Head Misc ends **/];

        let global_settings_offer_confirmation_fields = [

            {
                type: "label",
                label: "",
                styleClasses: ["wfocu_gsettings_sec_note", "wfocu_to_html"],
                model: "offer_header_label",
                inputName: 'offer_header_label',
            },
            {
                type: "input",
                inputType: "text",
                label: "",
                model: "offer_header_text",
                inputName: 'offer_header_text',
            }, {
                type: "input",
                inputType: "text",
                label: "",
                model: "offer_yes_btn_text",
                inputName: 'offer_yes_btn_text',
            }, {
                type: "input",
                inputType: "text",
                label: "",
                model: "offer_skip_link_text",
                inputName: 'offer_skip_link_text',
            },
            {
                type: "input",

                label: "",
                model: "offer_yes_btn_bg_cl",
                inputName: 'offer_yes_btn_bg_cl',
            }, {
                type: "input",

                label: "",
                model: "offer_yes_btn_sh_cl",
                inputName: 'offer_yes_btn_sh_cl',
            }, {
                type: "input",

                label: "",
                model: "offer_yes_btn_txt_cl",
                inputName: 'offer_yes_btn_txt_cl',
            },
            {
                type: "input",

                label: "",
                model: "offer_yes_btn_bg_cl_h",
                inputName: 'offer_yes_btn_bg_cl_h',
            }, {
                type: "input",

                label: "",
                model: "offer_yes_btn_sh_cl_h",
                inputName: 'offer_yes_btn_sh_cl_h',
            }, {
                type: "input",

                label: "",
                model: "offer_yes_btn_txt_cl_h",
                inputName: 'offer_yes_btn_txt_cl_h',
            },
            {
                type: "input",

                label: "",
                model: "offer_no_btn_txt_cl",
                inputName: 'offer_no_btn_txt_cl',
            }, {
                type: "input",

                label: "",
                model: "offer_no_btn_txt_cl_h",
                inputName: 'offer_no_btn_txt_cl_h',
            }, {
                type: "input",
                inputType: "text",
                label: "",
                model: "cart_opener_text",
                inputName: 'cart_opener_text',
            }, {
                type: "input",
                label: "",
                model: "cart_opener_text_color",
                inputName: 'cart_opener_text_color',
            }, {
                type: "input",
                label: "",
                model: "cart_opener_background_color",
                inputName: 'cart_opener_background_color',
            },

        ];

        this.ol = function (obj) {
            let c = 0;
            if (obj != null && typeof obj === "object") {
                c = Object.keys(obj).length;
            }
            return c;
        };
        this.isEmpty = function (obj) {
            for (let key in obj) {
                if (obj.hasOwnProperty(key)) {
                    return false;
                }
            }
            return true;
        };

        this.hp = function (obj, key) {
            let c = false;
            if (typeof obj === "object" && key !== undefined) {
                c = obj.hasOwnProperty(key);
            }
            return c;
        };

        this.jsp = function (obj) {
            if (typeof obj === 'object') {
                let doc = JSON.stringify(obj);
                doc = JSON.parse(doc);
                return doc;
            } else {
                return obj;
            }
        };
        this.kys = function (obj) {
            if (typeof obj === 'object' && obj != null && this.ol(obj) > 0) {
                return Object.keys(obj);
            }
            return [];
        };

        let wfo_ajax = function () {
            let ajax = new wp_admin_ajax();
            return ajax;

        };
        this.offer_product_settings = null;
        this.offer_settings_btn_bottom = null;
        this.funnel_setting = null;
        this.product_search_setting = null;
        this.page_search_setting = null;
        this.add_new_offer_setting = null;
        this.update_offer_setting = null;
        this.offer_setting = null;
        this.design_settings = null;

        this.get_current_url = function () {
            return window.location.href;
        };
        this.get_customizer_url = function () {
            return wfocu.customize_url;
        };

        this.build_customize_url = function (template, custom_url) {
            if (typeof template !== 'undefined' && template !== 'undefined') {
                let params = {};
                let index_id = this.get_current_index();
                let steps = this.get_offer_step(index_id);
                let step = JSON.stringify(steps);
                step = JSON.parse(step);
                let customize_url = this.get_customizer_url();

                params.wfocu_customize = 'loaded';
                params.offer_id = step.id;
                if (typeof custom_url != "undefined" && custom_url != '') {
                    step.url = custom_url;

                    params.url = decodeURIComponent(step.url);
                    params.return = decodeURIComponent(this.get_current_url());
                    let query_string = $.param(params);
                    return customize_url + "?" + query_string;
                } else {

                    step.url += "?wfocu_customize=loaded&offer_id=" + step.id+"&funnel_id="+self.get_funnel_id();



                    params.url = decodeURIComponent(step.url);
                    params.return = decodeURIComponent(this.get_current_url());
                    let query_string = $.param(params);
                    return customize_url + "?" + query_string;

                }

            }
            return "";

        };
        this.get_funnel_name = function () {
            return wfocu.funnel_name;
        };
        this.get_funnel_desc = function () {
            return wfocu.funnel_desc;
        };
        this.update_funnel = function (data) {
            if (this.ol(data) > 0) {
                if (data.name !== '') {
                    wfocu.name = data.name;
                    $('#wfocu_funnel_name').text(data.name);
                }
                if (data.desc !== '') {
                    wfocu.desc = data.name;
                    $('#wfocu_funnel_description').html(data.desc);
                }
            }

        };
        this.update = function () {
            setTimeout(
                function () {
                    let offer_id = self.get_offer_id();
                    let offer = self.get_offer_form(offer_id);
                    offer = self.jsp(offer);
                    let data = {"id": funnel_id, "offer_id": offer_id, "steps": self.get_offer_steps(), "offer": offer};
                    let wp_ajax = wfo_ajax();
                    // wp_ajax.ajax(ajax_action, data);
                    // wp_ajax.success = function (rsp) {
                    //
                    // }
                }, 300
            );
        };
        this.create_step = function (rsp) {
            if (this.ol(rsp) > 0 && this.hp(rsp, 'id') && this.hp(rsp, "title")) {
                let non_draggble = "";
                if (this.get_offer_id() === 0) {
                    this.set_offer_id(rsp.id);

                    this.offer_product_settings.current_offer = rsp.title;

                    this.offer_product_settings.url = rsp.url;

                    this.offer_product_settings.current_offer_name = rsp.slug;

                    // non_draggble = "ui-state-disabled";
                }

                self.offer_setting.product_count = 0;
                self.add_new_offer_setting.model.show_select_btn = true;

                let div = document.createElement('div');
                div.className = 'wfocu_step_container ' + non_draggble;
                div.setAttribute('data-offer_id', rsp.id);
                div.setAttribute('data-offer_url', rsp.url);
                div.setAttribute('data-offer_title', rsp.title);
                div.setAttribute('data-offer_type', rsp.type);
                div.setAttribute('data-offer_state', default_offer_state);

                div.setAttribute('data-offer-slug', rsp.slug);
                let step_el = document.createElement('a');
                step_el.setAttribute('data-offer_id', rsp.id);
                step_el.setAttribute('data-offer_title', rsp.title);
                step_el.setAttribute('data-offer_type', rsp.type);
                step_el.className = "wfocu_step";
                let i = document.createElement('i');
                if (rsp.type === 'upsell') {
                    i.className = "wfocu_icon_fixed dashicons dashicons-arrow-up";
                } else {
                    i.className = "wfocu_icon_fixed dashicons dashicons-arrow-down";
                }
                step_el.appendChild(i);

                let sp1 = document.createElement('span');
                sp1.className = "step_name";
                let t1 = document.createTextNode(rsp.title);
                sp1.appendChild(t1);
                step_el.appendChild(sp1);

                let sp2 = document.createElement('span');
                sp2.className = "wfocu_up_arrow";
                step_el.appendChild(sp2);

                let sp3 = document.createElement('span');
                sp3.className = "wfocu_down_arrow";
                step_el.appendChild(sp3);

                let sp4 = document.createElement('span');
                sp4.className = "wfocu_step_offer_state";
                step_el.appendChild(sp4);

                let sp5 = document.createElement('span');
                sp5.className = "wfocu_remove_step";
                sp5.setAttribute('onClick', "wfocuBuilder.offer_settings_btn_bottom.delete_offer(this, " + rsp.id + ")");
                let sp5i = document.createElement('i');
                sp5i.className = "dashicons dashicons-no-alt";
                sp5.appendChild(sp5i);
                step_el.appendChild(sp5);

                div.appendChild(step_el);
                let step_dom = document.getElementsByClassName('wfocu_steps_sortable');
                if (step_dom.length > 0) {
                    step_dom[0].appendChild(div);
                    sortable();
                    offer_build_layout();
                    offer_steps_event();
                    $('div.wfocu_step_container a[data-offer_id="' + rsp.id + '"]').trigger('click');
                }
            }
        };
        this.step_change = function () {
            clearTimeout(this.step_change_timeout);

            this.step_change_timeout = setTimeout(
                function () {
                    let offer_id = self.get_offer_id();
                    let data = {"funnel_id": self.get_funnel_id(), "offer_id": offer_id, "steps": self.get_offer_steps()};
                    console.log(data);
                    let wp_ajax = wfo_ajax();
                    wp_ajax.ajax("save_funnel_steps", data);
                }, 300
            );
            // this.update();
        };
        this.get_current_index = function () {
            return current_index;
        };
        this.set_current_index = function (index) {
            current_index = index;
            return current_index;
        };
        this.get_offer_id = function () {
            return current_offer_id;
        };
        this.set_offer_id = function (offer_id) {
            if (offer_id > 0) {
                this.offer_product_settings.current_offer_id = offer_id;
                this.offer_setting.current_offer_id = offer_id;
                current_offer_id = offer_id;
                return current_offer_id;
            }
            return 0;
        };
        this.get_selected_product = function () {
            return selected_product;
        };

        this.get_funnel_id = function () {
            return funnel_id;
        };
        this.get_offer_forms = function () {
            return offer_forms;
        };
        this.get_offer_form = function (id) {
            let forms = this.get_offer_forms();
            if (this.hp(forms, id)) {
                return forms[id];
            }
            return get_default_offer_forms();
        };
        this.delete_offer_form = function (offer_id) {
            if (this.hp(offer_forms, offer_id)) {
                delete offer_forms[offer_id];
            }
        };
        this.update_offer_form = function (offer_id, data) {
            if (offer_id > 0 && this.ol(data) > 0) {

                if (this.ol(data) > 0) {
                    offer_forms[offer_id] = data;
                    this.update();
                }
            }
        };
        this.get_offer_steps = function () {
            return offer_steps;
        };
        this.get_offer_step = function (id) {
            let forms = this.get_offer_steps();

            return (this.ol(forms) > 0 && this.hp(forms, id)) ? forms[id] : {};
        };
        this.delete_offer_step = function (index_id) {
            if (this.hp(offer_steps, index_id)) {
                delete offer_steps.splice(index_id, 1);

            }
        };
        this.delete_offer_product = function (product_id, callback) {
            if (product_id !== "") {
                let offer_id = self.get_offer_id();
                let field_data = {}, product_data = {};
                if (offer_id > 0 && offer_forms.hasOwnProperty(offer_id)) {
                    let product_data = JSON.stringify(offer_forms[offer_id]['products'][product_id]);
                    let field_data = JSON.stringify(offer_forms[offer_id]['fields'][product_id]);
                    delete offer_forms[offer_id]['products'][product_id];
                    delete offer_forms[offer_id]['fields'][product_id];

                }
                if (this.isEmpty(offer_forms[offer_id]['products'])) {
                    self.offer_setting.product_count = 0;
                    self.offer_settings_btn_bottom.products = {};
                }

                if (typeof callback === "function") {
                    callback(product_data, field_data);
                }
                this.update();
            }

        };
        this.update_offer_state = function (state) {

            if ('1' == state || true == state) {
                state = '1';
            } else {
                state = '0';
            }
            let offer_id = this.get_offer_id();
            let current_index = this.get_current_index();
            let element = $('.wfocu_step_container[data-offer_id=' + offer_id + ']');
            if (element.length > 0) {
                offer_steps[current_index].state = state;

                element.attr('data-offer_state', state);

                if (state === '0') {
                    element.find('.wfocu_step_offer_state').addClass('state_off');
                    element.find('.wfocu_step_offer_state').attr('title', 'Inactive');
                } else {
                    element.find('.wfocu_step_offer_state').removeClass('state_off');
                    element.find('.wfocu_step_offer_state').attr('title', 'Active');
                }
            }
        };
        this.update_current_step = function (key, value) {

            if (typeof key !== 'undefined' && key !== "undefined" && typeof value !== 'undefined' && value !== 'undefined') {
                let index_id = this.get_current_index();
                let offer_id = this.get_offer_id();
                let steps = this.get_offer_step(index_id);

                if (steps.hasOwnProperty(key)) {
                    steps[key] = value;
                    $('.wfocu_step_container[data-offer_id=' + offer_id + ']').find('.step_name').text(steps.name);
                    $('.wfocu_step_container[data-offer_id=' + offer_id + ']').attr("data-offer_title", steps.name);
                    if (key === 'type') {
                        $('.wfocu_step_container[data-offer_id=' + offer_id + ']').find('.wfocu_icon_fixed').removeClass('dashicons-arrow-down');
                        $('.wfocu_step_container[data-offer_id=' + offer_id + ']').find('.wfocu_icon_fixed').removeClass('dashicons-arrow-up');

                        if (value === 'downsell') {
                            $('.wfocu_step_container[data-offer_id=' + offer_id + ']').find('.wfocu_icon_fixed').addClass('dashicons-arrow-down');

                        } else {
                            $('.wfocu_step_container[data-offer_id=' + offer_id + ']').find('.wfocu_icon_fixed').addClass('dashicons-arrow-up');

                        }
                    }
                    $('.wfocu_step_container[data-offer_id=' + offer_id + ']').find('.step_name').text(steps.name);

                    this.offer_product_settings.current_offer = steps.name;
                    this.offer_product_settings.url = steps.url;
                    this.offer_product_settings.current_offer_name = steps.slug;
                    this.offer_product_settings.offer_type = steps.type;

                    this.step_change();
                }
            }
        };
        /**
         * Multiple product added once
         * @param data
         */
        this.add_products = function (data) {
            if (this.ol(data) > 0) {

                let offer_id = this.get_offer_id();
                let offer = this.get_offer_form(offer_id);

                if (this.ol(offer) > 0) {
                    if (this.hp(data, 'products') && this.ol(data['products']) > 0) {
                        let products = data['products'];
                        for (let p in products) {
                            offer['products'][p] = products[p];
                        }
                    }
                    if (this.hp(data, 'fields') && this.ol(data['fields']) > 0) {
                        let fields = data['fields'];
                        for (let p in fields) {
                            offer['fields'][p] = fields[p];
                        }
                    }
                    if (this.hp(data, 'variations') && this.ol(data['variations']) > 0) {
                        let variations = data['variations'];
                        for (let p in variations) {
                            offer['variations'][p] = variations[p];
                        }
                    }
                }
                initialize_offer(offer_id);
                this.update_offer_form(offer_id, offer);
            }
        };


        const get_default_offer_forms = function () {
            return {products: {}, fields: {}, settings: {}, variations: {}, template: "sp-classic", have_multiple_product: 1};
        };

        //Rules section start here
        const initialize_funnel_rules = function () {

        };
        // Rules step section start here

        // Offer step section start here
        const offer_build_layout = function () {

            let sort_el = $(".wfocu_steps_sortable");
            let step_els = sort_el.find('.wfocu_step_container');
            offer_steps = [];
            if (step_els.length > 0) {
                step_els.each(
                    function (i, e) {
                        let id = $(this).data('offer_id');
                        if (typeof id !== 'undefined') {
                            let title = $(this).attr('data-offer_title');
                            let type = $(this).attr('data-offer_type');
                            let state = $(this).attr('data-offer_state');
                            let url = $(this).attr('data-offer_url');
                            let slug = $(this).attr('data-offer-slug');
                            $(this).children('.wfocu_icon_delete').attr('data-index_id', i);
                            $(this).children('.wfocu_step').attr('data-index_id', i);
                            $(this).attr('data-index_id', i);
                            console.log(title);
                            offer_steps.push({'id': id, "name": title, "type": type, 'state': state, "url": url, "slug": slug})
                            if (offer_forms.hasOwnProperty(id) === false) {
                                offer_forms[id] = get_default_offer_forms();
                            }
                        }
                    }
                );
            } else {
                reset_offer();
            }
            self.step_change();
        };
        const sortable = function () {

            if ($(design_container).length > 0) {
                return;
            }

            if ($(step_container).length == 0) {
                return;
            }
            $(step_container).off('sortable');
            let container_wrap = $('.wfocu_step_container');
            $(step_container).sortable(
                {
                    items: ".wfocu_step_container:not(.ui-state-disabled)",
                    start: function (event, ui) {
                        ui.item.addClass("wfocu_start_sortable");
                        if (container_wrap.length > 0) {
                            container_wrap.addClass("wfocu_start_drag_on");
                        }
                    },
                    stop: function (event, ui) {
                        ui.item.removeClass("wfocu_start_sortable");
                        if (container_wrap.length > 0) {
                            container_wrap.removeClass("wfocu_start_drag_on");
                        }
                        offer_build_layout();
                    },
                    beforeStop: function (ev, ui) {

                        /**
                         * @todo this function is throwing js errors/ find a way to escape errors
                         * till then allow downsells to be first in the ladder
                         */
                        /**
                         * Prevent downsell on the first in the ladder
                         */
                        // const getFirstElementType = ui.placeholder.parent().find('.wfocu_step_container').eq(0).attr('data-offer_type');
                        //    if ('downsell' === getFirstElementType) {
                        //       $(this).sortable('cancel');
                        //   }

                    }
                }
            ).disableSelection();

        };
        const offer_steps_event = function () {

            if ($(step_list).length > 0) {
                $(step_list).off('click');
                $(step_list).on(
                    'click', function (e) {
                        e.preventDefault();
                        let index_id = $(this).attr('data-index_id');
                        let current_index = self.get_current_index();

                        if (index_id !== current_index) {
                            $('.wfocu_step').removeClass('current_offer');
                            $(this).addClass('current_offer');
                            let offer_id = $(this).data('offer_id');
                            self.set_current_index(index_id);
                            let step_data = self.get_offer_step(index_id);

                            self.offer_product_settings.current_offer = step_data.name;
                            self.offer_product_settings.current_offer_id = step_data.id;
                            self.offer_product_settings.current_offer_name = step_data.slug;
                            self.offer_product_settings.offer_type = step_data.type;
                            self.offer_product_settings.url = step_data.url;

                            self.set_offer_id(offer_id);
                            initialize_offer(offer_id);
                        }
                    }
                );
            }
        };
        const initialize_offer = function (offer_id) {
            let offer = self.get_offer_form(offer_id);
            if (self.ol(offer, 'products') === 0) {
                self.offer_product_settings.selected_product = '';
                self.offer_setting.product_count = 0;
                return;
            }

            build_offer();
        };

        const prepare_variations = function (i) {
            if (self.hp(self.offer_product_settings.products, i)) {
                let parent_discount = 0;
                let settings = self.offer_product_settings.products[i]['settings'];
                if (self.hp(settings, 'discount_amount')) {
                    parent_discount = parseFloat(settings.discount_amount);
                }

                let variations = self.offer_product_settings.products[i]['variations'];
                for (let v in variations) {
                    if (self.ol(variations[v], 'is_enable')) {
                        self.offer_product_settings.selected_variations[v] = true;
                    }
                    if (variations[v].discount_amount === 0) {
                        variations[v].discount_amount = parent_discount;
                    }
                }
            }
        };
        const reset_offer = function () {
            // reset offer setting
            self.offer_setting.current_offer_id = 0;
            self.offer_setting.product_count = 0;
            self.offer_setting.model = {
                ship_dynamic: false,
                ask_confirmation: false,
                allow_free_ship_select: false,
                skip_exist: false,
            };

            // reset offer product setting
            self.offer_product_settings.current_offer_id = 0;
            self.offer_product_settings.current_offer_name = '';
            self.offer_product_settings.offer_type = 'upsell';
            self.offer_product_settings.products = {};
            self.offer_product_settings.current_template = '';

            if (null == self.offer_settings_btn_bottom) {
                return;
            }
            self.offer_settings_btn_bottom.products = {};
            self.offer_settings_btn_bottom.current_offer_id = 0;

        };
        const make_offer = function () {

            reset_offer();
            let offer_id = self.get_offer_id();
            let index_id = self.get_current_index();
            let offer_forms = self.get_offer_form(offer_id);
            let offer_step = self.get_offer_step(index_id);
            if (self.ol(offer_forms) > 0) {
                self.offer_setting.product_count = self.ol(offer_forms.products);
                self.offer_setting.current_offer_id = offer_id;
                self.offer_setting.model = {
                    ship_dynamic: false,
                    ask_confirmation: false,
                    allow_free_ship_select: false,
                    skip_exist: false,
                };
                self.offer_product_settings.selected_variations = {};
                if (self.ol(offer_forms.settings) > 0) {
                    for (let k in offer_forms.settings) {
                        let fVal;
                        let st = offer_forms.settings[k];
                        if (st === 'false') {
                            fVal = false;
                        } else if (st === 'true') {
                            fVal = true;
                        } else {
                            fVal = st;
                        }
                        self.offer_setting.model[k] = fVal;
                    }
                }


                if (self.offer_setting.product_count > 0) {
                    for (let i in offer_forms.products) {
                        self.offer_product_settings.products[i] = {};
                        self.offer_product_settings.products[i] = offer_forms.products[i];
                        let vars_count = self.ol(offer_forms.variations[i]);
                        self.offer_product_settings.products[i]['vars_count'] = vars_count;
                        if (vars_count > 0) {
                            self.offer_product_settings.products[i]['settings'] = offer_forms.fields[i];
                            self.offer_product_settings.products[i]['variations'] = offer_forms.variations[i];
                            prepare_variations(i);
                        }
                    }
                    self.offer_settings_btn_bottom.products = self.offer_product_settings.products;
                }

                self.offer_product_settings.current_offer_id = offer_id;
                self.offer_product_settings.current_offer_name = offer_step.slug;
                self.offer_product_settings.current_offer = offer_step.name;
                self.offer_product_settings.offer_type = offer_step.type;
                self.offer_settings_btn_bottom.current_offer_id = offer_id;

                if ((0 !== offer_id)) {
                    if (offer_step.state == '1') {
                        self.offer_product_settings.offer_state = true;
                    } else {
                        self.offer_product_settings.offer_state = false;
                    }
                }

                self.update_offer_state(offer_step.state);
                return true;
            }
            return false;
        };
        const build_offer = function () {
            let offer_ready = make_offer();
            if (offer_ready) {
                self.offer_product_settings.selected_product = true;

                let step_el = $(".wfocu_s_menu");
                if (self.ol(self.offer_product_settings.products) > 0) {
                    if (step_el.length > 0) {
                        step_el.each(
                            function () {
                                let t_href = $(this).data('href');
                                if (t_href !== "") {
                                    $(this).attr("href", t_href);
                                }
                            }
                        );
                    }
                }
            }

        };
        const offer_settings = function () {
            self.offer_setting = new Vue(
                {
                    el: "#offer_settings",
                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    methods: {
                        setting_updated: function () {

                            let tempSetting = JSON.stringify(this.model);
                            tempSetting = JSON.parse(tempSetting);
                            this.setting_changes();
                            let data = {"test": true, "funnel_id": self.get_funnel_id(), "offer_id": self.get_offer_id(), "settings": tempSetting};
                            let wp_ajax = wfo_ajax();
                            wp_ajax.ajax("save_funnel_offer_settings", data);

                        },
                        setting_changes: function () {

                            let tempSetting = JSON.stringify(this.model);
                            tempSetting = JSON.parse(tempSetting);
                            let offer_id = self.get_offer_id();
                            let offer = self.get_offer_form(offer_id);
                            offer.settings = tempSetting;
                            self.update_offer_form(offer_id, offer);
                        }
                    },
                    mounted: function () {

                        // window.wfocuBuilderCommons.doAction('wfocu_offer_loaded');
                    },
                    updated: function () {

                        window.wfocuBuilderCommons.doAction('wfocu_offer_loaded');
                    },
                    data: {
                        current_offer_id: 0,
                        product_count: 0,
                        model: {
                            ship_dynamic: false,
                            ask_confirmation: false,
                            allow_free_ship_select: false,
                            skip_exist: false,
                        },

                        schema: {
                            fields: window.wfocuBuilderCommons.applyFilters(
                                'wfocu_offer_settings', [

                                    {
                                        type: "label",
                                        label: "Ask Confirmation",
                                        model: "label_confirmation"
                                    },
                                    {
                                        type: "checkbox",
                                        label: "Ask for confirmation every time user accepts this offer. A new side cart will trigger and ask for confirmation if this option is enabled.",
                                        model: "ask_confirmation",
                                        inputName: 'ask_confirmation',
                                        styleClasses: "wfocu_ask_confirmation",
                                    },
                                    /**
                                     {
								 type: "checkbox",
								 label: "Allow free shipping option for user selection if available in (Parent order) else Parent order shipping method would be considered (Shipping must be enabled for offer.)",
								 model: "allow_free_ship_select",
								 inputName: 'allow_free_ship_select',
								 styleClasses: ['wfocu_allow_free_ship_select', 'wfocu_inner_level_1'],
								 visible: function (model) {
									 return model && model.ship_dynamic == true && model.ask_confirmation == true;
								 }
								}, **/
                                    {
                                        type: "label",
                                        label: "Skip Offer",
                                        model: "label_order"
                                    },
                                    {
                                        type: "checkbox",
                                        label: "Skip this offer if product(s) exist in parent order",
                                        model: "skip_exist",
                                        inputName: 'skip_exist',
                                        styleClasses: "wfocu_skip_exist",
                                    },

                                    {
                                        type: "label",
                                        label: "Terminate Funnel",
                                        model: "label_terminate"
                                    },
                                    {
                                        type: "checkbox",
                                        label: "If this offer is accepted. Customer will be redirected to thank you page.",
                                        model: "terminate_if_accepted",
                                        inputName: 'terminate_if_accepted',
                                    },
                                    {
                                        type: "checkbox",
                                        label: "If this offer is declined. Customer will be redirected to thank you page.",
                                        model: "terminate_if_declined",
                                        inputName: 'terminate_if_declined',
                                    },
                                    {
                                        type: "label",
                                        label: "Tracking Code",
                                        model: "upsell_page_track_code_label"
                                    },
                                    {
                                        type: "checkbox",
                                        label: "Add tracking code if the buyer views this offer",
                                        model: "check_add_offer_script",
                                        inputName: 'check_add_offer_script',
                                    },
                                    {
                                        type: "textArea",
                                        label: "",
                                        model: "upsell_page_track_code",
                                        styleClasses: "wfocu_offer_settings_textarea",
                                        placeholder: 'Paste your code here',
                                        rows: 5,
                                        visible: function (model) {
                                            return model && model.check_add_offer_script == true && model.check_add_offer_script == true;
                                        }
                                    },
                                    {
                                        type: "checkbox",
                                        label: "Add tracking code if the buyer accepts this offer",

                                        model: "check_add_offer_purchase",
                                        inputName: 'check_add_offer_purchase',
                                    },

                                    {
                                        type: "textArea",
                                        label: "",
                                        model: "upsell_page_purchase_code",
                                        styleClasses: "wfocu_offer_settings_textarea",
                                        placeholder: 'Paste your code here',
                                        rows: 5,
                                        visible: function (model) {
                                            return model && model.check_add_offer_purchase == true && model.check_add_offer_purchase == true;
                                        }
                                    },


                                    {
                                        type: "label",
                                        label: "Quantity Selector",
                                        model: "qty_selector_label"
                                    },
                                    {
                                        type: "checkbox",
                                        label: "Allow customer to chose the quantity while purchasing this upsell product(s)",
                                        model: "qty_selector",
                                        inputName: 'qty_selector',
                                    },
                                    {
                                        type: "label",
                                        label: "Maximum Quantity",
                                        model: "qty_max_label",
                                        visible: function (model) {
                                            return model && model.qty_selector == true;
                                        }
                                    },
                                    {
                                        type: "input",
                                        label: "",
                                        inputType: "text",
                                        inputName: 'qty_max',
                                        model: "qty_max",
                                        styleClasses: "wfocu_offer_settings_textarea",
                                        placeholder: 'Input Max Quantity',
                                        visible: function (model) {
                                            return model && model.qty_selector == true;
                                        }
                                    },

                                ]
                            ),
                        },
                        formOptions: {
                            validateAfterLoad: false,
                            validateAfterChanged: true
                        }
                    }
                }
            );
        };

        const offer_settings_btn_bottom = function () {
            self.offer_settings_btn_bottom = new Vue(
                {
                    el: "#offer_settings_btn_bottom",
                    created: function () {
                        let tthis = this;
                        let index = self.get_current_index();

                        let step = self.get_offer_step(index);
                        if (step.hasOwnProperty('name')) {
                            this.current_offer = step.name;
                            this.current_offer_id = step.id;
                            this.products = self.offer_product_settings.products;
                        }
                    },
                    methods: {
                        submit: function () {
                            $('form[data-wfoaction="save_funnel_offer_products"]').trigger('submit');
                        },
                        delete_offer: function (elem, offerID) {
                            let index_id = self.get_current_index();
                            let offer_id = self.get_offer_id();

                            /* When offer tried to remove from ladder */
                            if (typeof offerID != 'undefined') {
                                index_id = $(elem).parents(".wfocu_step_container").attr("data-index_id");
                                offer_id = offerID;
                            }

                            wfocuSweetalert2(
                                $.extend(
                                    {
                                        title: "",
                                        text: "",
                                        type: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#0073aa',
                                        cancelButtonColor: '#e33b3b',
                                        confirmButtonText: '',
                                    }, wfocuParams.alerts.delete_offer
                                )
                            ).then(
                                (result) => {
                                    if (result.value) {
                                        let ladder = $(".wfocu_step_container[data-index_id=" + index_id + "]");
                                        if (ladder.length === 0) {
                                            return;
                                        }
                                        ladder.remove();
                                        self.delete_offer_form(offer_id);
                                        self.delete_offer_step(index_id);

                                        self.set_current_index(0);
                                        let offer = self.get_offer_step(0);
                                        self.set_offer_id(offer.id);
                                        let step = self.get_offer_id();
                                        $('.wfocu_step').removeClass('current_offer');
                                        $('.wfocu_step[data-index_id=0]').addClass('current_offer');

                                        initialize_offer(step);
                                        let wp_ajax = wfo_ajax();
                                        wp_ajax.ajax("remove_offer_from_funnel", {'offer_id': offer_id, 'funnel_id': self.get_funnel_id()});
                                        wp_ajax.success = function (rsp) {
                                            if (rsp.status === true) {
                                                self.update();
                                                offer_build_layout();
                                            }
                                        };
                                    }
                                }
                            ).catch(
                                (e) => {
                                    console.log("Remove offer from list error", e);
                                }
                            );

                        },
                        isEmpty: function isEmpty(obj) {
                            for (let key in obj) {
                                if (obj.hasOwnProperty(key)) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    },
                    data: {
                        current_offer: '',
                        current_offer_id: 0,
                        products: {},
                    }
                }
            );
        };
        const offer_product_settings = function () {
            self.offer_product_settings = new Vue(
                {
                    el: "#wfocu_offer_area",
                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    methods: {

                        prepare_price_help_html: function (product) {
                            let discount_str = '';
                            let init_price_str = this.formatMoney(product.regular_price_raw) + " x" + product.quantity;
                            let shipping_cost_str = '';
                            let discount = 0;
                            let total_str = '';
                            let total = [parseFloat(product.regular_price_raw * product.quantity)];
                            let finalStr = '';


                            switch (product.discount_type) {
                                case "percentage":
                                    discount = (parseFloat(product.regular_price_raw * product.quantity) * (product.discount_amount / 100));
                                    discount_str = this.formatMoney(discount) + ' (' + product.discount_amount + '% of ' + this.formatMoney(product.regular_price_raw * product.quantity) + ')';
                                    break;
                                case "fixed":
                                    discount = (product.discount_amount);
                                    discount_str = this.formatMoney(discount) + ' (fixed discount)';

                                    break;
                            }

                            if (false === self.offer_setting.model.ship_dynamic) {
                                total.push(parseFloat(product.shipping_cost_flat));
                                shipping_cost_str = this.formatMoney(product.shipping_cost_flat) + ' (shipping)';


                            } else {

                                shipping_cost_str = ' (Dynamic Shipping Cost)';


                            }


                            total_str = '<strong>' + this.formatMoney(this.sum(total) - discount) + '</strong>';
                            finalStr = [init_price_str, shipping_cost_str].join(' + ') + ' - ' + discount_str + ' = ' + total_str;

                            return finalStr;
                        },
                        update_offer_price: function (e, index) {
                            let product = this.products[index];


                            if (product.type === 'variable' || product.type === 'variable-subscription') {
                                for (var key in product.variations) {

                                    $('.wfocu_of_price_var_' + index + '_' + key).html(this.offer_price_html_var(product.variations[key], product));
                                    $('.wfocu_of_price_data_var_' + index + '_' + key).html(this.offer_price_tooltip_var(product.variations[key], product));
                                }

                            } else {

                                let gethtml = this.offer_price_html(product);
                                $('.wfocu_of_price_' + index).html(gethtml);
                                $('.wfocu_of_price_data_' + index).html(this.prepare_price_help_html(product));

                            }

                        },
                        offer_price_html: function (product) {

                            let discount = 0;
                            let total_str = '';
                            let total = [parseFloat(product.regular_price_raw * product.quantity)];
                            let finalStr = '';


                            switch (product.discount_type) {
                                case "percentage":

                                    discount = (parseFloat(product.regular_price_raw * product.quantity) * (product.discount_amount / 100));

                                    break;
                                case "fixed":
                                    discount = (product.discount_amount);

                                    break;
                            }

                            if (false === self.offer_setting.model.ship_dynamic) {
                                total.push(parseFloat(product.shipping_cost_flat));

                            }


                            total_str = '<strong>' + this.formatMoney(this.sum(total) - discount) + '</strong>';

                            return total_str;
                        },
                        offer_price_html_var: function (variation, product) {
                            let discount = 0;
                            let total_str = '';
                            let total = [parseFloat(variation.regular_price_raw * product.quantity)];
                            let finalStr = '';


                            switch (product.discount_type) {
                                case "percentage":

                                    discount = (parseFloat(variation.regular_price_raw * product.quantity) * (variation.discount_amount / 100));

                                    break;
                                case "fixed":
                                    discount = (variation.discount_amount);

                                    break;
                            }

                            if (false === self.offer_setting.model.ship_dynamic) {
                                total.push(parseFloat(product.shipping_cost_flat));


                            }


                            total_str = '<strong>' + this.formatMoney(this.sum(total) - discount) + '</strong>';

                            return total_str;
                        },
                        offer_price_tooltip_var: function (variation, product) {


                            let discount_str = '';
                            let init_price_str = this.formatMoney(variation.regular_price_raw) + " x" + product.quantity;
                            let shipping_cost_str = '';
                            let discount = 0;
                            let total_str = '';
                            let total = [parseFloat(variation.regular_price_raw * product.quantity)];
                            let finalStr = '';


                            switch (product.discount_type) {
                                case "percentage":
                                    discount = (parseFloat(variation.regular_price_raw * product.quantity) * (variation.discount_amount / 100));
                                    discount_str = this.formatMoney(discount) + ' (' + variation.discount_amount + '% of ' + this.formatMoney(variation.regular_price_raw * product.quantity) + ')';
                                    break;
                                case "fixed":
                                    discount = (variation.discount_amount);
                                    discount_str = this.formatMoney(discount) + ' (fixed discount)';

                                    break;
                            }


                            if (false === self.offer_setting.model.ship_dynamic) {
                                total.push(parseFloat(product.shipping_cost_flat));
                                shipping_cost_str = this.formatMoney(product.shipping_cost_flat) + ' (shipping)';


                            } else {

                                shipping_cost_str = ' (Dynamic Shipping Cost)';


                            }


                            total_str = '<strong>' + this.formatMoney(this.sum(total) - discount) + '</strong>';
                            finalStr = [init_price_str, shipping_cost_str].join(' + ') + ' - ' + discount_str + ' = ' + total_str;

                            return finalStr;
                        },
                        sum: function (thing) {
                            return thing.reduce(
                                function (a, b) {
                                    return a + b;
                                }, 0
                            );
                        },
                        formatMoney: function (amt) {

                            return accounting.formatMoney(
                                amt, {
                                    symbol: wfocu_wc_params.currency_format_symbol,
                                    decimal: wfocu_wc_params.currency_format_decimal_sep,
                                    thousand: wfocu_wc_params.currency_format_thousand_sep,
                                    precision: wfocu_wc_params.currency_format_num_decimals,
                                    format: wfocu_wc_params.currency_format
                                }
                            )
                        },
                        prettyJSON: function (json) {
                            if (json) {
                                json = JSON.stringify(json, undefined, 4);
                                json = json.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
                                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                                    var cls = 'number';
                                    if (/^"/.test(match)) {
                                        if (/:$/.test(match)) {
                                            cls = 'key';
                                        } else {
                                            cls = 'string';
                                        }
                                    } else if (/true|false/.test(match)) {
                                        cls = 'boolean';
                                    } else if (/null/.test(match)) {
                                        cls = 'null';
                                    }
                                    return '<span class="' + cls + '">' + match + '</span>';
                                });
                            }
                        },
                        set_product: function (product_id) {
                            build_offer();
                        },
                        product_changes: function () {
                            let offer_id = self.get_offer_id();
                            let offer = self.get_offer_form(offer_id);
                            offer.state = this.offer_state;
                            self.update_offer_state(offer.state);
                            self.update_offer_form(offer_id, offer);
                        },
                        update_offer_state: function (event) {

                            let offer_id = self.get_offer_id();
                            let offer = self.get_offer_form(offer_id);
                            if (event.target.checked) {
                                offer.state = '1';
                            } else {
                                offer.state = 0;
                            }
                            self.update_offer_state(offer.state);
                            self.update_offer_form(offer_id, offer);
                        },
                        remove_product: function (unique_id) {
                            let vthis = this;
                            wfocuSweetalert2(
                                $.extend(
                                    {
                                        title: '',
                                        text: "",
                                        type: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#0073aa',
                                        cancelButtonColor: '#e33b3b',
                                        confirmButtonText: ''
                                    }, wfocuParams.alerts.remove_product
                                )
                            ).then(
                                (result) => {
                                    if (result.value) {
                                        let removeProduct = document.getElementById(unique_id);
                                        let product_id = removeProduct.getAttribute('data-proid');

                                        if (typeof product_id === 'undefined' || product_id === "") {
                                            return;
                                        }

                                        if (!self.hp(vthis.products, product_id)) {
                                            return;
                                        }
                                        let wp_ajax = wfo_ajax();
                                        let add_query = {'funnel_id': self.get_funnel_id(), 'offer_id': self.get_offer_id(), 'product_key': product_id};
                                        wp_ajax.ajax('remove_product', add_query);
                                        wp_ajax.success = function (rsp) {
                                            delete vthis.products[product_id];
                                            let keys = self.kys(vthis.products);
                                            if (keys.length > 0) {
                                                let index = keys[0];
                                                setTimeout(
                                                    function (index) {
                                                        vthis.set_product(index);
                                                    }, 100, index
                                                )
                                            } else {

                                                vthis.selected_product = 0;
                                            }
                                            self.delete_offer_product(product_id);
                                        }

                                    }
                                }
                            );
                        },
                        set_variation_discount: function (event, index) {

                            if (typeof this.products[index].variations !== "undefined" && false === this.isEmpty(this.products[index].variations)) {

                                for (var varID in this.products[index].variations) {

                                    this.products[index].variations[varID].discount_amount = this.products[index].discount_amount;

                                }
                                this.hidden_v = Math.random();
                            }

                        },
                        disable_enable_variation_row: function (index, event, var_index) {

                            if ($(event.target).length > 0) {
                                let parent = $(event.target).parents(".variation_products");
                                if (event.target && event.target.checked) {
                                    let variations_length = parent.find(".variation_check").length;
                                    let checked_length = parent.find(".variation_check:checked").length;
                                    if (variations_length > 0 && (variations_length === checked_length)) {
                                        parent.find(".disable_enable_variation").prop("checked", true);
                                    }
                                    parent.find("[data-variation=" + var_index + "]").prop("readonly", false);
                                    parent.find(".default_variation[data-variation=" + var_index + "]").prop("disabled", false);

                                    let have_checke_variation = parent.find('.default_variation:checked');

                                    if (have_checke_variation.length === 0) {
                                        parent.find(".default_variation[data-variation=" + var_index + "]").prop("checked", true);
                                    }

                                    this.selected_variations[var_index] = true;
                                } else {
                                    parent.find(".disable_enable_variation").prop("checked", false);

                                    parent.find("[data-variation=" + var_index + "]").prop("readonly", true);
                                    parent.find(".default_variation[data-variation=" + var_index + "]").prop("checked", false);
                                    parent.find(".default_variation[data-variation=" + var_index + "]").prop("disabled", true);

                                    let have_default_variation = parent.find('.default_variation:checked');

                                    if (have_default_variation.length === 0) {
                                        let have_checke_variation = parent.find('.variation_check:checked');
                                        if (have_checke_variation.length > 0) {
                                            have_checke_variation.eq(0).parents(".product_variation_row").find(".default_variation").prop("checked", true);
                                        }
                                    }

                                    delete this.selected_variations[var_index];
                                }
                            }
                        },

                        disable_enable_variation: function (index, event) {


                            if ($(event.target).length > 0) {
                                let parent = $(event.target).parents(".variation_products");
                                if (event.target && event.target.checked) {
                                    let checked_length = parent.find(".default_variation:checked").length;
                                    if (checked_length === 0) {
                                        parent.find(".default_variation").eq(0).prop("checked", true);
                                    }
                                    parent.find(".default_variation").prop('disabled', false);
                                    parent.find(".variation_check").prop('checked', true);
                                    parent.find("[data-variation]").prop('readonly', false);

                                } else {
                                    parent.find(".default_variation").prop("checked", false).prop('disabled', true);
                                    parent.find(".variation_check").prop('checked', false);
                                    parent.find("[data-variation]").prop('readonly', true);
                                    this.selected_variations = {};
                                }
                            }

                        },
                        isEmpty: function isEmpty(obj) {
                            for (let key in obj) {
                                if (obj.hasOwnProperty(key)) {
                                    return false;
                                }
                            }
                            return true;
                        },

                        numberofproducts: function () {
                            return self.ol(this.products);
                        },
                        set_zero: function (event, index) {
                            console.log(event.keyCode);
                        }
                    },
                    data: {
                        modal: false,
                        current_offer: '',
                        current_offer_id: 0,
                        current_offer_name: '',
                        offer_type: '',
                        products: {},
                        custom: {},
                        variations: {},
                        selected_offer_options: "",
                        selected_product: 0,
                        selected_variations: {},
                        offer_state: default_offer_state,
                        current_template: "",
                        is_multi_product_allow: false,
                        url: "",
                        hidden_v: 0,

                    },

                }
            );
            let index = self.get_current_index();
            let step = self.get_offer_step(index);
            self.offer_product_settings.is_multi_product_allow = wfocu.is_multiple_product_search;

            if (step.hasOwnProperty('name')) {
                self.offer_product_settings.current_offer = step.name;
                self.offer_product_settings.url = step.url;
                self.offer_product_settings.current_offer_name = step.slug;
                self.offer_product_settings.offer_type = step.type;

                $('.wfocu_step[data-index_id=' + index + ']').addClass('current_offer');
            }
        };
        const add_new_step_reset = function () {
            if (self.add_new_offer_setting !== null) {

                self.add_new_offer_setting.model.step_type = "upsell";
                self.add_new_offer_setting.model.funnel_step_name = "";
                self.add_new_offer_setting.$refs.addOfferForm.validate();
            }
        }
        const add_new_step = function (modal) {
            if (add_new_step_is_open === true) {
                return;
            }
            add_new_step_is_open = true;

            self.add_new_offer_setting = new Vue(
                {
                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    data: {

                        modal: modal,
                        model: {
                            step_type: "upsell",
                            show_select_btn: false
                        },
                        schema: {
                            fields: add_new_offer_setting_fields,
                        },
                        formOptions: {
                            validateAfterLoad: true,
                            validateAfterChanged: true
                        }
                    }
                }
            ).$mount('#part3');

            if (self.ol(self.get_offer_forms()) > 0) {
                self.add_new_offer_setting.model.show_select_btn = true;
            }

        };

        const update_setting_populate = function () {
            if (typeof self.update_step_setting !== 'undefined') {

                self.update_step_setting.model.funnel_step_name = self.offer_product_settings.current_offer;
                self.update_step_setting.model.step_type = self.offer_product_settings.offer_type;
                self.update_step_setting.model.current_offer = self.offer_product_settings.current_offer_id;
                self.update_step_setting.model.funnel_step_slug = self.offer_product_settings.current_offer_name;

            }
        }
        const update_step = function () {

            if (update_step_is_open === true) {
                return;
            }
            update_step_is_open = true;

            self.update_step_setting = new Vue(
                {

                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    data: {

                        // modal: modal,
                        model: {
                            funnel_step_name: self.offer_product_settings.current_offer,
                            step_type: self.offer_product_settings.offer_type,
                            current_offer: self.offer_product_settings.current_offer_id,
                            url: '',
                            funnel_step_slug: self.offer_product_settings.current_offer_name,
                        },
                        schema: {
                            fields: update_step_settings,
                        },
                        formOptions: {
                            validateAfterLoad: false,
                            validateAfterChanged: true,
                            validateBeforeSubmit: true
                        }
                    }
                }
            ).$mount('#part4');

            if (self.ol(self.get_offer_forms()) > 0) {
                self.update_step_setting.model.show_select_btn = true;
            }

        };
        /**
         * Product search model
         * @param modal
         */
        const product_search_setting = function (modal) {
            if (product_search_is_open === true) {
                return;
            }
            product_search_is_open = true;
            self.product_search_setting = new Vue(
                {
                    el: '#modal-add-product-form',
                    components: {
                        Multiselect: window.VueMultiselect.default
                    },
                    data: {
                        modal: modal,
                        isLoading: false,
                        funnel_id: self.get_funnel_id(),
                        products: [],
                        is_single: '',
                        include_variations: false,
                        selectedProducts: []
                    },
                    methods: {
                        onSubmit: function () {
                            let wp_ajax = wfo_ajax();
                            let vthis = this;
                            vthis.modal.startLoading();
                            let selected_products = [];
                            let products = this.selectedProducts;
                            if (wfocu.is_multiple_product_search === true) {
                                if (self.ol(this.selectedProducts) > 0) {
                                    for (let pid in products) {
                                        selected_products.push(products[pid]["id"]);
                                    }
                                }
                            } else {
                                selected_products.push(products.id);
                            }
                            let add_query = {'funnel_id': this.funnel_id, 'offer_id': self.get_offer_id(), 'products': selected_products};
                            wp_ajax.ajax('add_product', add_query);
                            wp_ajax.success = function (rsp) {
                                if (self.ol(rsp) > 0 && self.hp(rsp, 'data')) {
                                    self.add_products(rsp.data);
                                }
                                $("#modal-add-product").iziModal('close');
                            };
                            wp_ajax.complete = function () {
                                vthis.clearAll();
                                vthis.modal.stopLoading();
                            };
                        },
                        asyncFind(query) {
                            let vthis = this;
                            vthis.isLoading = true
                            if (query !== "") {
                                clearTimeout(product_search_timeout);
                                product_search_timeout = setTimeout(
                                    function (query, vthis) {
                                        let wp_ajax = wfo_ajax();
                                        let product_query = {'term': query, 'variations': vthis.include_variations};
                                        wp_ajax.ajax('product_search', product_query);
                                        wp_ajax.success = function (rsp) {
                                            vthis.products = rsp;
                                            vthis.isLoading = false;
                                        };
                                        wp_ajax.complete = function () {
                                            vthis.isLoading = false;
                                        };
                                    }, 1000, query, vthis
                                );
                            } else {
                                vthis.isLoading = false;
                            }
                        },
                        clearAll() {
                            this.products = [];
                            this.selectedProducts = [];
                            this.isLoading = false;

                        }
                    }
                }
            );
        };
        /**
         * Page search model
         * @param modal
         */
        const page_search_setting = function (modal) {
            if (page_search_is_open === true) {
                return;
            }
            page_search_is_open = true;
            self.page_search_setting = new Vue(
                {
                    el: '#modal-page-search-form',
                    components: {
                        Multiselect: window.VueMultiselect.default
                    },
                    data: {
                        modal: modal,
                        isLoading: false,
                        funnel_id: self.get_funnel_id(),
                        page_id: 0,
                        products: [],
                        is_single: '',
                        include_variations: false,
                        selectedProducts: []
                    },
                    methods: {
                        onSubmit: function () {
                            let wp_ajax = wfo_ajax();
                            let vthis = this;
                            vthis.modal.startLoading();
                            let selected_page = '0';
                            let selected_page_name = '';
                            let products = this.selectedProducts;
                            if (products.hasOwnProperty('id') && products.hasOwnProperty('page_name')) {
                                selected_page = products.id;
                                selected_page_name = products.page_name;
                                self.design_settings.custom_url = products.url;
                                let offer_id = self.get_offer_id();
                                let add_query = {'funnel_id': this.funnel_id, offer_id: offer_id, 'page_id': selected_page};
                                wp_ajax.ajax('get_custom_page', add_query);
                                wp_ajax.success = function (rsp) {
                                    wfocu.offers[offer_id].template_custom_path = rsp.data.link;
                                    wfocu.offers[offer_id].template_custom_name = rsp.data.title;

                                    self.design_settings.set_template('custom-page');

                                    self.design_settings.currentTemplateName = rsp.data.title;
                                    self.design_settings.currentTemplatePath = rsp.data.link;

                                    $("#modal-prev-template_custom-page").iziModal('close');
                                };
                                wp_ajax.complete = function () {
                                    vthis.clearAll();
                                    vthis.modal.stopLoading();
                                };
                            }
                        },
                        asyncFind(query) {
                            let vthis = this;
                            vthis.isLoading = true
                            if (query !== "") {
                                clearTimeout(product_search_timeout);
                                product_search_timeout = setTimeout(
                                    function (query, vthis) {
                                        let wp_ajax = wfo_ajax();
                                        let product_query = {'term': query, 'funnel_id': self.get_funnel_id()};
                                        wp_ajax.ajax('page_search', product_query);
                                        wp_ajax.success = function (rsp) {
                                            vthis.products = rsp;
                                            vthis.isLoading = false;
                                        };
                                        wp_ajax.complete = function () {
                                            vthis.isLoading = false;
                                        };
                                    }, 1000, query, vthis
                                );
                            } else {
                                vthis.isLoading = false;
                            }
                        },
                        clearAll() {
                            this.products = [];
                            this.selectedProducts = [];
                            this.isLoading = false;

                        }
                    }
                }
            );
        };

        const inialize_offer_models = function () {
            let modal_add_offer_step = $("#modal-add-offer-step");
            if (modal_add_offer_step.length > 0) {
                modal_add_offer_step.iziModal(
                    {
                        title: wfocuParams.modal_add_offer_step_text,
                        headerColor: '#6dbe45',
                        background: '#efefef',
                        borderBottom: false,
                        width: 600,
                        overlayColor: 'rgba(0, 0, 0, 0.6)',
                        transitionIn: 'fadeInDown',
                        transitionOut: 'fadeOutDown',
                        navigateArrows: "false",
                        onOpening: function (modal) {
                            modal.startLoading();
                        },
                        onOpened: function (modal) {
                            modal.stopLoading();
                            add_new_step_reset();
                            add_new_step(modal);

                        },
                        onClosed: function (modal) {

                        }
                    }
                );
            }
            let modal_add_add_product = $("#modal-add-product");
            if (modal_add_add_product.length > 0) {
                modal_add_add_product.iziModal(
                    {
                        title: wfocuParams.modal_add_add_product,
                        headerColor: '#6dbe45',
                        background: '#efefef',
                        borderBottom: false,
                        width: 600,
                        overlayColor: 'rgba(0, 0, 0, 0.6)',
                        transitionIn: 'fadeInDown',
                        transitionOut: 'fadeOutDown',
                        navigateArrows: "false",
                        onOpening: function (modal) {
                            modal.startLoading();
                        },
                        onOpened: function (modal) {
                            modal.stopLoading();
                            product_search_setting(modal);
                        },
                        onClosed: function (modal) {
                            console.log('onClosed');
                        }
                    }
                );
            }

            if ($('#modal-template_success').length > 0) {
                $("#modal-template_success").iziModal(
                    {
                        title: wfocu.texts.update_template,
                        icon: 'icon-check',
                        headerColor: '#6dbe45',
                        background: '#6dbe45',
                        borderBottom: false,
                        width: 600,
                        timeout: 1500,
                        timeoutProgressbar: true,
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOutDown',
                        bottom: 0,
                        loop: true,
                        pauseOnHover: true,
                        overlay: false
                    }
                );
            }

            if ($('#modal-settings_success').length > 0) {

                $("#modal-settings_success").iziModal(
                    {
                        title: wfocu.texts.settings_success,
                        icon: 'icon-check',
                        headerColor: '#6dbe45',
                        background: '#6dbe45',
                        borderBottom: false,
                        width: 600,
                        timeout: 1500,
                        timeoutProgressbar: true,
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOutDown',
                        bottom: 0,
                        loop: true,
                        pauseOnHover: true,
                        overlay: false
                    }
                );
            }

            if ($('#modal-global-settings_success').length > 0) {

                $("#modal-global-settings_success").iziModal(
                    {
                        title: wfocu.texts.settings_success,
                        icon: 'icon-check',
                        headerColor: '#6dbe45',
                        background: '#6dbe45',
                        borderBottom: false,
                        width: 600,
                        timeout: 1500,
                        timeoutProgressbar: true,
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOutDown',
                        //  bottom: 0,
                        loop: true,
                        pauseOnHover: true
                    }
                );
            }
            if ($('#modal-wfocu-state-change-success').length > 0) {

                $("#modal-wfocu-state-change-success").iziModal(
                    {
                        title: '',
                        icon: 'icon-check',
                        headerColor: '#6dbe45',
                        background: '#6dbe45',
                        borderBottom: false,
                        width: 600,
                        timeout: 1500,
                        timeoutProgressbar: true,
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOutDown',
                        bottom: 0,
                        loop: true,
                        pauseOnHover: true,
                        overlay: false
                    }
                );
            }
            if ($('#modal-rules-settings_success').length > 0) {

                $("#modal-rules-settings_success").iziModal(
                    {
                        title: wfocu.texts.rules_success,
                        icon: 'icon-check',
                        headerColor: '#6dbe45',
                        background: '#6dbe45',
                        borderBottom: false,
                        width: 600,
                        timeout: 1500,
                        timeoutProgressbar: true,
                        transitionIn: 'fadeInUp',
                        transitionOut: 'fadeOutDown',
                        bottom: 0,
                        loop: true,
                        pauseOnHover: true,
                        overlay: false
                    }
                );
            }

            $("#modal-section_product_success").iziModal(
                {
                    title: wfocu.texts.product_success,
                    icon: 'icon-check',
                    headerColor: '#6dbe45',
                    background: '#6dbe45',
                    borderBottom: false,
                    width: 600,
                    timeout: 1500,
                    timeoutProgressbar: true,
                    transitionIn: 'fadeInUp',
                    transitionOut: 'fadeOutDown',
                    bottom: 0,
                    loop: true,
                    pauseOnHover: true,
                    overlay: false
                }
            );

            let modal_update_offer = $("#modal-update-offer")
            if (modal_update_offer.length > 0) {
                modal_update_offer.iziModal(
                    {
                        title: wfocuParams.modal_update_offer,
                        headerColor: '#6dbe45',
                        background: '#efefef',
                        borderBottom: false,
                        history: false,
                        width: 600,
                        overlayColor: 'rgba(0, 0, 0, 0.6)',
                        transitionIn: 'bounceInDown',
                        transitionOut: 'bounceOutDown',
                        navigateCaption: true,
                        navigateArrows: "false",
                        onOpening: function (modal) {
                            modal.startLoading();

                        },
                        onOpened: function (modal) {
                            modal.stopLoading();
                            update_setting_populate();
                            update_step();
                        },
                        onClosed: function (modal) {
                            //console.log('onClosed');
                        }
                    }
                );
            }
            $("input[name='update_step_type']").on(
                'change', function () {

                }
            );
            $("#modal-section-success_shortcodes6456").iziModal(
                {
                    title: wfocu.texts.shortcode_copy_message,
                    icon: 'icon-check',
                    headerColor: '#6dbe45',
                    background: '#6dbe45',
                    borderBottom: false,
                    width: 600,
                    timeout: 1000,
                    timeoutProgressbar: true,
                    transitionIn: 'fadeInUp',
                    transitionOut: 'fadeOutDown',
                    bottom: 0,
                    loop: true,
                    pauseOnHover: true,
                    overlay: false
                }
            );
            let modal_prev_temp = $(".modal-prev-temp")
            if (modal_prev_temp.length > 0) {
                modal_prev_temp.iziModal(
                    {
                        headerColor: '#6dbe45',
                        background: '#efefef',
                        borderBottom: false,
                        history: false,
                        overlayColor: 'rgba(0, 0, 0, 0.6)',
                        transitionIn: 'bounceInDown',
                        transitionOut: 'bounceOutDown',
                        navigateCaption: true,
                        navigateArrows: "false",
                        onOpening: function (modal) {
                            modal.startLoading();
                        },
                        onOpened: function (modal) {
                            modal.stopLoading();

                            page_search_setting(modal);
                        },
                        onClosed: function (modal) {
                            //console.log('onClosed');
                        }

                    }
                );

            }
            let modal_global_Settings_offer_help = $(".wfocu-global-settings-help-ofc");

            if (modal_global_Settings_offer_help.length > 0) {
                modal_global_Settings_offer_help.iziModal(
                    {
                        headerColor: '#6dbe45',
                        background: '#efefef',
                        borderBottom: false,
                        history: false,
                        overlayColor: 'rgba(0, 0, 0, 0.6)',
                        transitionIn: 'bounceInDown',
                        transitionOut: 'bounceOutDown',
                        navigateCaption: true,
                        navigateArrows: "false",
                        width: 1000,


                    }
                );

            }
            let modal_funnel_Settings_help_messages = $(".wfocu-funnel-settings-help-messages");

            if (modal_funnel_Settings_help_messages.length > 0) {
                modal_funnel_Settings_help_messages.iziModal(
                    {
                        headerColor: '#6dbe45',
                        background: '#efefef',
                        borderBottom: false,
                        history: false,
                        overlayColor: 'rgba(0, 0, 0, 0.6)',
                        transitionIn: 'bounceInDown',
                        transitionOut: 'bounceOutDown',
                        navigateCaption: true,
                        navigateArrows: "false",
                        width: 1000,


                    }
                );

            }
        };
        this.show_offer_design_help = function () {
            $(".wfocu-global-settings-help-ofc").iziModal('open');
            return false;
        }
        this.show_funnel_design_messages = function () {
            $(".wfocu-funnel-settings-help-messages").iziModal('open');
            return false;
        }
        const initialize_offer_ajax_handlers = function () {
            return new wp_admin_ajax(
                '.wfocu_forms_wrap', true, function (ajax) {
                    ajax.before_send = function (element, action) {
                        if (ajax.action === 'wfocu_update_funnel') {
                            self.funnel_setting.modal.startLoading();
                        }

                        if (ajax.action === 'wfocu_add_offer') {
                            self.add_new_offer_setting.modal.startLoading();
                        }

                    };
                    ajax.data = function (data, element) {
                        data.append('funnel_id', self.get_funnel_id());
                        data.append('offer_id', self.get_offer_id());

                        return data;
                    };
                    ajax.success = function (rsp) {

                        if (ajax.action === 'wfocu_update_funnel') {
                            if (rsp.status === true) {
                                self.update_funnel(rsp.data);
                            }
                            self.funnel_setting.modal.stopLoading();
                            $("#modal-update-funnel").iziModal('close');
                        }

                        if (ajax.action === 'wfocu_add_offer') {


                            self.create_step(rsp);
                            self.add_new_offer_setting.modal.stopLoading();
                            $("#modal-add-offer-step").iziModal('close');


                        }

                        if (ajax.action === 'wfocu_update_offer') {
                            if (rsp.status === true) {
                                let newname = rsp.name;
                                let url = rsp.url;
                                let slug = rsp.slug;
                                let type = rsp.type;
                                self.update_current_step('name', newname);
                                self.update_current_step('url', url);
                                self.update_current_step('slug', slug);
                                self.update_current_step('type', type);
                                $("#modal-update-offer").iziModal('close');
                            }
                        }
                        if (ajax.action === "wfocu_save_funnel_offer_products") {
                            if (rsp.status === true) {
                                self.offer_setting.setting_updated();
                            }
                        }
                        if (ajax.action === "wfocu_save_funnel_offer_products") {

                        }
                    }
                }
            );

        };
        const initialize_funnel_offer = function () {
            sortable();
            offer_steps_event();
            inialize_offer_models();

            offer_product_settings();

            offer_settings();
            offer_settings_btn_bottom();
            build_offer();

        };

        const update_funnel_settings = function (modal) {
            if (update_funnel_model === true) {
                return;
            }
            update_funnel_model = true;
            self.funnel_setting = new Vue(
                {
                    el: "#part-update-funnel",
                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    data: {
                        modal: modal,
                        model: {
                            funnel_name: self.get_funnel_name(),
                            funnel_desc: self.get_funnel_desc(),
                        },
                        schema: {
                            fields: funnel_setting_fields
                        },
                        formOptions: {
                            validateAfterLoad: false,
                            validateAfterChanged: true,
                            validateBeforeSubmit: true
                        }
                    }
                }
            );

        };

        // Offer step section end here

        const design_settings = function () {
            self.design_settings = new Vue(
                {
                    el: "#wfocu_step_design",
                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    created: function () {
                        let indexI = self.get_current_index();
                        let step = self.get_offer_step(indexI);
                        let current_offer = self.get_offer_form(step.id);
                        this.products = current_offer.products;
                        this.shortcodes = {};

                        for (let key in current_offer.products) {

                            this.shortcodes[key] = {name: current_offer.products[key].name, 'shortcodes': {}};

                            for (let index1 in wfocuParams.shortcodes) {

                                this.shortcodes[key].shortcodes[index1] = {};
                                this.shortcodes[key].shortcodes[index1].label = wfocuParams.shortcodes[index1].label;
                                this.shortcodes[key].shortcodes[index1].value = wfocuParams.shortcodes[index1].code.replace('%s', key);
                            }
                        }
                    },
                    methods: {
                        isEmpty: function (obj) {
                            for (let key in obj) {
                                if (obj.hasOwnProperty(key)) {
                                    return false;
                                }
                            }
                            return true;
                        },
                        getOfferNameByID: function () {
                            let offerID = this.current_offer_id;

                            let steps = wfocu.steps;
                            for (var key in  steps) {

                                if (steps[key].id == offerID) {
                                    return steps[key].name;
                                }
                            }
                            return '';
                        },
                        getTemplateNiceName: function (slug) {
                            if (typeof slug == 'undefined') {
                                slug = this.current_template;
                            }

                            if (slug !== 'custom-page') {
                                for (var key in wfocuParams.templates) {
                                    for (var tempSlug in wfocuParams.templates[key]) {
                                        if (tempSlug === slug) {
                                            return wfocuParams.templates[key][tempSlug].name;
                                        }
                                    }
                                }
                            } else {
                                return this.currentTemplateName;
                            }

                            return '';
                        },
                        copy: function (event) {
                            var getInput = event.target.parentNode.querySelector('.wfocu-scode-text input');
                            getInput.select();
                            document.execCommand("copy");
                            $("#modal-section-success_shortcodes6456").iziModal('open');
                        },

                        set_template: function (template_id) {

                            if (this.current_template == template_id) {
                                return false;
                            }

                            self.design_settings.current_template = template_id;
                            let id = self.get_funnel_id();
                            let offer_id = self.get_offer_id();
                            wfocu.offers[offer_id].template = template_id;
                            let data = {"id": funnel_id, 'offer_id': offer_id, 'template': template_id};
                            let wp_ajax = wfo_ajax();
                            wp_ajax.ajax('update_template', data);
                            wp_ajax.success = function (rsp) {
                                $('.wfocu_apply_template').css('display', 'inline-block');
                                $('.wfocu_customize_template').css('display', 'none');
                                $("#modal-template_success").iziModal('open');
                                $("#modal-template_success").iziModal('setTitle', wfocu.texts.update_template);
                            }

                        },
                        set_preview: function (template_id) {

                        },
                        customize_template: function (template_id) {
                            let offer_id = self.get_offer_id();

                            let custom_url
                            if (template_id == 'custom-page') {
                                custom_url = (window.wfocu.offers[offer_id].template_custom_path);

                                window.open(custom_url.replace(/&amp;/g, '&'));
                            } else {
                                custom_url = "";
                                this.custom_url = "";
                                let url = self.build_customize_url(template_id, custom_url);

                                if (url != "") {
                                    window.open(url);
                                }
                            }

                        }
                    },
                    data: {
                        current_offer: '',
                        custom_url: "",
                        current_offer_id: 0,
                        offer_state: default_offer_state,
                        current_template: 'sp-classic',
                        have_multiple_product: 1,
                        index_id: 0,
                        currentTemplateName: '',
                        currentTemplatePath: '',
                        shortcodes: {},
                        products: {},
                    }

                }
            );
            let index = self.get_current_index();
            let step = self.get_offer_step(index);
            if (step.hasOwnProperty('name')) {
                self.design_settings.current_offer = step.name;
                self.design_settings.current_offer_id = step.id;
                self.design_settings.index_id = step.index;
                let current_offer = self.get_offer_form(step.id);

                self.design_settings.current_template = current_offer.template;
                self.design_settings.currentTemplateName = self.hp(current_offer, 'template_custom_name') ? current_offer.template_custom_name : 'Custom Page';
                self.design_settings.currentTemplatePath = self.hp(current_offer, 'template_custom_path') ? current_offer.template_custom_path : '';
                self.design_settings.have_multiple_product = self.ol(current_offer.products) > 1 ? 2 : 1;
                $('.wfocu_step[data-index_id=' + index + ']').addClass('current_offer');

            }
        };

        const design_setting_steps_events = function () {
            if ($(step_list).length > 0) {
                $(step_list).off('click');
                $(step_list).on('click', function (e) {
                        e.preventDefault();
                        let index_id = $(this).data('index_id');
                        let current_index = self.get_current_index();
                        if (index_id !== current_index) {
                            $('.wfocu_step').removeClass('current_offer');
                            $(this).addClass('current_offer');
                            let offer_id = $(this).data('offer_id');

                            let offer_title = $(this).data('offer_title');
                            self.set_current_index(index_id);
                            self.set_offer_id(offer_id);
                            let offer = self.get_offer_form(offer_id);

                            self.design_settings.current_offer = offer_title;
                            self.design_settings.index_id = index_id;
                            self.design_settings.current_offer_id = offer_id;
                            self.design_settings.current_template = offer.template;
                            self.design_settings.currentTemplateName = self.hp(offer, 'template_custom_name') ? offer.template_custom_name : 'Custom Page';
                            self.design_settings.currentTemplatePath = self.hp(offer, 'template_custom_path') ? offer.template_custom_path : '';

                            self.design_settings.shortcodes = {};
                            self.design_settings.products = offer.products;
                            for (let key in offer.products) {
                                self.design_settings.shortcodes[key] = {name: offer.products[key].name, 'shortcodes': {}};
                                for (let index in wfocuParams.shortcodes) {
                                    self.design_settings.shortcodes[key].shortcodes[index] = {};
                                    self.design_settings.shortcodes[key].shortcodes[index].label = wfocuParams.shortcodes[index].label
                                    self.design_settings.shortcodes[key].shortcodes[index].value = wfocuParams.shortcodes[index].code.replace('%s', key);

                                }
                            }

                            let have_multiple_product = JSON.stringify(offer.have_multiple_product);
                            have_multiple_product = JSON.parse(have_multiple_product);
                            self.design_settings.have_multiple_product = have_multiple_product;
                            window.wfocuBuilderCommons.doAction('wfocu_offer_switched');
                        }
                    }
                );
            }
        };

        const funnel_settings = function () {
            self.offer_setting = new Vue(
                {
                    el: "#wfocu_funnel_setting",
                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    methods: {
                        onSubmit: function () {
                            let tempSetting = JSON.stringify(this.model);
                            tempSetting = JSON.parse(tempSetting);
                            let data = {"funnel_id": self.get_funnel_id(), "data": tempSetting};

                            let wp_ajax = wfo_ajax();
                            let ajax_loader = $('#wfocu_funnel_setting').find('.wfocu_save_funnel_setting_ajax_loader');
                            ajax_loader.addClass('ajax_loader_show');
                            wp_ajax.ajax("save_funnel_settings", data);
                            wp_ajax.success = function (rsp) {
                                ajax_loader.removeClass('ajax_loader_show');
                                $('#modal-settings_success').iziModal('open');
                            };
                            return false;
                        },

                    },
                    data: {
                        current_offer_id: 0,
                        product_count: 0,
                        model: wfocuParams.funnel_settings,
                        schema: {
                            fields: funnel_settings_fields,

                        },
                        formOptions: {
                            validateAfterLoad: false,
                            validateAfterChanged: true
                        }
                    },
                    mounted: function () {
                        $('.wfocu_to_html label').each(
                            function (k, v) {
                                var html = this.innerHTML;
                                let newHtml = '<label>' + html + '</label>';
                                newHtml = newHtml.replace(/&lt;/g, '<');
                                newHtml = newHtml.replace(/&gt;/g, '>');

                                $(this).replaceWith(newHtml);
                            }
                        );
                    },
                }
            );
        };

        const global_settings = function () {

            self.global_settings_funnels = new Vue(
                {
                    el: "#wfocu_global_setting_vue",
                    components: {

                        "vue-form-generator": VueFormGenerator.component,
                    },
                    methods: {
                        onSubmit: function () {
                            $(".wfocu_save_btn_style").addClass('disabled');
                            $('.wfocu_loader_global_save').addClass('ajax_loader_show');
                            let tempSetting = JSON.stringify(this.model);
                            tempSetting = JSON.parse(tempSetting);
                            let data = {"data": tempSetting};

                            let wp_ajax = wfo_ajax();
                            wp_ajax.ajax("save_global_settings", data);
                            wp_ajax.success = function (rsp) {
                                $('#modal-global-settings_success').iziModal('open');
                                $(".wfocu_save_btn_style").removeClass('disabled');
                                $('.wfocu_loader_global_save').removeClass('ajax_loader_show');
                            };
                            return false;
                        },

                    },
                    data: {
                        current_offer_id: 0,
                        product_count: 0,
                        colorPickerFields: [
                            'offer_yes_btn_bg_cl',
                            'offer_yes_btn_sh_cl',
                            'offer_yes_btn_txt_cl',
                            'offer_yes_btn_bg_cl_h',
                            'offer_yes_btn_sh_cl_h',
                            'offer_yes_btn_txt_cl_h',
                            'offer_no_btn_txt_cl',
                            'offer_no_btn_txt_cl_h',
                            'cart_opener_text_color',
                            'cart_opener_background_color'
                        ],
                        model: wfocuParams.global_settings,
                        schema: {
                            groups: [
                                {
                                    legend: wfocuParams.legends_texts.gateways,
                                    fields: global_settings_gateway_fields
                                },
                                {
                                    legend: wfocuParams.legends_texts.order_statuses,
                                    fields: global_settings_order_statuses_fields
                                },
                                {
                                    legend: wfocuParams.legends_texts.emails,
                                    fields: global_settings_emails_fields
                                },
                                {
                                    legend: wfocuParams.legends_texts.tan,
                                    fields: global_settings_tan_fields
                                },
                                {
                                    legend: wfocuParams.legends_texts.scripts,
                                    fields: global_settings_scripts_fields
                                },

                                {
                                    legend: wfocuParams.legends_texts.offer_conf,
                                    fields: global_settings_offer_confirmation_fields
                                },
                                {
                                    legend: wfocuParams.legends_texts.misc,
                                    fields: global_settings_misc_fields
                                },

                            ],

                        },
                        formOptions: {
                            validateAfterLoad: false,
                            validateAfterChanged: true
                        }
                    },

                    mounted: function () {

                        $('.hint').each(
                            function (k, v) {
                                var html = this.innerHTML;
                                let newHtml = '<span class="hint">' + html + '</span>';
                                newHtml = newHtml.replace(/&lt;/g, '<');
                                newHtml = newHtml.replace(/&gt;/g, '>');

                                $(this).replaceWith(newHtml);
                            }
                        );
                        $('.wfocu_to_html label').each(
                            function (k, v) {
                                var html = this.innerHTML;
                                let newHtml = '<label>' + html + '</label>';
                                newHtml = newHtml.replace(/&lt;/g, '<');
                                newHtml = newHtml.replace(/&gt;/g, '>');

                                $(this).replaceWith(newHtml);
                            }
                        );

                        for (let key in this.colorPickerFields) {
                            $('input[name="' + this.colorPickerFields[key] + '"]').wpColorPicker(
                                {
                                    change: function (event, ui) {
                                        console.log(ui.color.toString());

                                        var element = event.target;

                                        var name = element.name;

                                        self.global_settings_funnels.model[name] = ui.color.toString();
                                    }
                                    // Add your code here
                                }
                            );
                        }

                    }
                }
            );
        };

        const initialize_funnel_design = function () {
            design_settings();
            design_setting_steps_events();

        };

        const initialize_global_models = function () {
            let funnel_div = $("#modal-update-funnel");
            if (funnel_div.length > 0) {
                funnel_div.iziModal(
                    {
                        title: wfocuParams.modal_funnel_div,
                        headerColor: '#6dbe45',
                        background: '#efefef',
                        borderBottom: false,
                        history: false,
                        width: 600,
                        overlayColor: 'rgba(0, 0, 0, 0.6)',
                        transitionIn: 'bounceInDown',
                        transitionOut: 'bounceOutDown',
                        navigateCaption: true,
                        navigateArrows: "false",
                        onOpening: function (modal) {
                            modal.startLoading();
                        },
                        onOpened: function (modal) {
                            modal.stopLoading();
                            update_funnel_settings(modal);
                        },
                        onClosed: function (modal) {
                            console.log('onClosed');
                        }
                    }
                );
            }
        };

        const jquery_event = function () {


            $(".wfocu_funnel_rule_add_settings").on(
                "click", function () {
                    $("#wfocu_funnel_rule_add_settings").attr("data-is_rules_saved", "yes");
                    $("#wfocu_funnel_rule_settings").removeClass('wfocu-tgl');
                }
            );

            $("#update_offer_base").on(
                "keyup", function () {
                    let vl = $(this).val();
                    vl = vl.replace(/[\r\n\t -]+/g, '-');
                    let site_url = self.jsp(wfocu.site_url);
                    let offer_slug = self.jsp(wfocu.offer_slug);
                    $(".show_half_link").text(site_url + "/" + offer_slug);
                }
            );

        };

        let prepare_funnel_data = function () {
            current_offer_id = (wfocu.hasOwnProperty('steps') && self.ol(wfocu.steps) > 0) ? wfocu.steps[0]['id'] : 0;
            funnel_id = wfocu.hasOwnProperty('id') ? wfocu.id : 0;
            offer_steps = (wfocu.hasOwnProperty('steps') && self.ol(wfocu.steps) > 0) ? wfocu.steps : [];
            offer_forms = (wfocu.hasOwnProperty('offers') && self.ol(wfocu.offers) > 0) ? wfocu.offers : {};
            current_index = 0;

            if (self.hp(offer_forms, current_offer_id) && self.hp(offer_forms[current_offer_id], 'products')) {
                let offer_product_keys = self.kys(offer_forms[current_offer_id]['products']);
                selected_product = offer_product_keys.length > 0 ? offer_product_keys[0] : '';
            }
        };
        const init = function () {
            prepare_funnel_data();

            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in update_step_settings) {
                let model = update_step_settings[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.update_step, model)) {
                    $.extend(update_step_settings[keyfields], wfocuParams.forms_labels.update_step[model]);
                }
            }
            //update_step();
            initialize_funnel_offer();

            if ($(rules_container).length > 0) {
                initialize_funnel_rules()
            }
            if ($(design_container).length > 0) {
                initialize_funnel_design();
            }
            /**
             * handling of localized label/description coming from php to form fields in vue
             */
            if ($(settings_container).length > 0) {
                for (let keyfields in funnel_settings_fields) {
                    let model = funnel_settings_fields[keyfields].model;
                    if (self.hp(wfocuParams.forms_labels.settings, model)) {
                        $.extend(funnel_settings_fields[keyfields], wfocuParams.forms_labels.settings[model]);
                    }
                }
                funnel_settings();
            }

            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in global_settings_order_statuses_fields) {
                let model = global_settings_order_statuses_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.global_settings, model)) {

                    $.extend(global_settings_order_statuses_fields[keyfields], wfocuParams.forms_labels.global_settings[model]);
                }
            }
            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in global_settings_offer_confirmation_fields) {
                let model = global_settings_offer_confirmation_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.global_settings_offer_confirmation, model)) {
                    $.extend(global_settings_offer_confirmation_fields[keyfields], wfocuParams.forms_labels.global_settings_offer_confirmation[model]);
                }
            }
            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in global_settings_emails_fields) {
                let model = global_settings_emails_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.global_settings, model)) {
                    $.extend(global_settings_emails_fields[keyfields], wfocuParams.forms_labels.global_settings[model]);
                }
            }
            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in global_settings_tan_fields) {
                let model = global_settings_tan_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.global_settings, model)) {
                    $.extend(global_settings_tan_fields[keyfields], wfocuParams.forms_labels.global_settings[model]);
                }
            }
            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in global_settings_gateway_fields) {
                let model = global_settings_gateway_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.global_settings, model)) {
                    $.extend(global_settings_gateway_fields[keyfields], wfocuParams.forms_labels.global_settings[model]);
                }
            }

            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in global_settings_scripts_fields) {
                let model = global_settings_scripts_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.global_settings, model)) {
                    $.extend(global_settings_scripts_fields[keyfields], wfocuParams.forms_labels.global_settings[model]);
                }
            }
            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in global_settings_misc_fields) {
                let model = global_settings_misc_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.global_settings, model)) {
                    $.extend(global_settings_misc_fields[keyfields], wfocuParams.forms_labels.global_settings[model]);
                }
            }


            /**
             * handling of localized label/description coming from php to form fields in vue
             */
            if ($(global_settings_container).length > 0) {

                global_settings();
            }

            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in add_new_offer_setting_fields) {
                let model = add_new_offer_setting_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.add_new_offer_setting, model)) {
                    $.extend(add_new_offer_setting_fields[keyfields], wfocuParams.forms_labels.add_new_offer_setting[model]);
                }
            }

            /**
             * handling of localized label/description coming from php to form fields in vue
             */

            for (let keyfields in funnel_setting_fields) {
                let model = funnel_setting_fields[keyfields].model;
                if (self.hp(wfocuParams.forms_labels.funnel_setting, model)) {
                    $.extend(funnel_setting_fields[keyfields], wfocuParams.forms_labels.funnel_setting[model]);
                }
            }

            initialize_global_models();
            jquery_event();
            initialize_offer_ajax_handlers();
        };


        $(document).on('click', '.wfocu_apply_template', function () {
            let template = $(this);
            let template_slug = $(template).attr('data-slug');
            let temp_name = $('.wfocu_selected_template .wfocu_img_thumbnail').find('img').attr('alt');

            if (template_slug != "") {
                $(template).parent().find('.wfocu-ajax-apply-preset-loader').removeClass('wfocu_hide');
                $.ajax({
                    url: ajaxurl,
                    method: 'post',
                    data: {
                        'action': 'wfocu_apply_template',
                        'template_slug': template_slug,
                        'offer_id': self.get_offer_id()
                    },
                    success: function (rsp) {
                        if (rsp.status == true) {
                            setTimeout(function () {
                                if ($('#modal-template_success').length > 0) {
                                    $("#modal-template_success").iziModal('open');
                                    $("#modal-template_success").iziModal('setTitle', 'Preset applied to ' + temp_name);
                                }
                                $('.wfocu_customize_template').css('display', 'none');
                                $('.wfocu_apply_template').css('display', 'inline-block');
                                $(template).parent().find('.wfocu-ajax-apply-preset-loader').addClass('wfocu_hide');
                                $(template).parent().find('.wfocu_apply_template').css('display', 'none');
                                $(template).parent().find('.wfocu_customize_template').css('display', 'inline-block');
                                $(template).parent().find('.wfocu_customize_template').removeClass('button-primary');
                                $(template).parent().find('.wfocu_customize_template').addClass('button');
                                $(template).parent().find('.wfocu_customize_template').attr('disabled', true);
                            }, 1500);
                        }
                    }
                });
            }

        });

        $(document).on(
            'wfocu_rules_updated', function (event) {
                $('#modal-rules-settings_success').iziModal('open');
                let steps_menu_el = $(".wfocu_s_menu_offers");
                if (steps_menu_el.length > 0) {
                    let temp_href = steps_menu_el.data("href");
                    steps_menu_el.attr("href", temp_href);
                }

                if ($('.wfocu_rules_container').attr('data-is_rules_saved') !== 'yes') {
                    setTimeout(
                        function () {

                            window.location = $('.wfocu_s_menu_offers').attr('href');
                        }, 500
                    );
                    return;
                }
            }
        );

        init();
        wfocu_admin_tabs();
        wfocu_toggle_table();

        return self;
    };
    $(win).load(
        function () {
            window.wfocuBuilder = new wfo_builder();
        }
    );

    $(function () {
        $(".modal-temp-iframe").iziModal({
            history: false,
            width: 620,
            iframe: true,
            iframeHeight: 500,
            loop: true,
            headerColor: '#6dbe45',
        });
    })

    function wfocu_toggle_table() {
        if ($(".wfocu-instance-table .wfocu-tgl,.funnel_state_toggle .wfocu-tgl").length > 0) {
            $(".wfocu-instance-table .wfocu-tgl,.funnel_state_toggle .wfocu-tgl").on(
                'click', function () {
                    let checkedVal = this.checked.toString();
                    if (true === this.checked) {
                        if ($('.wfocu_head_funnel_state_on').length > 0) {
                            $('.wfocu_head_funnel_state_on').show();
                        }
                        if ($('.wfocu_head_funnel_state_off').length > 0) {
                            $('.wfocu_head_funnel_state_off').hide();
                        }
                        if ($('.wfocu_head_mr').length) {
                            $('.wfocu_head_mr').attr('data-status', 'live');
                        }

                    } else {
                        if ($('.wfocu_head_funnel_state_on').length > 0) {
                            $('.wfocu_head_funnel_state_on').hide();
                        }
                        if ($('.wfocu_head_funnel_state_off').length > 0) {
                            $('.wfocu_head_funnel_state_off').show();
                        }
                        if ($('.wfocu_head_mr').length > 0) {
                            $('.wfocu_head_mr').attr('data-status', 'sandbox');
                        }
                    }
                    $.post(
                        wfocuParams.ajax_url, {'state': checkedVal, 'id': $(this).attr('data-id'), 'action': 'wfocu_toggle_funnel_state'}, function (resp) {

                            if (checkedVal === 'true') {
                                $("#modal-wfocu-state-change-success").iziModal('open');
                                $("#modal-wfocu-state-change-success").iziModal('setTitle', wfocu.texts.state_changed_true);
                            } else {
                                $("#modal-wfocu-state-change-success").iziModal('open');
                                $("#modal-wfocu-state-change-success").iziModal('setTitle', wfocu.texts.state_changed_false);
                            }

                        }
                    );
                }
            );
        }
    }


    function wfocu_admin_tabs() {
        if ($(".wfocu-product-widget-tabs").length > 0) {
            let wfctb = $('.wfocu-product-widget-tabs .wfocu-tab-title');
            wfctb.on(
                'click', function (event) {
                    let $this = $(this).closest('.wfocu-product-widget-tabs');
                    let tabindex = $(this).attr('data-tab');

                    $this.find('.wfocu-tab-title').removeClass('wfocu-active');

                    $this.find('.wfocu-tab-title[data-tab=' + tabindex + ']').addClass('wfocu-active');

                    $($this).find('.wfocu-tab-content').removeClass('wfocu-activeTab');
                    $($this).find('.wfocu_forms_global_settings .vue-form-generator fieldset').hide();
                    $($this).find('.wfocu_forms_global_settings .vue-form-generator fieldset').eq(tabindex - 1).addClass('wfocu-activeTab');
                    $($this).find('.wfocu_forms_global_settings .vue-form-generator fieldset').eq(tabindex - 1).show();


                }
            );
            wfctb.eq(0).trigger('click');
        }
    }


    window.wfocuBuilderCommons = wfocuBuilderCommons;
})(jQuery, document, window);
