(function ($) {
    'use strict';

    $(document).ready(function () {
            let add_funnel = false;
            let admin_ajax = new wp_admin_ajax();
            let add_funnel_setting = false;
            let vue_add_funnel = function (modal) {
                if (add_funnel === true) {
                    return add_funnel_setting;
                }
                add_funnel = true;
                add_funnel_setting = new Vue({
                    el: "#part-add-funnel",
                    components: {
                        "vue-form-generator": VueFormGenerator.component
                    },
                    data: {
                        modal: modal,
                        model: {
                            funnel_name: "",
                            funnel_desc: "",
                        },
                        schema: {
                            fields: [{
                                type: "input",
                                inputType: "text",
                                label: "Name",
                                model: "funnel_name",
                                inputName: 'funnel_name',
                                featured: true,
                                // required: true,
                                placeholder: "Enter Name",
                                validator: VueFormGenerator.validators.string
                            }, {
                                type: "textArea",
                                label: "Description",
                                model: "funnel_desc",
                                inputName: 'funnel_desc',
                                featured: true,
                                rows: 3,
                                placeholder: "Enter Description (Optional)"
                            }]
                        },
                        formOptions: {
                            validateAfterLoad: false,
                            validateAfterChanged: true,
                        }
                    }
                });
                return add_funnel_setting;
            };

            if ($("#modal-add-funnel").length > 0) {
                $("#modal-add-funnel").iziModal({
                    title: 'Create Funnel',
                    headerColor: '#6dbe45',
                    background: '#efefef',
                    borderBottom: false,
                    history: false,
                    width: 600,
                    overlayColor: 'rgba(0, 0, 0, 0.6)',
                    transitionIn: 'bounceInDown',
                    transitionOut: 'bounceOutDown',
                    navigateCaption: true,
                    navigateArrows: "false",
                    onOpening: function (modal) {
                        modal.startLoading();
                    },
                    onOpened: function (modal) {
                        modal.stopLoading();
                        vue_add_funnel(modal);
                    },
                    onClosed: function (modal) {
                        //console.log('onClosed');
                    }
                });
            }

            if ($(".wfocu_add_funnel").length > 0) {
                let wp_form_ajax = new wp_admin_ajax('.wfocu_add_funnel', true, function (ajax) {
                    ajax.before_send = function (element, action) {
                        if (ajax.action === 'wfocu_add_new_funnel') {
                            $('.wfocu_submit_btn_style').text('Creating...');

                        }
                    };
                    ajax.success = function (rsp) {
                        if (ajax.action === 'wfocu_add_new_funnel') {

                            if (rsp.status === true) {
                                $('form.wfocu_add_funnel').hide();
                                $('.wfocu-funnel-create-success-wrap').show();
                                // $('.wfocu_form_response').html(rsp.msg);
                                setTimeout(function () {
                                    window.location.href = rsp.redirect_url;
                                }, 3000);

                            } else {
                                $('.wfocu_form_response').html(rsp.msg);
                            }

                        }
                    };
                });
            }

            $(document).on("click", ".vue-form-generator .form-group.field-checkbox label", function () {
                let $this = $(this);
                let parent = $this.parent(".form-group");
                if (parent.find("input[type='checkbox']").length > 0) {
                    parent.find("input[type='checkbox']").trigger("click");
                }
            });
            $(".wfocu-preview").on("click", function () {
                let funnel_id = $(this).attr('data-funnel-id');
                let elem = $(this);
                elem.addClass('disabled');
                if (funnel_id > 0) {
                    let wp_ajax = new wp_admin_ajax();
                    wp_ajax.ajax("preview_details", {'funnel_id': funnel_id});
                    wp_ajax.success = function (rsp) {
                        if (rsp.status === true) {
                            // console.log()
                        }
                        $(this).WCBackboneModal({
                            template: 'wfocu-funnel-popup',
                            variable: rsp
                        });
                        elem.removeClass('disabled');
                    };
                }
                return false;
            });

            $(".wfocu-duplicate").on("click", function () {
                let funnel_id = $(this).attr('data-funnel-id');

                let elem = $(this);
                elem.addClass('disabled');
                if (funnel_id > 0) {
                    let wp_ajax = new wp_admin_ajax();
                    wp_ajax.ajax("duplicate_funnel", {'funnel_id': funnel_id});

                    $("#modal-duplicate-funnel").iziModal({
                            title: 'Duplicating funnel...',
                            icon: 'icon-check',
                            headerColor: '#6dbe45',
                            background: '#6dbe45',
                            borderBottom: false,
                            width: 600,
                            timeoutProgressbar: true,
                            transitionIn: 'fadeInUp',
                            transitionOut: 'fadeOutDown',
                            bottom: 400,
                            loop: true,
                            closeButton: false,
                            pauseOnHover: false,
                            overlay: true
                        }
                    );
                    $('#modal-duplicate-funnel').iziModal('open');
                    wp_ajax.success = function (rsp) {
                        if (rsp.status === true) {
                            $("#modal-duplicate-funnel").iziModal('setTitle', 'Funnel duplicated.');
                            setTimeout(function () {
                                $('#modal-duplicate-funnel').iziModal('close');
                                location.reload();
                            }, 3000);
                        } else {
                            $("#modal-duplicate-funnel").iziModal('setTitle', rsp.msg);
                            setTimeout(function () {
                                $('#modal-duplicate-funnel').iziModal('close');
                                location.reload();
                            }, 3000);
                        }
                        elem.removeClass('disabled');
                    };
                }
                return false;
            });


            /**
             * Sometime having issues in Mac/Safari about loader sticking infinitely
             */
            if (document.readyState == 'complete') {
                if ($('.wfocu-loader').length > 0) {
                    $('.wfocu-loader').each(function () {
                        let $this = $(this);
                        console.log($this);
                        if ($this.is(":visible")) {
                            console.log($this);
                            setTimeout(function ($this) {
                                console.log($this);
                                $this.remove();
                            }, 400, $this);
                        }
                    });
                }
            } else {

                $(window).bind('load', function () {
                    if ($('.wfocu-loader').length > 0) {
                        $('.wfocu-loader').each(function () {
                            let $this = $(this);

                            if ($this.is(":visible")) {

                                setTimeout(function ($this) {

                                    $this.remove();
                                }, 400, $this);
                            }
                        });
                    }
                });
            }
            /** Metabox panel close */
            $(".wfocu_allow_panel_close .hndle").on("click", function () {
                var $this = $(this);
                var parentPanel = $(this).parents(".wfocu_allow_panel_close");
                parentPanel.toggleClass("closed");
            });

        }
    )
})(jQuery);
