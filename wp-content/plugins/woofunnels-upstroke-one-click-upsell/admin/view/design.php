<?php
$allTemplates = WFOCU_Core()->template_loader->get_templates();
$offers       = WFOCU_Core()->funnels->get_funnel_offers_admin();

?>
<div class="wfocu_wrap_l">
    <div class="wfocu_p15">
        <div class="wfocu_heading_l"><?php echo __( 'Offers', 'woo-funnels-one-click-upsell' ); ?></div>
        <div class="wfocu_steps">
            <div class="wfocu_step">
				<?php echo __( 'Checkout', 'woo-funnels-one-click-upsell' ); ?>
                <span class="wfocu_down_arrow"></span>
            </div>
            <div class="wfocu_steps_sortable">
				<?php include __DIR__ . '/steps/offer-ladder.php'; ?>
            </div>
            <div class="wfocu_step">
				<?php echo __( 'Thank You Page', 'woo-funnels-one-click-upsell' ); ?>
                <span class="wfocu_up_arrow"></span>
            </div>
        </div>
    </div>
</div>

<?php

if ( false === is_array( $offers['steps'] ) || ( is_array( $offers['steps'] ) && count( $offers['steps'] ) == 0 ) ) {
	$funnel_id      = $offers['id'];
	$section_url    = add_query_arg( array(
		'page'    => 'upstroke',
		'section' => 'offers',
		'edit'    => $funnel_id,
	), admin_url( 'admin.php' ) );
	$offer_page_url = $section_url;
	?>
    <div class="wfocu_wrap_r wfocu_no_offer_wrap_r">
        <div class="wfocu_no_offers_wrapper wfocu_p20">
            <div class="wfocu_welcome_wrap">
                <div class="wfocu_welcome_wrap_in">
                    <div class="wfocu_no_offers_notice">
                        <div class="wfocu_welc_head">
                            <div class="wfocu_welc_icon"><img src="<?php echo WFOCU_PLUGIN_URL ?>/admin/assets/img/clap.png" alt="" title=""/></div>
                            <div class="wfocu_welc_title"> <?php _e( 'No offers in Current Funnel', 'woofunnels-upstroke-one-click-upsell' ); ?>
                            </div>
                        </div>
                        <div class="wfocu_welc_text">
                            <p><?php _e( ' You have to create some offers and add products in there.', 'woofunnels-upstroke-one-click-upsell' ); ?></p>

                        </div>
                    </div>
                    <a href="<?php echo $offer_page_url ?>" class="wfocu_step wfocu_button_add wfocu_button_inline  wfocu_welc_btn">
						<?php _e( '+ Create Your First Offer', 'woofunnels-upstroke-one-click-upsell' ); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
	<?php
} else {
	?>
    <div class="wfocu_wrap_r wfocu_table" id="wfocu_step_design">
        <div class="wfocu-loader"><img src="<?php echo admin_url( 'images/spinner-2x.gif' ); ?>"/></div>
        <div class="wfocu_template">
            <div class="wfocu_fsetting_table_head">
                <div class="wfocu_fsetting_table_head_in wfocu_clearfix">
                    <div class="wfocu_fsetting_table_title"><?php echo __( sprintf( 'Customizing Design for Offer <strong>%s</strong>', '{{getOfferNameByID()}}' ), 'woo-funnels-one-click-upsell' ); ?></div>
                </div>
            </div>

            <div class="wfocu_template_box_holder">
                <div class="wfocu_template_holder_head">
                    <h2 class="wfocu_screen_title"><?php _e( sprintf( 'Template <span>%s</span>', '{{getTemplateNiceName()}}' ), 'woofunnels-upstroke-one-click-upsell' ); ?></h2>
                    <div class="wfocu_screen_desc"><?php _e( 'Customize current selected template or activate a new template.', 'woofunnels-upstroke-one-click-upsell' ); ?></div>
                </div>
                <div class="wfocu_template_type_holder_in">
                    <div class="wfocu_single_template_list wfocu_template_list wfocu_clearfix" v-if="have_multiple_product==1">
						<?php
						$single_templates = $allTemplates['single'];
						$custom_page      = array(
							'name'      => __( 'Custom Page', 'woofunnels-upstroke-one-click-upsell' ),
							'thumbnail' => WFOCU_PLUGIN_URL . '/admin/assets/img/thumbnail-link-to-custom.jpg',
							'type'      => 'link',
						);
						foreach ( $single_templates as $temp_slug => $temp_det ) {
							$temp_name      = isset( $temp_det['name'] ) ? $temp_det['name'] : '';
							$prev_thumbnail = isset( $temp_det['thumbnail'] ) ? $temp_det['thumbnail'] : '';
							$prev_full      = isset( $temp_det['large_img'] ) ? $temp_det['large_img'] : '';

							if ( $temp_slug != 'custom-page' ) {
								$overlay_icon   = '<i class="dashicons dashicons-visibility"></i>';
								$template_class = 'wfocu_temp_box_normal';
							} else {
								$overlay_icon   = '';
								$template_class = 'wfocu_temp_box_custom';
							}
							$custom_active_img = WFOCU_PLUGIN_URL . '/admin/assets/img/thumbnail-custom-page.jpg';
							?>
                            <div class="wfocu_template_box" v-bind:class="current_template==`<?php echo $temp_slug; ?>`?`<?php echo $template_class; ?> wfocu_template_box_single  wfocu_selected_template`:`wfocu_template_box <?php echo $template_class; ?> wfocu_template_box_single`  " data-slug="<?php echo $temp_slug; ?>">
                                <div class="wfocu_template_box_inner">
                                    <a href="javascript:void(0)" class="wfocu_template_img_cover" data-izimodal-open="#modal-prev-template_<?php echo $temp_slug; ?>">
                                        <div class="wfocu_overlay">
                                            <div class="wfocu_overlay_icon"><?php echo $overlay_icon; ?></div>
                                        </div>
                                        <div class="wfocu_template_thumbnail">
                                            <div class="wfocu_img_thumbnail ">

                                                <img src="<?php echo $prev_thumbnail; ?>" alt="<?php echo $temp_name; ?>"/>

                                            </div>
                                        </div>
                                    </a>
                                    <div class="wfocu_template_btm_strip wfocu_clearfix">
                                        <div class="wfocu_template_name" id=""></div>
										<?php
										echo $temp_name;
										?>
                                        <div class="wfocu_template_button">
                                            <a v-if="current_template==`<?php echo $temp_slug; ?>`" href="javascript:void(0)" class="button-primary" v-on:click="customize_template(`<?php echo $temp_slug; ?>`)"><?php echo ( $temp_slug === 'custom-page' ) ? __( 'Edit', 'woo-funnels-one-click-upsell' ) : __( 'Customize', 'woo-funnels-one-click-upsell' ); ?></a>
                                            <a v-else href="javascript:void(0)" class="button-primary" v-on:click="set_template(`<?php echo $temp_slug; ?>`)"><?php echo __( 'Activate', 'woo-funnels-one-click-upsell' ); ?></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: none" id="modal-prev-template_<?php echo $temp_slug; ?>" class="modal-prev-temp modal-temp-iframe" data-izimodal-iframeurl="<?php echo $prev_full; ?>" data-iziModal-title="<?php echo $temp_name; ?>" data-iziModal-icon="icon-home" data-izimodal-transitionin="fadeInDown">
                            </div>
							<?php
						}
						?>

                        <div class="wfocu_template_box" v-bind:class="current_template==`custom-page`?` wfocu_temp_box_custom wfocu_template_box_single  wfocu_selected_template`:`wfocu_template_box wfocu_temp_box_custom wfocu_template_box_single`  " data-slug="custom-page">
                            <div class="wfocu_template_box_inner">
                                <a href="javascript:void(0)" class="wfocu_template_img_cover" data-izimodal-open="#modal-prev-template_custom-page" data-izimodal-transitionin="fadeInDown">
                                    <div class="wfocu_overlay">
                                        <div class="wfocu_overlay_icon"><i class="dashicons dashicons-admin-links"></i></div>
                                    </div>
                                    <div class="wfocu_template_thumbnail">
                                        <div class="wfocu_img_thumbnail ">

                                            <div class="wfocu_thumb_def">
                                                <img src="<?php echo $custom_page['thumbnail']; ?>" alt="<?php echo __( 'Custom Page', 'woofunnels-upstroke-one-click-upsell' ); ?>"/></div>
                                            <div class="wfocu_thumb_active">
                                                <img src="<?php echo $custom_active_img; ?>" alt="<?php echo __( 'Custom Page', 'woofunnels-upstroke-one-click-upsell' ); ?>"/></div>

                                        </div>
                                    </div>
                                </a>
                                <div class="wfocu_template_btm_strip wfocu_clearfix">
                                    <div class="wfocu_template_name" id="custom_template_name"></div>

                                    {{currentTemplateName}}

                                    <div class="wfocu_template_button">
                                        <a v-if="currentTemplatePath ==``" href="javascript:void(0)" style="display: none;" v-on:click="customize_template(`custom-page`)"><?php ''; ?></a>
                                        <a v-else-if="currentTemplatePath !==`` && current_template==`custom-page`" href="javascript:void(0)" class="button-primary" v-on:click="customize_template(`custom-page`)"><?php echo __( 'Edit', 'woo-funnels-one-click-upsell' ); ?></a>
                                        <a v-else href="javascript:void(0)" class="button-primary" v-on:click="set_template(`custom-page`)"><?php echo __( 'Activate', 'woo-funnels-one-click-upsell' ); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: none" id="modal-prev-template_custom-page" class="modal-prev-temp wfocu_izimodal_default" data-iziModal-title="<?php echo $custom_page['name']; ?>" data-iziModal-icon="icon-home">

                            <div class="sections wfocu_custom_preview">
                                <form class="wfocu_add_funnel" id="modal-page-search-form" data-wfoaction="get_custom_page" v-on:submit.prevent="onSubmit">
                                    <div class="wfocu_vue_forms">
                                        <fieldset>
                                            <div class="form-group ">
                                                <div id="part-custom-template">
                                                    <label><?php _e( 'Select a Page', 'woofunnels-upstroke-one-click-upsell' ); ?></label>
                                                    <multiselect v-model="selectedProducts" id="ajax" label="page_name" track-by="page_name" placeholder="Type to search" open-direction="bottom" :options="products" :multiple="false" :searchable="true" :loading="isLoading" :internal-search="true" :clear-on-select="true" :close-on-select="true" :options-limit="300" :limit="3" :max-height="600" :show-no-results="true" :hide-selected="true" @search-change="asyncFind">
                                                        <template slot="clear" slot-scope="props">
                                                            <div class="multiselect__clear" v-if="selectedProducts.length" @mousedown.prevent.stop="clearAll(props.search)"></div>
                                                        </template>
                                                        <span slot="noResult"><?php echo __( 'Oops! No elements found. Consider changing the search query.', 'woo-funnels-one-click-upsell' ); ?></span>
                                                    </multiselect>
                                                    <input type="hidden" name="funnel_id" v-bind:value="funnel_id">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="wfocu_form_submit">
                                                <button type="submit" class="wfocu_submit_btn_style" value="save_page"><?php echo __( 'Save Template', 'woo-funnels-one-click-upsell' ); ?></button>
                                            </div>
                                            <div class="wfocu_form_response">
                                            </div>
                                        </fieldset>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="wfocu_template_type_holder_in">
                    <div class="wfocu_multiple_template_list wfocu_template_list wfocu_clearfix" v-if="have_multiple_product==2">
						<?php
						$multi_templates   = $allTemplates['multiple'];
						$custom_active_img = WFOCU_PLUGIN_URL . '/admin/assets/img/thumbnail-custom-page.jpg';

						foreach ( $multi_templates as $temp_slug => $temp_det ) {
							$temp_name      = isset( $temp_det['name'] ) ? $temp_det['name'] : '';
							$prev_thumbnail = isset( $temp_det['thumbnail'] ) ? $temp_det['thumbnail'] : '';
							$prev_full      = isset( $temp_det['large_img'] ) ? $temp_det['large_img'] : '';


							$overlay_icon   = '<i class="dashicons dashicons-visibility"></i>';
							$template_class = 'wfocu_temp_box_normal';

							?>
                            <div v-bind:class="current_template==`<?php echo $temp_slug; ?>`?`wfocu_template_box <?php echo $template_class; ?> wfocu_template_box_multi wfocu_selected_template`:`wfocu_template_box <?php echo $template_class; ?> wfocu_template_box_multi`" data-slug="<?php echo $temp_slug; ?>">
                                <div class="wfocu_template_box_inner">
                                    <a href="javascript:void(0)" class="wfocu_template_img_cover" data-izimodal-open="#modal-prev-template_<?php echo $temp_slug; ?>" data-izimodal-transitionin="fadeInDown">
                                        <div class="wfocu_overlay">
                                            <div class="wfocu_overlay_icon"><?php echo $overlay_icon; ?></div>
                                        </div>
                                        <div class="wfocu_template_thumbnail">
                                            <div class="wfocu_img_thumbnail ">
                                                <img src="<?php echo $prev_thumbnail; ?>" alt="<?php echo $temp_name; ?>"/>
                                            </div>
                                        </div>
                                    </a>

                                    <div class="wfocu_template_btm_strip wfocu_clearfix">
                                        <div class="wfocu_template_name x"><?php echo $temp_name; ?></div>
                                        <div class="wfocu_template_button">
                                            <a v-if="current_template==`<?php echo $temp_slug; ?>`" href="javascript:void(0)" class="button-primary" v-on:click="customize_template(`<?php echo $temp_slug; ?>`)"><?php echo __( 'Customize', 'woo-funnels-one-click-upsell' ); ?></a>
                                            <a v-else href="javascript:void(0)" class="button-primary" v-on:click="set_template(`<?php echo $temp_slug; ?>`)"><?php echo __( 'Activate' ); ?></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div style="display: none" id="modal-prev-template_<?php echo $temp_slug; ?>" class="modal-prev-temp" data-iziModal-title="<?php echo $temp_name; ?>" data-iziModal-icon="icon-home">
                                <div class="sections wfocu_img_preview" style="height: 254px;">
                                    <img src="<?php echo $prev_full; ?>" alt="<?php echo $temp_name; ?>"/>
                                </div>
                            </div>
							<?php
						}
						?>
                        <div class="wfocu_success_modal" style="display: none" id="modal-template_success" data-iziModal-icon="icon-home">

                        </div>

                        <div class="wfocu_template_box" v-bind:class="current_template==`custom-page`?` wfocu_temp_box_custom wfocu_template_box_single  wfocu_selected_template`:`wfocu_template_box wfocu_temp_box_custom wfocu_template_box_single`  " data-slug="custom-page">
                            <div class="wfocu_template_box_inner">
                                <a href="javascript:void(0)" class="wfocu_template_img_cover" data-izimodal-open="#modal-prev-template_custom-page" data-izimodal-transitionin="fadeInDown">
                                    <div class="wfocu_overlay">
                                        <div class="wfocu_overlay_icon"><i class="dashicons dashicons-admin-links"></i></div>
                                    </div>
                                    <div class="wfocu_template_thumbnail">
                                        <div class="wfocu_img_thumbnail ">

                                            <div class="wfocu_thumb_def">
                                                <img src="<?php echo $custom_page['thumbnail']; ?>" alt="<?php echo __( 'Custom Page', 'woofunnels-upstroke-one-click-upsell' ); ?>"/></div>
                                            <div class="wfocu_thumb_active">
                                                <img src="<?php echo $custom_active_img; ?>" alt="<?php echo __( 'Custom Page', 'woofunnels-upstroke-one-click-upsell' ); ?>"/></div>

                                        </div>
                                    </div>
                                </a>
                                <div class="wfocu_template_btm_strip wfocu_clearfix">
                                    <div class="wfocu_template_name" id="custom_template_name"></div>

                                    {{currentTemplateName}}

                                    <div class="wfocu_template_button">
                                        <a v-if="currentTemplatePath ==``" href="javascript:void(0)" style="display: none;" v-on:click="customize_template(`custom-page`)"><?php ''; ?></a>
                                        <a v-else-if="currentTemplatePath !==`` && current_template==`custom-page`" href="javascript:void(0)" class="button-primary" v-on:click="customize_template(`custom-page`)"><?php echo __( 'Edit', 'woo-funnels-one-click-upsell' ); ?></a>
                                        <a v-else href="javascript:void(0)" class="button-primary" v-on:click="set_template(`custom-page`)"><?php echo __( 'Activate', 'woo-funnels-one-click-upsell' ); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: none" id="modal-prev-template_custom-page" class="modal-prev-temp wfocu_izimodal_default" data-iziModal-title="<?php echo $custom_page['name']; ?>" data-iziModal-icon="icon-home">

                            <div class="sections wfocu_custom_preview">
                                <form class="wfocu_add_funnel" id="modal-page-search-form" data-wfoaction="get_custom_page" v-on:submit.prevent="onSubmit">
                                    <div class="wfocu_vue_forms">
                                        <fieldset>
                                            <div class="form-group ">
                                                <div id="part-custom-template">
                                                    <label><?php _e( 'Select a Page', 'woofunnels-upstroke-one-click-upsell' ); ?></label>
                                                    <multiselect v-model="selectedProducts" id="ajax" label="page_name" track-by="page_name" placeholder="Type to search" open-direction="bottom" :options="products" :multiple="false" :searchable="true" :loading="isLoading" :internal-search="true" :clear-on-select="true" :close-on-select="true" :options-limit="300" :limit="3" :max-height="600" :show-no-results="true" :hide-selected="true" @search-change="asyncFind">
                                                        <template slot="clear" slot-scope="props">
                                                            <div class="multiselect__clear" v-if="selectedProducts.length" @mousedown.prevent.stop="clearAll(props.search)"></div>
                                                        </template>
                                                        <span slot="noResult"><?php echo __( 'Oops! No elements found. Consider changing the search query.', 'woo-funnels-one-click-upsell' ); ?></span>
                                                    </multiselect>
                                                    <input type="hidden" name="funnel_id" v-bind:value="funnel_id">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="wfocu_form_submit">
                                                <button type="submit" class="wfocu_submit_btn_style" value="save_page"><?php echo __( 'Save Template', 'woo-funnels-one-click-upsell' ); ?></button>
                                            </div>
                                            <div class="wfocu_form_response">
                                            </div>
                                        </fieldset>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div v-if="isEmpty(products)" class="wfocu-scodes-wrap">
            <div class="wfocu-scodes-head"><?php _e( 'This offer does not have any products.', 'woofunnels-upstroke-one-click-upsell' ); ?></div>
        </div>
        <div v-else-if="current_template == 'custom-page'" class="wfocu-scodes-wrap">
            <div class="wfocu-scodes-head">
				<?php _e( 'Shortcodes For Custom Page', 'woofunnels-upstroke-one-click-upsell' ); ?>

            </div>
            <div class="wfocu-scodes-desc"> Using page builders to build custom upsell pages?
                <a href="https://buildwoofunnels.com/docs/upstroke/design/custom-designed-one-click-upsell-pages/" target="_blank">Read this guide to learn more</a> about using Button widgets of your
                page builder.
            </div>


            <div v-for="shortcode in shortcodes" class="wfocu-scodes-inner-wrap">
                <div class="wfocu-scodes-list-wrap">
                    <div class="wfocu-scode-product-head"><?php _e( 'Product - ', 'woofunnels-upstroke-one-click-upsell' ); ?> {{shortcode.name}}</div>
                    <div class="wfocu-scodes-products">
                        <div v-for="key in shortcode.shortcodes" class="wfocu-scodes-row">

                            <div class="wfocu-scodes-label">{{key.label}}</div>
                            <div class="wfocu-scodes-value">
                                <div class="wfocu-scodes-value-in">
                                    <span class="wfocu-scode-text"><input readonly type="text" v-bind:value="key.value"></span>
                                    <a href="javascript:void(0)" v-on:click="copy" class="wfocu_copy_text"><?php _e( 'Copy', 'woofunnels-upstroke-one-click-upsell' ); ?></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


		<?php
		$template_names = get_option( 'wfocu_template_names', [] );
		if ( count( $template_names ) > 0 ) { ?>
            <div v-if="current_template !== 'custom-page'" class="wfocu-scodes-wrap preset-holder">
                <div class="wfocu-scodes-head">
					<?php _e( 'Your Saved Presets', 'woofunnels-upstroke-one-click-upsell' ); ?>
                </div>
                <div class="wfocu-scodes-desc"> <?php _e('Click on the button to apply preset to the selected template. This will modify the default settings of the template and load it with settings of preset.','woofunnels-upstroke-one-click-upsell') ?>

                </div>
                <div class="wfocu-scodes-inner-wrap">
                    <div class="wfocu-scodes-list-wrap">
                        <div class="wfocu-scode-product-head"><?php _e( 'Apply and Customize Saved Presets', 'woofunnels-upstroke-one-click-upsell' ); ?> </div>
                        <div class="wfocu-scodes-products">

							<?php
							foreach ( $template_names as $template_slug => $template ) { ?>
                                <div class="customize-inside-control-row wfocu_template_holder wfocu-scodes-row">
                                    <div class="wfocu-scodes-label"><?php echo $template['name']; ?></div>
                                    <div class="wfocu-scodes-value-in wfocu-preset-right">
                                        <span class="wfocu-ajax-apply-preset-loader wfocu_hide"><img src="<?php echo admin_url( 'images/spinner.gif' ); ?>"></span>
                                        <a href="javascript:void(0);" class="wfocu_apply_template button-primary"  data-slug="<?php echo $template_slug ?>"><?php _e( 'Apply', 'woofunnels-upstroke-one-click-upsell' ) ?></a>
                                        <a href="javascript:void(0)" class="wfocu_customize_template button-primary" style="display: none;" ><?php echo __( 'Applied', 'woo-funnels-one-click-upsell' ); ?></a>
                                    </div>
                                </div>
								<?php
							} ?>
                        </div>
                    </div>
                </div>
            </div>
		<?php } /*else { */?><!--
            <div v-if="current_template !== 'custom-page'" class="wfocu-scodes-wrap">
                <div class="wfocu-scodes-head"><?php /*_e( 'No presets.', 'woofunnels-upstroke-one-click-upsell' ); */?></div>
            </div>
		--><?php /*} */?>
    </div>
<?php } ?>
