<?php

$sidebar_menu        = WFOCU_Common::get_sidebar_menu();
$funnel_sticky_line  = __( 'Now Building', 'woofunnels-upstroke-one-click-upsell' );
$funnel_sticky_title = '';
$funnel_onboarding   = true;
if ( isset( $_GET['edit'] ) && ! empty( $_GET['edit'] ) ) {
	$funnel_sticky_title = get_the_title( $_GET['edit'] );

	$funnel_onboarding_status = get_post_meta( $_GET['edit'], '_wfocu_is_rules_saved',true );

	if ( 'yes' == $funnel_onboarding_status ) {
		$funnel_onboarding  = false;
		$funnel_sticky_line = __( 'Now Editing', 'woofunnels-upstroke-one-click-upsell' );
	}
}

$status = get_post_status( $_GET['edit'] );
$funnel_id = ( $_GET['edit'] );

?>
<div class="wfocu_body">
    <div class="wfocu_fixed_header">
        <div class="wfocu_p20_wrap wfocu_box_size wfocu_table">
            <div class="wfocu_head_m wfocu_tl wfocu_table_cell">
                <div class="wfocu_head_mr" data-status="<?php echo ( $status !== 'publish' ) ? 'sandbox' : 'live'; ?>">
                    <div class="funnel_state_toggle wfocu_toggle_btn">
                        <input name="offer_state" id="state<?php echo $funnel_id; ?>" data-id="<?php echo $funnel_id; ?>" type="checkbox" class="wfocu-tgl wfocu-tgl-ios" <?php echo ( $status == 'publish' ) ? 'checked="checked"' : ''; ?> />
                        <label for="state<?php echo $funnel_id; ?>" class="wfocu-tgl-btn wfocu-tgl-btn-small"></label>
                    </div>
                    <span class="wfocu_head_funnel_state_on" <?php echo ( $status !== 'publish' ) ? ' style="display:none"' : ''; ?>><?php _e( 'Live', 'woofunnels-upstroke-one-click-upsell' ); ?></span>
                    <span class="wfocu_head_funnel_state_off" <?php echo ( $status == 'publish' ) ? 'style="display:none"' : ''; ?>> <?php _e( 'Sandbox', 'woofunnels-upstroke-one-click-upsell' ); ?></span>
                </div>
                <div class="wfocu_head_ml">
					<?php echo $funnel_sticky_line; ?> <strong><span id="wfocu_funnel_name"><?php echo $funnel_sticky_title; ?></span></strong>
                    <a href="javascript:void()" data-izimodal-open="#modal-update-funnel" data-izimodal-transitionin="fadeInDown"><i class="dashicons dashicons-edit"></i></a>
                </div>
            </div>
            <div style="display: none;" id="wfocu_funnel_description">

            </div>
            <div class="wfocu_head_r wfocu_tr wfocu_table_cell">
                <a href="<?php echo admin_url( 'admin.php?page=upstroke' ); ?>" class="wfocu_head_close"><i class="dashicons dashicons-no-alt"></i></a></div>
        </div>
    </div>
    <div class="wfocu_fixed_sidebar">
		<?php
		if ( is_array( $sidebar_menu ) && count( $sidebar_menu ) > 0 ) {
			ksort( $sidebar_menu );
			$funnel_data = WFOCU_Core()->funnels->get_funnel_offers_admin();
			foreach ( $sidebar_menu as $menu ) {
				$menu_icon = ( isset( $menu['icon'] ) && ! empty( $menu['icon'] ) ) ? $menu['icon'] : 'dashicons dashicons-admin-generic';
				if ( isset( $menu['name'] ) && ! empty( $menu['name'] ) ) {

					$section_url = add_query_arg( array(
						'page'    => 'upstroke',
						'section' => $menu['key'],
						'edit'    => WFOCU_Core()->funnels->get_funnel_id(),
					), admin_url( 'admin.php' ) );

					$class = '';
					if ( $menu['key'] === $_GET['section'] ) {
						$class = 'active';
					}

					global $wfocu_is_rules_saved;

					$main_url = $section_url;
//					if ( "yes" !== $wfocu_is_rules_saved ) {
//						$main_url = "javascript:void(0)";
//					}
//
//					if ( ( $menu['key'] != "rules" && $menu['key'] != "offers" ) && empty( $funnel_data['offers'] ) ) {
//						$main_url = "javascript:void(0)";
//					}
					?>
                    <a class="wfocu_s_menu <?php echo $class; ?> wfocu_s_menu_<?php echo $menu['key'] ?>" href="<?php echo $main_url; ?>" data-href="<?php echo $section_url; ?>">
					<span class="wfocu_s_menu_i">
						<i class="<?php echo $menu_icon; ?>"></i>
					</span>
                        <span class="wfocu_s_menu_n">
						<?php echo $menu['name']; ?>
					</span>
                    </a>
					<?php
				}
			}
		}
		?>
    </div>
    <div class="wfocu_wrap wfocu_box_size">
        <div class="wfocu_p20 wfocu_box_size">
            <div class="wfocu_wrap_inner <?php echo ( isset( $_REQUEST['section'] ) ) ? 'wfocu_wrap_inner_' . $_REQUEST['section'] : ''; ?>">

				<?php
				$get_keys = wp_list_pluck( $sidebar_menu, 'key' );


				/**
				 * Redirect if any unregistered action found
				 */
				if ( false == in_array( $this->section_page, $get_keys ) ) {
					wp_redirect( admin_url( 'admin.php?page=upstroke&section=offers&edit=' . $_GET['edit'] ) );

				} else {

					/**
					 * Any registered section should also apply an action in order to show the content inside the tab
					 * like if action is 'stats' then add_action('wfocu_dashboard_page_stats', __FUNCTION__);
					 */
					if ( false === has_action( 'wfocu_dashboard_page_' . $this->section_page ) ) {
						include_once( $this->admin_path . '/view/' . $this->section_page . '.php' );

					} else {
						/**
						 * Allow other add-ons to show the content
						 */
						do_action( 'wfocu_dashboard_page_' . $this->section_page );
					}
				}


				do_action( 'wfocu_funnel_page', $this->section_page, WFOCU_Core()->funnels->get_funnel_id() );
				?>

                <div class="wfocu_clear"></div>
            </div>
        </div>
    </div>

    <div class="wfocu_izimodal_default" id="modal-update-funnel" style="display: none;">
        <div class="sections">
            <form class="wfocu_forms_wrap" data-wfoaction="update_funnel" novalidate>
                <div class="wfocu_vue_forms" id="part-update-funnel">
                    <vue-form-generator :schema="schema" :model="model" :options="formOptions"></vue-form-generator>
                </div>
                <fieldset>
                    <div class="wfocu_form_submit">
                        <button type="submit" class="wfocu_submit_btn_style" value="add_new">Updated</button>
                    </div>
                    <div class="wfocu_form_response">

                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
