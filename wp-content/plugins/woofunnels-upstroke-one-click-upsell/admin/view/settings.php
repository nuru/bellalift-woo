<?php
?>
<div class="wfocu_funnel_setting" id="wfocu_funnel_setting">
    <div class="wfocu_funnel_setting_inner">
        <div class="wfocu_fsetting_table_head">
            <div class="wfocu_fsetting_table_head_in wfocu_clearfix">
                <div class="wfocu_fsetting_table_title "><?php echo __( 'Settings', 'woo-funnels-one-click-upsell' ); ?>
                </div>
                <div class="setting_save_buttons wfocu_form_submit" >
                    <span class="wfocu_save_funnel_setting_ajax_loader spinner"></span>
                    <button v-on:click.self="onSubmit" class="wfocu_save_btn_style"><?php _e( 'Save Settings', 'woofunnels-upstroke-one-click-upsell' ) ?></button>
                </div>
            </div>
        </div>
        <!--	ADD CLASS "wfocu_hr_gap" to class form-group to add separator-->
        <form class="wfocu_forms_wrap wfocu_forms_funnel_settings">
            <fieldset>
                <vue-form-generator :schema="schema" :model="model" :options="formOptions">
                </vue-form-generator>
            </fieldset>
        </form>
        <div class="wfocu_form_submit wfocu_btm_grey_area wfocu_clearfix">
            <div class="wfocu_btm_save_wrap wfocu_clearfix">
                <span class="wfocu_save_funnel_setting_ajax_loader spinner"></span>
                <button v-on:click.self="onSubmit" class="wfocu_save_btn_style"><?php _e( 'Save Settings', 'woofunnels-upstroke-one-click-upsell' ) ?></button>
            </div>
        </div>
        <div class="wfocu_success_modal" style="display: none" id="modal-settings_success" data-iziModal-icon="icon-home">
        </div>
    </div>
    <div style="display: none" class="wfocu-funnel-settings-help-messages" data-iziModal-title="<?php echo __('Offer Success/Failure Messages Help', 'woofunnels-upstroke-one-click-upsell') ?>" data-iziModal-icon="icon-home">
        <div class="sections wfocu_img_preview" style="height: 254px;">
            <img src="<?php echo plugin_dir_url( WFOCU_PLUGIN_FILE).'assets/img/funnel-settings-prop.jpg' ?>"/>
        </div>
    </div>
</div>
