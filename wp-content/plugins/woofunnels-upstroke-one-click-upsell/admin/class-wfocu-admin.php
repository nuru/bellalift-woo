<?php

class WFOCU_Admin {

	private static $ins = null;
	public $admin_path;
	public $admin_url;
	public $section_page = '';
	public $should_show_shortcodes = null;
	public $updater = null;

	public function __construct() {

		$this->admin_path = WFOCU_PLUGIN_DIR . '/admin';
		$this->admin_url  = WFOCU_PLUGIN_URL . '/admin';

		$this->section_page = ( $this->is_upstroke_page() ) ? strtolower( filter_input( INPUT_GET, 'section' ) ) : '';

		add_action( 'admin_menu', array( $this, 'register_admin_menu' ), 90 );

		/**
		 * Admin enqueue scripts
		 */
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_assets' ), 99 );

		/**
		 * Admin customizer enqueue scripts
		 */
		add_action( 'customize_controls_print_styles', array( $this, 'admin_customizer_enqueue_assets' ), 10 );

		/**
		 * Admin footer text
		 */
		add_filter( 'admin_footer_text', array( $this, 'admin_footer_text' ), 9999, 1 );
		add_filter( 'update_footer', array( $this, 'update_footer' ), 9999, 1 );

		add_action( 'save_post', array( $this, 'maybe_reset_transients' ), 10, 2 );
		add_action( 'admin_head', array( $this, 'js_variables' ) );
		add_action( 'admin_init', array( $this, 'maybe_set_funnel_id' ) );

		add_action( 'delete_post', array( $this, 'clear_transients_on_delete' ), 10 );
		add_action( 'delete_post', array( $this, 'clear_session_record_on_shop_order_delete' ), 10 );

		/**
		 * Hooks to check if activation and deactivation request for post.
		 */
		add_action( 'admin_init', array( $this, 'maybe_activate_post' ) );
		add_action( 'admin_init', array( $this, 'maybe_deactivate_post' ) );

		add_action( 'customize_controls_print_footer_scripts', array( $this, 'maybe_print_mergetag_helpbox' ) );
		add_filter( 'plugin_action_links_' . WFOCU_PLUGIN_BASENAME, array( $this, 'plugin_actions' ) );
		add_action( 'admin_init', array( $this, 'maybe_handle_http_referer' ) );

		add_action( 'woocommerce_admin_field_payment_gateways', array( $this, 'hide_test_gateway_from_admin_list' ) );
		add_action( 'admin_init', array( $this, 'maybe_show_wizard' ) );

		add_action( 'in_admin_header', array( $this, 'maybe_remove_all_notices_on_page' ) );

		add_action( 'admin_init', array( $this, 'check_db_version' ), 990 );

		add_filter( 'woocommerce_get_formatted_order_total', array( $this, 'show_upsell_total_in_order_listings' ), 999, 2 );

		add_action( 'admin_init', array( $this, 'maybe_update_upsell_gross_total_to_order_meta' ), 999 );

		add_action( 'admin_bar_menu', array( $this, 'toolbar_link_to_xlplugins' ), 999 );

		add_filter( 'woocommerce_payment_gateways_setting_columns', array( $this, 'set_wc_payment_gateway_column' ) );

		add_action( 'woocommerce_payment_gateways_setting_column_wfocu', array( $this, 'wc_payment_gateway_column_content' ) );

		add_action( 'woocommerce_after_register_post_type', array( $this, 'maybe_flush_rewrite_rules' ) );

		/**
		 * Initiate Background Database updaters
		 */
		add_action( 'admin_init', array( $this, 'init_background_updater' ) );
		add_action( 'admin_head', array( $this, 'maybe_update_database_update' ) );

		add_action( 'admin_init', array( $this, 'maybe_update_upstroke_version_in_option' ) );

	}


	public static function get_instance() {
		if ( null == self::$ins ) {
			self::$ins = new self;
		}

		return self::$ins;
	}

	public function get_admin_url() {
		return plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'admin';
	}

	public function register_admin_menu() {

		add_submenu_page(
			'woofunnels', __( 'UpStroke', 'woofunnels-upstroke-one-click-upsell' ), __( 'UpStroke', 'woofunnels-upstroke-one-click-upsell' ), 'manage_woocommerce', 'upstroke', array(
				$this,
				'upstroke_page',
			)
		);
	}

	public function admin_enqueue_assets() {

		wp_enqueue_style( 'woofunnels-admin-font', $this->get_admin_url() . '/assets/css/wfocu-admin-font.css', array(), WFOCU_VERSION_DEV );

		$suffix = ( ! defined( 'SCRIPT_DEBUG' ) || true !== SCRIPT_DEBUG ) ? '.min' : '';

		if ( $this->is_upstroke_page() ) {
			WFOCU_Core()->funnels->setup_funnel_options( ( isset( $_GET['edit'] ) ? $_GET['edit'] : 0 ) );
		}
		if ( 'rules' == $this->section_page ) {
			wp_register_script( 'wfocu-chosen', $this->get_admin_url() . '/assets/js/chosen/chosen.jquery.min.js', array( 'jquery' ), WFOCU_VERSION_DEV );
			wp_register_script(
				'wfocu-ajax-chosen', $this->get_admin_url() . '/assets/js/chosen/ajax-chosen.jquery.min.js', array(
				'jquery',
				'wfocu-chosen',
			), WFOCU_VERSION_DEV
			);
			wp_enqueue_script( 'wfocu-ajax-chosen' );
			wp_enqueue_style( 'wfocu-chosen-app', $this->get_admin_url() . '/assets/css/chosen.css', array(), WFOCU_VERSION_DEV );
			wp_enqueue_style( 'wfocu-admin-app', $this->get_admin_url() . '/assets/css/wfocu-admin-app.css', array(), WFOCU_VERSION_DEV );
			wp_register_script( 'jquery-masked-input', $this->get_admin_url() . '/assets/js/jquery.maskedinput.min.js', array( 'jquery' ), WFOCU_VERSION_DEV );
			wp_enqueue_script( 'jquery-masked-input' );
			wp_enqueue_script(
				'wfocu-admin-app', $this->get_admin_url() . '/assets/js/wfocu-admin-app.js', array(
				'jquery',
				'jquery-ui-datepicker',
				'underscore',
				'backbone',
			), WFOCU_VERSION_DEV
			);

		}

		/**
		 * Load Color Picker
		 */
		if ( WFOCU_Common::is_load_admin_assets( 'settings' ) ) {
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'wp-color-picker' );
		}

		/**
		 * Load Funnel Builder page assets
		 */
		if ( WFOCU_Common::is_load_admin_assets( 'builder' ) ) {
			wp_enqueue_style( 'wfocu-funnel-bg', $this->admin_url . '/assets/css/wfocu-funnel-bg.css', array(), WFOCU_VERSION_DEV );
			wp_enqueue_style( 'wfocu-opensans-font', '//fonts.googleapis.com/css?family=Open+Sans', array(), WFOCU_VERSION_DEV );

		}

		/**
		 * Including izimodal assets
		 */
		if ( WFOCU_Common::is_load_admin_assets( 'all' ) ) {
			wp_enqueue_style( 'wfocu-izimodal', $this->admin_url . '/includes/iziModal/iziModal.css', array(), WFOCU_VERSION_DEV );
			wp_enqueue_script( 'wfocu-izimodal', $this->admin_url . '/includes/iziModal/iziModal.js', array(), WFOCU_VERSION_DEV );
		}
		if ( WFOCU_Common::is_load_admin_assets( 'settings' ) ) {

			wp_enqueue_script( 'jquery-tiptip' );

		}
		/**
		 * Including vuejs assets
		 */
		if ( WFOCU_Common::is_load_admin_assets( 'all' ) ) {

			wp_enqueue_style( 'wfocu-vue-multiselect', $this->admin_url . '/includes/vuejs/vue-multiselect.min.css', array(), WFOCU_VERSION_DEV );
			wp_enqueue_script( 'jquery-ui-sortable' );
			wp_enqueue_script( 'wfocu-vuejs', $this->admin_url . '/includes/vuejs/vue.min.js', array(), '2.5.13' );
			wp_enqueue_script( 'wfocu-vue-vfg', $this->admin_url . '/includes/vuejs/vfg.min.js', array(), '2.1.0' );
			wp_enqueue_script( 'wfocu-vue-multiselect', $this->admin_url . '/includes/vuejs/vue-multiselect.min.js', array(), WFOCU_VERSION_DEV );
		}
		if ( WFOCU_Common::is_load_admin_assets( 'builder' ) ) {
			wp_enqueue_script( 'accounting' );

			wp_localize_script(
				'accounting', 'wfocu_wc_params', array(
					'currency_format_num_decimals' => wc_get_price_decimals(),
					'currency_format_symbol'       => get_woocommerce_currency_symbol(),
					'currency_format_decimal_sep'  => esc_attr( wc_get_price_decimal_separator() ),
					'currency_format_thousand_sep' => esc_attr( wc_get_price_thousand_separator() ),
					'currency_format'              => esc_attr( str_replace( array( '%1$s', '%2$s' ), array( '%s', '%v' ), get_woocommerce_price_format() ) ),
				)
			);

		}
		/**
		 * Including One Click Upsell assets on all OCU pages.
		 */
		if ( WFOCU_Common::is_load_admin_assets( 'all' ) ) {
			wp_enqueue_style( 'woocommerce_admin_styles' );
			wp_enqueue_script( 'wc-backbone-modal' );
			wp_enqueue_style( 'wfocu-admin', $this->admin_url . '/assets/css/wfocu-admin.css', array(), WFOCU_VERSION_DEV );
			wp_enqueue_script( 'wfocu-admin', $this->admin_url . '/assets/js/wfocu-admin.js', array(), WFOCU_VERSION_DEV );
			wp_enqueue_script( 'wfocu-swal', $this->admin_url . '/assets/js/wfocu-sweetalert.min.js', array(), WFOCU_VERSION_DEV );
			wp_enqueue_script( 'wfocu-admin-builder', $this->admin_url . '/assets/js/wfocu-admin-builder.js', array( 'jquery', 'wfocu-swal' ), WFOCU_VERSION_DEV );
		}

		if ( WFOCU_Common::is_load_admin_assets( 'customizer' ) ) {

			wp_enqueue_script( 'wfocu-modal', plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'admin/assets/js/wfocu-modal.js', array( 'jquery' ), WFOCU_VERSION );
			wp_enqueue_style( 'wfocu-modal', plugin_dir_url( WFOCU_PLUGIN_FILE ) . 'admin/assets/css/wfocu-modal.css', null, WFOCU_VERSION );

		}
		$gateways_list       = WFOCU_Core()->gateways->get_gateways_list();
		$data                = array(
			'ajax_nonce'                => wp_create_nonce( 'wfocuaction-admin' ),
			'plugin_url'                => plugin_dir_url( WFOCU_PLUGIN_FILE ),
			'ajax_url'                  => admin_url( 'admin-ajax.php' ),
			'admin_url'                 => admin_url(),
			'ajax_chosen'               => wp_create_nonce( 'json-search' ),
			'search_products_nonce'     => wp_create_nonce( 'search-products' ),
			'text_or'                   => __( 'or', 'woofunnels-upstroke-one-click-upsell' ),
			'text_apply_when'           => __( 'Open this page when these conditions are matched', 'woofunnels-upstroke-one-click-upsell' ),
			'remove_text'               => __( 'Remove', 'woofunnels-upstroke-one-click-upsell' ),
			'modal_add_offer_step_text' => __( 'Add Offer', 'woofunnels-upstroke-one-click-upsell' ),
			'modal_add_add_product'     => __( 'Add Products', 'woofunnels-upstroke-one-click-upsell' ),
			'modal_update_offer'        => __( 'Offer', 'woofunnels-upstroke-one-click-upsell' ),
			'modal_funnel_div'          => __( 'Funnel', 'woofunnels-upstroke-one-click-upsell' ),
			'section_page'              => $this->section_page,
			'legends_texts'             => array(

				'order_statuses' => __( 'Order Statuses', 'woofunnels-upstroke-one-click-upsell' ),
				'offer_conf'     => __( 'Offer Confirmation Settings', 'woofunnels-upstroke-one-click-upsell' ),
				'gateways'       => __( 'Gateways', 'woofunnels-upstroke-one-click-upsell' ),
				'tan'            => __( 'Tracking & Analytics', 'woofunnels-upstroke-one-click-upsell' ),
				'emails'         => __( 'Confirmation Email', 'woofunnels-upstroke-one-click-upsell' ),
				'misc'           => __( 'Miscellaneous', 'woofunnels-upstroke-one-click-upsell' ),
				'fb_tracking'    => __( 'Facebook Pixel', 'woofunnels-upstroke-one-click-upsell' ),
				'scripts'        => __( 'External Scripts', 'woofunnels-upstroke-one-click-upsell' ),
				'scripts_head'        => __( 'External Scripts H', 'woofunnels-upstroke-one-click-upsell' ),

			),
			'alerts'                    => array(
				'delete_offer' => array(
					'title'             => __( 'Want to Remove this offer from your funnel?', 'woofunnels-upstroke-one-click-upsell' ),
					'text'              => __( 'You won\'t be able to revert this!', 'woofunnels-upstroke-one-click-upsell' ),
					'confirmButtonText' => __( 'Yes, Remove it!', 'woofunnels-upstroke-one-click-upsell' ),
				),

				'offer_edit'           => array(
					'title'             => __( 'Hey! A gentle reminder that this offer is inactive.', 'woofunnels-upstroke-one-click-upsell' ),
					'text'              => __( 'Do activate the offer when you have completed the setup.', 'woofunnels-upstroke-one-click-upsell' ),
					'confirmButtonText' => __( 'Continue and Save!', 'woofunnels-upstroke-one-click-upsell' ),

				),
				'no_variations_chosen' => array(
					'title'             => __( 'Oops! Unable to save this offer', 'woofunnels-upstroke-one-click-upsell' ),
					'text'              => __( 'This offer contains product(s) with no variation selected. Please select atleast one variation', 'woofunnels-upstroke-one-click-upsell' ),
					'confirmButtonText' => __( 'Okay! Got it', 'woofunnels-upstroke-one-click-upsell' ),
					'type'              => 'error',

				),

				'remove_product' => array(
					'title'             => __( 'Want to Remove this product from the offer?', 'woofunnels-upstroke-one-click-upsell' ),
					'text'              => __( 'You won\'t be able to revert this!', 'woofunnels-upstroke-one-click-upsell' ),
					'confirmButtonText' => __( 'Yes, Remove it!', 'woofunnels-upstroke-one-click-upsell' ),
				),
			),
			'forms_labels'              => array(

				'funnel_setting'        => array(
					array(
						'funnel_name' => array(
							'label' => __( 'Name Of Funnel', 'woofunnels-upstroke-one-click-upsell' ),
						),
					),
				),
				'add_new_offer_setting' => array(
					'funnel_step_name' => array(
						'label'       => __( 'Offer Name', 'woofunnels-upstroke-one-click-upsell' ),
						'placeholder' => __( 'Enter Offer Name', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'step_type' => array(
						'label'  => __( 'Type', 'woofunnels-upstroke-one-click-upsell' ),
						'help'   => __( '<strong>Upsell</strong> <br/>The upsell is when you present a new offer.<hr/><strong>Downsell</strong><br/>The downsell is when your Upsell offer was declined and you present a new offer usually at a lower price.', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(

							array(
								'name'  => __( 'Upsell', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'upsell',
							),
							array(
								'name'  => __( 'Downsell', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'downsell',
							),

						),
					),
				),
				'update_step'           => array(
					'funnel_step_name' => array(
						'label'       => __( 'Offer Name', 'woofunnels-upstroke-one-click-upsell' ),
						'placeholder' => __( 'Enter Offer Name', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'step_type'        => array(
						'label'  => __( 'Type', 'woofunnels-upstroke-one-click-upsell' ),
						'help'   => __( '<strong>Upsell</strong> <br/>The upsell is when you present a new offer.<hr/><strong>Downsell</strong><br/>The downsell is when your Upsell offer was declined and you present a new offer usually at a lower price.', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(

							array(
								'name'  => __( 'Upsell', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'upsell',
							),
							array(
								'name'  => __( 'Downsell', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'downsell',
							),

						),
					),
					'funnel_step_slug' => array(
						'label'       => __( 'Offer URL', 'woofunnels-upstroke-one-click-upsell' ),
						'placeholder' => __( 'Enter Offer Slug', 'woofunnels-upstroke-one-click-upsell' ),
					),
				),
				'settings'              => array(
					'funnel_order_label'        => array(

						'label' => __( 'Behavioural Settings', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'order_behavior'            => array(

						'label'  => __( 'Accepted Upsell Order', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(

							array(
								'name'  => __( 'Add to Main Order', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'batching',
							),
							array(
								'name'  => __( 'Create a New Order', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'create_order',
							),

						),
					),
					'is_cancel_order'           => array(

						'label'  => __( 'Do Cancel Primary Order?', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(

							array(
								'name'  => __( 'Yes', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
							array(
								'name'  => __( 'No', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'no',
							),

						),
					),
					'funnel_priority_label'     => array(

						'label' => __( 'Priority', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'funnel_priority'           => array(

						'label' => __( 'Priority Number', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( "There may be a chance that Rules can be set up in a way that Two Upsell Funnels can trigger. \r\n In such cases, Funnel Prioirty is used to determine which Funnel will trigger. Priority Number 1 is considered highest.", 'woofunnels-upstroke-one-click-upsell' ),

					),
					'funnel_display_label'      => array(

						'label' => __( 'Prices', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'is_tax_included'           => array(

						'label' => __( 'Show Prices with taxes', 'woofunnels-upstroke-one-click-upsell' ),
						//'hint'  => __( "There may be a chance that Rules can be set up in a way that Two Upsell Funnels can trigger. \r\n In such cases, Funnel Prioirty is used to determine which Funnel will trigger. Priority Number 1 is considered highest.", 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(

							array(
								'name'  => __( 'Yes (Recommended)', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
							array(
								'name'  => __( 'No', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'no',
							),

						),
					),
					'offer_messages_label_help' => array(
						'label' => __( 'These messages show when buyer\'s upsell order is charged & confirmed. If unable to charge user, a failure message will show.<a href="javascript:void(0);" onclick="window.wfocuBuilder.show_funnel_design_messages()">Click here to learn about these settings.</a> ', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'offer_messages_label'      => array(

						'label' => __( 'Upsell Confirmation Messages', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_success_message_pop' => array(

						'label' => __( 'Upsell Success Message', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_failure_message_pop' => array(

						'label' => __( 'Upsell Failure Message', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_wait_message_pop'    => array(

						'label' => __( 'Upsell Processing Message', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_scripts_label'       => array(

						'label' => __( 'External Tracking Code', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'funnel_success_script'     => array(

						'label'       => __( 'Add Tracking code to fire when user reached to thankyou page after visiting this funnel', 'woofunnels-upstroke-one-click-upsell' ),
						'placeholder' => __( 'Paste your code here', 'woofunnels-upstroke-one-click-upsell' ),

					),

				),
				'global_settings'       => array(

					'label_section_head_tan' => array(
						'label' => __( 'Facebook Pixel Tracking', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'label_section_head_tan_ga' => array(
						'label' => __( 'Google Analytics Tracking', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'label_section_head_orders' => array(
						'label' => __( 'Order Status When Upsell Are To Be Merged With original Order', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'label_section_head_orders_no_batch_cancel' => array(
						'label' => __( 'Order Status When Primary Order Is Cancelled & Upsell Is Accepted', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'label_section_head_orders_upsell_fails'    => array(
						'label' => __( 'Order Status When Charge For Upsell Fails.', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'label_section_head_emails'                 => array(
						'label' => __( 'Applicable When Upsell Are To Be Merged With Original Order', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'label_section_head_emails_no_batch'        => array(
						'label' => __( 'Applicable When Upsell Are To Be Created Separate Orders', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'label_section_head_emails_no_batch_cancel' => array(
						'label' => __( 'Applicable When Primary Order Is Cancelled & Upsell Is Accepted', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'label_section_head_emails_upsell_fails'    => array(
						'label' => __( 'Applicable When Charge For Upsell Fails.', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'label_section_head_orders_no_batch_note'        => array(
						'label' => __( 'No change happens in order emails in this scenario.', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'label_section_head_orders_no_batch_cancel_note' => array(
						'label' => __( 'No change happens in order emails in this scenario.', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'label_section_head_orders_upsell_fails_note'    => array(
						'label' => __( 'No change happens in order emails in this scenario.', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'fb_pixel_key'                  => array(
						'label' => __( 'Facebook Pixel ID', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( 'Log into your facebook ads account to find your Pixel ID. <a target="_blank" href="https://www.facebook.com/ads/manager/pixel/facebook_pixel">Click here for more info.</a>', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'is_fb_view_event'              => array(
						'label'  => __( 'Tracking Events', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Enable PageView Event', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'is_fb_purchase_event'          => array(
						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'Note: UpStroke will send total order value and store currency based on order. <a target="_blank" href="https://developers.facebook.com/docs/facebook-pixel/pixel-with-ads/conversion-tracking#add-value">Click here to know more.</a>', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Enable Purchase Event', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'is_fb_synced_event'            => array(
						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'Note: Your Product catalog must be synced with Facebook. <a target="_blank" href="https://developers.facebook.com/docs/facebook-pixel/implementation/dynamic-ads">Click here to know more.</a>', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Enable Content Settings for Dynamic Ads', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'content_id_value'              => array(

						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'Select either Product ID or SKU to pass value in content_id parameter', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'id' => '',
								'name' => __( 'Select content_id parameter', 'woofunnels-upstroke-one-click-upsell' ),
							),
							array(
								'id' => 'product_id',
								'name' => __( 'Product ID', 'woofunnels-upstroke-one-click-upsell' ),
							),
							array(
								'id' => 'product_sku',
								'name' => __( 'Product SKU', 'woofunnels-upstroke-one-click-upsell' ),
							),

						),
					),
					'content_id_variable'           => array(
						//
						//                      'label' => __( 'Treat variable products like simple products', 'woofunnels-upstroke-one-click-upsell' ),
						//                      'hint'  => __( 'Turn this option ON when your Product Catalog doesn\'t include the variants for variable products.', 'woofunnels-upstroke-one-click-upsell' ),

						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'Turn this option ON when your Product Catalog doesn\'t include the variants for variable products.', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Treat variable products like simple products', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'content_id_prefix'             => array(

						'label'       => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'placeholder' => __( 'content_id prefix', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'        => __( 'Add prefix to the content_id parameter (optional)', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'content_id_suffix'             => array(

						'label'       => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'placeholder' => __( 'content_id suffix', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'        => __( 'Add suffix to the content_id parameter (optional)', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'is_fb_advanced_event'          => array(
						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'Note: UpStroke will send customer\'s email, name, phone, address fields whichever available in the order. <a target="_blank" href="https://developers.facebook.com/docs/facebook-pixel/pixel-with-ads/conversion-tracking#advanced_match">Click here to know more.', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Enable Advanced Matching With the Pixel', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'ga_key'                        => array(
						'label' => __( 'Google Analytics ID', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( 'Log into your google analytics account to find your ID. eg: UA-XXXXXX-X.', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'is_ga_view_event'              => array(
						'label'  => __( 'Tracking Events', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Enable PageView Event', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'is_ga_purchase_event'          => array(
						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Enable Purchase Event', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'scripts'                       => array(
						'label' => __( 'External Scripts On Offer Pages', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( 'These scripts will be globally embedded in all the funnels\' offer pages. ', 'woofunnels-upstroke-one-click-upsell' ),
					),
                    'scripts_head'                       => array(
						'label' => __( 'External Scripts On Offer Pages (in <head> tag)', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( 'These scripts will be globally embedded in all the funnels\' offer pages. ', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'label_section_head_paypal_ref' => array(
						'label' => '',
						'hint'  => __( 'UpStroke works with or without reference transactions. If you have reference transactions enabled in your PayPal account select Yes otherwise No.', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'gateways' => array(
						'label'  => __( 'Enable Gateways', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => $gateways_list,
						'hint'   => __( 'Could not find your gateway in the list? <a target="_blank" href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">Click here to check enabled Payment Methods</a>', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'no_gateways' => array(
						'label' => __( 'Enable Gateways', 'woofunnels-upstroke-one-click-upsell' ),

						'hint' => __( 'No Gateways Found. Could not find your gateway in the list?<br> <a target="_blank" href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">Click here to check enabled Payment Methods</a>', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'paypal_ref_trans' => array(

						'label' => __( 'PayPal Reference Transactions', 'woofunnels-upstroke-one-click-upsell' ),

						'values' => array(

							array(
								'name'  => __( 'Yes, Reference transactions are enabled on my PayPal account', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
							array(
								'name'  => __( 'No, Reference transaction are not enabled on my PayPal account.', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'no',
							),

						),

					),

					'gateway_test'         => array(
						'label'  => __( 'Enable Test Gateway', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'To quickly test Upsell Funnels , create a Test Gateway. This is only visible to Admin & will not be available for the orders containing WooCommerce Subscription Products.', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Test Gateway By WooFunnels', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'offer_post_type_slug' => array(

						'label' => __( 'Offer Post Type Slug', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'enable_log'           => array(

						'label' => __( 'Enable Logging', 'woofunnels-upstroke-one-click-upsell' ),

					),

					'order_copy_meta_keys' => array(

						'label'       => __( 'Meta Keys To Copy From Primary Order', 'woofunnels-upstroke-one-click-upsell' ),
						'placeholder' => __( 'Enter keys like utm_campaign|utm_source|...', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'        => __( 'Applicable in case of new upsell order is created.', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'send_processing_mail_on'                 => array(

						'label' => __( 'Send Order Confirmation Email When', 'woofunnels-upstroke-one-click-upsell' ),
						//                      'hint'  => __( 'When user enters the funnel, you can decide whether to send an email right away or when funnel ends.', 'woofunnels-upstroke-one-click-upsell' ),

						'values' => array(

							array(
								'name'  => __( 'Funnel Start', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'start',
							),
							array(
								'name'  => __( 'Funnel Ends (Recommended)', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'end',
							),

						),

					),
					'send_processing_mail_on_no_batch'        => array(

						'label' => __( 'Send Order Confirmation Email When', 'woofunnels-upstroke-one-click-upsell' ),
						//                      'hint'  => __( 'When user enters the funnel, you can decide whether to send an email right away or when funnel ends.', 'woofunnels-upstroke-one-click-upsell' ),

						'values' => array(

							array(
								'name'  => __( 'Funnel Start (Recommended)', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'start',
							),
							array(
								'name'  => __( 'Funnel Ends', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'end',
							),

						),

					),
					'send_processing_mail_on_no_batch_cancel' => array(

						'label' => __( 'Send Order Confirmation Email When', 'woofunnels-upstroke-one-click-upsell' ),
						//                      'hint'  => __( 'When user enters the funnel, you can decide whether to send an email right away or when funnel ends.', 'woofunnels-upstroke-one-click-upsell' ),

						'values' => array(

							array(
								'name'  => __( 'Funnel Start', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'start',
							),
							array(
								'name'  => __( 'Funnel Ends (Recommended)', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'end',
							),

						),

					),
					'send_emails_label'                       => array(
						'label' => __( 'When user enters the funnel, you can decide whether to send an email right away or when funnel ends.', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'primary_order_status_title'   => array(

						'hint' => __(
							'<br/><strong>What is custom order state?</strong><br/>
						    It is an intermediary state when funnel is running. Once user has accepted or rejected or time of offer expired, order status is automatically switched to successful order status.

 <br/><br/> <strong>Why it is needed?</strong> <br/>There can be additional processes such as sending of data to external CRMs which can trigger when order is successful. By having an intermediate order state, we wait for upsell funnel to get complete. Once it is done order status automatically moves to successful order state. This method enables additional processes to process correct order data.', 'woofunnels-upstroke-one-click-upsell'
						),

						'label' => __( 'Custom Order State Label', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'flat_shipping_label'          => array(

						'label' => __( 'Shipping Label For Custom Fixed Rates', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( 'When Fixed rate shipping is applied, what would be the label of the shipping method?', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'create_new_order_status_fail' => array(

						'label'  => __( 'Order Status Of Failed Order When Upsell Is Accepted', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __(
							'<strong> Why it is needed?</strong> <br/>
Sometimes it may happen that due to failure of payment gateways, the user could not be charged for upsell rightaway.
In such scenarios a separate order is created for your record and is created and marked as failed. ', 'woofunnels-upstroke-one-click-upsell'
						),
						'values' => WFOCU_Common::get_order_status_settings(),
					),
					'ttl_funnel'                   => array(

						'label' => __( 'Forcefully End Funnel (in minutes)', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __(
							'<strong> Why it is needed? </strong><br/>Sometimes users may keep Offer Page open and not take a decision. Set up a realistic time in minutes after which funnel forcefully ends.
This setting will determine time of Order Confirmation emails if it set to "When Funnel Ends". <br/> If you are not sure keep it by default to 15 mins', 'woofunnels-upstroke-one-click-upsell'
						),
					),
					'track_traffic_source'         => array(

						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'Add traffic source as traffic_source and URL parameters (UTM) as parameters to all your events.', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Track Traffic Source & UTMs', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),
					),
					'enable_general_event'         => array(

						'label'  => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'   => __( 'Use the GeneralEvent for your Custom Audiences and Custom Conversions.', 'woofunnels-upstroke-one-click-upsell' ),
						'values' => array(
							array(
								'name'  => __( 'Enable General Event', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'yes',
							),
						),

					),
					'general_event_name'           => array(

						'label'       => '',
						'placeholder' => __( 'General Event Name', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'        => __( 'Customize the name of general event.', 'woofunnels-upstroke-one-click-upsell' ),
					),

					'custom_aud_opt_conf' => array(
						'label' => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( 'Choose the parameters you want to send with purchase event', 'woofunnels-upstroke-one-click-upsell' ),

						'values' => array(
							array(
								'name'  => __( 'Add Town,State & Country Parameters', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'add_town_s_c',
							),
							array(
								'name'  => __( 'Add Payment Method Parameters', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'add_payment_method',
							),
							array(
								'name'  => __( 'Add Shipping Method Parameters', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'add_shipping_method',
							),
							array(
								'name'  => __( 'Add Coupon parameters', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'add_coupon',
							),
						),
					),

					'exclude_from_total' => array(
						'label' => __( '', 'woofunnels-upstroke-one-click-upsell' ),
						'hint'  => __( 'Check above boxes to exclude shipping/taxes from the total.', 'woofunnels-upstroke-one-click-upsell' ),

						'values' => array(
							array(
								'name'  => __( 'Exclude Shipping from Total', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'is_disable_shipping',
							),
							array(
								'name'  => __( 'Exclude Taxes from Total', 'woofunnels-upstroke-one-click-upsell' ),
								'value' => 'is_disable_taxes',
							),

						),
					),

				),

				'global_settings_offer_confirmation' => array(
					'offer_header_label'   => array(
						'label' => __( 'These settings are applicable when you use custom upsell offer pages and have enabled confirmation.Need help with these settings? <a href="https://buildwoofunnels.com/docs/upstroke/global-settings/offer-confirmation/" target="_blank">Click here to learn about it.</a> ', 'woofunnels-upstroke-one-click-upsell' ),
					),
					'offer_header_text'    => array(

						'label' => __( 'Header Text', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_yes_btn_text'   => array(

						'label' => __( 'Acceptance Button Text', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_skip_link_text' => array(

						'label' => __( 'Skip Link Text', 'woofunnels-upstroke-one-click-upsell' ),

					),

					'offer_yes_btn_bg_cl'  => array(

						'label' => __( 'Acceptance Button Background Color', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_yes_btn_sh_cl'  => array(

						'label' => __( 'Acceptance Button Shadow Color', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_yes_btn_txt_cl' => array(

						'label' => __( 'Acceptance Button Text Color', 'woofunnels-upstroke-one-click-upsell' ),

					),

					'offer_yes_btn_bg_cl_h'  => array(

						'label' => __( 'Acceptance Button Background Color (Hover)', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_yes_btn_sh_cl_h'  => array(

						'label' => __( 'Acceptance Button Shadow Color (Hover)', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_yes_btn_txt_cl_h' => array(

						'label' => __( 'Acceptance Button Text Color (Hover)', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_no_btn_txt_cl'    => array(

						'label' => __( 'Skip Link Text Color', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'offer_no_btn_txt_cl_h'  => array(

						'label' => __( 'Skip Link Hover Text Color', 'woofunnels-upstroke-one-click-upsell' ),

					),

					'cart_opener_text'             => array(

						'label' => __( 'Re-open Badge Text', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'cart_opener_text_color'       => array(

						'label' => __( 'Re-open Badge Text Color', 'woofunnels-upstroke-one-click-upsell' ),

					),
					'cart_opener_background_color' => array(

						'label' => __( 'Re-open Badge Background Color', 'woofunnels-upstroke-one-click-upsell' ),

					),
				),

			),
			'funnel_settings'           => WFOCU_Core()->funnels->get_funnel_option(),
			'global_settings'           => WFOCU_Core()->data->get_option(),

			'shortcodes' => array(
				array(
					'label' => __( 'Product Offer Accept Link', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_yes_link key="%s"]' . __( 'Add to my Order', 'woofunnels-upstroke-one-click-upsell' ) . '[/wfocu_yes_link]',
				),
				array(
					'label' => __( 'Product Offer Skip Link', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_no_link key="%s" ]' . __( 'No, thanks', 'woofunnels-upstroke-one-click-upsell' ) . '[/wfocu_no_link]',
				),
				array(
					'label' => __( 'Product Offer Variation Selector', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_variation_selector_form key="%s"]',
				),
				array(
					'label' => __( 'Product Quantity Selector', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_qty_selector key="%s"]',
				),
				array(
					'label' => __( 'Product Offer Price', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_product_offer_price key="%s"]',
				),
				array(
					'label' => __( 'Product Regular Price', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_product_regular_price key="%s"]',
				),
				array(
					'label' => __( 'Product Price HTML', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_product_price_full key="%s"]',
				),
				array(
					'label' => __( 'Product Offer Save Value', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_product_save_value key="%s"]',
				),
				array(
					'label' => __( 'Product Offer Save Percentage', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_product_save_percentage key="%s"]',
				),
				array(
					'label' => __( 'Product Offer Save value & Percentage', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_product_savings key="%s"]',
				),
				array(
					'label' => __( 'Product Single Unit Price', 'woofunnels-upstroke-one-click-upsell' ),
					'code'  => '[wfocu_product_single_unit_price key="%s"]',
				),
			),

			'templates'       => WFOCU_Core()->template_loader->get_templates(),
			'permalinkStruct' => get_option( 'permalink_structure' ),
		);
		$data['isNOGateway'] = true;
		if ( $gateways_list && is_array( $gateways_list ) && count( $gateways_list ) > 0 ) {
			$data['isNOGateway'] = false;
		}
		wp_localize_script( 'wfocu-admin', 'wfocuParams', $data );

	}

	public function admin_customizer_enqueue_assets() {
		if ( WFOCU_Common::is_load_admin_assets( 'customizer' ) ) {
			wp_enqueue_style( 'wfocu-customizer', $this->admin_url . '/assets/css/wfocu-customizer.css', array(), WFOCU_VERSION_DEV );
		}
	}

	public function upstroke_page() {
		if ( isset( $_GET['page'] ) && 'upstroke' === $_GET['page'] ) {
			if ( isset( $_GET['section'] ) ) {
				include_once( $this->admin_path . '/view/funnel-builder-view.php' );
			} elseif ( isset( $_GET['tab'] ) && $_GET['tab'] == 'settings' ) {
				include_once( $this->admin_path . '/view/global-settings.php' );
			} else {
				require_once( WFOCU_PLUGIN_DIR . '/admin/includes/class-wfocu-post-table.php' );
				include_once( $this->admin_path . '/view/funnel-admin.php' );
			}
		}
		if ( 'yes' == filter_input( INPUT_GET, 'activated' ) ) {
			flush_rewrite_rules();
		}
	}

	public function js_variables() {
		$data        = array(
			'site_url'   => site_url(),
			'offer_slug' => WFOCU_Core()->data->get_option( 'offer_post_type_slug' ),
			'texts'      => array(
				'update_template'        => __( 'Template Updated Successfully', 'woofunnels-upstroke-one-click-upsell' ),
				'settings_success'       => __( 'Settings Saved Successfully', 'woofunnels-upstroke-one-click-upsell' ),
				'state_changed_true'     => __( 'Funnel Activated.', 'woofunnels-upstroke-one-click-upsell' ),
				'state_changed_false'    => __( 'Funnel Deactivated', 'woofunnels-upstroke-one-click-upsell' ),
				'product_success'        => __( 'Offer Saved Successfully', 'woofunnels-upstroke-one-click-upsell' ),
				'rules_success'          => __( 'Rules Saved Successfully', 'woofunnels-upstroke-one-click-upsell' ),
				'shortcode_copy_message' => __( 'Shortcode Copied!', 'woofunnels-upstroke-one-click-upsell' ),
			),
		);
		$funnel_post = get_post( WFOCU_Core()->funnels->get_funnel_id() );

		if ( false === is_null( $funnel_post ) ) {
			$data['id']          = WFOCU_Core()->funnels->get_funnel_id();
			$data['funnel_name'] = get_the_title( $funnel_post );
			$data['funnel_desc'] = $funnel_post->post_content;
		}

		if ( $this->is_upstroke_page( 'offers' ) || $this->is_upstroke_page( 'design' ) ) {
			$data_funnels = WFOCU_Core()->funnels->get_funnel_offers_admin();

			$data = array_merge( $data, $data_funnels );

		}

		$wfo = 'window.wfocu=' . json_encode( $data ) . ';';
		echo "<script>$wfo</script>";

	}

	public function is_upstroke_page( $section = '' ) {
		if ( isset( $_GET['page'] ) && $_GET['page'] == 'upstroke' && '' == $section ) {
			return true;
		}

		if ( isset( $_GET['page'] ) && $_GET['page'] == 'upstroke' && isset( $_GET['section'] ) && $_GET['section'] == $section ) {
			return true;
		}

		return false;
	}

	public function admin_footer_text( $footer_text ) {
		if ( WFOCU_Common::is_load_admin_assets( 'builder' ) ) {
			return '';
		}

		return $footer_text;
	}

	public function update_footer( $footer_text ) {
		if ( WFOCU_Common::is_load_admin_assets( 'builder' ) ) {
			return '';
		}

		return $footer_text;
	}

	public function maybe_reset_transients( $post_id, $post = null ) {
		//Check it's not an auto save routine
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		//Perform permission checks! For example:
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( class_exists( 'WooFunnels_Transient' ) && ( is_object( $post ) && $post->post_type === WFOCU_Common::get_funnel_post_type_slug() ) ) {
			$woofunnels_transient_obj = WooFunnels_Transient::get_instance();
			$woofunnels_transient_obj->delete_all_transients( 'upstroke' );
		}

	}


	public function maybe_set_funnel_id() {

		if ( $this->is_upstroke_page() && isset( $_GET['edit'] ) ) {

			WFOCU_Core()->funnels->set_funnel_id( $_GET['edit'] );
		}
	}

	/**
	 * @hooked over `delete_post`
	 *
	 * @param $post_id
	 */
	public function clear_transients_on_delete( $post_id ) {

		$get_post_type = get_post_type( $post_id );

		if ( WFOCU_Common::get_funnel_post_type_slug() === $get_post_type ) {
			if ( class_exists( 'WooFunnels_Transient' ) ) {
				$woofunnels_transient_obj = WooFunnels_Transient::get_instance();
				$woofunnels_transient_obj->delete_all_transients( 'upstroke' );
			}
			do_action( 'wfocu_funnel_admin_deleted', $post_id );
		}

	}


	/**
	 * @hooked over `delete_post`
	 * Delete the funnel record if any from the database on permanent deletion of order.
	 *
	 * @param mixed $post_id
	 */
	public function clear_session_record_on_shop_order_delete( $post_id ) {
		$get_post_type = get_post_type( $post_id );

		/**
		 * @todo keep eye on WC upgrades so that we can handle this when Orders are no longer be posts.
		 */
		if ( 'shop_order' === $get_post_type ) {

			$sess_id = WFOCU_Core()->session_db->get_session_id_by_order_id( $post_id );

			if ( ! empty( $sess_id ) ) {
				WFOCU_Core()->session_db->delete( $sess_id );
			}
		}
	}

	public function maybe_activate_post() {

		if ( isset( $_GET['action'] ) && $_GET['action'] == 'wfocu-post-activate' ) {
			if ( wp_verify_nonce( $_GET['_wpnonce'], 'wfocu-post-activate' ) ) {

				$postID = filter_input( INPUT_GET, 'postid' );
				if ( $postID ) {
					wp_update_post(
						array(
							'ID'          => $postID,
							'post_status' => 'publish',
						)
					);
					wp_safe_redirect( admin_url( 'admin.php?page=upstroke' ) );
				}
			} else {
				die( __( 'Unable to Activate', 'woofunnels-upstroke-one-click-upsell' ) );
			}
		}
	}

	public function maybe_deactivate_post() {
		if ( isset( $_GET['action'] ) && $_GET['action'] == 'wfocu-post-deactivate' ) {

			if ( wp_verify_nonce( $_GET['_wpnonce'], 'wfocu-post-deactivate' ) ) {

				$postID = filter_input( INPUT_GET, 'postid' );
				if ( $postID ) {
					wp_update_post(
						array(
							'ID'          => $postID,
							'post_status' => WFOCU_SLUG . '-disabled',
						)
					);

					wp_safe_redirect( admin_url( 'admin.php?page=upstroke' ) );
				}
			} else {
				die( __( 'Unable to Deactivate', 'woofunnels-upstroke-one-click-upsell' ) );
			}
		}
	}


	public function maybe_print_mergetag_helpbox() {

		if ( false === WFOCU_Common::is_load_admin_assets( 'customizer' ) ) {
			return;
		}
		$offer_data = WFOCU_Core()->offers->get_offer_meta( WFOCU_Core()->customizer->offer_id );

		?>

        <div class='' id="wfocu_shortcode_help_box" style="display: none;">

            <h3><?php _e( 'Merge Tags', 'woofunnels-upstroke-one-click-upsell' ); ?></h3>
            <div style="font-size: 1.1em; margin: 5px;"><?php _e( 'Here are are set of Merge Tags that can be used on this page.', 'woofunnels-upstroke-one-click-upsell' ); ?> </i> </div>
			<?php foreach ( $offer_data->products as $hash => $product_id ) { ?>
                <h4><?php _e( sprintf( 'Product: %1$s', wc_get_product( $product_id )->get_title() ), 'woofunnels-upstroke-one-click-upsell' ); ?></h4>

                <table class="table widefat">
                    <thead>
                    <tr>
                        <td><?php _e( 'Title', 'woofunnels-upstroke-one-click-upsell' ); ?></td>
                        <td style="width: 70%;"><?php _e( 'Merge Tags', 'woofunnels-upstroke-one-click-upsell' ); ?></td>

                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>
							<?php _e( 'Product Offer Price', 'woofunnels-upstroke-one-click-upsell' ); ?>


                        </td>
                        <td>
                            <input type="text" style="width: 75%;" readonly onClick="this.select()"
                                   value='<?php printf( '{{product_offer_price key="%s"}}', $hash ); ?>'/>
                        </td>

                    </tr>
                    <tr>
                        <td>
							<?php _e( 'Product Regular Price', 'woofunnels-upstroke-one-click-upsell' ); ?>
                        </td>
                        <td>
                            <input type="text" style="width: 75%;" readonly onClick="this.select()"
                                   value='<?php printf( '{{product_regular_price key="%s"}}', $hash ); ?>'/>
                        </td>

                    </tr>
                    <tr>
                        <td>

							<?php _e( ' Product Price Html', 'woofunnels-upstroke-one-click-upsell' ); ?>
                        </td>
                        <td>
                            <input type="text" style="width: 75%;" readonly onClick="this.select()"
                                   value='<?php printf( '{{product_price_full key="%s"}}', $hash ); ?>'/>
                        </td>

                    </tr>

                    <tr>
                        <td>
							<?php _e( 'Product Offer Save Value', 'woofunnels-upstroke-one-click-upsell' ); ?>
                        </td>
                        <td>
                            <input type="text" style="width: 75%;" readonly onClick="this.select()"
                                   value='<?php printf( '{{product_save_value key="%s"}}', $hash ); ?>'/>
                        </td>

                    </tr>
                    <tr>
                        <td>
							<?php _e( ' Product Offer Save Percentage', 'woofunnels-upstroke-one-click-upsell' ); ?>
                        </td>

                        <td>
                            <input type="text" style="width: 75%;" readonly onClick="this.select()"
                                   value='<?php printf( '{{product_save_percentage key="%s"}}', $hash ); ?>'/>
                        </td>

                    </tr>

                    <tr>
                        <td>
							<?php _e( ' Product Single Unit Price', 'woofunnels-upstroke-one-click-upsell' ); ?>
                        </td>

                        <td>
                            <input type="text" style="width: 75%;" readonly onClick="this.select()"
                                   value='<?php printf( '{{product_single_unit_price key="%s"}}', $hash ); ?>'/>
                        </td>

                    </tr>

                    <tr>
                        <td>
							<?php _e( 'Product Offer Save Value & Percentage', 'woofunnels-upstroke-one-click-upsell' ); ?>

                        </td>
                        <td>
                            <input type="text" style="width: 75%;" readonly onClick="this.select()"
                                   value='<?php printf( '{{product_savings key="%s"}}', $hash ); ?>'/>
                        </td>

                    </tr>


                    </tbody>


                </table>
			<?php } ?>
            <br/>

            <h3>Order Merge Tags</h3>
            <table class="table widefat">
                <thead>
                <tr>
                    <td width="300">Name</td>
                    <td>Syntax</td>
                </tr>
                </thead>
                <tbody>
				<?php foreach ( WFOCU_Dynamic_Merge_Tags::get_all_tags() as $tag ) : ?>
                    <tr>
                        <td>
							<?php echo $tag['name']; ?>
                        </td>
                        <td>
                            <input type="text" style="width: 75%;" onClick="this.select()" readonly
                                   value='<?php echo '{{' . $tag['tag'] . '}}'; ?>'/>
							<?php
							if ( isset( $tag['desc'] ) && $tag['desc'] != '' ) {
								echo '<p>' . $tag['desc'] . '</p>';
							}
							?>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
            <br/>

            <h3>Other Merge Tags</h3>
            <table class="table widefat">
                <thead>
                <tr>
                    <td width="300">Name</td>
                    <td>Syntax</td>
                </tr>
                </thead>
                <tbody>
				<?php foreach ( WFOCU_Dynamic_Merge_Tags::get_all_other_tags() as $tag ) : ?>
                    <tr>
                        <td>
							<?php echo $tag['name']; ?>
                        </td>
                        <td>
                            <input type="text" style="width: 75%;" onClick="this.select()" readonly
                                   value='<?php echo '{{' . $tag['tag'] . '}}'; ?>'/>
							<?php
							if ( isset( $tag['desc'] ) && $tag['desc'] != '' ) {
								echo '<p>' . $tag['desc'] . '</p>';
							}
							?>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>

		<?php
	}

	/**
	 * Hooked over 'plugin_action_links_{PLUGIN_BASENAME}' WordPress hook to add deactivate popup support
	 *
	 * @param array $links array of existing links
	 *
	 * @return array modified array
	 */
	public function plugin_actions( $links ) {
		$links['deactivate'] .= '<i class="woofunnels-slug" data-slug="' . WFOCU_PLUGIN_BASENAME . '"></i>';

		return $links;
	}

	public function maybe_handle_http_referer() {
		if ( $this->is_upstroke_page() && ( ! empty( $_REQUEST['_wp_http_referer'] ) ) ) {
			wp_redirect( remove_query_arg( array( '_wp_http_referer', '_wpnonce', 'offer_state' ), wp_unslash( $_SERVER['REQUEST_URI'] ) ) );
			exit;
		}

	}

	public function tooltip( $text ) {
		?>
        <span class="wfocu-help"><i class="icon"></i><div class="helpText"><?php echo $text; ?></div></span>
		<?php
	}

	public function hide_test_gateway_from_admin_list() {
		?>
        <style>
            table.wc_gateways tr[data-gateway_id="wfocu_test"] {
                display: none !important;
            }
        </style>
		<?php
	}

	public function maybe_show_wizard() {
		if ( empty( $_GET['page'] ) || 'upstroke' !== $_GET['page'] ) {
			return;
		}

		if ( WFOCU_Core()->support->is_license_present() === false ) {
			wp_redirect( admin_url( 'admin.php?page=woofunnels&tab=' . WFOCU_SLUG . '-wizard' ) );
		}
	}

	/**
	 * Remove all the notices in our dashboard pages as they might break the design.
	 */
	public function maybe_remove_all_notices_on_page() {
		if ( isset( $_GET['page'] ) && 'upstroke' == $_GET['page'] && isset( $_GET['section'] ) ) {
			remove_all_actions( 'admin_notices' );
		}
	}


	public function check_db_version() {

		$get_db_version = get_option( '_wfocu_db_version', '0.0.0' );

		if ( version_compare( WFOCU_DB_VERSION, $get_db_version, '>' ) ) {

			//needs checking
			global $wpdb;
			include_once plugin_dir_path( WFOCU_PLUGIN_FILE ) . 'db/tables.php';
			$tables = new WFOCU_DB_Tables( $wpdb );

			$tables->add_if_needed();

		}

	}


	function show_upsell_total_in_order_listings( $total, $order ) {
		global $current_screen;
		if ( ! $current_screen instanceof WP_Screen || ! $order instanceof WC_Order || $current_screen->base !== 'edit' || $current_screen->id !== 'edit-shop_order' || $current_screen->post_type !== 'shop_order' ) {
			return $total;
		}

		$result = $order->get_meta( '_wfocu_upsell_amount', true );

		if ( empty( $result ) ) {
			$order_id = WFOCU_WC_Compatibility::get_order_id( $order );

			$upstroke_data = WFOCU_Core()->track->query_results(
				array(
					'data'         => array(
						'value' => array(
							'type'     => 'col',
							'function' => 'SUM',
							'name'     => 'upsells',
						),
					),
					'where'        => array(
						array(
							'key'      => 'events.action_type_id',
							'value'    => 4,
							'operator' => '=',
						),
						array(
							'key'      => 'session.order_id',
							'value'    => $order_id,
							'operator' => '=',
						),
					),
					'query_type'   => 'get_results',
					'session_join' => true,
				)
			);

			$result = ( count( $upstroke_data ) > 0 && $upstroke_data[0]->upsells !== null ) ? $upstroke_data[0]->upsells : 0;

			if ( $result === 0 ) {
				return $total;
			}

			if ( 0 < $result ) {
				$order->update_meta_data( '_wfocu_upsell_amount', $result );
				$order->save_meta_data();
			}
		}
		$html  = '<br/>
<p style="font-size: 12px;"><em> ' . sprintf( esc_html__( 'UpStroke: %s' ), wc_price( $result, array( 'currency' => get_option( 'woocommerce_currency' ) ) ) ) . '</em></p>';
		$total = $total . $html;

		return $total;
	}


	/**
	 * @hooked `shutdown`
	 * As we moved to DB version 1.0 with the new table structure.
	 * We need to ensure the data for the upsell gross amount get stored in the order meta.
	 *
	 */
	public function maybe_update_upsell_gross_total_to_order_meta() {

		$check_if_already_updated_meta = get_option( '_wfocu_db_total_meta', 'no' );

		if ( 'no' === $check_if_already_updated_meta ) {
			global $wpdb;

			$query = $wpdb->prepare( 'SELECT `order_id`, SUM(`value`) as `amount` FROM `' . $wpdb->prefix . 'wfocu_events` WHERE `action_type_id` = %s GROUP BY `order_id` LIMIT 5000', '4' );

			$a = $wpdb->get_results( $query, ARRAY_A );

			if ( is_array( $a ) && count( $a ) > 0 ) {
				foreach ( $a as $v ) {
					$order_id   = $v['order_id'];
					$upsell_val = $v['amount'];
					update_post_meta( $order_id, '_wfocu_upsell_amount', number_format( $upsell_val, 2, '.', '' ) );
				}
			}
			update_option( '_wfocu_db_total_meta', 'yes', true );
		}
	}


	public function toolbar_link_to_xlplugins( $wp_admin_bar ) {

		if ( defined( 'WFOCU_IS_DEV' ) && true === WFOCU_IS_DEV ) {
			$args = array(
				'id'    => 'wfocu_admin_logs',
				'title' => __( 'WC Logs', 'woofunnels-upstroke-one-click-upsell' ),
				'href'  => admin_url( 'admin.php?page=wc-status&tab=logs' ),
				'meta'  => array( 'class' => 'wfocu_admin_logs' ),
			);
			$wp_admin_bar->add_node( $args );
		}

		return $wp_admin_bar;

	}


	public function set_wc_payment_gateway_column( $header ) {

		$header_new = array_slice( $header, 0, count( $header ) - 1, true ) + array( 'wfocu' => __( 'Upsell Allowed', 'woocommerce-subscriptions' ) ) + // Ideally, we could add a link to the docs here, but the title is passed through esc_html()
		              array_slice( $header, count( $header ) - 1, count( $header ) - ( count( $header ) - 1 ), true );

		return $header_new;
	}

	public function wc_payment_gateway_column_content( $gateway ) {
		$supported_gateways = WFOCU_Core()->gateways->get_supported_gateways();
		echo '<td class="renewals">';
		if ( ( is_array( $supported_gateways ) && array_key_exists( $gateway->id, $supported_gateways ) ) ) {
			$status_html = '<span class="status-enabled tips" data-tip="' . esc_attr__( 'Supports UpSell payments with the UpStroke One Click Upsell.', 'woocommerce-subscriptions' ) . '">' . esc_html__( 'Yes', 'woocommerce-subscriptions' ) . '</span>';
		} else {
			$status_html = '-';
		}

		$allowed_html                     = wp_kses_allowed_html( 'post' );
		$allowed_html['span']['data-tip'] = true;

		/**
		 * Automatic Renewal Payments Support Status HTML Filter.
		 *
		 * @since 2.0
		 *
		 * @param string $status_html
		 * @param \WC_Payment_Gateway $gateway
		 */
		echo wp_kses( apply_filters( 'woocommerce_payment_gateways_upstroke_support_status_html', $status_html, $gateway ), $allowed_html );

		echo '</td>';
	}

	public function maybe_flush_rewrite_rules() {
		$is_required_rewrite = get_option( 'wfocu_needs_rewrite', 'no' );

		if ( 'yes' === $is_required_rewrite ) {
			flush_rewrite_rules();
			update_option( 'wfocu_needs_rewrite', 'no', true );
		}
	}

	/**
	 * Initiate WFOCU_Background_Updater class
	 * @see maybe_update_database_update()
	 */
	public function init_background_updater() {

		if ( class_exists( 'WFOCU_Background_Updater' ) ) {
			$this->updater = new WFOCU_Background_Updater();
		}

	}


	/**
	 * @hooked over `admin_head`
	 * This method takes care of database updating process.
	 * Checks whether there is a need to update the database
	 * Iterates over define callbacks and passes it to background updater class
	 */
	public function maybe_update_database_update() {

		if ( is_null( $this->updater ) ) {

			/**
			 * Update the option as tables are updated.
			 */
			update_option( '_wfocu_db_version', WFOCU_DB_VERSION, true );

			return;
		}
		$task_list          = array(
			'2.0' => array( 'wfocu_maybe_update_sessions_on_2_0', 'wfocu_rest_update_missing_gateways' ),
		);
		$current_db_version = get_option( '_wfocu_db_version', '0.0.0' );
		$update_queued      = false;

		foreach ( $task_list as $version => $tasks ) {
			if ( version_compare( $current_db_version, $version, '<' ) ) {
				foreach ( $tasks as $update_callback ) {

					$this->updater->push_to_queue( $update_callback );
					$update_queued = true;
				}
			}
		}

		if ( $update_queued ) {

			$this->updater->save()->dispatch();
		}

		update_option( '_wfocu_db_version', WFOCU_DB_VERSION, true );

	}

	public function maybe_update_upstroke_version_in_option() {

		$get_db_version = get_option( '_wfocu_plugin_version', '' );

		if ( version_compare( $get_db_version, WFOCU_VERSION, '<' ) ) {
			update_option( '_wfocu_plugin_version', WFOCU_VERSION, true );
			update_option( '_wfocu_plugin_last_updated', time(), true );
		}

	}
}

if ( class_exists( 'WFOCU_Core' ) ) {
	WFOCU_Core::register( 'admin', 'WFOCU_Admin' );
}

